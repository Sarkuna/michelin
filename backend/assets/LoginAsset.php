<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css',
        'themes/adminLTE/dist/css/AdminLTE.min.css',
        'themes/adminLTE/plugins/iCheck/square/blue.css',
        'themes/adminLTE/custom/css/logincustom.css',
    ];
    public $jsOptions = [
        //'position' => \yii\web\View::POS_HEAD
    ];  
    public $js = [        
        'themes/adminLTE/bootstrap/js/bootstrap.min.js',
        'themes/adminLTE/plugins/iCheck/icheck.min.js',
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
	//'yii\bootstrap\BootstrapPluginAsset',
    ];
}
