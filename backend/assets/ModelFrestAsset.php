<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ModelFrestAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700',
        
        // BEGIN: Vendor CSS-->
        'themes/frestadmin/app-assets/vendors/css/vendors.min.css',
        'themes/frestadmin/app-assets/vendors/css/forms/select/select2.min.css',
        'themes/frestadmin/app-assets/vendors/css/tables/datatable/datatables.min.css',
        'themes/frestadmin/app-assets/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css',
        'themes/frestadmin/app-assets/vendors/css/tables/datatable/responsive.bootstrap.min.css',
        'themes/frestadmin/app-assets/vendors/css/extensions/dragula.min.css',
        // END: Vendor CSS-->

        // BEGIN: Theme CSS-->
        'themes/frestadmin/app-assets/css/bootstrap.css',
        'themes/frestadmin/app-assets/css/bootstrap-extended.css',
        'themes/frestadmin/app-assets/css/colors.css',
        'themes/frestadmin/app-assets/css/components.css',
        'themes/frestadmin/app-assets/css/themes/dark-layout.css',
        'themes/frestadmin/app-assets/css/themes/semi-dark-layout.css',
        // END: Theme CSS-->
        
        

        // BEGIN: Page CSS-->
        'themes/frestadmin/app-assets/css/core/menu/menu-types/vertical-menu.css',
        'themes/frestadmin/app-assets/css/plugins/forms/validation/form-validation.css',
        'themes/frestadmin/app-assets/css/pages/app-invoice.css',
        'themes/frestadmin/app-assets/css/plugins/extensions/drag-and-drop.css',
        
        //'themes/frestadmin/app-assets/vendors/css/pickers/daterange/daterangepicker.css',
        
        // END: Page CSS-->

        // BEGIN: Custom CSS-->
        'themes/frestadmin/assets/css/style.css',
        //'themes/frestadmin/assets/css/photoviewer.css',
        // END: Custom CSS-->
    ];
    /*public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];*/    
    public $js = [
        // BEGIN: Vendor JS-->
        //['themes/frestadmin/app-assets/vendors/js/vendors.min.js', 'position' => \yii\web\View::POS_HEAD],
        'themes/frestadmin/app-assets/vendors/js/vendors.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js',
        'themes/frestadmin/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js',
        'themes/frestadmin/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js',
        'themes/frestadmin/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js',
        // BEGIN Vendor JS-->
        
        // BEGIN: Page Vendor JS-->

         // END: Page Vendor JS-->

        // BEGIN: Page Vendor JS-->
        'themes/frestadmin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js',
        'themes/frestadmin/app-assets/vendors/js/forms/select/select2.full.min.js',

        // END: Page Vendor JS-->

        // BEGIN: Theme JS-->
        'themes/frestadmin/app-assets/js/scripts/configs/vertical-menu-dark.js',
        'themes/frestadmin/app-assets/js/core/app-menu.js',
        'themes/frestadmin/app-assets/js/core/app.js',
        'themes/frestadmin/app-assets/js/scripts/components.js',
        'themes/frestadmin/app-assets/js/scripts/footer.js',
        // END: Theme JS-->
        
        // BEGIN: Page JS-->
        'themes/frestadmin/app-assets/js/scripts/forms/validation/form-validation.js',
        'themes/frestadmin/assets/js/scripts.js',
        // END: Page JS-->
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
	//'yii\bootstrap\BootstrapPluginAsset',
    ];
}
