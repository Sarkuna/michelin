<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
//use yii\helpers\Html;

class CopyrightFrestAdmin extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        return $this->render('copyrightfrestadmin');
        
    }
}