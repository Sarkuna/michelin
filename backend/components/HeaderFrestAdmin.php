<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
//use yii\helpers\Html;

class HeaderFrestAdmin extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $userinfo = \common\models\User::find()->where(['id' => Yii::$app->user->id])->one();
        //$painterprofile = \common\models\PainterProfile::find()->where(['user_id' => Yii::$app->user->id])->one();
        return $this->render('headerfrestadmin', [
            'userinfo' => $userinfo,
            //'painterprofile' => $painterprofile,
        ]);
        
    }
}