<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;
use common\models\VIPOrder;

class LeftMenuFrestAdmin extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        $orders_pending = VIPOrder::find()->where(['order_status_id' => 10])->count();
        $orders_processing = VIPOrder::find()->where(['order_status_id' => 20])->count();
        $orders_shipped = VIPOrder::find()->where(['order_status_id' => 30])->count();
        
        
        return $this->render('leftmenufrestadmin',
        [
            'orders_pending' => $orders_pending,
            'orders_processing' => $orders_processing,
            'orders_shipped' => $orders_shipped
        ]);
        
    }
}