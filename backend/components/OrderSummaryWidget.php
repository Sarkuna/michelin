<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use common\models\VIPOrder;

class OrderSummaryWidget extends Widget
{
    public $path;

    /*public function init()
    {
        parent::init();
    }

    public function run()
    {        
        return $this->render('painteroverview');
        
    }*/
    public function init()
    {
        
        parent::init();

    }

    public function run()
    {
        $session = Yii::$app->session;
        $PaymentChart = $fcDay = [];
        /*$i=1;
        while($i <= 12) {
        //for ($i = 0; $i <= 2; $i++) {
            $date = date('Y', strtotime(date('Y-m') . " -" . $i . " month"));
            $month = date('m', strtotime(date('Y-m') . " -" . $i . " month"));
            $month2 = date('M', strtotime(date('Y-m') . " -" . $i . " month"));
            $dayCountTmp = (new \yii\db\Query())->from('vip_order')
                            ->where(['YEAR(created_datetime)' => $date, 'MONTH(created_datetime)' => $month])->count();
            $dayCount[] = (int) $dayCountTmp;
            $fcDay[] = $month2.' '.$date;
            $i++;
        }*/
        
        $start = -4;
        $end = 7;
        for($i=$start; $i<=$end;$i++) {
            $date = date('Y', strtotime("$i months"));
            $month = date('m', strtotime("$i months"));
            $month2 = date("M 'y", strtotime("$i months"));
            
            $dayCountTmp = (new \yii\db\Query())->from('vip_order')
                            ->where(['YEAR(created_datetime)' => $date, 'MONTH(created_datetime)' => $month])->count();
            $dayCount[] = (int) $dayCountTmp;
            $fcDay[] = $month2;
        }
        
        
        $ApprovedAmount = [];
        $amount = 0;
        $start1 = -4;
        $end1 = 7;
        $amountCount = [];
        for($ii=$start1; $ii<=$end1;$ii++) {
        //for ($i = 0; $i <= 2; $i++) {
            $date = date('Y', strtotime("$ii months"));
            $month = date('m', strtotime("$ii months"));
            
            $amounts = \common\models\VIPOrder::find()
                ->joinWith(['vipOrderProduct'])    
                ->where([
                    //'point_order.order_status' => 17,
                    'YEAR(vip_order.created_datetime)' => $date,
                    'MONTH(vip_order.created_datetime)' => $month
                ])
                ->sum('point_total');
            
            if (count($amounts) > 0) {
                $amount = $amounts;
            }else {
                $amount = 0;
            }
            $amountCount[] = $amount;
            $i++;
        }

        $dayResults = ['name' => 'Orders', 'data' => $dayCount, 'type' => 'column'];
        $dayResults2 = ['name' => 'Point Redeemed', 'data' => $amountCount, 'type' => 'line'];
        $PaymentChart = array($dayResults, $dayResults2);
        
        $gql = 'SELECT product_id,name, SUM(Quantity) AS TotalQuantity FROM vip_order_product GROUP BY product_id ORDER BY SUM(Quantity) DESC';
        
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($gql);
        $bestsellers = $command->queryAll();
        
        return $this->render('ordersummarywidget',array('PaymentChart' => $PaymentChart, 'categories' => $fcDay, 'bestsellers' => $bestsellers));
        
    }
}