<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use common\models\VIPOrder;

class RedemptionOverviewWidget extends Widget
{
    public $path;

    /*public function init()
    {
        parent::init();
    }

    public function run()
    {        
        return $this->render('painteroverview');
        
    }*/
    public function init()
    {
        
        parent::init();

    }

    public function run()
    {
        $accepted = VIPOrder::find()->where(['order_status_id' => 10])->count();
        $processing = VIPOrder::find()->where(['order_status_id' => 20])->count();
        $complete = VIPOrder::find()->where(['order_status_id' => 40])->count();
        
        $session = Yii::$app->session;
        $query = VIPOrder::find();
        //$query->joinWith(['user']);
        //status
        //$query->andWhere(['=','redemption_status', '1']);
        /*$query = \common\models\User::find();

        $query->select([
                    '*',
                ])
                ->where(['user_type' => 'P'])
                ->all();*/

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5, 
            ],
            'sort' => [
                'defaultOrder' => [
                    'order_id' => SORT_DESC
                    //'id' => SORT_DESC
                ]
            ],
        ]);
        
        return $this->render('redemptionoverview',array('dataProvider'=>$dataProvider, 'accepted' => $accepted, 'processing' => $processing, 'complete' => $complete));
        
    }
}