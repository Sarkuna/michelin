<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use common\models\VIPOrder;

class RedemptionStateWidget extends Widget
{
    public $path;

    /*public function init()
    {
        parent::init();
    }

    public function run()
    {        
        return $this->render('painteroverview');
        
    }*/
    public function init()
    {
        
        parent::init();

    }

    public function run()
    {
        $session = Yii::$app->session;
         $StatesChart = $states = [];

        for ($i = 1; $i <= 14; $i++){
            $state = \common\models\VIPStates::find()
                ->where(['states_id' => $i])->one();
            
            $StatesCountTmp = (new \yii\db\Query())->from('vip_order')
                            ->where(['shipping_zone' => $state->state_name])->count();
            $StatesCount[] = (int) $StatesCountTmp;
            
            
            $states[] = $state->state_name;
            //$i++;
        }
        $dayResults = ['data' => $StatesCount];
        $StatesChart[] = $dayResults;
        
        return $this->render('redemptionstatewidget',array('states' => $states, 'StatesChart' => $StatesChart));
        
    }
}