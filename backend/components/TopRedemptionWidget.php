<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use common\models\VIPOrder;

class TopRedemptionWidget extends Widget
{
    public $path;

    /*public function init()
    {
        parent::init();
    }

    public function run()
    {        
        return $this->render('painteroverview');
        
    }*/
    public function init()
    {
        
        parent::init();

    }

    public function run()
    {
        $session = Yii::$app->session;
        
        $gql = 'SELECT customer_id,customer_group_id,shipping_firstname, SUM(vip_order_product.point_total) AS TotalQuantity FROM vip_order INNER JOIN vip_order_product ON vip_order.order_id=vip_order_product.order_id GROUP BY vip_order.customer_id ORDER BY SUM(vip_order_product.point_total) DESC LIMIT 10';
        
        $rstate = 'SELECT customer_id,shipping_country_id,shipping_zone, SUM(vip_order_product.point_total) AS TotalQuantity FROM vip_order INNER JOIN vip_order_product ON vip_order.order_id=vip_order_product.order_id GROUP BY vip_order.shipping_zone ORDER BY SUM(vip_order_product.point_total) DESC LIMIT 10';
        
        $login = 'SELECT username,date_last_login, count(userapp.id) AS TotalQuantity FROM userapp INNER JOIN actionlog ON userapp.id=actionlog.user_id GROUP BY userapp.id ORDER BY count(userapp.id) DESC LIMIT 10';
        
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($gql);
        $top_redeemed = $command->queryAll();
        
        $command2 = $connection->createCommand($rstate);
        $redeemed_by_states = $command2->queryAll();
        
        $command3 = $connection->createCommand($login);
        $highest_logins = $command3->queryAll();
        
        return $this->render('topredemptionwidget',array('top_redeemed' => $top_redeemed, 'redeemed_by_states' => $redeemed_by_states, 'highest_logins' => $highest_logins));
        
    }
}