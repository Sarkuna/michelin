<?php
$session = Yii::$app->session;
use yii\helpers\Url;
use yii\helpers\Html;
//$role = Yii::$app->session->get('currentRole');
?>

<div class="header-navbar-shadow"></div>
<nav class="header-navbar main-header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top ">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav">
                        <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon bx bx-menu"></i></a></li>
                    </ul>
                </div>
                <ul class="nav navbar-nav float-right">
                    
                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                            <div class="user-nav d-sm-flex d-none">
                                <span class="user-name">
                                    <?= Yii::$app->VIPglobal->profileName() ?>
                                </span>
                                <span class="user-status text-muted"><?= Yii::t('app','Online') ?></span>                                
                            </div>
                            <span>
                                <?php
                                $picPro = '<img alt="Profile picture" src="/site/view-avatar" class="round" height="40" width="40">';
                                echo $picPro;
                                ?>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right pb-0">
                            <a class="dropdown-item" href="/site/change-password"><i class="bx bx-lock mr-50"></i> <?= Yii::t('app','Change Password') ?></a>
                            <div class="dropdown-divider mb-0"></div><a class="dropdown-item" href="<?php echo Url::to(['/site/logout']); ?>" data-method="post"><i class="bx bx-power-off mr-50"></i> <?= Yii::t('app','Logout') ?></a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>