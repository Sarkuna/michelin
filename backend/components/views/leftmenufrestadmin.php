<?php
$session = Yii::$app->session;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
?>
<style>
    .navbar-header {background: #ffffff;}
    li.nav-item.mr-auto img {width: 150px;}
</style>
<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="">
                    <?php
            if(!empty($session['currentLogo'])){
                echo '<img src="/upload/client_logos/'.$session['currentLogo'].'" class="img-responsive">';                
            }else{
                echo '<img src="/images/logo.png" class="img-responsive">';
            }
            ?>
                </a>
            </li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="bx bx-x d-block d-xl-none font-medium-4 primary"></i><i class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block primary" data-ticon="bx-disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <?php

        $myurl = Yii::$app->request->url;
        $menuItems[] = '';
        $home = ['label' => '<i class="menu-livicon" data-icon="desktop"></i><span class="menu-title" data-i18n="Dashboard">'.Yii::t('app', 'Home').'</span>', 'url' => [Yii::$app->homeUrl]];

        //if ($session['currentRole'] == Yii::$app->params['role.type.administrator'] || $session['currentRole'] == Yii::$app->params['role.type.management']) {
            $menuItems = [
                $home,
                ['label' => '<span>Apps</span>', 'options' => ['class' => 'navigation-header']],
                ['label' => '<i class="menu-livicon" data-icon="shoppingcart"></i><span class="menu-title" data-i18n="Customer">Orders</span>',
                    'url' => ['#'],
                    'options' => ['class' => 'nav-item'],
                    'template' => '<a href="#" >{label}</a>',
                    'items' => [
                        ['label' => '<i class="bx bxs-info-circle"></i><span class="menu-item" data-i18n="Pending">Accepted</span><span class="badge badge-pill badge-round badge-light-warning ml-2">'.$orders_pending.'</span>', 'url' => ['/sales/orders/index'], 'active' => $myurl == '/sales/orders/index',],
                        ['label' => '<i class="bx bx-check"></i><span class="menu-item" data-i18n="Processing">Processing</span><span class="badge badge-pill badge-round badge-light-primary ml-2">'.$orders_processing.'</span>', 'url' => ['/sales/orders/processing'], 'active' => $myurl == '/sales/orders/processing',],
                        ['label' => '<i class="bx bx-package"></i><span class="menu-item" data-i18n="Shipped">Shipped</span><span class="badge badge-pill badge-round badge-light-success ml-2">'.$orders_shipped.'</span>', 'url' => ['/sales/orders/shipped'], 'active' => $myurl == '/sales/orders/shipped'],
                        ['label' => '<i class="bx bxs-receipt"></i> <span class="menu-item" data-i18n="Complete">Complete</span>', 'url' => ['/sales/orders/complete'], 'active' => $myurl == '/sales/orders/complete'],
                        ['label' => '<i class="bx bxs-discount"></i> <span class="menu-item" data-i18n="Refunded">Refunded</span>', 'url' => ['/sales/orders/refunded'], 'active' => $myurl == '/sales/orders/refunded'],
                        ['label' => '<i class="bx bx-x"></i> <span class="menu-item" data-i18n="Cancelled">Cancelled</span>', 'url' => ['/sales/orders/cancelled'], 'active' => $myurl == '/sales/orders/cancelled'],
                    ],
                ],

                ['label' => '<i class="menu-livicon" data-icon="magic"></i><span class="menu-title" data-i18n="Reports">Reports</span>',
                    'url' => ['#'],
                    'options' => ['class' => 'nav-item'],
                    'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                    'items' => [
                        //['label' => '<i class="bx bx-search"></i><span class="menu-item" data-i18n="Search">Search</span>', 'url' => ['/management/pointorder/search'], 'active' => $myurl == '/management/pointorder/search'],
                        ['label' => '<i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Orders Summary">Orders Summary</span>', 'url' => ['/reports/report/orders-summary'], 'active' => $myurl == '/reports/report/orders-summary'],
                        ['label' => '<i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Suppliers Summary">Suppliers Summary</span>', 'url' => ['/reports/report/suppliers'], 'active' => $myurl == '/reports/report/suppliers'],
                    ],
                ],                
                ['label' => '<i class="menu-livicon" data-icon="grid"></i><span class="menu-title" data-i18n="catalog">Catalog</span>',
                    'url' => ['#'],
                    'options' => ['class' => 'nav-item'],
                    'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                    'items' => [
                        ['label' => '<i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="import products">Import Products</span>', 'url' => ['/catalog/products/uploadexcel'], 'active' => $myurl == '/catalog/products/uploadexcel'],
                        ['label' => '<i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="products">Products</span>', 'url' => ['/catalog/products/index'], 'active' => $myurl == '/catalog/products/index'],                        
                        ['label' => '<i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="categories">Categories</span>', 'url' => ['/catalog/categories/index'], 'active' => $myurl == '/catalog/categories/index'],                        
                        ['label' => '<i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="manufacturers">Manufacturers</span>', 'url' => ['/catalog/manufacturers/index'], 'active' => $myurl == '/catalog/manufacturers/index'],
                    ],
                ],
                ['label' => '<i class="menu-livicon" data-icon="gear"></i><span class="menu-title" data-i18n="Settings">Settings</span>',
                    'url' => ['#'],
                    'options' => ['class' => 'nav-item'],
                    'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                    'items' => [
                        ['label' => '<i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="logo">Logo</span>', 'url' => ['/settings/logo/index'], 'active' => $myurl == '/settings/logo/index'],
                        ['label' => '<i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="pages">Pages</span>', 'url' => ['/admin/pages/index'], 'active' => in_array($myurl,['/admin/pages/index','/admin/pages/create','/admin/pages/update?id='.Yii::$app->request->getQueryParam('id')])],                        
                        ['label' => '<i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="users">Admin Users</span>', 'url' => ['/clients/users/index'], 'active' => in_array($myurl,['/clients/users/index','/clients/users/create','/clients/users/update?id='.Yii::$app->request->getQueryParam('id')])],
                        
                        ['label' => '<i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="email">Email Templates</span>', 'url' => ['/admin/email-template/index'], 'active' => in_array($myurl,['/admin/email-template/index','/admin/email-template/update?id='.Yii::$app->request->getQueryParam('id')])],
                        ['label' => '<i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="history">User Access History</span>', 'url' => ['/admin/access-history/index'], 'active' => $myurl == '/admin/access-history/index'],
                    ],
                ],
                ['label' => '<i class="menu-livicon" data-icon="notebook"></i><span class="menu-title" data-i18n="Invoicing">Invoicing</span>',
                    'url' => ['#'],
                    'options' => ['class' => 'nav-item'],
                    'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                    'items' => [
                        //['label' => '<i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="pages">Pages</span>', 'url' => ['/admin/pages/index'], 'active' => in_array($myurl,['/admin/pages/index','/admin/pages/create','/admin/pages/update?id='.Yii::$app->request->getQueryParam('id')])],                        
                        ['label' => '<i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="users">General Invoice</span>', 'url' => ['/admin/internal-invoicing/index'], 'active' => in_array($myurl,['/admin/internal-invoicing/index','/admin/internal-invoicing/create','/admin/internal-invoicing/update?id='.Yii::$app->request->getQueryParam('id')])],
                        ['label' => '<i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="email">Statement of Account</span>', 'url' => ['/admin/statements/index'], 'active' => in_array($myurl,['/admin/statements/index','/admin/statements/payment','/admin/statements/update?id='.Yii::$app->request->getQueryParam('id')])],
                        ['label' => '<i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="email">Redemption</span>', 'url' => ['/admin/redemption/index'], 'active' => in_array($myurl,['/admin/redemption/index','/admin/redemption/create','/admin/redemption/update?id='.Yii::$app->request->getQueryParam('id')])],
                    ],
                ]
            ];
        //}
        ?>
        
        
        <?php
            echo Menu::widget([
                'options' => ['class' => 'navigation navigation-main', 'id' => 'main-menu-navigation', 'data-menu' => 'menu-navigation', 'data-icon-style' => 'lines'],
                //'linkOptions' => ['class' => 'nav-item',],
                'items' => $menuItems,
                'submenuTemplate' => "<ul class='menu-content'>{items}</ul>",
                'encodeLabels' => false, //allows you to use html in labels
                //'activateParents' => true,
                
                ]);
        ?>
    </div>
</div>