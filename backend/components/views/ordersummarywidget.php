<?php
    $session = Yii::$app->session;

    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
?>
<div class="card">
    <div class="row">
        <!-- Order Summary Starts -->
        <div class="col-md-8 col-12 order-summary border-right pr-md-0">
            <div class="card mb-0">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h4 class="card-title">Order Summary</h4>
                </div>
                <div class="card-content">
                    <div class="card-body p-0">
                        <?php
                        $labelsorders = ['01 Jan 2001', '02 Jan 2001', '03 Jan 2001', '04 Jan 2001', '05 Jan 2001', '06 Jan 2001', '07 Jan 2001', '08 Jan 2001', '09 Jan 2001', '10 Jan 2001', '11 Jan 2001', '12 Jan 2001'];
                        /*$orders = [
                            [
                                'name' => 'Orders',
                                'data' => [44, 50, 41, 67, 22, 41, 20, 35, 75, 32, 25, 60],
                                'type' => 'column'
                            ],
                            [
                                'name' => 'Point Redeemed',
                                'data' => [23, 42, 35, 27, 43, 22, 17, 31, 22, 22, 12, 16],
                                'type' => 'line'
                            ]
                        ];*/
                        echo \onmotion\apexcharts\ApexchartsWidget::widget([
                            'type' => 'line', // default area
                            'height' => '400', // default 350
                            //'width' => '500', // default 100%
                            'chartOptions' => [
                                'chart' => [
                                    'toolbar' => [
                                        'show' => false,
                                    ],
                                ],
                                'stroke' => [
                                    'width' => [0, 4],
                                ],
                                'dataLabels' => [
                                    'enabled' => true,
                                    'enabledOnSeries' => [1],
                                ],
                                'labels' => $categories,
                                'xaxis' => [
                                    'type' => 'date',
                                ],
                                'yaxis'=> [[
                                    'title'=> [
                                      'text'=> 'No. of Orders',
                                    ],

                                  ], [
                                    'opposite'=> true,
                                    'title'=> [
                                      'text'=> 'Point Redeemed'
                                    ]
                                  ]]
                            ],
                            'series' => $PaymentChart,
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sales History Starts -->
        <div class="col-md-4 col-12 pl-md-0">
            <div class="card mb-0">
                <div class="card-header pb-50">
                    <h4 class="card-title">Popular Items</h4>
                </div>
                <div class="card-content">
                    <div class="card-body py-1">
                        <?php
                        foreach($bestsellers as $bestseller) {
                           echo '<div class="d-flex justify-content-between align-items-center mb-2">
                                <div class="sales-item-name">
                                    <p class="mb-0">'.$bestseller['name'].'</p>
                                </div>
                                <div class="sales-item-amount">
                                    <h6 class="mb-0">'.$bestseller['TotalQuantity'].'</h6>
                                </div>
                            </div>';
                        }
                        ?>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>