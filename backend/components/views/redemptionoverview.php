<?php
    $session = Yii::$app->session;

    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
?>

<?php
$copunt = 0;
$redemptionoverviews = $dataProvider->getModels();
//echo '<pre>';
//print_r($announcements);
//die();
$count = count($redemptionoverviews);

?>


<div class="card marketing-campaigns h-100">
    <div class="card-header d-flex justify-content-between align-items-center pb-1">
        <h4 class="card-title">Redemption Overview</h4>
    </div>
    <div class="card-content">
        <div class="card-body pb-0">
            <div class="row">
                <div class="col-md-9 col-12">
                    <div class="d-inline-block">
                        <div class="card text-center">
                            <div class="card-content">
                                <div class="card-body py-1">
                                    <div class="badge-circle badge-circle-lg badge-circle-light-warning mx-auto mb-50">
                                        <i class="bx bxs-info-circle font-medium-5"></i>
                                    </div>
                                    <div class="text-muted line-ellipsis">Accepted</div>
                                    <h3 class="mb-0"><?= $accepted ?></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-inline-block">
                        <div class="card text-center">
                            <div class="card-content">
                                <div class="card-body py-1">
                                    <div class="badge-circle badge-circle-lg badge-circle-light-info mx-auto mb-50">
                                        <i class="bx bx-check font-medium-5"></i>
                                    </div>
                                    <div class="text-muted line-ellipsis">Processing</div>
                                    <h3 class="mb-0"><?= $processing ?></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-inline-block">
                        <div class="card text-center">
                            <div class="card-content">
                                <div class="card-body py-1">
                                    <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto mb-50">
                                        <i class="bx bxs-receipt font-medium-5"></i>
                                    </div>
                                    <div class="text-muted line-ellipsis">Complete</div>
                                    <h3 class="mb-0"><?= $complete ?></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-12 text-md-right">
                    <a href="/reports/report/orders-summary" class="btn btn-sm btn-primary glow mt-md-2 mb-1">View Report</a>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <!-- table start -->
        <table id="table-marketing-campaigns" class="table table-borderless">
            <thead>
                <tr>
                    <th >Date</th>
                    <th >Redemption #</th>
                    <th >Point Value</th>
                    <th class="text-center">Status</th> 
                </tr>
            </thead>
            <tbody>
                <?php
                    if ($count > 0) {
                        foreach ($redemptionoverviews as $redemptionoverview) {
                            $ahrf = '<a href="/sales/orders/view?id='.$redemptionoverview->order_id.'">' . $redemptionoverview->invoice_prefix . '</a>';
                            $totalpoint = $redemptionoverview->getTotalPoints() + $redemptionoverview->getShippingPointTotal();
                    //return Yii::$app->formatter->asInteger($totalpoint);
                            echo '<tr>';
                            echo '<td style="width: 29%;">' . date('d-m-Y', strtotime($redemptionoverview->created_datetime)) . '</td>';
                            echo '<td style="width: 29%;">'.$ahrf.'</td>';
                            //echo '<td>' . $redemptionoverview->shipping_firstname . '</td>';
                            echo '<td>' . $totalpoint . '</td>';
                            echo '<td><span class="label label-warning">'.$redemptionoverview->orderStatus->name.'</span></td>';
                            echo '</tr>';
                        }
                    }
                    ?>
                
            </tbody>
        </table>
        <!-- table ends -->
        <div class="ps__rail-x" style="width: 672px; left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 652px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
</div>