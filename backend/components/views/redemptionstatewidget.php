<?php
    $session = Yii::$app->session;

    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\web\JsExpression;
?>



            <div class="card h-100">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h4 class="card-title">Point redeemed by State</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <?php
                        
                        echo \onmotion\apexcharts\ApexchartsWidget::widget([
                            'type' => 'bar', // default area
                            'height' => '400', // default 350
                            //'width' => '500', // default 100%
                            'chartOptions' => [
                                'chart' => [
                                    'toolbar' => [
                                        'show' => false,
                                    ],
                                ],
                                'plotOptions' => [
                                    'bar' => [
                                      'barHeight' => '100%',
                                      'distributed' => true,
                                      'horizontal' => true,
                                      'dataLabels' => [
                                        'position' => 'bottom'
                                      ],
                                    ]
                                  ],
                                'colors' => ['#33b2df', '#546E7A', '#d4526e', '#13d8aa', '#A5978B', '#2b908f', '#f9a3a4', '#90ee7e','#f48024', '#69d2e7'],
                                'dataLabels' => [
                                    'enabled' => true,
                                    'textAnchor' => 'start',
                                    'style' => [
                                          'colors' => ['#000']
                                    ],
                                    'formatter' => new JsExpression('function (val, opt){ return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val; }'),
                                    'offsetX' => 0,
                                    'dropShadow' => [
                                          'enabled' => false
                                    ]
                                ],
                                'stroke' => [
                                    'width' => 1,
                                    'colors' => ['#fff']
                                  ],
                                'xaxis' => [
                                    'categories' => $states,
                                ],
                                'yaxis' => [
                                    'labels' => [
                                      'show' => false
                                    ]
                                ],
                                'tooltip' => [
                                    'theme' => 'dark',
                                    'x' => [
                                      'show' => false
                                    ],
                                    'y' => [
                                      'title' => [
                                        'formatter' => new JsExpression('function (){ return ""; }'),
                                      ]
                                    ]
                                  ]
                            ],
                            'series' => $StatesChart,
                        ]);
                        ?>
                    </div>
                </div>
            </div>
