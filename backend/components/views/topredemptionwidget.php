<?php
    $session = Yii::$app->session;

    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
?>

<?php

$count = count($top_redeemed);
$count2 = count($redeemed_by_states);
$count3 = count($highest_logins);
?>

<div class="card h-100">
    <div class="card-header border-bottom d-flex justify-content-between align-items-center">
        <h5 class="card-title"><i class="bx bx-dollar font-medium-5 align-middle"></i> <span class="align-middle">Earnings</span></h5>
    </div>
    
    <div class="card-content">
        <div class="card-body py-1 px-0">
            <!-- earnings swiper starts -->
            <div class="widget-earnings-swiper swiper-container p-1">
                <div class="swiper-wrapper">
                    <div class="swiper-slide rounded swiper-shadow py-50 px-2 d-flex align-items-center" id="repo-design">
                        <i class="bx bx-gift mr-1 font-weight-normal font-medium-4"></i>
                        <div class="swiper-text">
                            <div class="swiper-heading">Top Redeemed</div>
                        </div>
                    </div>
                    <div class="swiper-slide rounded swiper-shadow py-50 px-2 d-flex align-items-center" id="laravel-temp">
                        <i class="bx bx-check-shield mr-50 font-large-1"></i>
                        <div class="swiper-text">
                            <div class="swiper-heading">Top Redeemed by State</div>
                        </div>
                    </div>
                    <div class="swiper-slide rounded swiper-shadow py-50 px-2 d-flex align-items-center" id="admin-theme">
                        <i class="bx bx-user-check mr-50 font-large-1"></i>
                        <div class="swiper-text">
                            <div class="swiper-heading">Highest User Logins</div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
            <!-- earnings swiper ends -->
        </div>
    </div>
    
    <div class="main-wrapper-content">
        <div class="wrapper-content" data-earnings="repo-design">
            <div class="widget-earnings-scroll table-responsive">
                <table class="table table-borderless widget-earnings-width mb-0">
                    <tbody>
                        <?php
                            if ($count > 0) {
                                $tr_percentage = 0;
                                foreach ($top_redeemed as $top_redeeme) {
                                    $img = '<img src="/site/view-photo?user_id='.$top_redeeme['shipping_firstname'].'" alt="avatar" class="rounded-circle" height="30" width="30">';
                                    //$totalpoint = $redemptionoverview->getTotalPoints() + $redemptionoverview->getShippingPointTotal();
                                    $tr_percentage = $top_redeeme['TotalQuantity'] / 100;
                                    echo '<tr>
                                        <td class="pr-75">
                                            <div class="media align-items-center">
                                                <a class="media-left mr-50" href="#">
                                                    '.$img.'
                                                </a>
                                                <div class="media-body">
                                                    <h6 class="media-heading mb-0">' . $top_redeeme['shipping_firstname'] . '</h6>
                                                    <span class="font-small-2">' . $top_redeeme['customer_group_id'] . '</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="px-0 w-25">
                                            <div class="progress progress-bar-info progress-sm mb-0">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="'.$tr_percentage.'" aria-valuemin="80" aria-valuemax="100" style="width:'.$tr_percentage.'%;"></div>
                                            </div>
                                        </td>
                                        <td class="text-center"><span class="badge badge-light-primary">'.$top_redeeme['TotalQuantity'].'</span>
                                        </td>
                                    </tr>';
                                }
                            }
                        ?>
                        
                        
                    </tbody>
                </table>
            </div>
        </div>
        
        <div class="wrapper-content" data-earnings="laravel-temp">
            <div class="widget-earnings-scroll table-responsive">
                <table class="table table-borderless widget-earnings-width mb-0">
                    <tbody>
                        <?php
                            if ($count2 > 0) {
                                $tr_percentage = 0;
                                foreach ($redeemed_by_states as $redeemed_by_state) {
                                    $img = '<img src="/site/view-photo?user_id='.$redeemed_by_state['shipping_zone'].'" alt="avatar" class="rounded-circle" height="30" width="30">';
                                    //$totalpoint = $redemptionoverview->getTotalPoints() + $redemptionoverview->getShippingPointTotal();
                                    $tr_percentage = $redeemed_by_state['TotalQuantity'] / 100;
                                    echo '<tr>
                                        <td class="pr-75">
                                            <div class="media align-items-center">
                                                <a class="media-left mr-50" href="#">
                                                    '.$img.'
                                                </a>
                                                <div class="media-body">
                                                    <h6 class="media-heading mb-0">' . $redeemed_by_state['shipping_zone'] . '</h6>
                                                    <span class="font-small-2">' . $redeemed_by_state['shipping_country_id'] . '</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="px-0 w-25">
                                            <div class="progress progress-bar-info progress-sm mb-0">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="'.$tr_percentage.'" aria-valuemin="80" aria-valuemax="100" style="width:'.$tr_percentage.'%;"></div>
                                            </div>
                                        </td>
                                        <td class="text-center"><span class="badge badge-light-primary">'.$redeemed_by_state['TotalQuantity'].'</span>
                                        </td>
                                    </tr>';
                                }
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        
        <div class="wrapper-content" data-earnings="admin-theme">
            <div class="widget-earnings-scroll table-responsive">
                <table class="table table-borderless widget-earnings-width mb-0">
                    <tbody>
                        <?php
                            if ($count3 > 0) {
                                $tr_percentage = 0;
                                foreach ($highest_logins as $highest_login) {
                                    $img = '<img src="/site/view-photo?user_id='.$highest_login['username'].'" alt="avatar" class="rounded-circle" height="30" width="30">';
                                    //$totalpoint = $redemptionoverview->getTotalPoints() + $redemptionoverview->getShippingPointTotal();
                                    $tr_percentage = $highest_login['TotalQuantity'] / 100;
                                    echo '<tr>
                                        <td class="pr-75">
                                            <div class="media align-items-center">
                                                <a class="media-left mr-50" href="#">
                                                    '.$img.'
                                                </a>
                                                <div class="media-body">
                                                    <h6 class="media-heading mb-0">' . $highest_login['username'] . '</h6>
                                                    <span class="font-small-2">' . date('d-m-y', strtotime($highest_login['date_last_login'])) . '</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="px-0 w-25">
                                            <div class="progress progress-bar-info progress-sm mb-0">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="'.$tr_percentage.'" aria-valuemin="'.$tr_percentage.'" aria-valuemax="100" style="width:'.$tr_percentage.'%;"></div>
                                            </div>
                                        </td>
                                        <td class="text-center"><span class="badge badge-light-primary">'.$highest_login['TotalQuantity'].'</span>
                                        </td>
                                    </tr>';
                                }
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>
</div>



