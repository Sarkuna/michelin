<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use YoHang88\LetterAvatar\LetterAvatar;
use Imagick;
use yii\imagine\Image;

use common\models\LoginForm;
use backend\models\PasswordResetRequestForm;
use backend\models\ResetPasswordForm;
use backend\models\SignupForm;
use common\models\User;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','request-password-reset','reset-password','kis','copy'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'importcustomer', 'lelong', 'copy','importproduct','importcustomer2','change-password','importdealer','info','testimage','view-avatar','send','view-photo'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
        $session = Yii::$app->session;
        $this->layout = 'dashboardlayout';
        if (!Yii::$app->user->isGuest) {
            return $this->render('dashboard_administrator');
        } else {
            return $this->redirect(['home']);
        }
        //return $this->redirect(['home']);
    }
    
    public function actionChangePassword()
    {
	$model= new \backend\models\ChangePasswordForm();
	//$model->scenario = 'change';

	if ($model->load(Yii::$app->request->post()) && $model->validate()) {
		//$model->attributes = $_POST['User'];
		$user = User::findOne(Yii::$app->user->id);
                $user->setPassword($model->repassword);
                $user->auth_key = Yii::$app->security->generateRandomString();
		if($user->save()){                 
                    \Yii::$app->getSession()->setFlash('success',['title' => 'Change Password', 'text' => 'Your password has been changed successfully']);
                    return $this->goHome();
                }else{
                    print_r($user->getErrors());
                }
	}

	return $this->render('password_changeform',[
		'model'=>$model,
	]);
     }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'loginlayout';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        //Yii::app()->user->logout(false);

        return $this->goHome();
    }
    
    

    public function actionRequestPasswordReset()
    {
        $this->layout = 'loginlayout';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                //Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Reset Password', 'text' => 'Check your email for further instructions.']);

                return $this->goHome();
            } else {
                //Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
                \Yii::$app->getSession()->setFlash('error', ['title' => 'Reset Password', 'text' => 'Sorry, we are unable to reset password for the provided email address.']);
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = 'loginlayout';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');
            \Yii::$app->getSession()->setFlash('success', ['title' => 'Reset Password', 'text' => 'New password saved.']);

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    public function actionViewAvatar() {
        $session = Yii::$app->session;
        $userID = \Yii::$app->user->id;
        
        //$id = Yii::$app->request->get('id');
        $size = Yii::$app->request->get('size');
        $name = "N A";
        
        $profile = \common\models\UserProfile::find()
            ->where(['userID' => $userID])
            ->one();
        $name = $profile->first_name.' '.$profile->last_name;
        
        if(!empty($name)){
            header('Content-Type: image/png');
            $imageSize = !empty($size) ? $size : 200;
            $setting = array('size' => $imageSize);
            $letterName = str_replace(' - ', ' ', $name);
            $letterName = str_replace(' -', ' ', $letterName);
            $avatar = \common\helper\Avatar::generateAvatarFromLetter($letterName, $setting);
            echo $avatar;
            exit();
        }else if(empty($photo)){
            $extension = 'png';
            $photo = Yii::getAlias('@webroot') . '/web/images/avatar_image.jpg';
        }
        
        header('Content-Type: image/'.$extension);
        $file = file_get_contents($photo);
        echo $file;
        exit();
    }
    
    public function actionViewPhoto() {
        $userId = Yii::$app->request->get('user_id');
        $name = $userId;
        $size = Yii::$app->request->get('size');
        //$name = Yii::$app->VIPglobal->webapiuserinfo()['ContactPerson'];
        if(!empty($name)){
            header('Content-Type: image/png');
            $imageSize = !empty($size) ? $size : 200;
            $setting = array('size' => $imageSize);
            $letterName = str_replace(' - ', ' ', $name);
            $letterName = str_replace(' -', ' ', $letterName);
            $avatar = \common\helper\Avatar::generateAvatarFromLetter($letterName, $setting);
            echo $avatar;
            exit();
        }else if(empty($photo)){
            $extension = 'png';
            $photo = Yii::getAlias('@webroot') . '/web/images/avatar_image.jpg';
        }
        
        header('Content-Type: image/'.$extension);
        $file = file_get_contents($photo);
        echo $file;
        exit();
    }
    

    
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
