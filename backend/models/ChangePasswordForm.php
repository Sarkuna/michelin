<?php

namespace backend\models;

use Yii;
use yii\base\Model;


class ChangePasswordForm extends Model
{

    public $password;
    public $repassword;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password','repassword'], 'required'],
            ['password', 'string', 'min' => 6],
            ['repassword', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Password',
            'repassword' => 'Re-enter Password',
        ];
    }

    
}
