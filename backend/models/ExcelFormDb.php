<?php
namespace backend\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;


class ExcelFormDb extends Model
{
    public $excel;
    public $account_type;
    public $description;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['excel','account_type'], 'required'],
            ['description', 'safe'],
            //['excel', 'file'],
            [['excel'], 'file', 'extensions' => 'xls, xlsx'],
        ];
    }

    
}
