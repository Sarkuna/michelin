<?php
namespace backend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class OrderActionForm extends Model
{
    public $order_status_id;
    public $comment;
    public $notify;
    public $bb_invoice_no;
    public $file;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        //$client_id = '6';
        return [
            [['order_status_id', 'comment'], 'required'],
            ['order_status_id', 'integer'],
            ['comment', 'string', 'max' => 500],
            [['bb_invoice_no','notify'], 'safe'],
            [['file'], 'file', 'skipOnEmpty' => true, 'checkExtensionByMimeType' => false, 'extensions' => 'jpg,gif,png,bmp,jpeg','wrongExtension'=>'You can only upload following files: {extensions}','maxSize' => 5242880, 'tooBig' => 'Maximum file size cannot exceed 5MB', 'maxFiles' => 4],

            
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
        
    public function attributeLabels()
    {
        return [
            'order_status_id' => 'Status',
            'comment' => 'Comment',
            'notify' => 'Email notify to customer',
            'bb_invoice_no' => 'Invoice Number',
            'file' => 'Payment Receipt',
        ];
    }
}