<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupFormDealers extends Model
{
    public $dealer_code;
    public $company;
    
    public $email;
    public $first_name;
    public $last_name;
    
    
    public $tel;
    public $client_id;
    public $address1;
    public $address2;
    public $city;
    public $postcode;
    public $country;
    public $states;
    public $region;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['dealer_code', 'required'],
            ['dealer_code', 'string', 'min' => 3, 'max' => 10],
            ['dealer_code', 'uniqueDealerCode'],
            
            ['company', 'required'],
            ['company', 'string', 'min' => 1, 'max' => 200],
            
            ['first_name', 'required'],
            ['first_name', 'string', 'min' => 1, 'max' => 50],

            ['last_name', 'required'],
            ['last_name', 'string', 'min' => 1, 'max' => 50],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'uniqueEmail'],
            [['address1', 'address2', 'city', 'postcode', 'region', 'country', 'tel'], 'safe'],
            
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }
    
    public function attributeLabels()
    {
        return [
            'dealer_code' => 'Dealer No',
            'company' => 'Company Name',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'address1' => 'Address1',
            'address2' => 'Address2',
            'country' => 'Country',
            'city' => 'City',
            'postcode' => 'Post Code',
            'states' => 'States',
            'region' => 'Region',
        ];
    }
    
    public function uniqueEmail($attribute, $params)
    {
        $session = Yii::$app->session;
        $client_id = $session['currentclientID'];
        
        if(
            $user = User::find()->where(['email'=>$this->email, 'client_id' => $client_id, 'user_type' => 'B'])->exists()
            //$user = User::->exists('email=:email',array('email'=>$this->email))
        )
          $this->addError($attribute, 'Email "'.$this->email.'" has already been taken.');
    }
    
    public function uniqueDealerCode($attribute, $params)
    {
        if(
            $user = \common\models\UserProfile::find()->where(['dealer_code'=>$this->dealer_code])->exists()
            //$user = User::->exists('email=:email',array('email'=>$this->email))
        )
          $this->addError($attribute, 'Dealer Code "'.$this->dealer_code.'" has already been taken.');
    }
}
