<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class StatementForm extends Model
{
    public $statement_date;
    public $particulars;
    public $amount;
    public $payment;
    public $payment_type;
    public $payment_status;
    public $payment_id;
    public $note;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['statement_date', 'payment', 'payment_type', 'payment_status'], 'required'],
            [['payment_id', 'note', 'particulars'], 'safe'],
            
        ];
    }

    
    
    public function attributeLabels()
    {
        return [
            'statement_date' => 'Date',
            'particulars' => 'Particulars',
            'payment' => 'Payment',
            'payment_id' => 'Payment For'
        ];
    }
}
