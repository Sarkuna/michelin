<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\models\Model;
use common\models\InternalInvoicing;
use common\models\InternalInvoicingSearch;
use common\models\InternalInvoicingItems;

/**
 * InternalInvoicingController implements the CRUD actions for InternalInvoicing model.
 */
class InternalInvoicingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InternalInvoicing models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = '@app/themes/frestadmin/layouts/reportlayout';
        $searchModel = new InternalInvoicingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single InternalInvoicing model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
            'invoiceitems' => $model->internalInvoicingItems,
        ]);
    }

    /**
     * Creates a new InternalInvoicing model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new InternalInvoicing();
        $invoiceitems = [new InternalInvoicingItems];

        if ($model->load(Yii::$app->request->post())) {
            $model->invoice_date = date('Y-m-d', strtotime($model->invoice_date));
            $model->due_date = date('Y-m-d', strtotime($model->due_date));
            
            $invoiceitems = Model::createMultiple(InternalInvoicingItems::classname());
            Model::loadMultiple($invoiceitems, Yii::$app->request->post()); 
            
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if ($flag = $model->save(false)) {
                    if ($flag) {
                        foreach ($invoiceitems as $invoiceitem) {
                            if ($invoiceitem != null){
                                $invoiceitem->internal_invoicing_id = $model->internal_invoicing_id;
                                //$invoiceitem->invoice_date = date('Y-m-d', strtotime($invoiceitem->invoice_date));
                                //$invoiceitem->invoice_due_date = date('Y-m-d', strtotime($invoiceitem->invoice_due_date));
                                if (($flag = $invoiceitem->save(false)) === false) {        
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                        $statementofaccount = new \common\models\StatementOfAccount();
                        $statementofaccount->statement_date = $model->invoice_date;
                        $statementofaccount->amount = $model->total;
                        $statementofaccount->payment_type = $model->invoice_type;
                        $statementofaccount->internal_invoicing_id = $model->internal_invoicing_id;
                        $statementofaccount->type = $model->redemption_allocation;
                        $statementofaccount->save();
                        
                    }
                }
                
                if ($flag) { 
                    $transaction->commit();
                    \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'Invoice create successfuly']);     
                    return $this->redirect(['index']);
                }
            } catch (Exception $e) {
                $transaction->rollBack();
            }
            //return $this->redirect(['view', 'id' => $model->invoice_id]);
        } else {
            $model->bill_from_name = 'Rewards Solution Sdn Bhd';
            $model->bill_from_add = "No 24A, 1st Floor, Jalan BP 6/6,
Bandar Bukit Puchong, 47100, Selangor
Tel: +603-74909139 | Fax: +603-62076716";
            $model->bill_to_name = 'Kendra';
            $model->bill_to_add = "Michelin Malaysia Sdn Bhd
Unit 901, Uptown 2, 2, Jalan SS 21/37, Damansara Utama, 47400 Petaling Jaya, Selangor, Malaysia";
            $model->terms_conditions = '<p>Note :<br />1. All cheques should be crossed and made payable to <strong>Rewards Solution Sdn Bhd.</strong><br />2. For online transfer,<br />Account Name : Rewards Solution Sdn Bhd.<br />Bank Name : Malayan Banking Berhad<br />Account No. : 512343627558&nbsp;</p>';
            return $this->render('create', [
                'model' => $model,
                'invoiceitems' => $invoiceitems,
            ]);
        }
    }

    /**
     * Updates an existing InternalInvoicing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->invoice_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing InternalInvoicing model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the InternalInvoicing model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InternalInvoicing the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InternalInvoicing::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
