<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

use common\models\Model;
use common\models\Invoice;
use common\models\InvoiceSearch;
use common\models\InvoiceItem;
use common\models\InvoiceItemSearch;

/**
 * InvoiceController implements the CRUD actions for Invoice model.
 */
class InvoiceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Invoice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InvoiceItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Invoice model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Invoice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = '/main_krajee_input_mask';
        $session = Yii::$app->session;
        $model = new Invoice();
        $invoiceitems = [new InvoiceItem];
        
        $pending_orders = \common\models\VIPOrder::find()->select('order_id')
                ->where(['clientID' => $session['currentclientID']])
                ->andwhere(['order_status_id' => 10])
                ->column();

        $vouchersids = \common\models\VIPOrderProduct::find()
            ->select('order_product_id')    
            ->joinWith([
                'order',
                'masterProduct' => function($que) {
                    $que->select(['product_id','voucher']);
                },        
            ])->where(['vip_order.clientID' => $session['currentclientID']])
            ->andwhere(['vip_order.order_status_id' => 10]) 
            ->andwhere(['IN', 'vip_order.order_id', $pending_orders])             
            ->andwhere(['vip_product.voucher' => 'Y'])
            ->orderBy(['vip_order.created_datetime' => SORT_DESC])->column();
                
        $itemsids = \common\models\VIPOrderProduct::find()
            ->select('order_product_id')    
            ->joinWith([
                'order',
                'masterProduct' => function($que) {
                    $que->select(['product_id','voucher']);
                },        
            ])->where(['vip_order.clientID' => $session['currentclientID']])
            ->andwhere(['vip_order.order_status_id' => 10]) 
            ->andwhere(['IN', 'vip_order.order_id', $pending_orders])             
            ->andwhere(['vip_product.voucher' => 'N'])
            ->orderBy(['vip_order.created_datetime' => SORT_DESC])->column();
        

        $vouchers = \common\models\VIPOrderProduct::find()->joinWith([
                'order',
                'masterProduct' => function($que) {
                    $que->select(['product_id','voucher']);
                },        
            ])->where(['vip_order.clientID' => $session['currentclientID']])
            ->andwhere(['vip_order.order_status_id' => 10])       
            ->andwhere(['vip_product.voucher' => 'Y'])
            ->orderBy(['vip_order.created_datetime' => SORT_DESC])->count();
        
        $items = \common\models\VIPOrderProduct::find()->joinWith([
                'order',
                'masterProduct' => function($que) {
                    $que->select(['product_id','voucher']);
                },        
            ])->where(['vip_order.clientID' => $session['currentclientID']])
            ->andwhere(['vip_order.order_status_id' => 10])       
            ->andwhere(['vip_product.voucher' => 'N'])
            ->orderBy(['vip_order.created_datetime' => SORT_DESC])->count();

        if ($model->load(Yii::$app->request->post())) {
            $invoiceitems = Model::createMultiple(InvoiceItem::classname());
            Model::loadMultiple($invoiceitems, Yii::$app->request->post()); 
            
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if ($flag = $model->save(false)) {
                    if ($flag) {
                        foreach ($invoiceitems as $invoiceitem) {
                            if ($invoiceitem != null){
                                $invoiceitem->invoice_id = $model->invoice_id;
                                $invoiceitem->invoice_date = date('Y-m-d', strtotime($invoiceitem->invoice_date));
                                $invoiceitem->invoice_due_date = date('Y-m-d', strtotime($invoiceitem->invoice_due_date));
                                if (($flag = $invoiceitem->save(false)) === false) {        
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                    }
                }
                
                if ($flag) { 
                    $transaction->commit();
                    $invoiceitemsall = InvoiceItem::find()
                        ->where('invoice_id = :invoice_id', [':invoice_id' => $model->invoice_id])
                        ->all();
                    $statement_data = array();
                    foreach ($invoiceitemsall as $myinvoiceitem) {
                        $invoiceitemamount = InvoiceItem::find()->where('invoice_item_id = :invoice_item_id', [':invoice_item_id' => $myinvoiceitem->invoice_item_id])->one();
                        
                        if($myinvoiceitem->invoice_type == 'I') {
                            \common\models\VIPOrderProduct::updateAll(['invoice_item_id' => $myinvoiceitem->invoice_item_id], ['IN', 'order_product_id', $itemsids]);
                            $statement_data[] = [$myinvoiceitem->invoice_date,str_replace( ',', '', $myinvoiceitem->invoiceAmount),'Item', $myinvoiceitem->invoice_item_id];
                        }else if($myinvoiceitem->invoice_type == 'V') {
                            \common\models\VIPOrderProduct::updateAll(['invoice_item_id' => $myinvoiceitem->invoice_item_id], ['IN', 'order_product_id', $vouchersids]);
                            $statement_data[] = [$myinvoiceitem->invoice_date,str_replace( ',', '', $myinvoiceitem->invoiceAmount),'Voucher', $myinvoiceitem->invoice_item_id];
                        }
                        
                    }                    
                    
                    
                    $data = array();
                    foreach ($pending_orders as $key => $my_orders) {
                        $data[] = [$my_orders,20,0,0,$model->comment,date('Y-m-d'),date('Y-m-d H:i:s'),date('Y-m-d H:i:s'),Yii::$app->user->id,Yii::$app->user->id];
                    }
                    
                    Yii::$app->db
                        ->createCommand()
                        ->batchInsert('vip_order_history', ['order_id','order_status_id','notify','type', 'comment','date_added','created_datetime','updated_datetime','created_by','updated_by'],$data)
                        ->execute();
                    
                    Yii::$app->db
                        ->createCommand()
                        ->batchInsert('statement_of_account', ['statement_date','amount','payment_type','invoice_item_id'],$statement_data)
                        ->execute();
                    
                    \common\models\VIPOrder::updateAll(['order_status_id' => 20, 'invoice_id' => $model->invoice_id], ['IN', 'order_id', $pending_orders]);
                    
                    \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => 'Invoice create successfuly']);     
                    return $this->redirect(['index']);
                }
            } catch (Exception $e) {
                $transaction->rollBack();
            }
            //return $this->redirect(['view', 'id' => $model->invoice_id]);
        } else {
            $model->bill_from_name = 'Rewards Solution Sdn Bhd';
            $model->bill_from_add = "No 24A, 1st Floor, Jalan BP 6/6,
Bandar Bukit Puchong, 47100, Selangor
Tel: +603-74909139 | Fax: +603-62076716";
            $model->bill_to_name = 'Hayley Lim Chew Yong ';
            $model->bill_to_add = "Kansai Paint Asia Pacific Sdn. Bhd
4, Solo Waja 2, Kawasan Perindustrian Bukit Raja, 
P.O.Box 159, 41710 Klang, Selangor";
            $model->terms_conditions = "<p>Note :<br />
1. All cheques should be crossed and made payable to <strong>Rewards Solution Sdn Bhd.</strong><br />
2. For online transfer,<br />
Account Name : Rewards Solution Sdn Bhd.<br />
Bank Name : Malayan Banking Berhad<br />
Account No. : 512343627558&nbsp;</p>";
            return $this->render('create', [
                'model' => $model,
                'invoiceitems' => $invoiceitems,
                'vouchers' => $vouchers,
                'items' => $items,
            ]);
        }
    }

    /**
     * Updates an existing Invoice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        //$key = '476';
        $key = '481';
        $adminpoints = Yii::$app->VIPglobal->myPriceInvoice($key);
        print_r($adminpoints);
        die;
        $session = Yii::$app->session;
        $pending_orders = \common\models\VIPOrder::find()->select('order_id')
                ->where(['clientID' => $session['currentclientID']])
                ->andwhere(['order_status_id' => 10])
                ->column();
        
        $vouchersids = \common\models\VIPOrderProduct::find()
            ->select('order_product_id')    
            ->joinWith([
                'order',
                'masterProduct' => function($que) {
                    $que->select(['product_id','voucher']);
                },        
            ])->where(['vip_order.clientID' => $session['currentclientID']])
            ->andwhere(['vip_order.order_status_id' => 10]) 
            ->andwhere(['IN', 'vip_order.order_id', $pending_orders])             
            ->andwhere(['vip_product.voucher' => 'Y'])
            ->orderBy(['vip_order.created_datetime' => SORT_DESC])->column();
                
        $itemsids = \common\models\VIPOrderProduct::find()
            ->select('order_product_id')    
            ->joinWith([
                'order',
                'masterProduct' => function($que) {
                    $que->select(['product_id','voucher']);
                },        
            ])->where(['vip_order.clientID' => $session['currentclientID']])
            ->andwhere(['vip_order.order_status_id' => 10]) 
            ->andwhere(['IN', 'vip_order.order_id', $pending_orders])             
            ->andwhere(['vip_product.voucher' => 'N'])
            ->orderBy(['vip_order.created_datetime' => SORT_DESC])->column();
                
        print_r($itemsids);
        die;
        echo 'Main';
        die;
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->invoice_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Invoice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Invoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Invoice::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
