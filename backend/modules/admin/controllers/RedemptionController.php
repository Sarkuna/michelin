<?php

namespace app\modules\admin\controllers;

use Yii;
use common\models\StatementOfRedemption;
use common\models\StatementOfRedemptionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RedemptionController implements the CRUD actions for StatementOfRedemption model.
 */
class RedemptionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StatementOfRedemption models.
     * @return mixed
     */
    public function actionIndex($date=null)
    {
        $this->layout = '@app/themes/frestadmin/layouts/reportlayout';
        $searchModel = new StatementOfRedemptionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);  
        
        if(!empty($date)) {
            $request = Yii::$app->request;
            $mydate = htmlentities(trim($request->get('date')), ENT_QUOTES);
            $mydate_select = $mydate;
            $mydate = date('Y-m', strtotime('01-'.$mydate));
            
        }else {
            $latestdate = StatementOfRedemption::find()->orderBy(['redemption_date' => SORT_DESC])->one();
            if(!empty($latestdate)) {
                $mydate = date('Y-m', strtotime($latestdate->redemption_date));
                $mydate_select = date('m-Y', strtotime($latestdate->redemption_date));
            }
        }

        if(!empty($mydate)){
            $dataProvider->query->andFilterWhere(['like', 'redemption_date', $mydate]);            
            
        }else {
            $mydate_select = date('m-Y');
        }
        
        $premonth = '01-'.$mydate_select;
        $premonth = date('Y-m-t', strtotime($premonth." -1 month"));

        $openbal = StatementOfRedemption::find()->where(['<=', 'redemption_date', $premonth])->sum('credit_out');
        $closingbal = StatementOfRedemption::find()->where(['<=', 'redemption_date', $premonth])->sum('debit_in');
        $openbal = $closingbal - $openbal;
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'mydate' => $mydate_select,
            'opening_bal' => $openbal,
            'closing_bal' => $closingbal
        ]);
    }

    /**
     * Displays a single StatementOfRedemption model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StatementOfRedemption model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new StatementOfRedemption();

        if ($model->load(Yii::$app->request->post())) {
            $model->redemption_date = date('Y-m-d', strtotime($model->redemption_date));
            $model->manual = 1;
            if($model->save()) {
                \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'Acction Sccessfully']);                
            }else {
                \Yii::$app->getSession()->setFlash('error',['title' => 'Fail', 'text' => 'Acction Fail']);
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing StatementOfRedemption model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->redemption_date = date('Y-m-d', strtotime($model->redemption_date));
            if($model->save()) {
                \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'Acction Sccessfully']);                
            }else {
                \Yii::$app->getSession()->setFlash('error',['title' => 'Fail', 'text' => 'Acction Fail']);
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing StatementOfRedemption model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the StatementOfRedemption model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StatementOfRedemption the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StatementOfRedemption::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
