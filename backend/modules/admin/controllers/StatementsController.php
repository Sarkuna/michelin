<?php

namespace app\modules\admin\controllers;

use Yii;
use common\models\StatementOfAccount;
use common\models\StatementOfAccountSearch;
use common\models\StatementOfRedemption;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StatementsController implements the CRUD actions for StatementOfAccount model.
 */
class StatementsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StatementOfAccount models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = '@app/themes/frestadmin/layouts/reportlayout';
        $searchModel = new StatementOfAccountSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionPayment(){
        $selection=(array)Yii::$app->request->post('selection');//typecasting
        $model = new \backend\models\StatementForm();
        if ($model->load(Yii::$app->request->post())) {
            $selection = explode(',',$model->payment_id);
            $newmodel = new StatementOfAccount();
            $newmodel->statement_date = date('Y-m-d', strtotime($model->statement_date));
            if(!empty($model->particulars)) {
                $newmodel->particulars = $model->note.'('.$model->particulars.')';
            }else {
                $newmodel->particulars = $model->note;
            }
       
            $newmodel->payment = $model->payment;
            $newmodel->payment_type = $model->payment_type;
            $newmodel->payment_id = $model->payment_id;
            $newmodel->payment_status = $model->payment_status;
            
           if($newmodel->save(false)) {
                StatementOfAccount::updateAll(['payment_status' => $model->payment_status], ['IN', 'sa_id', $selection]);
                $sumr = 0;
                foreach($selection as $valueid){
                    $updatemodel = $this->findModel($valueid);                    
                    if(!empty($updatemodel->internal_invoicing_id)) {
                        \common\models\InternalInvoicing::updateAll(['internal_invoicing_status' => $model->payment_status], ['=', 'internal_invoicing_id', $updatemodel->internal_invoicing_id]);
                        
                        if($updatemodel->type == 1) {
                            $sumr+= $updatemodel->amount;                            
                        }
                    }else if(!empty($updatemodel->invoice_item_id)) {
                        \common\models\InvoiceItem::updateAll(['invoice_status' => $model->payment_status], ['=', 'invoice_item_id', $updatemodel->invoice_item_id]);
                    }
                }
                
                if($model->payment_status == 'paid') {
                    $redemption = new StatementOfRedemption();
                    $redemption->redemption_date = date('Y-m-d', strtotime($model->statement_date));
                    $redemption->particulars = $model->particulars;
                    $redemption->debit_in = $sumr;
                    $redemption->payment_id = $newmodel->sa_id;
                    $redemption->status = 'paid';
                    $redemption->save(false);
                }
                
                \Yii::$app->getSession()->setFlash('success', ['title' => 'Success', 'text' => ' Statement create successfuly']);
            }else {
                \Yii::$app->getSession()->setFlash('error', ['title' => 'Fail', 'text' => 'Sorry  statement have not create']);
            }

            return $this->redirect(['index']);
            
        } else {
            //$particulars = '';
            $particulars = array(); $invoicelist = array();
            $sum = 0;
            foreach($selection as $valueid){
                $updatemodel = $this->findModel($valueid);
                $sum+= $updatemodel->amount;
                if(!empty($updatemodel->internal_invoicing_id)) {
                    $particular = $updatemodel->internalInvoice->invoice_no;
                }else if(!empty($updatemodel->invoice_item_id)) {
                    $particular = $updatemodel->invoiceItem->invoice_no;
                }else {
                    $particular = '';
                }
                
                
                $particulars[] = $particular;
                $invoicelist[$particular] = $updatemodel->amount;
            }

            $model->payment_id = implode(",", $selection);
            $model->note = implode(", ", $particulars);
            $model->payment = $sum;
            return $this->render('create', [
                'model' => $model,
                'invoicelist' => $invoicelist,
            ]);
        }

    }

    
    /**
     * Finds the StatementOfAccount model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StatementOfAccount the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StatementOfAccount::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
