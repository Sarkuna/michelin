<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserappSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Access History';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-body access-history-index">
    <!-- Scroll - horizontal and vertical table -->
    <section id="horizontal-vertical">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><?= Html::encode($this->title) ?></h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">

                            <div class="table-responsive">
                                <?=
                                GridView::widget([
                                    'tableOptions' => ['class' => 'table zero-configuration'],
                                    'dataProvider' => $dataProvider,
                                    'layout' => "{items}",
                                    //'filterModel' => $searchModel,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],
                                        //['class' => 'yii\grid\SerialColumn','headerOptions' => ['width' => '10'],],
                                        //['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],
                                        'username',
                                        [
                                            'header' => 'Date',
                                            'value' => function($model){
                                                return date('d-m-Y', strtotime($model->created_datetime));
                                            },       
                                        ],
                                        [
                                            'header' => 'Date Last Login',
                                            'value' => function($model){
                                                return date('d-m-Y', strtotime($model->date_last_login));
                                            },       
                                        ],
                                        [
                                            'header' => 'OS',
                                            'value' => function($model){
                                                if(!empty($model->user_agent)) {
                                                    return $model->userAgent($model->user_agent)[1].' '.str_replace(';','',$model->userAgent($model->user_agent)[2]);
                                                }else {
                                                    return 'N/A';
                                                }
                                            },       
                                        ],
                                        [
                                            'header' => 'Model',
                                            'value' => function($model){
                                                if(!empty($model->user_agent)) {
                                                    return $model->userAgent($model->user_agent)[3];
                                                }else {
                                                    return 'N/A';
                                                }
                                            },       
                                        ],
                                        [
                                            'header' => 'Bulid',
                                            'value' => function($model){
                                                if(!empty($model->user_agent)) {                                
                                                    $bulid = str_replace('Build/','',$model->userAgent($model->user_agent)[4]);
                                                    $bulid = str_replace(';','',$bulid);
                                                    return $bulid;
                                                }else {
                                                    return 'N/A';
                                                }
                                            },       
                                        ],
                                    ],
                                ]);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

