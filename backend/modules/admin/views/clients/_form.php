<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- Smart Wizard -->
 <?php $form = ActiveForm::begin(['options' => ['id' => 'myForm', 'role' => 'form', 'data-toggle' => 'validator', 'class' => '', 'enctype' => 'multipart/form-data']]); ?>            
<div class="box box-solid box-info">
    <div class="box-header with-border">
        <h4 class="box-title"><i class="fa fa-globe"></i> Site Information</h4>
    </div>

    <div class="box-body">
        <div class="rowsadasdas">
            <?= $form->field($model, 'company')->textInput(array('placeholder' => 'Company Name')); ?>
            <?= $form->field($model, 'company_short_name')->textInput(array('placeholder' => 'Company Short Name')); ?>
            <?= $form->field($model, 'cart_domain')->textInput(array('placeholder' => 'URL/Domain')); ?>
            <?= $form->field($model, 'admin_domain')->textInput(array('placeholder' => 'Admin Domain')); ?>
            <?= $form->field($model, 'programme_title')->textInput(array('placeholder' => 'Programme Title')); ?>
        </div>


    </div> <!-- End personal Div -->
</div>


<div class="box box-solid box-info">
    <div class="box-header with-border">
        <h4 class="box-title"><i class="fa fa-building"></i>  Company Information</h4>
    </div>
    <div class="box-body">
        <?= $form->field($addressmodel, 'name')->textInput(array('placeholder' => 'Contact Name'));?>
        <?= $form->field($addressmodel, 'address_1')->textInput(array('placeholder' => 'Address 1'));?>
        <?= $form->field($addressmodel, 'address_2')->textInput(array('placeholder' => 'Address 2'));?>
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($addressmodel, 'city')->textInput(array('placeholder' => 'City'));?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($addressmodel, 'postcode')->textInput(array('placeholder' => 'Postcode'));?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($addressmodel, 'state')->textInput(array('placeholder' => 'State'));?>
            </div>
            <div class="col-lg-6">
                <?php
                    $gcountrys = common\models\VipCountry::find()->all();
                    $gcountryData = ArrayHelper::map($gcountrys, 'country_id', 'name');
                    echo $form->field($addressmodel, 'country_id')
                        ->dropDownList(
                            $gcountryData, 
                            ['prompt' => '--', 'id' => 'clientaddress-country_id', 'required' => true]
                        );
                ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($addressmodel, 'email_address')->textInput(array('placeholder' => 'Email'));?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($addressmodel, 'mobile_no')->textInput(array('placeholder' => 'Mobile'));?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($addressmodel, 'telephone')->textInput(array('placeholder' => 'Telephone'));?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($addressmodel, 'fax_no')->textInput(array('placeholder' => 'Fax'));?>
            </div>
        </div>
    </div> <!-- End personal Div -->
</div>
    
<div class="box box-solid box-info">
    <div class="box-header with-border">
        <h4 class="box-title"><i class="fa fa-cogs"></i> Setting</h4>
    </div>
    <div class="box-body">
        <?= $form->field($clientoption, 'invoice_prefix')->textInput(array('placeholder' => 'Invoice Prefix'));?>
        <?= $form->field($clientoption, 'receipts_prefix')->textInput(array('placeholder' => 'Receipts Prefix'));?>
        <?= $form->field($clientoption, 'point_value')->textInput(array('placeholder' => 'Point Value'));?>
        <?= $form->field($clientoption, 'bb_percent')->textInput(array('placeholder' => 'BB Percent'));?>
    </div> <!-- End personal Div -->
</div>
<?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Submit', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<?php ActiveForm::end(); ?>