<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\MembershipPack */
/* @var $form yii\widgets\ActiveForm */
//$this->title = 'Send Membership Pack';
?>


<div class="col-xs-12 col-lg-12">
    <div class="membership-pack-form">
        <?php
        $form = ActiveForm::begin([
                'id' => 'stu-master-update',
                //'enableAjaxValidation' => true,
            //'class' => 'form-horizontal',
                'enableClientValidation' => true,
                'fieldConfig' => [
                    'template' => "{label}{input}{error}",
                ],
            ]);
        ?>

        <?= $form->field($model, 'name')->textInput(array('placeholder' => 'Contact Name'));?>
        <?= $form->field($model, 'address_1')->textInput(array('placeholder' => 'Address 1'));?>
        <?= $form->field($model, 'address_2')->textInput(array('placeholder' => 'Address 2'));?>
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'city')->textInput(array('placeholder' => 'BB Percent'));?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'postcode')->textInput(array('placeholder' => 'BB Percent'));?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'state')->textInput(array('placeholder' => 'BB Percent'));?>
            </div>
            <div class="col-lg-6">
                <?php
                    $gcountrys = common\models\VipCountry::find()->all();
                    $gcountryData = ArrayHelper::map($gcountrys, 'country_id', 'name');
                    echo $form->field($model, 'country_id')
                        ->dropDownList(
                            $gcountryData, 
                            ['prompt' => '--', 'id' => 'clientaddress-country_id', 'required' => true]
                        );
                ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'email_address')->textInput(array('placeholder' => 'BB Percent'));?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'mobile_no')->textInput(array('placeholder' => 'BB Percent'));?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'telephone')->textInput(array('placeholder' => 'BB Percent'));?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'fax_no')->textInput(array('placeholder' => 'BB Percent'));?>
            </div>
        </div>

        <!-- Modal Footer -->
        <div class="modal-footer">                
            <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Submit', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    <?php ActiveForm::end(); ?>
        
    </div>    
</div>
