<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MembershipPack */
/* @var $form yii\widgets\ActiveForm */
//$this->title = 'Send Membership Pack';
?>


<div class="col-xs-12 col-lg-12">
    <div class="membership-pack-form">
        <?php
        $form = ActiveForm::begin([
                'id' => 'stu-master-update',
                //'enableAjaxValidation' => true,
            //'class' => 'form-horizontal',
                'enableClientValidation' => true,
                'fieldConfig' => [
                    'template' => "{label}{input}{error}",
                ],
            ]);
        ?>

        <?= $form->field($model, 'invoice_prefix')->textInput(array('placeholder' => 'Invoice Prefix'));?>
        <?= $form->field($model, 'receipts_prefix')->textInput(array('placeholder' => 'Receipts Prefix'));?>
        <?= $form->field($model, 'point_value')->textInput(array('placeholder' => 'Point Value'));?>
        <?= $form->field($model, 'bb_percent')->textInput(array('placeholder' => 'BB Percent'));?>
        <!-- Modal Footer -->
        <div class="modal-footer">                
            <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Submit', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    <?php ActiveForm::end(); ?>
        
    </div>    
</div>
