<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
  <div class="col-xs-12">
	<h2 class="page-header">	
	<i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Site Information'); ?>
	<div class="pull-right">
            <?php
            echo '<button class="btn btn-sm btn-primary pull-right" onclick="updateGuard('.$model->clientID.');return false;"><i class="fa fa-edit"></i> Edit</button>';
            ?>
	</div>
	</h2>
  </div><!-- /.col -->
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 edusec-profile-label">Company Name</div>
        <div class="col-md-9 col-sm-9 col-xs-6 edusec-profile-text"><?=  $model->company ? $model->company : Yii::t("app", "N/A") ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label">Company Short Name</div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text"><?=  $model->company_short_name ? $model->company_short_name : Yii::t("app", "N/A") ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 edusec-profile-label">URL/Domain</div>
        <div class="col-md-9 col-sm-9 col-xs-6 edusec-profile-text"><a href="<?= $model->cart_domain ?>" target="_blank"><?= $model->cart_domain ?></a></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label">Admin Domain</div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text"><a href="<?= $model->admin_domain ?>" target="_blank"><?= $model->admin_domain ?></a></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label">Programme Title</div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text"><?= $model->programme_title ?></div>
    </div>
</div>

<?php
	yii\bootstrap\Modal::begin([
		'id' => 'guardModal',
		'header' => "<h4 class='modal-title'><i class='fa fa-edit'></i> Edit Site Info </h4>",
	]);
 	yii\bootstrap\Modal::end(); 
?>
<script>
/***Start Update Gardian Jquery***/
function updateGuard(id) {
	$.ajax({
	  type:'GET',
	  url:'<?= Url::toRoute(["/admin/clients/edit-site"]) ?>',
	  data: { id : id},
	  success: function(data)
		   {
		       $(".modal-content").addClass("row");
		       $('.modal-body').html(data);
		       $('#guardModal').modal();

		   }
	});
}
</script>  