<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Client */

$this->title = 'Update Client: ' . $addressmodel->company;
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $addressmodel->company, 'url' => ['view', 'id' => $model->clientID]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="box box-primary client-update">
    <div class="box-header with-border">
        <h3 class="box-title"> <?= Html::encode($this->title) ?></h3>
    </div>
    <div class="vipcustomer-create">
        <?=
        $this->render('_form', [
            'model' => $model,
            'addressmodel' => $addressmodel,
            'billinginformation' => $billinginformation,
            'clientoption' => $clientoption,
            'clientpointoption' => $clientpointoption
        ])
        ?>

    </div>
</div>
