<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\EmailTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Email Templates';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-body">
    <!-- Scroll - horizontal and vertical table -->
    <section id="horizontal-vertical">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><?= Html::encode($this->title) ?></h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">

                            <div class="table-responsive">
                                <?=
                                GridView::widget([
                                    'tableOptions' => ['class' => 'table zero-configuration'],
                                    'dataProvider' => $dataProvider,
                                    'layout' => "{items}",
                                    //'filterModel' => $searchModel,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],
                                        //['class' => 'yii\grid\SerialColumn','headerOptions' => ['width' => '10'],],
                                        //['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],
                                        'code',
                                        'name',
                                        'subject',
                                        [
                                            'class' => 'yii\grid\ActionColumn',
                                            'template' => '{update}', // {update}{delete}
                                            'buttons' => [
                                                'update' => function ($url, $model) {
                                                    return (Html::a('<i class="bx bxs-edit-alt"></i>', $url, ['title' => Yii::t('app', 'Edit'),]));
                                                },
                                                    ],
                                                ],
                                            ],
                                        ]);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
