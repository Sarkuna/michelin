<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InternalInvoicing */

$this->title = 'New Invoice';
$this->params['breadcrumbs'][] = ['label' => 'All Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>



<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><?= Html::encode($this->title) ?></h4>
                </div>
                <div class="card-content">
                    <div class="card-body internal-invoicing-create">
                        <?=
                            $this->render('_form', [
                                'model' => $model,
                                'invoiceitems' => $invoiceitems,
                            ])
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
