<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Invoices';
$this->params['breadcrumbs'][] = ['label' => 'Invoices'];
//$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    #btnsubmit{float: right;}
    .table th, .table td {
        padding: 1.0rem 0.5rem;
    }
    .left-col {
        float: left;
        width: 50%;
    }
    .right-col {
        float: right;
        width: 50%;
    }
</style>

<section id="table-transactions">
    <div class="card">
        <div class="card-header">
            <!-- head -->
            <h5 class="card-title"><?= $this->title ?></h5>
        </div>

        <div class="card-content">
            <div class="card-body">

                <div class="painter-profile-index table-responsive">
                    <?= Html::a('Create Invoice', ['create'], [
                    'class' => 'btn btn-secondary', 
                    'title' => 'Create',
                    'id' => 'btnsubmit',
                    'class'=>'btn btn-success'
                ])?>
                <?=
                GridView::widget([
                    'tableOptions' => ['class' => 'table table-striped dataex-html5-selectors2'],
            'dataProvider' => $dataProvider,
            'layout'=>"{items}",
                    //'filterModel' => $searchModel,
                    'columns' => [
                       ['class' => 'yii\grid\SerialColumn'], 
                       [
                            'header' => 'Invoice Date',
                            'value' => function($model){
                                return date('d-m-Y', strtotime($model->invoice_date));

                            }      
                        ],
                        [
                            'header' => 'Invoice Name',
                            'value' => function($model){
                                return $model->subject;
                            },       
                        ], 

                        [
                            'attribute' => 'invoice_no12',
                            'label' => 'Invoice No',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '100'],
                            'value' => function ($model) {
                                return $model->invoice_no;
                            },
                        ],
                        [
                            'header' => 'Type',
                            'value' => function($model){
                                return $model->invoice_type;
                            },       
                        ],
                        [
                            'header' => 'Status',
                            'value' => function($model){
                                return ucfirst($model->internal_invoicing_status);
                            },       
                        ],
                        [
                            'header' => 'Invoice Amt',
                            'contentOptions'=>['class'=>'text-right'],
                            'headerOptions' => ['class'=>'text-right'],
                            'value' => function($model){
                                return Yii::$app->formatter->asDecimal($model->total);
                            },       
                        ],            
                        [
                            'header' => 'Payment Due',
                            'value' => function($model){
                                return date('d-m-Y', strtotime($model->due_date));
                            },       
                        ],

                        /*[
                            'header' => 'Balance Due',
                            'contentOptions'=>['class'=>'text-right'],
                            'headerOptions' => ['class'=>'text-right'],
                            'value' => function($model){
                                return 'MYR'.Yii::$app->formatter->asDecimal($model->total);
                            },       
                        ],*/          
                        [
                            'attribute' => 'action12',
                            'label' => 'Action',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '150'],
                            'value' => function ($model) {
                                return $model->getActions();
                            },
                        ], 
                    ],
                ]);
                ?>
                </div>
            </div>
        </div>        
    </div>
</section>

