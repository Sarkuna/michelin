<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\InternalInvoicing */

$this->title = $model->invoice_no;
$this->params['breadcrumbs'][] = ['label' => 'All Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="invoice-view-wrapper">
    <div class="row">
        <!-- invoice view page -->
        <div class="col-xl-9 col-md-8 col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body pb-0 mx-25">
                        <!-- header section -->
                        <div class="row">
                            <div class="col-xl-4 col-md-12">
                                <span class="invoice-number mr-50">Invoice#</span>
                                <span><?= $model->invoice_no ?></span>
                            </div>
                            <div class="col-xl-8 col-md-12">
                                <div class="d-flex align-items-center justify-content-xl-end flex-wrap">
                                    <div class="mr-3">
                                        <small class="text-muted">Invoice Date:</small>
                                        <span><?= date('d/m/Y', strtotime($model->invoice_date)) ?></span>
                                    </div>
                                    <div>
                                        <small class="text-muted">Date Due:</small>
                                        <span><?= date('d/m/Y', strtotime($model->due_date)) ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- logo and title -->
                        <div class="row my-3">
                            <div class="col-6">
                                <h4 class="text-primary">Invoice</h4>
                                <span><?= $model->subject ?></span>
                            </div>
                        </div>
                        <hr>
                        <!-- invoice address and contact -->
                        <div class="row invoice-info">
                            <div class="col-6 mt-1">
                                <h6 class="invoice-from">Bill From</h6>
                                <div class="mb-1">
                                    <span><?= $model->bill_from_name ?></span>
                                </div>
                                <div class="mb-1">
                                    <span><?= $model->bill_from_add ?></span>
                                </div>
                                
                            </div>
                            <div class="col-6 mt-1">
                                <h6 class="invoice-to">Bill To</h6>
                                <div class="mb-1">
                                    <span><?= $model->bill_to_name ?></span>
                                </div>
                                <div class="mb-1">
                                    <span><?= $model->bill_to_add ?></span>
                                </div>
                                
                            </div>
                        </div>
                        <hr>
                    </div>
                    <!-- product details table-->
                    <div class="invoice-product-details table-responsive mx-md-25">
                        <table class="table table-borderless mb-0">
                            <thead>
                                <tr class="border-0">
                                    <th scope="col">DESCRIPTION</th>
                                    <th scope="col">QUANTITY</th>
                                    <th scope="col" class="text-right">PER UNIT, RM</th>
                                    <th scope="col" class="text-right">AMOUNT, RM</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($invoiceitems as $iii => $invoiceitem) {
                                    echo '<tr>
                                    <td>'.$invoiceitem->description.'</td>
                                    <td>'.$invoiceitem->quantity.'</td>
                                    <td>'.Yii::$app->formatter->asDecimal($invoiceitem->per_unit).'</td>
                                    <td class="text-primary text-right font-weight-bold">'.Yii::$app->formatter->asDecimal($invoiceitem->amount).'</td>
                                </tr>';
                                }
                                ?>
                                
                                
                            </tbody>
                        </table>
                    </div>

                    <!-- invoice subtotal -->
                    <div class="card-body pt-0 mx-25">
                        <hr>
                        <div class="row">
                            <div class="col-5 col-sm-6 mt-75">
                                <p><?= $model->terms_conditions ?></p>
                            </div>
                            <div class="col-7 col-sm-6 d-flex justify-content-end mt-75">
                                <div class="invoice-subtotal">
                                    <div class="invoice-calc d-flex justify-content-between">
                                        <span class="invoice-title">SUB TOTAL </span>
                                        <span class="invoice-value">RM <?= Yii::$app->formatter->asDecimal($model->sub_total) ?></span>
                                    </div>
                                    <div class="invoice-calc d-flex justify-content-between">
                                        <span class="invoice-title">SST @ 0% </span>
                                        <span class="invoice-value">- RM <?= Yii::$app->formatter->asDecimal($model->adjustment) ?></span>
                                    </div>

                                    <hr>
                                    <div class="invoice-calc d-flex justify-content-between">
                                        <span class="invoice-title">NET AMOUNT</span>
                                        <span class="invoice-value">RM <?= Yii::$app->formatter->asDecimal($model->total) ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- invoice action  -->
        <div class="col-xl-3 col-md-4 col-12">
            <div class="card invoice-action-wrapper shadow-none border">
                <div class="card-body">
                    <div class="invoice-action-btn">
                        <?php
                            echo '<a href="' . Url::to(['/reports/report/download-internel-pdf', 'invoiceid' => $model->internal_invoicing_id]) . '" title="Download Invoice" target="_blank" class="btn btn-light-primary btn-block"><span>Download</span></a> ';
                        ?>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>