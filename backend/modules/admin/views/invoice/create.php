<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Invoice */

$this->title = 'Create Invoice';
$this->params['breadcrumbs'][] = ['label' => 'Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div><!-- /.box-header -->
    <div class="invoice-create">
        <?=
        $this->render('_form', [
            'model' => $model,
            'invoiceitems' => $invoiceitems,
            'vouchers' => $vouchers,
            'items' => $items,
        ])
        ?>
    </div>
</div>
