<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\Menus */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    div#menus-display_location label {display: block;}
    .list-group{
        max-height: 300px;
        margin-bottom: 10px;
        overflow:scroll;
        -webkit-overflow-scrolling: touch;
    }
    a {
  text-decoration: none !important;
}

.dd3-content,
.panel-heading {
  cursor: pointer;
  cursor: hand;
}

.dd3-handle {
  cursor: move;
}

textarea {
  resize: none;
}

#nestable {
  float: none;
}

.dd-item {
  position: relative;
}

.dd-item .jTrashNestable {
  position: absolute !important;
  top: 0;
  right: 0;
  float: none;
  text-indent: 0;
}
.field-nestable-output{display: none;}
.panel-heading .accordion-toggle:after {
    /* symbol for "opening" panels */
    font-family: 'Glyphicons Halflings';  /* essential for enabling glyphicon */
    content: "\e114";    /* adjust as needed, taken from bootstrap.css */
    float: right;        /* adjust as needed */
    color: grey;         /* adjust as needed */
}
.panel-heading .accordion-toggle.collapsed:after {
    /* symbol for "collapsed" panels */
    content: "\e080";    /* adjust as needed, taken from bootstrap.css */
}
#menus-name{width: 250px;display: inline-block;}
.field-menus-name {margin-bottom: 0px;}
</style>

<div class="menus-form">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="manage-menus">
                        <?= Html::beginForm(['/admin/menus/edit'], 'GET'); ?>
                        <label for="select-menu-to-edit" class="selected-menu">Select a menu to edit:</label>
                        <?= Html::dropDownList('id', Yii::$app->getRequest()->getQueryParam('id'),$menu_list_name, ['prompt' => '— Select —', 'class' => 'test']) ?>
                        <?= Html::submitButton('Select', ['class' => 'btn btn-defult btn-sm']); ?>
                        or <a href="/admin/menus/create">create a new menu</a>. Don't forget to save your changes!
                        <?= Html::endForm(); ?>
                        
                    </div>
                </div>
            </div>
            
        </div>

    </div>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'menu_nodes')->textarea(['id'=>'nestable-output'])->label(false); ?>
    
    
    <div class="row widget-menu">
            <div class="col-md-4 formdis">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                    Pages
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">                                
                                <ul class="list-unstyled" style="position: relative; overflow: visible; padding: 0px;">
                                    <?php
                                    foreach ($pages as $page) {
                                        echo '<li class="list" id="' . $page->page_id . '">
                                                <label for="menu_id_page_' . $page->page_id . '" >
                                                    <input id="menu_id_page_' . $page->page_id . '" data-title="' . $page->page_title . '" data-related-id="' . $page->page_id . '" data-type="page" data-target="_self" data-icon-font="" data-custom-url="" name="menu_id" type="checkbox" value="' . $page->page_id . '">
                                                    ' . $page->page_title . '
                                                </label>
                                            </li>';
                                    }
                                    ?>

                                </ul>
                            </div>
                            <div class="box-footer">
                                <div id="nestable-menu">
                                    <button type="button" class="btn btn-default" id="appendnestable"><i class="fa fa-plus"></i> Add to Menu</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                    Custom Links
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <?php
                                $model->external_node_url = 'http://';
                                ?>
                                <?= $form->field($model, 'external_node_title')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'external_node_url')->textInput(['maxlength' => true]) ?>
                                <?php
                                echo $form->field($model, 'external_node_target')->dropDownList(
                                        ['_self' => 'Open link directly', '_blank' => 'Open link in new tab']
                                );
                                ?>
                            </div>
                            <div class="box-footer">
                                <div id="nestable-menu">
                                    <button type="button" class="btn btn-default" id="appendnestableexternal"><i class="fa fa-plus"></i> Add to Menu</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-8">
                <div class="box box-default">
                    <div class="box-header with-border bg-gray-light">
                        <div class="col-lg-10">
                            <?= $form->field($model, 'name', [
                                'template' => '<div class="row"><div class="col-xs-10">{label}{input}{error}{hint}</div></div>',
                            ]) ?>
                        </div>
                        

                        <div class="col-lg-2">
                            <div class="form-group">
                                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Save', ['class' => 'btn btn-primary btn-flat pull-right btn-md']) ?>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <p>Give your menu a name, then click Create Menu.</p>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix bg-gray-light">
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Save', ['class' => 'btn btn-primary btn-flat pull-right btn-md']) ?>
                        </div>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </div>
        </div>
    <?php ActiveForm::end(); ?>

</div>



<?php
$script = <<< JS
    $('.formdis :input').prop('disabled', true);
JS;
$this->registerJs($script);
?>