<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MenusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menuses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary menus-index">
    <div class="box-header with-border">
        <div class="col-lg-10 text-left" style="padding-left: 0px;">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="col-lg-2 text-right" style="padding-left: 0px;">
            <p>
                <?= Html::a('Create Menus', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>

    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="pages-index table-responsive">
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    //'page_id',
                    //'clientID',
                    'name',
                    'created_at',
                    'status',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '60'],
                        'template' => '{update} {delete}', //{view} {delete}
                        'buttons' => [
                            'update' => function ($url, $model) {
                                return (Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                            },
                                    'delete' => function ($url, $model) {
                                return (Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('app', 'Delete'), 'data' => ['confirm' => 'Are you sure you want to delete this item?', 'method' => 'post'],]));
                            }
                        ],
                    ],
                ],
            ]);
            ?>
        </div>
    </div>
</div>