<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-body pages-index">
    <!-- Scroll - horizontal and vertical table -->
    <section id="horizontal-vertical">
        
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title float-left"><?= Html::encode($this->title) ?></h4>
                        <p class="float-right">
                                        <?= Html::a('Create Page', ['create'], ['class' => 'btn btn-primary glow invoice-create', 'title' => 'Add New']) ?>

                                    </p>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">

                                <div class="table-responsive">
                                    
                                <?= GridView::widget([
        'tableOptions' => ['class' => 'table zero-configuration'],
                                'dataProvider' => $dataProvider,
                                'layout' => "{items}",
                                //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //['class' => 'yii\grid\SerialColumn','headerOptions' => ['width' => '10'],],
            //['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],
            'page_title',
            [
                'attribute' => 'created_datetime',
                'label' => 'Created Date',
                'format' => 'html',
                'headerOptions' => ['width' => '120'],
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->created_datetime));
                },
            ],
            [
                'attribute' => 'updated_datetime',
                'label' => 'Modified Date',
                'format' => 'html',
                'headerOptions' => ['width' => '120'],
                'value' => function ($model) {
                    return date('d-m-Y', strtotime($model->updated_datetime));
                },
            ],
            [
                'attribute' => 'created_datetime',
                'label' => 'Created By',
                'format' => 'html',
                'headerOptions' => ['width' => '100'],
                'value' => function ($model) {
                    return $model->createdBy->first_name.' '.$model->createdBy->last_name;
                },
            ],  

                            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}', //{view} {delete}
                'buttons' => [
                    'update' => function ($url, $model) {
                        return (Html::a('<span class="bx bxs-edit-alt"></span>', $url, ['title' => Yii::t('app', 'Edit'),]));
                    },
                    'delete' => function ($url, $model) {
                        return (Html::a('<span class="bx bx-trash-alt"></span>', $url, ['title' => Yii::t('app', 'Delete'), 'data' => ['confirm' => 'Are you sure you want to delete this item?', 'method' => 'post'],]));
                    }        
                            /* ,
                          'view' => function ($url, $model) {
                          return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                          },
                           */
                    ],
                //'visible' => $visible,
                ],
        ],
    ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>