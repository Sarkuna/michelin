<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Pages */

$this->title = 'Edit Page';
$this->params['breadcrumbs'][] = ['label' => 'Pages', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->page_id, 'url' => ['view', 'id' => $model->page_id]];
$this->params['breadcrumbs'][] = 'Edit Page';
?>
<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><?= Html::encode($this->title) ?></h4>
                </div>
                <div class="card-content">
                    <div class="card-body pages-update">
                        <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>