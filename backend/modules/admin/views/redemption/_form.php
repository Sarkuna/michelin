<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StatementOfRedemption */
/* @var $form yii\widgets\ActiveForm */
?>



    <?php $form = ActiveForm::begin(); ?>

        
        
        <div class="row">
        <?= $form->field($model, 'redemption_date',[
                    'options' => [
                        'class' => 'col-lg-6 col-md-6',
                    ],
                ])->widget(\yii\jui\DatePicker::classname(), [
                    //'language' => 'ru',
                    'clientOptions' =>[
                        'dateFormat' => 'dd-mm-yy',
                        'yearRange' => "2015:+1",
                        'changeMonth' => true,
                        'changeYear' => true
                    ],
                    'options' => ['class' => 'form-control', 'readOnly'=>'readOnly']
                    ])
                ?>
                <?=
                    $form-> field($model, 'status', [
                        'options' => [
                            'class' => 'col-md-6 col-6 form-group',
                        ],
                    ])->dropDownList(['adjustment' => "Adjustment", 'paid' => "Paid", 'cancelled' => "Cancelled", 'others' => "Others"], ['prompt' => '']); 
                ?>
        </div>
        <div class="row">
            <?=
                $form-> field($model, 'debit_in', [
                    'options' => [
                        'class' => 'col-lg-6 col-md-6',
                    ],
                ])->textInput(['maxlength' => true, 'class' => 'form-control text-right']) 
            ?>
            <?=
                $form-> field($model, 'credit_out', [
                    'options' => [
                        'class' => 'col-lg-6 col-md-6',
                    ],
                ])->textInput(['maxlength' => true, 'class' => 'form-control text-right']) 
            ?>
        </div>
        <?= $form->field($model, 'particulars')->textarea(['rows' => 6]) ?>
        
        

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>


    <?php ActiveForm::end(); ?>
