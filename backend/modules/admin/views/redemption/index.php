<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Redemptions for '.$mydate;
$this->params['breadcrumbs'][] = ['label' => 'Invoices'];
//$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .table th, .table td {
        padding: 1.0rem 0.5rem;
    }
    th, td {
    //white-space: nowrap;
    }
   #btnsubmit{float: right;}
   .left-col {
    float: left;
    width: 50%;
    }

    .center-col {
        float: left;
        width: 50%;
    }

    .right-col {
        float: right;
        width: 50%;
    }
</style>

<section id="table-transactions">
    <div class="card">
        <div class="card-header row">
            <!-- head -->
            <div class="col-md-10">
                <h5 class="card-title"><?= $this->title ?></h5>
            </div>
            <div class="col-md-2">
                <?php
                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("SELECT DATE_FORMAT(`redemption_date`, '%m-%Y') as m_date FROM `statement_of_redemption` GROUP BY `m_date` ORDER BY `m_date` DESC");
                $redemptionmonths = $command->queryAll();
                if(count($redemptionmonths) > 0) {
                    echo '<select name="forma" id="header-link1" class="form-control">';
                    foreach ($redemptionmonths as $redemptionmonth){
                        if($mydate == $redemptionmonth['m_date']) {
                            $sele = 'selected';
                        }else {
                            $sele = '';
                        }
                        echo '<option '.$sele.' value="?date='.$redemptionmonth['m_date'].'">'.$redemptionmonth['m_date'].'</option>';
                    }
                    echo '</select>';
                }
                ?>
            </div>
        </div>

        <div class="card-content">
            <div class="card-body">

                <div class="painter-profile-index table-responsive">
                    <?= Html::a('New', ['create'], ['class' => 'btn buttons-excel buttons-html5 btn-success', 'id' => 'btnsubmit']); ?>
                    <table class="table table-striped dataex-html5-selectors3">
                        <thead>
                            <tr>
                                <th>Redemption Date</th>
                                <th>Description</th>
                                <th class="text-right">Debit (IN)</th>
                                <th class="text-right">Credit (OUT)</th>
                                <th class="text-right">Balance</th>
                                <th>Status</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $redemptions = $dataProvider->getModels();
                            if ($redemptions > 0) {
                                $balance = 0;
                                $debit_in = '';$credit_out='';
                                
                                echo '<tr>'; 
                                echo '<td>01-' . $mydate . '</td>';
                                echo '<td>Opening Balance</td>';
                                echo '<td class="text-right"></td>';
                                echo '<td class="text-right"></td>';
                                echo '<td class="text-right">'.Yii::$app->formatter->asDecimal($opening_bal).'</td>';
                                echo '<td></td>';
                                echo '<td></td>';
                                echo '</tr>';
                                $balance = $opening_bal;
                                foreach ($redemptions as $redemption) {                                    
                                    $chkbala = $redemption->debit_in - $redemption->credit_out;
                                        
                                    $balance += $chkbala;                                    
                                    
                                    if($redemption->debit_in > 0){
                                        $debit_in = Yii::$app->formatter->asDecimal($redemption->debit_in);                                        
                                    }else {
                                        $debit_in = '';
                                    }
                                    if($redemption->credit_out > 0) { 
                                        $credit_out = Yii::$app->formatter->asDecimal($redemption->credit_out);                                        
                                    }else {
                                        
                                        if(!empty($redemption->credit_out)) {
                                            $credit_out = Yii::$app->formatter->asDecimal($redemption->credit_out);
                                        }else {
                                            $credit_out='';
                                        }
                                    }
                                    
                                    if (!empty($redemption->order_id)) {
                                        if($redemption->order_type == 'michelin') {
                                            $url = Html::a($redemption->particulars, ['/sales/orders/view-detail', 'id' => $redemption->order_id], ['target' => '_blank']);
                                        }else {
                                            $url = Html::a($redemption->particulars, ['/sales/orders/view', 'id' => $redemption->order_id], ['target' => '_blank']);
                                        }
                                    }else {
                                        $url = $redemption->particulars;
                                    }
                                    echo '<tr>'; 
                                    echo '<td>' . date('d-m-Y', strtotime($redemption->redemption_date)) . '</td>';
                                    echo '<td>' . $url. '</td>';
                                    echo '<td class="text-right">' . $debit_in . '</td>';
                                    echo '<td class="text-right">' . $credit_out . '</td>';
                                    echo '<td class="text-right">' . Yii::$app->formatter->asDecimal($balance) . '</td>';
                                    echo '<td><span class="label label-">'.ucfirst($redemption->status).'</span></td>';
                                    if($redemption->manual == 1) {
                                        echo '<td>'.Html::a('<i class="bx bxs-edit-alt"></i>', ['update', 'id' => $redemption->redemption_id], ['title' => 'Edit']).'</td>';
                                    }else {
                                        echo '<td></td>';
                                    }
                                    echo '</tr>';
                                }
                                echo '<tr>'; 
                                echo '<td>' . date('t-m-Y',strtotime('01-'.$mydate)) . '</td>';
                                echo '<td>Closing Balance</td>';
                                echo '<td class="text-right"></td>';
                                echo '<td class="text-right"></td>';
                                echo '<td class="text-right">'.Yii::$app->formatter->asDecimal($balance).'</td>';
                                echo '<td></td>';
                                echo '<td></td>';
                                echo '</tr>';
                            }

                            ?>
                        </tbody>
                    </table>
                    
                    
             
                </div>
            </div>
        </div>        
    </div>
</section>
      
<?php
$script = <<<EOD
    $(function(){
      // bind change event to select
      $('#header-link1').on('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });   
     
EOD;
$this->registerJs($script);



?>