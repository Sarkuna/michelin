<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StatementOfRedemption */

$this->title = $model->redemption_id;
$this->params['breadcrumbs'][] = ['label' => 'Statement Of Redemptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="statement-of-redemption-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->redemption_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->redemption_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'redemption_id',
            'redemption_date',
            'particulars:ntext',
            'debit_in',
            'credit_out',
            'payment_type',
            'status',
            'payment_id',
            'order_id',
        ],
    ]) ?>

</div>
