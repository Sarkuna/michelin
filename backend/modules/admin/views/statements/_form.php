<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StatementOfAccount */
/* @var $form yii\widgets\ActiveForm */
?>



    <?php $form = ActiveForm::begin(); ?>

        
        
        <div class="row">
        <?= $form->field($model, 'statement_date',[
                    'options' => [
                        'class' => 'col-lg-6 col-md-6',
                    ],
                ])->widget(\yii\jui\DatePicker::classname(), [
                    //'language' => 'ru',
                    'clientOptions' =>[
                        'dateFormat' => 'dd-mm-yy',
                        'yearRange' => "2015:+1",
                        'changeMonth' => true,
                        'changeYear' => true
                    ],
                    'options' => ['class' => 'form-control', 'readOnly'=>'readOnly']
                    ])
                ?>
                <?=
                $form-> field($model, 'payment_type', [
                    'options' => [
                        'class' => 'col-lg-6 col-md-6',
                    ],
                ])->textInput(['maxlength' => true, 'Value' => 'Payment']) 
                ?> 
        </div>
        <?= $form->field($model, 'payment_id')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'note')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'particulars')->textarea(['rows' => 6]) ?>
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Invoice</th>
                    <th style="width: 40px">Amount</th>
                </tr>
                <?php
                $no = 1;
                foreach($invoicelist as $key=>$value) {
                    echo '<tr>
                    <td>'.$no.'</td>
                    <td>'.$key.'</td>
                    <td class="text-right">'.$value.'</td>
                </tr>';
                    $no ++;
                }
                echo '<tr  style="background: #e4e0e0;font-size: 18px;font-weight: 600;">
                    <td colspan="2">Total</td>
                    <td class="text-right">'.Yii::$app->formatter->asDecimal($model->payment).'</td>
                </tr>';
                ?>                
            </tbody>
        </table>
        <div class="row">
            <?= $form->field($model, 'payment')->hiddenInput()->label(false) ?>
            

            <?=
                $form-> field($model, 'payment_status', [
                    'options' => [
                        'class' => 'col-md-6 col-6 form-group',
                    ],
                ])->dropDownList(['paid' => "Paid", 'cancelled' => "Cancelled"], ['prompt' => '']); 
            ?>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>


    <?php ActiveForm::end(); ?>

