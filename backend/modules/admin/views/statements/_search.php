<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StatementOfAccountSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="statement-of-account-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'sa_id') ?>

    <?= $form->field($model, 'statement_date') ?>

    <?= $form->field($model, 'particulars') ?>

    <?= $form->field($model, 'amount') ?>

    <?= $form->field($model, 'payment') ?>

    <?php // echo $form->field($model, 'payment_type') ?>

    <?php // echo $form->field($model, 'payment_status') ?>

    <?php // echo $form->field($model, 'internal_invoicing_id') ?>

    <?php // echo $form->field($model, 'invoice_item_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
