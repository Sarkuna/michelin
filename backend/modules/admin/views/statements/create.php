<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StatementOfAccount */

$this->title = 'Create A Payment';
$this->params['breadcrumbs'][] = ['label' => 'Statement Of Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-6 col-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><?= Html::encode($this->title) ?></h4>
                </div>
                <div class="card-content">
                    <div class="card-body statement-of-account-create">
                        <?=
                        $this->render('_form', [
                            'model' => $model,
                            'invoicelist' => $invoicelist,
                        ])
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
