<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Statement of Account';
$this->params['breadcrumbs'][] = ['label' => 'Invoices'];
//$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .table th, .table td {
        padding: 1.0rem 0.5rem;
    }
    th, td {
    //white-space: nowrap;
    }
   #btnsubmit{float: right;}
   .left-col {
    float: left;
    width: 50%;
    }

    .center-col {
        float: left;
        width: 50%;
    }

    .right-col {
        float: right;
        width: 50%;
    }
</style>

<section id="table-transactions">
    <div class="card">
        <div class="card-header">
            <!-- head -->
            <h5 class="card-title"><?= $this->title ?></h5>
        </div>

        <div class="card-content">
            <div class="card-body">

                <div class="painter-profile-index table-responsive">
                    <?=Html::beginForm(['payment'],'post');?>
                    <?= Html::submitButton('Make Payment', [
                                'title' => 'Make Payment',
                                'class' => 'btn buttons-excel buttons-html5 btn-success',
                                'id' => 'btnsubmit'
                            ]); ?>
                <?=
                GridView::widget([
                    'tableOptions' => ['class' => 'table table-striped dataex-html5-selectors3'],
                    'dataProvider' => $dataProvider,
                    'layout'=>"{items}",
                    //'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => '\yii\grid\CheckboxColumn',
                            //'headerOptions' => ['width' => '10'],
                            'checkboxOptions' => function ($model, $key, $index, $column) {
                                //$totalpoint = $model->getTotalPoints() + $model->getShippingPointTotal();
                                 if ($model->payment_status == 'pending') {
                                     return ['value' => $key];
                                 }
                                 return ['disabled' => true];
                             },
                        ],
                        [
                            'attribute' => 'date',
                            'label' => 'Date',
                            //'headerOptions' => ['width' => '95'],
                            'value' => function($model) {
                                return date('d-m-Y', strtotime($model->statement_date));
                            },
                        ],
                        [
                            'attribute' => 'particularsno',
                            'label' => 'Particulars',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '50%'],
                            'value' => function($model) {
                                if (!empty($model->internal_invoicing_id)) {
                                    ///admin/internal-invoicing/index
                                    $url = Url::to(['/admin/internal-invoicing/view', 'id' => $model->internal_invoicing_id]);
                                    $options = ['target' => '_blank'];
                                    $particulars = Html::a($model->internalInvoice->invoice_no, $url, $options) . ' ' . $model->internalInvoice->subject;
                                } else if (!empty($model->order_id)) {
                                    $url = Url::to(['/sales/orders/view', 'id' => $model->order_id]);
                                    $options = ['target' => '_blank'];
                                    $particulars = Html::a($model->particulars, $url, $options) . ' (' . $model->order_status.')';
                                } else {
                                    $particulars = $model->particulars;
                                }
                                return $particulars;
                            },
                        ],
                        [
                            'attribute' => 'Amount',
                            'label' => 'Amount',
                            'contentOptions' => ['class' => 'text-right'],
                            'headerOptions' => ['class' => 'text-right'],
                            'value' => function($model) {
                                if ($model->amount > 0) {
                                    return Yii::$app->formatter->asDecimal($model->amount);
                                } else {
                                    return '';
                                }
                            },
                        ],
                        [
                            'attribute' => 'Payment',
                            'label' => 'Payment',
                            'contentOptions' => ['class' => 'text-right'],
                            'headerOptions' => ['class' => 'text-right'],
                            'value' => function($model) {
                            if ($model->payment > 0) {
                                    return Yii::$app->formatter->asDecimal($model->payment);
                                } else {
                                    return '';
                                }
                            },
                        ],
                        [
                            'attribute' => 'Balance',
                            'label' => 'Balance',
                            'contentOptions' => ['class' => 'text-right'],
                            'headerOptions' => ['class' => 'text-right'],
                            'value' => function ($model, $key, $index, $widget) use(&$runningtotal) {
                                $runningtotal+= ($model->amount - $model->payment);
                                return Yii::$app->formatter->asDecimal($runningtotal);
                            },
                        ],
                        [
                            'attribute' => 'Payment Type',
                            'label' => 'Payment Type',
                            'value' => function($model) {
                                return $model->payment_type;
                            },
                        ],
                        [
                            'attribute' => 'Status',
                            'label' => 'Status',
                            'value' => function($model) {
                                return ucfirst($model->payment_status);
                            },
                        ],
                    ],
                ]);
                ?>
                </div>
            </div>
        </div>        
    </div>
</section>


<?php
    $clientScript = '

        $("#btnsubmit").click(function()
        {
            var keys = $(\'#DataTables_Table_0\').find(\'input[type=checkbox]:checked\').length;
            if(keys > "0"){
                return true;
            }else{
                alert("Please check at least one checkbox");
                return false;
            }
            
        });

    ';
    $this->registerJs($clientScript, \yii\web\View::POS_END, 'booking-period');
    ?>