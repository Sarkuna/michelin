<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StatementOfAccount */

$this->title = 'Update Statement Of Account: ' . $model->sa_id;
$this->params['breadcrumbs'][] = ['label' => 'Statement Of Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sa_id, 'url' => ['view', 'id' => $model->sa_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="statement-of-account-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
