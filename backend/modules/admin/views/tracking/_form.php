<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
/* @var $this yii\web\View */
/* @var $model common\models\VIPOrderProduct */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
.datepicker {
    z-index: 2000 !important;
}
</style>

<div class="viporder-product-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">
        <div class="row">
            <div class="col-lg-4">
                <?= $form->field($model, 'tracking_type')->dropDownList(['N' => 'No', 'Y' => 'Yes']); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <?= $form->field($model, 'shipped_date')->widget(DatePicker::classname(), [
                        'options' => ['readOnly' => true, 'placeholder' => 'Shipped Date ...'],
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayHighlight' => true,
                            'todayBtn' => true,
                            //'format' => 'yyyy-mm-dd',
                            'format' => 'dd-mm-yyyy',
                        //'startDate' => $startDate,
                ]])
                ?>
            </div>
            <div class="col-lg-4">
                <?= $form->field($model, 'tracking_no')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-lg-5">
                <?= $form->field($model, 'tracking_link')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
