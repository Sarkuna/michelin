<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VIPOrderProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="viporder-product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'order_product_id') ?>

    <?= $form->field($model, 'order_id') ?>

    <?= $form->field($model, 'product_id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'model') ?>

    <?php // echo $form->field($model, 'quantity') ?>

    <?php // echo $form->field($model, 'point') ?>

    <?php // echo $form->field($model, 'point_admin') ?>

    <?php // echo $form->field($model, 'point_total') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'total') ?>

    <?php // echo $form->field($model, 'shipping_price') ?>

    <?php // echo $form->field($model, 'shipping_price_total') ?>

    <?php // echo $form->field($model, 'shipping_point') ?>

    <?php // echo $form->field($model, 'shipping_point_total') ?>

    <?php // echo $form->field($model, 'product_status') ?>

    <?php // echo $form->field($model, 'invoice_item_id') ?>

    <?php // echo $form->field($model, 'tracking_type') ?>

    <?php // echo $form->field($model, 'shipped_date') ?>

    <?php // echo $form->field($model, 'tracking_no') ?>

    <?php // echo $form->field($model, 'tracking_link') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
