<?php

namespace app\modules\catalog\controllers;

use Yii;
use common\models\VIPManufacturer;
use common\models\VIPManufacturerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ManufacturerController implements the CRUD actions for VIPManufacturer model.
 */
class ManufacturersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VIPManufacturer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VIPManufacturerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VIPManufacturer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VIPManufacturer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VIPManufacturer();

        if ($model->load(Yii::$app->request->post())) {
            $model->slug = Yii::$app->VIPglobal->createSlug($model->name);
            $model->save();
            \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified manufacturer!']);
            return $this->redirect(['index']);
            //return $this->redirect(['view', 'id' => $model->manufacturer_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing VIPManufacturer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->slug = Yii::$app->VIPglobal->createSlug($model->name);
            $model->save();
            \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified manufacturer!']);
            return $this->redirect(['index']);
            //return $this->redirect(['view', 'id' => $model->manufacturer_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionOrder()
    {
        $position = $_POST['position'];
        foreach($position as $k=>$v){
            $model = $this->findModel($v);
            $model->sort_order = $k;
            $model->save(false);
        }
    }

    /**
     * Deletes an existing VIPManufacturer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VIPManufacturer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VIPManufacturer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VIPManufacturer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
