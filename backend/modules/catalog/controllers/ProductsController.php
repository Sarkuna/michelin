<?php

namespace app\modules\catalog\controllers;

use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

use yii\imagine\Image;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Image\ImageInterface;
use Eventviva\ImageResize;
use backend\models\DynamicForms;
use backend\models\Model;
use common\models\Uploads;
use common\models\VIPProduct;
use common\models\VIPProductSearch;

/**
 * ProductsController implements the CRUD actions for VIPProduct model.
 */
class ProductsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VIPProduct models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VIPProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$dataProvider->pagination->pageSize=50;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VIPProduct model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        
        $model = $this->findModel($id);
        list($initialPreview,$initialPreviewConfig) = $this->getInitialPreview($model->ref);
        return $this->render('view', [
            'model' => $model,
            'initialPreview' => $initialPreview,
            'initialPreviewConfig' => $initialPreviewConfig,
        ]);
    }

    /**
     * Creates a new VIPProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = '@app/themes/frestadmin/layouts/product_upload';
        $model = new VIPProduct();
        //$modelsOptionvalue = [new VIPProductOptionValue];
        //$deliveryzone = [new VIPProductDeliveryZone];

        if ($model->load(Yii::$app->request->post())) {                
            if($model->save(false)) {
                $this->Uploads(false);
                            $newCover = UploadedFile::getInstance($model, 'main_image1');
                            if (!empty($newCover)) {
                                //unlink(Yii::$app->basePath .'/web/upload/product_cover/'.$model->main_image);
                                $path = Yii::$app->basePath .'/web/upload/product_cover/';
                                $thumbnailImagePath = Yii::$app->basePath .'/web/upload/product_cover/thumbnail/';
                                $newCoverName = Yii::$app->security->generateRandomString();
                                $imgname = $newCoverName . '.' . $newCover->extension;
                                $model->main_image = $imgname;
                                $newCover->saveAs($path . $newCoverName . '.' . $newCover->extension);
                                
                                /*$imagine = Image::getImagine();
                                $image = $imagine->open($path . $imgname);
                                $image->resize(new Box(600, 798))->save($thumbnailImagePath . $imgname, ['quality' => 100]);*/
                                $imagine = Image::getImagine();
                                $image = $imagine->open($path . $imgname);
                                $image->resize(new Box(600, 600))->save($thumbnailImagePath . $imgname, ['quality' => 100]);
                                /*$image = new ImageResize($path);
                                $image->resizeToBestFit(600, 798);
                                $image->save($thumbnailImagePath.$imgname);*/
                            }
                            \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified clients!']);
                        return $this->redirect(['index']);
            }else {
                \Yii::$app->getSession()->setFlash('danger',['title' => 'Fail', 'text' => 'You have modified clients!']);
                return $this->redirect(['index']);
            }
        } else {
            $model->mark_up = Yii::$app->VIPglobal->clientSetup()['pervalue'];
            $model->ref = substr(Yii::$app->getSecurity()->generateRandomString(),10);
            $model->handling_fee_em = '0.00'; $model->handling_fee_wm = '0.00';
            $model->insurance_em = '0.00'; $model->insurance_wm = '0.00';
            $model->shipping_em = '0.00'; $model->shipping_wm = '0.00';
        }
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    

    /**
     * Updates an existing VIPProduct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        list($initialPreview,$initialPreviewConfig) = $this->getInitialPreview($model->ref);
        if ($model->load(Yii::$app->request->post())) {
            $this->Uploads(false);
            $newCover = UploadedFile::getInstance($model, 'main_image1');
            if (!empty($newCover)) {
                $path = Yii::$app->basePath .'/web/upload/product_cover/';
                $thumbnailImagePath = Yii::$app->basePath .'/web/upload/product_cover/thumbnail/';
                $newCoverName = Yii::$app->security->generateRandomString();
                $imgname = $newCoverName . '.' . $newCover->extension;
                $model->main_image = $imgname;
                $newCover->saveAs($path . $newCoverName . '.' . $newCover->extension);
                
                $imagine = Image::getImagine();
                $image = $imagine->open($path . $imgname);
                $image->resize(new Box(600, 600))->save($thumbnailImagePath . $imgname, ['quality' => 100]);
            }
            
            if($model->save()){
                \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'You have modified products!']);
                return $this->redirect(['index']);
            }else {
               \Yii::$app->getSession()->setFlash('error',['title' => 'Fail', 'text' => 'You have modified products fail!']);
                return $this->redirect(['index']); 
            }
            //eturn $this->redirect(['view', 'id' => $model->product_id]);
        } else {
            if(empty($model->ref)){
                $model->ref = substr(Yii::$app->getSecurity()->generateRandomString(),10);
            }
            return $this->render('update', [
                'model' => $model,
                'initialPreview'=>$initialPreview,
                'initialPreviewConfig'=>$initialPreviewConfig,
                
            ]);
        }
    }
    
    public function actionResize()
    {
        $imagine = Image::getImagine();
        $pros = VIPProduct::find()
                ->where(['main_image' => null])
                //->limit(10)
                ->all();        
        foreach($pros as $pro){
            $id = $pro->product_id;
            $model = $this->findModel($id);
            //$product_code = $pro->product_code;
            $product_code = str_replace(' ','',$pro->product_code);
            $path = Yii::$app->basePath .'/web/upload/product_cover/original/'.$product_code.'_1.jpg';
            if (file_exists($path)) {
                $thumbnailImagePath = Yii::$app->basePath .'/web/upload/product_cover/thumbnail/'.$product_code.'_1.jpg';

                /*$image = new ImageResize($path);
                $image->resizeToBestFit(600, 798);
                $image->save($thumbnailImagePath);*/
                
                $imagine = Image::getImagine();
                $image = $imagine->open($path);
                $image->resize(new Box(600, 600))->save($thumbnailImagePath, ['quality' => 100]);

                $model->main_image = $product_code.'_1.jpg';
                $model->save(false);
            }
        }        
    }
    public function actionRename()
    {
        $path = Yii::$app->basePath .'/web/upload/photolibrarys/Shihan';
        $newpath = Yii::$app->basePath .'/web/upload/photolibrarys/Shihan123';
        if(file_exists($path)) {
            rename($path,$newpath);
        }else {
            echo 'Folder not exisit';
        }
    }
    
    public function actionUploadexcel() {
        
        $session = Yii::$app->session;
        $model = new \backend\models\ExcelForm();
        $msg = '';

        if ($model->load(Yii::$app->request->post())) {
            $model->excel = UploadedFile::getInstance($model, 'excel');

            if ($model->excel && $model->validate()) {
                $newName = date('mmssh_dmy');
                $model->excel->saveAs(Yii::$app->basePath .'/web/upload/' . $newName . '.' . $model->excel->extension);
                $inputFile = Yii::$app->basePath .'/web/upload/' . $newName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                //

                $pre_defined = array( "product name","product code","variant","price","handling fee em","handling fee wm","insurance em","insurance wm","shipping em","shipping wm","quantity","status","manufacturer id","category id","type");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));

                if($checkmatch > 9){
                    $data = array();

                    for ($row = 2; $row <= $highestRow; ++ $row) {
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);

                        
                        $data[] = [$rowData[0]['product name'],$rowData[0]['product code'],$rowData[0]['variant'],$rowData[0]['price'],$rowData[0]['handling fee em'],$rowData[0]['handling fee wm'],$rowData[0]['insurance em'],$rowData[0]['insurance wm'],$rowData[0]['shipping em'],$rowData[0]['shipping wm'],$rowData[0]['quantity'],substr($rowData[0]['status'],0,1),$rowData[0]['manufacturer id'],$rowData[0]['category id'],$rowData[0]['type'],date('Y-m-d')];

                    }

                    Yii::$app->db
                    ->createCommand()
                    ->batchInsert('product_excel_upload', ['product_name','product_code','variant','price','handling_fee_em','handling_fee_wm','insurance_em','insurance_wm','shipping_em','shipping_wm','quantity','status_pro','manufacturer','category','type','create_date'],$data)
                    ->execute();

                    $returnedValue = Yii::$app->VIPglobal->importProductUpload();

                    if ($returnedValue) {
                        \Yii::$app->getSession()->setFlash('success',['title' => 'Import Success', 'text' => 'Product Import Sccessfully']);
                        //return $this->redirect(['uploadexcel']);
                    } else {
                        \Yii::$app->getSession()->setFlash('warning',['title' => 'Import Fail', 'text' => 'Sorry product import fail!']);
                    }
                    unlink($inputFile);
                }else {
                    $teml = 'product name, links, meta tag title, product code, suppliers product code (isku), variant, msrp, partner price, peninsular, sabah, sarawak, singapore, manufacturer id, category id, sub category id';
                    \Yii::$app->getSession()->setFlash('warning',['title' => 'Fail', 'text' => 'Sorry your excel template not match. This should on your excel ! ('.$teml.') Note:case sensitive no issue']);
                    //return $this->redirect(['uploadexcel']);
                }

                return $this->render('uploadexcelreport', [
                    'msg' => $returnedValue,
                ]);
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('uploadexcel', [
                        'model' => $model,
            ]);
        }
        //echo("6");
    }
    
    public function actionUploadexcelprice() {
        $session = Yii::$app->session;
        $model = new \backend\models\ExcelForm();
        $msg = '';

        if ($model->load(Yii::$app->request->post())) {
            $model->excel = UploadedFile::getInstance($model, 'excel');

            if ($model->excel && $model->validate()) {
                $model->excel->saveAs(Yii::$app->basePath .'/web/upload/' . $model->excel->baseName . '.' . $model->excel->extension);

                //ini_set('auto_detect_line_endings', TRUE);
                //$handle = fopen('/var/www/residenz/frontend/web/upload/' . $model->csv->baseName . '.' . $model->csv->extension, 'r');
                $inputFile = Yii::$app->basePath .'/web/upload/' . $model->excel->baseName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //$objPHPExcel = \PHPExcel_IOFactory::load('./test.xlsx');
                $sheetData = $objPHPExcel->getActiveSheet(0)->toArray(null, true, true, true);
                for ($row = 1; $row <= $highestRow; ++ $row) {
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    if ($row == 1) {
                        continue;
                    }
                    $product_code = $rowData[0][0];
                    $product = VIPProduct::find()->where(['product_code' => $product_code])->one();
                    $count = count($product);
                    if($count > 0){
                        $chkentry = VIPProduct::find()->where(['product_code' => $product_code, 'status' => "E"])->count();
                        if($chkentry == 0){
                            $points_value = $rowData[0][1];
                            $price = $rowData[0][2];
                            $id = $product->product_id;
                            $model = $this->findModel($id);
                            //$productoptionvalue = new VIPProductOptionValue();
                            $model->points_value = $points_value;
                            $model->price = "$price";
                            $model->status = "E";
                            if($model->save()){
                                $msg .= '<li class="list-group-item"><span class="badge pull-right bg-success"><i class="fa fa-check" aria-hidden="true"></i></span>  Product Code. <b>('.$product_code.')</b> added.</li>';
                            }else {
                                $msg .= '<li class="list-group-item"><span class="badge pull-right bg-warning"><i class="fa fa-exclamation" aria-hidden="true"></i></span>  Product Code. <b>('.$product_code.')</b> add fail.</li>';
                            }
                        }else{
                            $msg .= '<li class="list-group-item"><span class="badge pull-right bg-warning"><i class="fa fa-exclamation" aria-hidden="true"></i></span>  Product Code. <b>('.$product_code.')</b> already exist.</li>';
                        }
                    }else{
                        $msg .= '<li class="list-group-item"><span class="badge pull-right bg-danger"><i class="fa fa-ban" aria-hidden="true"></i></span>  Product Code. <b>('.$product_code.')</b> not found in our System!.</li>';
                    }
                    
                }
                return $this->render('uploadexcelreport', [
                    'msg' => $msg,
                ]);
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('uploadexcel', [
                        'model' => $model,
            ]);
        }
        //echo("6");
    }
    /**
     * Deletes an existing VIPProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VIPProduct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VIPProduct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VIPProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    
  /*|*********************************************************************************|
  |================================ Upload Ajax ====================================|
  |*********************************************************************************|*/
    public function actionUploadajax(){
           $this->Uploads(true);
     }
    private function CreateDir($folderName){
        if($folderName != NULL){
            $basePath = VIPProduct::getUploadPath();
            if(FileHelper::createDirectory($basePath.$folderName,0777)){
                FileHelper::createDirectory($basePath.$folderName.'/thumbnail',0777);
            }
        }
        return;
    }
    private function removeUploadDir($dir){
        FileHelper::removeDirectory(VIPProduct::getUploadPath().$dir);
    }
    private function Uploads($isAjax=false) {
             if (Yii::$app->request->isPost) {
                $images = UploadedFile::getInstancesByName('upload_ajax');
                if ($images) {
                    if($isAjax===true){
                        $ref =Yii::$app->request->post('ref');
                    }else{
                        $PhotoLibrary = Yii::$app->request->post('VIPProduct');
                        $ref = $PhotoLibrary['ref'];
                    }
                    $this->CreateDir($ref);
                    foreach ($images as $file){
                        $fileName       = $file->baseName . '.' . $file->extension;
                        $realFileName   = md5($file->baseName.time()) . '.' . $file->extension;
                        $savePath       = VIPProduct::UPLOAD_FOLDER.'/'.$ref.'/'. $realFileName;
    
                    
                        if($file->saveAs($savePath)){
                            if($this->isImage(Url::base(true).'/'.$savePath)){
                                //var_dump(createThumbnail($ref,$realFileName));
                                $this->createThumbnail($ref,$realFileName);
                            } 
                          
                            $model                  = new Uploads;
                            $model->ref             = $ref;
                            $model->file_name       = $fileName;
                            $model->real_filename   = $realFileName;
                            $model->save();
                            if($isAjax===true){
                                echo json_encode(['success' => 'true']);
                            }
                            
                        }else{
                            if($isAjax===true){
                                echo json_encode(['success'=>'false','eror'=>$file->error]);
                            }
                        }
                        
                        
                        
                    }
                }
            }
    }
    private function getInitialPreview($ref) {
            $datas = Uploads::find()->where(['ref'=>$ref])->all();
            $initialPreview = [];
            $initialPreviewConfig = [];
            foreach ($datas as $key => $value) {
                array_push($initialPreview, $this->getTemplatePreview($value));
                array_push($initialPreviewConfig, [
                    'caption'=> $value->file_name,
                    'width'  => '120px',
                    'url'    => Url::to(['/catalog/products/deletefile-ajax']),
                    'key'    => $value->upload_id
                ]);
            }
            return  [$initialPreview,$initialPreviewConfig];
    }
    public function isImage($filePath){
            return @is_array(getimagesize($filePath)) ? true : false;
    }
    private function getTemplatePreview(Uploads $model){     
            $filePath = VIPProduct::getUploadUrl().$model->ref.'/thumbnail/'.$model->real_filename;
            $isImage  = $this->isImage($filePath);
            if($isImage){
                $file = Html::img($filePath,['class'=>'file-preview-image', 'alt'=>$model->file_name, 'title'=>$model->file_name]);
            }else{
                $file =  "<div class='file-preview-other'> " .
                         "<h2><i class='glyphicon glyphicon-file'></i></h2>" .
                         "</div>";
            }
            return $file;
    }
    private function createThumbnail($folderName,$fileName,$width=270){
      $uploadPath   = VIPProduct::getUploadPath().'/'.$folderName.'/'; 
      $file         = $uploadPath.$fileName;
      $image        = Yii::$app->image->load($file);
      $image->resize($width);
      $image->save($uploadPath.'thumbnail/'.$fileName);
      return;
    }
    
    public function actionDeletefileAjax(){
        $this->enableCsrfValidation = false;
        $model = Uploads::findOne(Yii::$app->request->post('key'));
        if($model!==NULL){
            $filename  = VIPProduct::getUploadPath().$model->ref.'/'.$model->real_filename;
            $thumbnail = VIPProduct::getUploadPath().$model->ref.'/thumbnail/'.$model->real_filename;
            if($model->delete()){
                @unlink($filename);
                @unlink($thumbnail);
                echo json_encode(['success'=>true]);
            }else{
                print_r($model->getErrors());
                echo json_encode(['success'=>false]);
            }
        }else{
          echo json_encode(['success'=>false]);  
        }
    }
    
    public static function validateDeposit($taboptions, $modelsOptionvalue)
    {
        $valid = true;
        for ($i=0; $i<count($taboptions); $i++) {
            $valid = Model::validateMultiple($modelsOptionvalue[$i]);
        }

        return $valid;
    }
    
    public function actionShihan(){
        //die;
        $path = Yii::$app->basePath .'/web/upload/product_cover/thumbnail/';
        //$path_parts = pathinfo($path.'MCH0001'); // return "fname"
        $ext = pathinfo($path.'MCH0001.png', PATHINFO_FILENAME);
        //$files = scandir($path);
        $files = preg_grep('~\.(jpeg|jpg|png)$~', scandir($path));
        echo 'product_code'.','.'main_image<br>';
        foreach($files as $filew)
        {
            $name = pathinfo($path.$filew, PATHINFO_FILENAME);
            echo $name.','.$filew.'<br>';
        }
        //echo '<pre>';
        //var_dump($files);
        die;
    }
}
