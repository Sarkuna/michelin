<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCategories */

$this->title = 'Edit';
//$this->title = 'Categories';
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->vip_categories_id]];
//$this->params['breadcrumbs'][] = $this->title;
?>

<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><?= Html::encode($model->name) ?></h4>
                </div>
                <div class="card-content">
                    <div class="card-body pages-create">
                        <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
