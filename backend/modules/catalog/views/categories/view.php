<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCategories */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vipcategories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vipcategories-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->vip_categories_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->vip_categories_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'vip_categories_id',
            'name',
            'slug',
            'description:ntext',
            'image',
            'meta_title',
            'meta_keywords',
            'meta_description',
            'position',
            'is_active',
        ],
    ]) ?>

</div>
