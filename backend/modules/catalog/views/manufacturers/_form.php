<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\widgets\FileInput;
/* @var $this yii\web\View */
/* @var $model common\models\VIPManufacturer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vipmanufacturer-form">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal form-label-left']]); ?>
    <div class="box-body">
        <?=
        $form->field($model, 'name', [
            //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput(array('placeholder' => 'Manufacturer Name'));
        ?>

        <?=
        $form->field($model, 'sort_order', [
            //'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
        ])->textInput(array('placeholder' => ''));
        ?>
        
    </div>
    <div class="box-footer">
        <div class="form-group">

                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
