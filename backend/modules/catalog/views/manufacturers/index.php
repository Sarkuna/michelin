
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPManufacturerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manufacturers';
$this->params['breadcrumbs'][] = $this->title;
$manufacturers = $dataProvider->getModels();
$count = count($manufacturers);
?>

<style>
    .small-text{font-size: 12px;}
    h5, .h5 {font-size: 1.0rem;}
</style>
<section id="sortable-lists">
    <div class="row">
        <!-- Basic List Group -->
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title float-left"><?= Html::encode($this->title) ?></h4>
                    <p class="float-right">
                                        <?= Html::a('Create Manufacturer', ['create'], ['class' => 'btn btn-primary glow invoice-create', 'title' => 'Create Manufacturer']) ?>

                                    </p>
                </div>
                <div class="card-content">
                    <div class="card-body">

                        <ul class="list-group" id="basic-list-group">

                            <?php
                            if ($count > 0) {
                                $totalproducts = 0;
                                foreach ($manufacturers as $manufacturer) {
                                    $totalproducts = $manufacturer->getCategoriesEnabled() + $manufacturer->getCategoriesDisabled();
                                    echo '<li class="list-group-item" id="'.$manufacturer->manufacturer_id.'">
                                        <div class="media">
                                            <div class="media-body row">
                                                <div class="col-xl-5 col-md-5 col-sm-12"><h5 class="mt-0">'.$manufacturer->name.'</h5></div>
                                                <div class="col-xl-2 col-md-2 col-sm-12 text-center text-primary"><span class="small-text">Total Products</span><h5 class="mt-0 text-primary">'.$totalproducts.'</h5></div>
                                                <div class="col-xl-2 col-md-2 col-sm-12 text-center"><span class="small-text text-success">Enabled Products</span><h5 class="mt-0 text-success">'.$manufacturer->getCategoriesEnabled().'</h5></div>
                                                <div class="col-xl-2 col-md-2 col-sm-12 text-center"><span class="small-text text-danger">Disabled Products</span><h5 class="mt-0 text-danger">'.$manufacturer->getCategoriesDisabled().'</h5></div>
                                                <div class="col-xl-1 col-md-1 col-sm-12 text-right">
                                                '.Html::a('<span class="bx bxs-edit-alt"></span>', ['update', 'id' => $manufacturer->manufacturer_id], ['title' => Yii::t('app', 'Edit'),]).'
                                                </div>
                                            </div>
                                        </div>
                                    </li>';
                                }
                            }
                            ?>
                            
                            
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>

</section>



<script>
    $(document).ready(function () {

  // Sortable Lists
  dragula([
      document.getElementById('basic-list-group')
  ]).on('drop', function(el, container ){
          var Lists = $(container).find('.list-group-item');
          var reOrder = [];
           $.each( Lists, function( key, value ) {
                //reOrder.push({'film_id':$(value).data('film_id'),'trailer_id' : $(value).data('trailer-id')});
                reOrder.push($(this).attr("id"));
              });
              //console.log(reOrder);
              updateOrder(reOrder);
           //_UpdateFetaureTrailerOdering(el, reOrder);
         });//;
         function updateOrder(data) {
            $.ajax({
                url:"/catalog/manufacturers/order",
                type:'post',
                data:{position:data},
                success:function(data){
                    toastr.success('Your Change Successfully Saved.');
                }
            })
        }


});
</script>
