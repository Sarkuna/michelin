<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
//use wbraganca\dynamicform\DynamicFormWidget;
use kartik\widgets\FileInput;
use dosamigos\ckeditor\CKEditor;
//use kartik\widgets\DatePicker;
//use yii\widgets\MaskedInput;
use kartik\select2\Select2;
use kartik\money\MaskMoney;

use common\models\VIPManufacturer;
use common\models\VIPCategories;
use common\models\VIPSubCategories;
use common\models\VIPStockStatus;

$pervalue = Yii::$app->VIPglobal->clientSetup()['pervalue'];
$point_value = Yii::$app->VIPglobal->clientSetup()['point_value'];

$display_price0 = $model->price / 100 * $model->mark_up;
$display_price = $model->price + $display_price0;
$display_point = $display_price / $point_value;

$total_em_display_price = $display_price + $model->handling_fee_em + $model->insurance_em + $model->shipping_em;
$total_wm_display_price = $display_price + $model->handling_fee_wm + $model->insurance_wm + $model->shipping_wm;

$handling_fee_em_point = $model->handling_fee_em / $point_value;
$handling_fee_wm_point = $model->handling_fee_wm / $point_value;

$insurance_em_point = $model->insurance_em / $point_value;
$insurance_wm_point = $model->insurance_wm / $point_value;

$shipping_em_point = $model->shipping_em / $point_value;
$shipping_wm_point = $model->shipping_wm / $point_value;

$total_cost_em_point = $display_point + $handling_fee_em_point + $insurance_em_point + $shipping_em_point;
$total_cost_wm_point = $display_point + $handling_fee_wm_point + $insurance_wm_point + $shipping_wm_point;
/* @var $this yii\web\View */
/* @var $model common\models\VIPProduct */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .kv-file-content img {width: 200px !important;}
    .thumbnail{width: 100%;}
    .input-group-addon, .input-group-btn {width: auto;}
    .file-actions {display: none;}
    .main-menu .main-menu-content {
        height: calc(100% - 6rem) !important;
        position: relative;
        display: inline-block;
    }
</style>

<div class="vipproduct-form">

    <?php $form = ActiveForm::begin(['options' => ['id' => 'vipproduct-form','class' => 'form-horizontal form-label-left','enctype' => 'multipart/form-data']]); ?>
    <div class="box-body">
    <?= $form->field($model, 'ref')->hiddenInput(['maxlength' => 50])->label(false); ?>
        
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" aria-controls="home" role="tab" aria-selected="true">
                    <i class="bx bx-home align-middle"></i>
                    <span class="align-middle">General</span>
                </a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" aria-controls="profile" role="tab" aria-selected="false">
                    <i class="bx bx-money align-middle"></i>
                    <span class="align-middle">Price</span>
                </a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link" id="about-tab" data-toggle="tab" href="#about" aria-controls="about" role="tab" aria-selected="false">
                    <i class="bx bx-slider-alt align-middle"></i>
                    <span class="align-middle">Data</span>
                </a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link" id="about-tab" data-toggle="tab" href="#links" aria-controls="links" role="tab" aria-selected="false">
                    <i class="bx bx-directions align-middle"></i>
                    <span class="align-middle">Links</span>
                </a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link" id="images-tab" data-toggle="tab" href="#images" aria-controls="images" role="tab" aria-selected="false">
                    <i class="bx bx-photo-album align-middle"></i>
                    <span class="align-middle">Images</span>
                </a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link" id="cover-tab" data-toggle="tab" href="#cover" aria-controls="cover" role="tab" aria-selected="false">
                    <i class="bx bx-image align-middle"></i>
                    <span class="align-middle">Cover</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="home" aria-labelledby="home-tab" role="tabpanel">
                <?=
            $form->field($model, 'product_code', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->textInput(array('placeholder' => ''));
            ?>  
              <?= $form->field($model, 'product_name', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
              ])->textInput(array('placeholder' => ''));  ?>

              <?= $form->field($model, 'product_description', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-10 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
              ])->widget(CKEditor::className(), [
                      'options' => ['rows' => 6],
                      'preset' => 'full'
                  ]);  ?>


            
            <?= $form->field($model, 'featured_status', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
              ])->radioList(array('N'=>'No', 'Y'=>'Yes')); ?>
            </div>
            
            <div class="tab-pane" id="profile" aria-labelledby="profile-tab" role="tabpanel">
                <?=
            $form->field($model, 'price', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12'>{label}</div>\n<div class='col-md-3 col-sm-6 col-xs-12 text-right'>{input}</div><div class='col-md-7 col-sm-6 col-xs-12'></div>\n{hint}\n{error}"
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => 2,
                    'digitsOptional' => false,
                    'radixPoint' => '.',
                    'groupSeparator' => '',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ],
            ])->textInput(array('placeholder' => ''));
            ?>
            <?=
            $form->field($model, 'mark_up', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12'>{label}</div>\n<div class='col-md-3 col-sm-6 col-xs-12'>{input}</div><div class='col-md-7 col-sm-6 col-xs-12'></div>\n{hint}\n{error}"
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => 1,
                    'digitsOptional' => false,
                    'radixPoint' => '.',
                    'groupSeparator' => '',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ],
            ])->textInput(array('placeholder' => ''));
            ?>
            <div class="form-group">
                <div class='col-md-2 col-sm-3 col-xs-12'><label class="control-label">Display Price RM</label></div>
                <div class='col-md-3 col-sm-6 col-xs-12'><input type="text" id="display_price" readonly="readonly" class="form-control text-right prices" value="<?= Yii::$app->formatter->asDecimal($display_price) ?>"></div><div class='col-md-7 col-sm-6 col-xs-12'></div>
            </div>
            <div class="form-group">
                <div class='col-md-2 col-sm-3 col-xs-12'><label class="control-label">Point (<?= $point_value ?>)</label></div>
                <div class='col-md-3 col-sm-6 col-xs-12'><input type="text" id="display_point" readonly="readonly" class="form-control text-right points" value="<?= $display_point ?>"></div><div class='col-md-7 col-sm-6 col-xs-12'></div>
            </div>
            <div class="form-group">
                <div class='col-md-2 col-sm-3 col-xs-12'></div><div class='col-md-10 col-sm-12 col-xs-12'><h4>Handling Fee</h4></div>
                <div class='col-md-2 col-sm-3 col-xs-12'></div><div class='col-md-3 col-sm-6 col-xs-12'><label class="control-label">RM</label></div><div class='col-md-7 col-sm-6 col-xs-12'><label class="control-label">Pts</label></div>
            </div>
            <?=
            $form->field($model, 'handling_fee_em', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12'><label class='control-label'>East Malaysia</label></div>\n<div class='col-md-3 col-sm-6 col-xs-12'>{input}</div><div class='col-md-3 col-sm-6 col-xs-12'><input type='text' id='handling_fee_em_point' readonly='readonly' class='form-control text-right points_em' value='$handling_fee_em_point'></div>\n{hint}\n{error}"
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => 2,
                    'digitsOptional' => false,
                    'radixPoint' => '.',
                    'groupSeparator' => '',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ],
            ])->textInput(array('placeholder' => '', 'class'=>'form-control price_em'));
            ?>
            <?=
            $form->field($model, 'handling_fee_wm', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12'><label class='control-label'>West Malaysia</label></div>\n<div class='col-md-3 col-sm-6 col-xs-12'>{input}</div><div class='col-md-3 col-sm-6 col-xs-12'><input type='text' id='handling_fee_wm_point' readonly='readonly' class='form-control text-right points_wm' value='$handling_fee_wm_point'></div>\n{hint}\n{error}"
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => 2,
                    'digitsOptional' => false,
                    'radixPoint' => '.',
                    'groupSeparator' => '',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ],
            ])->textInput(array('placeholder' => '' ,'class'=>'form-control price_wm'));
            ?>
            
            <div class="form-group">
                <div class='col-md-2 col-sm-3 col-xs-12'></div><div class='col-md-10 col-sm-12 col-xs-12'><h4>Insurance</h4></div>
                <div class='col-md-2 col-sm-3 col-xs-12'></div><div class='col-md-3 col-sm-6 col-xs-12'><label class="control-label">RM</label></div><div class='col-md-7 col-sm-6 col-xs-12'><label class="control-label">Pts</label></div>
            </div>
            <?=
            $form->field($model, 'insurance_em', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12'><label class='control-label'>East Malaysia</label></div>\n<div class='col-md-3 col-sm-6 col-xs-12'>{input}</div><div class='col-md-3 col-sm-6 col-xs-12'><input type='text' id='insurance_em_point' readonly='readonly' class='form-control text-right points_em' value='$insurance_em_point'></div>\n{hint}\n{error}"
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => 2,
                    'digitsOptional' => false,
                    'radixPoint' => '.',
                    'groupSeparator' => '',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ],
            ])->textInput(array('placeholder' => '', 'class'=>'form-control price_em'));
            ?>
            <?=
            $form->field($model, 'insurance_wm', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12'><label class='control-label'>West Malaysia</label></div>\n<div class='col-md-3 col-sm-6 col-xs-12'>{input}</div><div class='col-md-3 col-sm-6 col-xs-12'><input type='text' id='insurance_wm_point' readonly='readonly' class='form-control text-right points_wm' value='$insurance_wm_point'></div>\n{hint}\n{error}"
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => 2,
                    'digitsOptional' => false,
                    'radixPoint' => '.',
                    'groupSeparator' => '',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ],
            ])->textInput(array('placeholder' => '', 'class'=>'form-control price_wm'));
            ?>
            
            <div class="form-group">
                <div class='col-md-2 col-sm-3 col-xs-12'></div><div class='col-md-10 col-sm-12 col-xs-12'><h4>Shipping</h4></div>
                <div class='col-md-2 col-sm-3 col-xs-12'></div><div class='col-md-3 col-sm-6 col-xs-12'><label class="control-label">RM</label></div><div class='col-md-7 col-sm-6 col-xs-12'><label class="control-label">Pts</label></div>
            </div>
            <?=
            $form->field($model, 'shipping_em', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12'><label class='control-label'>East Malaysia</label></div>\n<div class='col-md-3 col-sm-6 col-xs-12'>{input}</div><div class='col-md-3 col-sm-6 col-xs-12'><input type='text' id='shipping_em_point' readonly='readonly' class='form-control text-right points_em' value='$shipping_em_point'></div>\n{hint}\n{error}"
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => 2,
                    'digitsOptional' => false,
                    'radixPoint' => '.',
                    'groupSeparator' => '',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ],
            ])->textInput(array('placeholder' => '', 'class'=>'form-control price_em'));
            ?>
            <?=
            $form->field($model, 'shipping_wm', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12'><label class='control-label'>West Malaysia</label></div>\n<div class='col-md-3 col-sm-6 col-xs-12'>{input}</div><div class='col-md-3 col-sm-6 col-xs-12'><input type='text' id='shipping_wm_point' readonly='readonly' class='form-control text-right points_wm' value='$shipping_wm_point'></div>\n{hint}\n{error}"
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'clientOptions' => [
                    'alias' => 'decimal',
                    'digits' => 2,
                    'digitsOptional' => false,
                    'radixPoint' => '.',
                    'groupSeparator' => '',
                    'autoGroup' => true,
                    'removeMaskOnSubmit' => true,
                ],
            ])->textInput(array('placeholder' => '', 'class'=>'form-control price_wm'));
            ?>
            
            <div class="form-group">
                <div class='col-md-2 col-sm-3 col-xs-12'></div><div class='col-md-10 col-sm-12 col-xs-12'><h4>Total Cost</h4></div>
                <div class='col-md-2 col-sm-3 col-xs-12'></div><div class='col-md-3 col-sm-6 col-xs-12'><label class="control-label">RM</label></div><div class='col-md-7 col-sm-6 col-xs-12'><label class="control-label">Pts</label></div>
            </div>
            <div class="form-group">
                <div class='col-md-2 col-sm-2 col-xs-12'><label class='control-label'>East Malaysia</label></div>
                <div class='col-md-3 col-sm-3 col-xs-12'><input type='text' id='total_cost_em_price' readonly='readonly' class='form-control text-right' value='<?= Yii::$app->formatter->asDecimal($total_em_display_price) ?>'></div>
                <div class='col-md-3 col-sm-3 col-xs-12'><input type='text' id='total_cost_em_point' readonly='readonly' class='form-control text-right' value='<?= $total_cost_em_point ?>'></div>
            </div>
            <div class="form-group">
                <div class='col-md-2 col-sm-2 col-xs-12'><label class='control-label'>West Malaysia</label></div>
                <div class='col-md-3 col-sm-3 col-xs-12'><input type='text' id='total_cost_wm_price' readonly='readonly' class='form-control text-right' value='<?= Yii::$app->formatter->asDecimal($total_wm_display_price) ?>'></div>
                <div class='col-md-3 col-sm-3 col-xs-12'><input type='text' id='total_cost_wm_point' readonly='readonly' class='form-control text-right' value='<?= $total_cost_wm_point ?>'></div>
            </div>
            </div>
            
            <div class="tab-pane" id="about" aria-labelledby="about-tab" role="tabpanel">
                <?=
            $form->field($model, 'quantity', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9',
                'clientOptions' => ['repeat' => 4, 'greedy' => false],
            ]);
            ?>
            	
            <?=
            $form->field($model, 'variant', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->textInput(array('placeholder' => ''));
            ?>    
            <?=
            $form->field($model, 'minimum', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9',
                'clientOptions' => ['repeat' => 3, 'greedy' => false],
            ]);
            ?>
            
            <?php
            echo $form->field($model, 'item_type', [
                        'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
                    ])
                    ->dropDownList(
                            ['item' => 'Item', 'voucher' => 'Voucher'], ['prompt' => '--', 'id' => 'Item Type']
            );
            ?>
            <?php
            echo $form->field($model, 'type', [
                        'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
                    ])
                    ->dropDownList(
                            ['michelin' => 'Michelin', 'bb' => 'Business Boosters'], ['prompt' => '--', 'id' => 'Item Type']
            );
            ?>

            <?=
            $form->field($model, 'remarks', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->textarea(['rows' => '6']);
            ?> 
            <?php
            $statusData = ['E' => 'Enabled', 'D' => 'Disabled'];
//echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
            echo $form->field($model, 'status', [
                        'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
                    ])
                    ->dropDownList(
                            $statusData, ['prompt' => '--', 'id' => 'status']
            );
            ?> 
            </div>
            
            <div class="tab-pane" id="links" aria-labelledby="links-tab" role="tabpanel">
                <?php
            $manufacturer = VIPManufacturer::find()->all();
            $manufacturerData = ArrayHelper::map($manufacturer, 'manufacturer_id', 'name');
            ?>
            <?=
            $form->field($model, 'manufacturer_id', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
            ])->widget(Select2::classname(), [
                'data' => $manufacturerData,
                'options' => ['placeholder' => 'Select...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?> 

            <?php
            $categories = VIPCategories::find()->all();
            $categoriesData = ArrayHelper::map($categories, 'vip_categories_id', 'name');
            //echo $form->field($model, 'country')->dropDownList($listData, ['prompt' => 'Select...']);
            echo $form->field($model, 'category_id', [
                        'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div><div class='col-md-7'>{input}</div>\n{hint}\n{error}"
                    ])
                    ->dropDownList(
                    $categoriesData, [
                        'prompt' => '--',
                        'id' => 'categories_id',
                     ]
            );
            ?>
            

            <?=
            $form->field($model, 'links', [
                'template' => "<div class='col-md-2 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-7 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
            ])->textInput(array('placeholder' => ''));
            ?>
            </div>
            
            <div class="tab-pane" id="images" aria-labelledby="images-tab" role="tabpanel">
                <?=
                    FileInput::widget([
                        'name' => 'upload_ajax[]',
                        'options' => ['multiple' => true, 'accept' => 'image/*'], //'accept' => 'image/*' หากต้องเฉพาะ image
                        'pluginOptions' => [
                            'showPreview' => true,
                            'overwriteInitial' => false,
                            'initialPreviewShowDelete' => true,
                            'allowedFileExtensions' => ['jpg','png','jpeg','bmp'],
                            'initialPreview' => $initialPreview,
                            //'initialPreview'=> $initialPreview[$model->ref],
                            //'initialPreviewConfig'=>$model->initialPreview($model->docs,'docs','config'),
                            'initialPreviewConfig' => $initialPreviewConfig,
                            'uploadUrl' => Url::to(['/catalog/products/uploadajax']),
                            'uploadExtraData' => [
                                'ref' => $model->ref,
                            ],
                            'maxFileCount' => 10
                        ]
                    ]);
                
                    ?>
            </div>
            
            <div class="tab-pane" id="cover" aria-labelledby="cover-tab" role="tabpanel">
                <?php
                    echo $form->field($model, 'main_image1')->widget(FileInput::classname(), [
                        'options' => ['accept' => 'image/*'],
                        'pluginOptions' => [
                            'allowedFileExtensions' => ['jpg','png','jpeg','bmp'],
                            'initialPreview' => [
                                //Html::img("/images/moon.jpg", ['class'=>'file-preview-image', 'alt'=>'The Moon', 'title'=>'The Moon'])
                                $model->main_image ? Html::img('/upload/product_cover/thumbnail/' . $model->main_image, ['class' => 'thumbnail', 'alt' => 'Logo', 'title' => 'Logo']) : null, // checks the models to display the preview
                            ],
                        ]
                    ])->label(false);
                    ?>
                
            </div>
            
        </div>
    
    
    </div>
    <div class="box-footer">
        <div class="form-group">
        <div class="col-lg-offset-2 col-lg-10 col-md-12 col-sm-6 col-xs-12">
            <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary mr-1 mb-1' : 'btn btn-primary mr-1 mb-1']) ?>
            <?= \yii\helpers\Html::a( '<i class="fa fa-reply"></i>', Yii::$app->request->referrer, ['class' => 'btn btn-default']); ?>
        </div>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<script>
    
    $.ajaxSetup({
        data: <?= \yii\helpers\Json::encode([
            \Yii::$app->request->csrfParam => \Yii::$app->request->csrfToken,
        ]) ?>
    });
</script>
<?php

    $script = <<<EOD
            
            function getPointem()
            {
                var sum_em = 0;
                var display_point = 0;
                $('.points_em').each(function() {
                    sum_em += Number($(this).val());
                });
                display_point = parseInt($("#display_point").val()) + parseInt(sum_em);
                $("#total_cost_em_point").val(display_point);
            
                var price_sum_em = 0;
                var display_price = 0;
                $('.price_em').each(function() {
                    price_sum_em += Number($(this).val());
                });
                display_price = parseInt($("#display_price").val().replace(",", "")) + parseInt(price_sum_em);
                $("#total_cost_em_price").val(display_price.toFixed(2));
            }
            
            function getPointwm()
            {
                var sum_wm = 0;
                var display_point = 0;
                $('.points_wm').each(function() {
                    sum_wm += Number($(this).val());
                });
                display_point = parseInt($("#display_point").val()) + parseInt(sum_wm);
                $("#total_cost_wm_point").val(display_point);
            
                var price_sum_wm = 0;
                var display_price = 0;
                $('.price_wm').each(function() {
                    price_sum_wm += Number($(this).val());
                });
                display_price = parseInt($("#display_price").val().replace(",", "")) + parseInt(price_sum_wm);
                $("#total_cost_wm_price").val(display_price.toFixed(2));
            }

            
            var getPriceem = function() {
                var sum_em = 0;
                $('.points_em').each(function() {
                    sum_em += Number($(this).val());
                });
                
            };
            
            $('#vipproduct-price').change(function() {
                var mark_up = $("#vipproduct-mark_up").val();
                var productprice = $(this).val() / 100 * mark_up;
                var display_price = parseFloat($(this).val(), 10) + parseInt(productprice, 10);
                var display_point = parseFloat(display_price) / $point_value;

                $("#display_price").val(display_price.toFixed(2));            
                $("#display_point").val(display_point);
                getPointem();
                getPointwm();
            });
            
            $('#vipproduct-mark_up').change(function() {

                var mark_up = $("#vipproduct-mark_up").val();
                var productprice = $("#vipproduct-price").val() / 100 * mark_up; 
            
                var display_price = parseFloat($("#vipproduct-price").val()) + parseFloat(productprice);
                var display_point = parseFloat(display_price) / $point_value;

                $("#display_price").val(display_price.toFixed(2));
                $("#display_point").val(display_point);
                getPointem();
                getPointwm();
            });
            
            $('#vipproduct-handling_fee_em').change(function() {
                var display_point = $(this).val() / $point_value;
                display_point = Math.round(display_point.toFixed(2));
                $("#handling_fee_em_point").val(display_point);
                getPointem();                
            });
            
            $('#vipproduct-handling_fee_wm').change(function() {
                var display_point = $(this).val() / $point_value;
                display_point = Math.round(display_point.toFixed(2));
                $("#handling_fee_wm_point").val(display_point);
                getPointwm();
            });
            
            $('#vipproduct-insurance_em').change(function() {
                var display_point = $(this).val() / $point_value;
                display_point = Math.round(display_point.toFixed(2));
                $("#insurance_em_point").val(display_point);
                getPointem();
            });
            
            $('#vipproduct-insurance_wm').change(function() {
                var display_point = $(this).val() / $point_value;
                display_point = Math.round(display_point.toFixed(2));
                $("#insurance_wm_point").val(display_point);
                getPointwm();
            });
            
            $('#vipproduct-shipping_em').change(function() {
                var display_point = $(this).val() / $point_value;
                display_point = Math.round(display_point.toFixed(2));
                $("#shipping_em_point").val(display_point);
                getPointem();
            });
            
            $('#vipproduct-shipping_wm').change(function() {
                var display_point = $(this).val() / $point_value;
                display_point = Math.round(display_point.toFixed(2));
                $("#shipping_wm_point").val(display_point);
                getPointwm();
            });
            
            

EOD;
$this->registerJs($script);