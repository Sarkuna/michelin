<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
//use kartik\select2\Select2;


?>


<?php DynamicFormWidget::begin([
    'widgetContainer' => "dynamicform_inner$indexHouse",
    'widgetBody' => ".container-rooms$indexHouse",
    'widgetItem' => ".room-item$indexHouse",
    'limit' => 20,
    'min' => 0,
    'insertButton' => ".add-room$indexHouse",
    'deleteButton' => ".remove-room$indexHouse",
    'model' => $modelsOptionvalue[0],
    'formId' => 'vipproduct-form',
    'formFields' => [
        'product_option_value_id',
        'option_value_id',
        'quantity','subtract','price_prefix','price','points_prefix','points'
    ],
]); ?>
<table class="table table-bordered">
    <thead>
        <tr>
            <th width="18%">Option Value</th>
            <th width="15%">Quantity</th>
            <th width="12%">Subtract Stock</th>
            <th width="10%">Price</th>
            <th width="10%">Points</th>
            <th class="text-center" width="5%">
                <button type="button" class="add-room<?= $indexHouse ?> btn btn-success btn-xs"><span class="glyphicon glyphicon-plus"></span></button>
            </th>
        </tr>
    </thead>
    <tbody class="container-rooms<?= $indexHouse ?>">
    <?php 
    
    foreach ($modelsOptionvalue as $ix => $modelLoads): ?>
        <tr class="room-item<?= $indexHouse ?>">
            <td class="vcenter">
                <?php
                    // necessary for update action.
                    if (! $modelLoads->isNewRecord) {
                        echo Html::activeHiddenInput($modelLoads, "[{$indexHouse}][{$ix}]product_option_value_id");
                    }
                   $optcolour = common\models\VIPOptionValueDescription::find()->where(['option_id' => $optionID])->all();
                   $optcolourData = ArrayHelper::map($optcolour, 'option_value_id', 'name');
                   echo $form->field($modelLoads, "[{$indexHouse}][{$ix}]option_value_id")->label(false)->dropDownList($optcolourData, ['prompt' => 'Select...']); 
                ?>
            </td>
            <td class="vcenter">
                <?= $form->field($modelLoads, "[{$indexHouse}][{$ix}]quantity")->label(false)->textInput(['maxlength' => true]) ?>
            </td>
            <td class="vcenter">
                <?= $form->field($modelLoads, "[{$indexHouse}][{$ix}]subtract")->label(false)->dropDownList(['1' => 'Yes', '0' => 'No'], ['prompt' => 'Select...']) ?>
            </td>
            <td class="vcenter">
                <?= $form->field($modelLoads, "[{$indexHouse}][{$ix}]price_prefix")->label(false)->dropDownList(['+' => '+', '-' => '-'], ['prompt' => 'Select...']) ?>
                <?= $form->field($modelLoads, "[{$indexHouse}][{$ix}]price")->label(false)->textInput(['maxlength' => true]) ?>
            </td>
            <td class="vcenter">
                <?= $form->field($modelLoads, "[{$indexHouse}][{$ix}]points_prefix")->label(false)->dropDownList(['+' => '+', '-' => '-'], ['prompt' => 'Select...']) ?>
                <?= $form->field($modelLoads, "[{$indexHouse}][{$ix}]points")->label(false)->textInput(['maxlength' => true]) ?>
            </td>
            <td class="text-center vcenter" style="width: 90px;">
                <button type="button" class="remove-room<?= $indexHouse ?> btn btn-danger btn-xs"><span class="glyphicon glyphicon-minus"></span></button>
            </td>
        </tr>
     <?php endforeach; ?>
    </tbody>
</table>
<?php DynamicFormWidget::end(); ?>