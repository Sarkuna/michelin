<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPProduct */

$this->title = 'Add Product';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><?= Html::encode($this->title) ?></h4>
                </div>
                <div class="card-content">
                    <div class="card-body product-create">
                        <?=
                        $this->render('_form', [
                            'model' => $model,
                            'initialPreview' => [],
                            'initialPreviewConfig' => [],
                        ])
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


