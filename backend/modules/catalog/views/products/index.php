<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;

$enabled = common\models\VIPProduct::find()->where(['status' => 'E'])->count();
$disabled = common\models\VIPProduct::find()->where(['status' => 'D'])->count();
?>

<div class="content-body">
    <!-- Scroll - horizontal and vertical table -->
    <section id="horizontal-vertical">
        <div class="row">
            <div class="col-xl-4 col-md-4 col-sm-4">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="badge-circle badge-circle-lg badge-circle-light-info mx-auto my-1">
                                <i class="bx bx-edit-alt font-medium-5"></i>
                            </div>
                            <p class="text-muted mb-0 line-ellipsis">Total No of Products</p>
                            <h2 class="mb-0"><?= $enabled + $disabled ?></h2>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-xl-4 col-md-4 col-sm-4">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto my-1">
                                <i class="bx bx-show-alt font-medium-5"></i>
                            </div>
                            <p class="text-muted mb-0 line-ellipsis">Enabled</p>
                            <h2 class="mb-0"><?= $enabled ?></h2>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-xl-4 col-md-4 col-sm-4">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto my-1">
                                <i class="bx bx-hide font-medium-5"></i>
                            </div>
                            <p class="text-muted mb-0 line-ellipsis">Disabled</p>
                            <h2 class="mb-0"><?= $disabled ?></h2>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title float-left"><?= Html::encode($this->title) ?></h4>
                        <p class="float-right">
                                        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-primary glow invoice-create', 'title' => 'Create Product']) ?>
        <?= Html::a('Download', ['/reports/report/download-product'], ['class' => 'btn btn-info glow invoice-create', 'title' => 'Download']) ?>
        
                                    </p>
  
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">

                                <div class="table-responsive">
                                    
                                <?= GridView::widget([
        'tableOptions' => ['class' => 'table zero-configuration'],
                                'dataProvider' => $dataProvider,
                                'layout' => "{items}",
                                //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //['class' => 'yii\grid\SerialColumn','headerOptions' => ['width' => '10'],],
            //['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],
            [
                    'attribute' => 'image',
                    'label' => 'Image',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $path = Yii::$app->basePath .'/web/upload/product_cover/thumbnail/';
                        if (file_exists($path.$model->main_image)) {
                            $url = $model->main_image ? Html::img('/upload/product_cover/thumbnail/' . $model->main_image, ['alt' => 'myImage', 'width' => '70'], ['class' => 'thumbnail', 'alt' => 'Logo', 'title' => 'Logo']) : '<i class="fa fa-picture-o fa-5x" aria-hidden="true"></i>';
                        }else {
                            $url = Html::img('/upload/product_cover/thumbnail/no_image.png', ['alt' => 'myImage', 'width' => '70'], ['class' => 'thumbnail', 'alt' => 'Logo', 'title' => 'Logo']);
                        }
                        
                        //$url = \Yii::getAlias('@backend/web/upload/product_cover/') . $model->main_image;
                        //return Html::img($url, ['alt' => 'myImage', 'width' => '70', 'height' => '50']);
                        return $url;
                    }
                        ],
                        'product_name',
                        'product_code',
                        //'price',
                        [
                            'attribute' => 'price',
                            'label' => 'Total Cost EM',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->VIPglobal->getTotalcostem($model->product_id);
                            },
                        ],
                        [
                            'attribute' => 'price',
                            'label' => 'Total Cost WM',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '180'],
                            'contentOptions' => ['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->VIPglobal->getTotalcostwm($model->product_id);
                            },
                        ],             
                        'quantity',
                        [
                            'attribute' => 'status',
                            'label' => 'Status',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->getStatus();
                            },
                            //'options' => ['width' => '100'],
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],
                            'filter' => ["E" => "Enabled", "D" => "Disabled "],
                        ], 
                       

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}', // {update}{delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        return (Html::a('<i class="bx bx-show-alt"></i>', $url, ['title' => Yii::t('app', 'View'), 'class' => 'invoice-action-view mr-1']));
                    },
                    'update' => function ($url, $model) {
                        return (Html::a('<i class="bx bxs-edit-alt"></i>', $url, ['title' => Yii::t('app', 'Edit'),]));
                    }, 
                ],
            ],             
        ],
    ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

