<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPProduct */

$this->title = $model->product_code.'-' . $model->product_name;
$this->params['breadcrumbs'][] = ['label' => 'Product', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->product_id, 'url' => ['view', 'id' => $model->product_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
  
<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><?= Html::encode($this->title) ?></h4>
                </div>
                <div class="card-content">
                    <div class="card-body vipproduct-update">
                        <?=
        $this->render('_form', [
            'model' => $model,
            'initialPreview' => $initialPreview,
            'initialPreviewConfig' => $initialPreviewConfig,
        ])
        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

