<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Upload Excel Summery';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Upload Excel'), 'url' => ['uploadexcel']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .list-group-item {
    border: none;
}
</style>
<section class="panel">
    <header class="panel-heading sm" data-color="theme-inverse">
        <h2><?= Html::encode($this->title) ?></h2>

    </header>
    <div class="panel-body">
        <div class="user-upload-csv gradient-widget">
            <div class="row">
                <div class="col-lg-10">                    
                    <ul class="list-group">
                        <?php print_r($msg) ?>
                    </ul>
                </div>
                <div class="col-lg-2"></div>
            </div>
        </div>
    </div>
</section>

