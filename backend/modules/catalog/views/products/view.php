<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\VIPProduct */

$this->title = $model->product_name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$pervalue = Yii::$app->VIPglobal->clientSetup()['pervalue'];
$point_value = Yii::$app->VIPglobal->clientSetup()['point_value'];



$display_price0 = $model->price / 100 * $model->mark_up;
$display_price = $model->price + $display_price0;
$display_point = $display_price / $point_value;

$total_em_display_price = $display_price + $model->handling_fee_em + $model->insurance_em + $model->shipping_em;
$total_wm_display_price = $display_price + $model->handling_fee_wm + $model->insurance_wm + $model->shipping_wm;

$handling_fee_em_point = $model->handling_fee_em / $point_value;
$handling_fee_wm_point = $model->handling_fee_wm / $point_value;

$insurance_em_point = $model->insurance_em / $point_value;
$insurance_wm_point = $model->insurance_wm / $point_value;

$shipping_em_point = $model->shipping_em / $point_value;
$shipping_wm_point = $model->shipping_wm / $point_value;

$total_cost_em_point = $display_point + $handling_fee_em_point + $insurance_em_point + $shipping_em_point;
$total_cost_wm_point = $display_point + $handling_fee_wm_point + $insurance_wm_point + $shipping_wm_point;

?>
<style>
    div#general img {max-width: 100%;}
    img.file-preview-image {
        padding: 5px;
        width: 25%;
        border: solid 1px #ccc;
        margin: 5px;
    }
    
</style>
<div class="content-body pages-index">
    <!-- Scroll - horizontal and vertical table -->
    <section id="horizontal-vertical">

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title float-left"><?= Html::encode($this->title) ?></h4>
                        <p class="float-right">
                            <?= Html::a('Edit Product', ['update', 'id' => $model->product_id], ['class' => 'btn btn-primary glow invoice-create', 'title' => 'Edit']) ?>

                        </p>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" aria-controls="home" role="tab" aria-selected="true">
                                        <i class="bx bx-home align-middle"></i>
                                        <span class="align-middle">General</span>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" aria-controls="profile" role="tab" aria-selected="false">
                                        <i class="bx bx-money align-middle"></i>
                                        <span class="align-middle">Price</span>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="about-tab" data-toggle="tab" href="#about" aria-controls="about" role="tab" aria-selected="false">
                                        <i class="bx bx-slider-alt align-middle"></i>
                                        <span class="align-middle">Data</span>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="about-tab" data-toggle="tab" href="#links" aria-controls="links" role="tab" aria-selected="false">
                                        <i class="bx bx-directions align-middle"></i>
                                        <span class="align-middle">Links</span>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="images-tab" data-toggle="tab" href="#images" aria-controls="images" role="tab" aria-selected="false">
                                        <i class="bx bx-photo-album align-middle"></i>
                                        <span class="align-middle">Images</span>
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="cover-tab" data-toggle="tab" href="#cover" aria-controls="cover" role="tab" aria-selected="false">
                                        <i class="bx bx-image align-middle"></i>
                                        <span class="align-middle">Cover</span>
                                    </a>
                                </li>
                            </ul>
                            
                            <div class="tab-content">
                                <div class="tab-pane active" id="home" aria-labelledby="home-tab" role="tabpanel">
                                        <table class="table table-borderless">
                                            <tbody>
                                                <tr>
                                                    <td><?= $model->getAttributeLabel('product_code') ?>:</td>
                                                    <td class="users-view-username"><?= $model->product_code ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= $model->getAttributeLabel('product_name') ?>:</td>
                                                    <td class="users-view-name"><?= $model->product_name ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= $model->getAttributeLabel('product_description') ?>:</td>
                                                    <td class="users-view-email"><?= $model->product_description == '' ? 'N\A' : $model->product_description ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= $model->getAttributeLabel('featured_status') ?>:</td>
                                                    <td><?= $model->featured_status == 'Y' ? 'Yes' : 'No' ?></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                </div>
                                
                                <div class="tab-pane" id="profile" aria-labelledby="profile-tab" role="tabpanel">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td><?= $model->getAttributeLabel('price') ?>:</td>
                                                <td class="users-view-username"><?= $model->price ?></td>
                                            </tr>
                                            <tr>
                                                <td><?= $model->getAttributeLabel('mark_up') ?>:</td>
                                                <td class="users-view-name"><?= $model->mark_up ?></td>
                                            </tr>
                                            <tr>
                                                <td>Display Price RM:</td>
                                                <td class="users-view-email"><?= Yii::$app->formatter->asDecimal($display_price) ?></td>
                                            </tr>
                                            <tr>
                                                <td>Point (<?= $point_value ?>):</td>
                                                <td><?= $display_point ?> pts</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                    <table class="table table-bordered">

                                        <tbody>
                                            <tr>
                                                <td rowspan="2"></td>
                                                <td colspan="2" class="text-center">Handling Fee</td>
                                                <td colspan="2" class="text-center">Insurance</td>
                                                <td colspan="2" class="text-center">Shipping</td>
                                                <td colspan="2" class="text-center">Total Cost</td>
                                            </tr>
                                            <tr>
                                                <td>RM</td>
                                                <td>Pts</td>
                                                <td>RM</td>
                                                <td>Pts</td>
                                                <td>RM</td>
                                                <td>Pts</td>
                                                <td>RM</td>
                                                <td>Pts</td>
                                            </tr>
                                            <tr>
                                                <td>East Malaysia</td>
                                                <td><?= Yii::$app->formatter->asDecimal($model->handling_fee_em) ?></td>
                                                <td><?= $handling_fee_em_point ?></td>
                                                <td><?= Yii::$app->formatter->asDecimal($model->insurance_em) ?></td>
                                                <td><?= $insurance_em_point ?></td>
                                                <td><?= Yii::$app->formatter->asDecimal($model->shipping_em) ?></td>
                                                <td><?= $shipping_em_point ?></td>
                                                <td><?= Yii::$app->formatter->asDecimal($total_em_display_price) ?></td>
                                                <td><?= $total_cost_em_point ?></td>
                                            </tr>
                                            <tr>
                                                <td>West Malaysia</td>
                                                <td><?= Yii::$app->formatter->asDecimal($model->handling_fee_wm) ?></td>
                                                <td><?= $handling_fee_wm_point ?></td>
                                                <td><?= Yii::$app->formatter->asDecimal($model->insurance_wm) ?></td>
                                                <td><?= $insurance_wm_point ?></td>
                                                <td><?= Yii::$app->formatter->asDecimal($model->shipping_wm) ?></td>
                                                <td><?= $shipping_wm_point ?></td>
                                                <td><?= Yii::$app->formatter->asDecimal($total_wm_display_price) ?></td>
                                                <td><?= $total_cost_wm_point ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                                <div class="tab-pane" id="about" aria-labelledby="about-tab" role="tabpanel">
                                    <table class="table table-borderless">
                                            <tbody>
                                                <tr>
                                                    <td><?= $model->getAttributeLabel('quantity') ?>:</td>
                                                    <td class="users-view-username"><?= $model->quantity ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= $model->getAttributeLabel('minimum') ?>:</td>
                                                    <td class="users-view-email"><?= $model->minimum ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= $model->getAttributeLabel('variant') ?>:</td>
                                                    <td class="users-view-name"><?= $model->variant ?></td>
                                                </tr>
                                                

                                                <tr>
                                                    <td><?= $model->getAttributeLabel('type') ?>:</td>
                                                    <td><?= $model->type == 'bb' ? 'BB' : 'Michelin' ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= $model->getAttributeLabel('remarks') ?>:</td>
                                                    <td class="users-view-email"><?= $model->remarks ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= $model->getAttributeLabel('status') ?>:</td>
                                                    <td><?= $model->status == 'E' ? 'Enabled' : 'Disabled' ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                </div>
                                
                                <div class="tab-pane" id="links" aria-labelledby="links-tab" role="tabpanel">
                                    
                                    <table class="table table-borderless">
                                            <tbody>
                                                <tr>
                                                    <td><?= $model->getAttributeLabel('manufacturer_id') ?>:</td>
                                                    <td class="users-view-username"><?= $model->manufacturer->name ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= $model->getAttributeLabel('category_id') ?>:</td>
                                                    <td class="users-view-name"><?= $model->categories->name ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= $model->getAttributeLabel('links') ?>:</td>
                                                    <td class="users-view-email"><?= $model->links ?></td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                </div>
                                <div class="tab-pane" id="images" aria-labelledby="images-tab" role="tabpanel">
                                    <?php
                                    foreach ($initialPreview as $image) {
                                        echo $image;
                                    }
                                    ?>
                                </div>
                                
                                <div class="tab-pane" id="cover" aria-labelledby="cover-tab" role="tabpanel">
                                    <?php
                                    $path = Yii::$app->basePath .'/web/upload/product_cover/thumbnail/';
                                    if (file_exists($path.$model->main_image)) {
                                        echo $model->main_image ? Html::img('/upload/product_cover/thumbnail/' . $model->main_image, ['alt' => 'myImage', 'width' => '50%'], ['class' => 'thumbnail', 'alt' => 'Logo', 'title' => 'Logo']) : '<i class="fa fa-picture-o fa-5x" aria-hidden="true"></i>';
                                    }else {
                                        echo Html::img('/upload/product_cover/thumbnail/no_image.png', ['alt' => 'myImage', 'width' => '70'], ['class' => 'thumbnail', 'alt' => 'Logo', 'title' => 'Logo']);
                                    }
                                    ?>
                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>