<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VIPManufacturer */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .form-group {
    margin-bottom: 1rem;
    /* clear: both; */
    display: contents;
    line-height: 50px;
}
</style>

<?php $form = ActiveForm::begin(['options' => ['class' => 'form form-horizontal']]);?>
<div class="form-body">
                                               
    <div class="form-body">
        <div class="row">

            <?=
            $form->field($model, 'first_name', [
                'template' => "<div class='col-md-4'>{label}</div>\n<div class='col-md-8'>{input}\n{hint}\n{error}</div>",
            ])->textInput(array('placeholder' => ''));
            ?>

            <?=
            $form->field($model, 'last_name', [
                'template' => "<div class='col-md-4'>{label}</div>\n<div class='col-md-8'>{input}\n{hint}\n{error}</div>",
            ])->textInput(array('placeholder' => ''));
            ?>

            
            <div class="col-sm-12 d-flex justify-content-end">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary mr-1 mb-1']) ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>



