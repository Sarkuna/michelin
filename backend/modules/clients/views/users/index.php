<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Admin User List';
$this->params['breadcrumbs'][] = $this->title;
?>





<div class="content-body">
    <!-- Scroll - horizontal and vertical table -->
    <section id="horizontal-vertical">
        
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title float-left"><?= Html::encode($this->title) ?></h4>
                        <p class="float-right">
                                        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-primary glow invoice-create', 'title' => 'Add New']) ?>

                                    </p>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">

                                <div class="table-responsive">
                                    
                                <?= GridView::widget([
        'tableOptions' => ['class' => 'table zero-configuration'],
                                'dataProvider' => $dataProvider,
                                'layout' => "{items}",
                                //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //['class' => 'yii\grid\SerialColumn','headerOptions' => ['width' => '10'],],
            //['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],
            [
                    'attribute' => 'email',
                    'label' => 'Email',
                    'format' => 'html',
                    //'headerOptions' => ['width' => '180'],
                    'value' => function ($model) {
                        return $model->email;
                    },
                ],
                    
                [
                    'attribute' => 'fname',
                    'label' => 'Name',
                    'format' => 'html',
                    //'headerOptions' => ['width' => '180'],
                    'value' => function ($model) {
                        return $model->adminUserProfile->first_name.' '.$model->adminUserProfile->last_name;
                    },
                ],    
                /*[
                    'attribute' => 'status',
                    'label' => 'Status',
                    'format' => 'html',
                    'headerOptions' => ['width' => '180'],
                    'value' => function ($model) {
                        return $model->getStatustext();
                    },
                ],*/
                [
                    'attribute' => 'created_datetime',
                    'label' => 'Date Added',
                    //'format' => 'html',
                    'format' => ['date', 'php:d/m/Y'],
                    //'headerOptions' => ['width' => '180'],
                    'value' => function ($model) {
                return $model->created_datetime;
            },
                ], 

                            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}', //{view} {delete}
                'buttons' => [
                    'update' => function ($url, $model) {
                        $url = '/clients/users/update?id='.$model->adminUserProfile->profile_ID;
                        return (Html::a('<span class="bx bxs-edit-alt"></span>', $url, ['title' => Yii::t('app', 'Edit'),]));
                    },
                    'delete' => function ($url, $model) {
                        return (Html::a('<span class="bx bx-trash-alt"></span>', $url, ['title' => Yii::t('app', 'Delete'), 'data' => ['confirm' => 'Are you sure you want to delete this item?', 'method' => 'post'],]));
                    }        
                            /* ,
                          'view' => function ($url, $model) {
                          return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                          },
                           */
                    ],
                //'visible' => $visible,
                ],
        ],
    ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>



