<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPManufacturer */

$this->title = 'Edit User';
$this->params['breadcrumbs'][] = ['label' => 'User List', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->manufacturer_id]];
$this->params['breadcrumbs'][] = 'Edit';
?>


<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-8 col-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><?= Html::encode($this->title) ?></h4>
                </div>
                <div class="card-content">
                    <div class="card-body user-update">
                        <?=
            $this->render('_form_update', [
                'model' => $model,
            ])
            ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
