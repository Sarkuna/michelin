<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


$session = Yii::$app->session;
//$this->title = 'Report';
//$this->params['breadcrumbs'][] = $this->title;
//$this->title = $model->full_name;
//$this->params['breadcrumbs'][] = ['label' => 'Manage Customers', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $this->title;


?>

        <?php $form = ActiveForm::begin(['id' => 'report-form']); ?>
<div class="row">
<div class="col-xl-6 col-6">    
    <?=
            $form->field($modelForm, 'date_range', [
                'template' => "<fieldset class='form-group position-relative has-icon-left'>{input}\n{hint}\n{error}<div class='form-control-position'><i class='bx bx-calendar-check'></i></div></fieldset>",

            ])->textInput(array('placeholder' => 'Select Date', 'class' => 'form-control dateranges'));
            ?>
    
</div>
<div class="col-xl-6 col-6">
    <div class="form-group">
            <?= Html::submitButton('Search...', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Reset', ['customer-form'], ['class'=>'btn btn-info']) ?>
            </div>
</div>
</div>
        <?php ActiveForm::end(); ?>
