<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders Summary';
$this->params['breadcrumbs'][] = ['label' => 'Reports'];
//$this->params['breadcrumbs'][] = $this->title;
?>

<section id="table-transactions">
    <div class="card">
        <div class="card-header">
            <!-- head -->
            <h5 class="card-title">Report Form</h5>
        </div>

        <div class="card-content">
            <div class="card-body">
                <?=
    $this->render('form_Orders', ['modelForm' => $modelForm]);
    ?>
                
            </div>
        </div>        
    </div>
</section>


<section id="table-transactions">
    <div class="card">
        <div class="card-header">
            <!-- head -->
            <h5 class="card-title"><?= $this->title ?></h5>
        </div>

        <div class="card-content">
            <div class="card-body table-responsive">

                <div class="painter-profile-index">
 
                <?=
                GridView::widget([
                    'tableOptions' => ['class' => 'table table-striped dataex-html5-selectors2'],
            'dataProvider' => $dataProvider,
            'layout'=>"{items}",
                    //'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Company Name',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '160'],
                            'value' => function ($model) {
                                return $model->order->shipping_company;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Code',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->order->customer_group_id;
                            },
                        ],
                        [
                            'attribute' => 'order_no',
                            'label' => 'Order No.',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->order->invoice_prefix;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Order Date',
                            'format' => 'html',
                            'value' => function ($model) {
                                return date("d-m-Y", strtotime($model->order->created_datetime));
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Invoice Number',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->order->bb_invoice_no;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Product Code',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->model;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Product Description',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->name;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Qty',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->quantity;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Unit/pts',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->point;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Shipping/pts',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->shipping_point;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Total Pts',
                            'format' => 'html',
                            'value' => function ($model) {
                                $total_pro = $model->point + $model->shipping_point;
                                $totl_f = $model->quantity * $total_pro;
                                return $totl_f;
                            },
                        ],
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Total RM',
                            'format' => 'html',
                            'value' => function ($model) {
                                $total_pro = $model->point + $model->shipping_point;
                                $totl_f = $model->quantity * $total_pro;
                                return $totl_f * Yii::$app->VIPglobal->clientSetupPoint();
                            },
                        ],            
                        [
                            'attribute' => 'clients_ref_no1',
                            'label' => 'Status',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->order->orderStatus->name;
                            },
                        ],
                    ],
                ]);
                ?>
                </div>
            </div>
        </div>        
    </div>
</section>

<?php
    $script = <<<EOD
       $('#DataTables_Table_0').DataTable({

            "bProcessing": true,
            "sAutoWidth": false,
            "bDestroy":true,
            "sPaginationType": "bootstrap", // full_numbers
            "iDisplayStart ": 10,
            "iDisplayLength": 10,
            "bPaginate": false, //hide pagination
            "bFilter": false, //hide Search bar
            "bInfo": false, // hide showing entries
            "ordering": false,
            "dom": 'Bfrtip',
            "buttons": [
                'excel'
              ]
    } );

EOD;
$this->registerJs($script);
    ?>
