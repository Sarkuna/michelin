<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\VIPOrder */
/* @var $form yii\widgets\ActiveForm */
use common\models\VipCountry;
use common\models\VIPZone;
use common\models\VIPOptionValueDescription;

$countrylists = VipCountry::find()
        ->where(['country_id' => '129'])
        ->orderBy([
            'name' => SORT_ASC,
        ])->all();
$country = ArrayHelper::map($countrylists, 'country_id', 'name');

$regionlists = VIPZone::find()
        ->where(['country_id' => '129'])
        ->orderBy([
    'name' => SORT_ASC,
])->all();
$regionlist = ArrayHelper::map($regionlists, 'zone_id', 'name');

$options = VIPOptionValueDescription::find()
        ->where(['option_id' => '2'])
        ->orderBy([
    'name' => SORT_ASC,
])->all();
$optionslist = ArrayHelper::map($options, 'option_value_id', 'name');

$OrderStatus = common\models\VIPOrderStatus::find()
        //->where(['option_id' => '2'])
        ->orderBy([
    'order_status_id' => SORT_ASC,
])->all();
$OrderStatuslist = ArrayHelper::map($OrderStatus, 'order_status_id', 'name');
?>

<div class="viporder-form row">
    <div class="col-md-12">
        <?php $form = ActiveForm::begin(['options' => ['id' => 'viporder-form','class' => 'form-horizontal form-label-left']]); ?>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#shipping_details" data-toggle="tab">Shipping Details</a></li>
              <li><a href="#products" data-toggle="tab">Products</a></li>
              <li><a href="#totals" data-toggle="tab">Totals</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="shipping_details">

                    <?=
                    $form->field($model, 'shipping_firstname', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(['maxlength' => true, 'placeholder' => '']);
                    ?>
                    
                    <?=
                    $form->field($model, 'shipping_lastname', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(['maxlength' => true, 'placeholder' => '']);
                    ?>
                    
                    <?=
                    $form->field($model, 'shipping_company', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(['maxlength' => true, 'placeholder' => '']);
                    ?>
                    
                    <?=
                    $form->field($model, 'shipping_address_1', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(['maxlength' => true, 'placeholder' => '']);
                    ?>
                    
                    <?=
                    $form->field($model, 'shipping_address_2', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(['maxlength' => true, 'placeholder' => '']);
                    ?>

                    <?=
                    $form->field($model, 'shipping_city', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(['maxlength' => true, 'placeholder' => '']);
                    ?>
                    
                    <?=
                    $form->field($model, 'shipping_postcode', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textInput(['maxlength' => true, 'placeholder' => '']);
                    ?>
                    
                    <?=
                    $form->field($model, 'shipping_country_id', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->dropDownList($country, ['prompt' => 'Select...']);
                    ?>
                    
                    <?=
                    $form->field($model, 'shipping_zone', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->dropDownList($regionlist, ['prompt' => 'Select...']);
                    ?>
                    
                    <?=
                    $form->field($model, 'shipping_zone_id', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->dropDownList($optionslist, ['prompt' => 'Select...']);
                    ?>

                </div>
                
                <div class="tab-pane" id="products">
                    <table class="table table-bordered">
                      <thead>
                          <tr>
                              <td class="text-left">Product</td>
                              <td class="text-left">Model</td>
                              <td class="text-right">Quantity</td>
                              <td class="text-right">Unit Points</td>
                              <td class="text-right">Total</td>
                              <td></td>
                          </tr>
                      </thead>
                      <tbody>
                          <?php
                  //print_r($model->getOrderProducts())
                  $total = '';
                  foreach($model->orderProducts as $key => $subproduct){
                      //echo $subproduct->name.'<br>';
                      $optlisit = '';
                      $OrderOption = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->count();
                      if($OrderOption > 0) {
                          $OrderOptionlisits = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->all();
                          foreach($OrderOptionlisits as $OrderOptionlisit){
                              $optlisit .= '<br>
                                  &nbsp;<small> - '.$OrderOptionlisit->name.': '.$OrderOptionlisit->value.'</small>';
                          }                          
                      }
                      echo '<tr>
                              <td class="text-left"><a href="'.Url::to(['/catalog/products/view', 'id' => $subproduct->product_id]).'" target="_blank">'.$subproduct->name.'</a>
                                  '.$optlisit.'
                              </td>
                              <td class="text-left">'.$subproduct->model.'</td>
                              <td class="text-right">'.$subproduct->quantity.'</td>
                              <td class="text-right">'.$subproduct->point.'pts</td>
                              <td class="text-right">'.Yii::$app->formatter->asInteger($subproduct->point_total).'pts</td>
                              <td class="text-center" style="width: 3px;"><a class="deletebagitem btn btn-danger" href="javascript::void(0)" onclick="deleteItem('.$key.');"><i class="fa fa-trash-o"></i></a></td>    
                          </tr>';
                  }
                  ?>
                          
                      </tbody>
                  </table>
                </div>
                
                <div class="tab-pane" id="totals">
                    <table class="table table-bordered">
                      <thead>
                          <tr>
                              <td class="text-left">Product</td>
                              <td class="text-left">Model</td>
                              <td class="text-right">Quantity</td>
                              <td class="text-right">Unit Points</td>
                              <td class="text-right">Total</td>
                          </tr>
                      </thead>
                      <tbody>
                          <?php
                  //print_r($model->getOrderProducts())
                  $total = '';
                  foreach($model->orderProducts as $subproduct){
                      //echo $subproduct->name.'<br>';
                      $optlisit = '';
                      $OrderOption = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->count();
                      if($OrderOption > 0) {
                          $OrderOptionlisits = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->all();
                          foreach($OrderOptionlisits as $OrderOptionlisit){
                              $optlisit .= '<br>
                                  &nbsp;<small> - '.$OrderOptionlisit->name.': '.$OrderOptionlisit->value.'</small>';
                          }                          
                      }
                      echo '<tr>
                              <td class="text-left"><a href="'.Url::to(['/catalog/products/view', 'id' => $subproduct->product_id]).'" target="_blank">'.$subproduct->name.'</a>
                                  '.$optlisit.'
                              </td>
                              <td class="text-left">'.$subproduct->model.'</td>
                              <td class="text-right">'.$subproduct->quantity.'</td>
                              <td class="text-right">'.$subproduct->point.'pts</td>
                              <td class="text-right">'.Yii::$app->formatter->asInteger($subproduct->point_total).'pts</td>  
                          </tr>';
                      $total += $subproduct->point_total;
                  }
                  ?>
                          
                          <tr>
                              <td colspan="4" class="text-right">Total:</td>
                              <td class="text-right"><?= Yii::$app->formatter->asInteger($total) ?>pts</td>
                          </tr>
                      </tbody>
                  </table>

                    <?=
                    $form->field($model, 'comment', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->textarea(['rows' => 6]);
                    ?>
                    <?=
                    $form->field($model, 'order_status_id', [
                        'template' => "<div class='col-md-3 col-sm-3 col-xs-12 text-right'>{label}</div>\n<div class='col-md-6 col-sm-6 col-xs-12'>{input}</div>\n{hint}\n{error}"
                    ])->dropDownList($OrderStatuslist, ['prompt' => 'Select...']);
                    ?>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        

    <?php ActiveForm::end(); ?>
    </div>

    

    

</div>
