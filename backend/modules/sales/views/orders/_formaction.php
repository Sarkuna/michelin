<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\MembershipPack */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Order Action';

$msg = '';
$OrderStatus = common\models\VIPOrderStatus::find()        
        ->andwhere(['!=', 'order_status_id', 10])
        ->orderBy(['order_status_id' => SORT_ASC,])
        ->all();    


$OrderStatuslist = ArrayHelper::map($OrderStatus, 'order_status_id', 'name');
$actionform->order_status_id = $model->order_status_id;
?>

            <?php
            $actionform->notify = 1;
        $form = ActiveForm::begin([
                //'id' => 'stu-master-update',
                'options' => ['enctype' => 'multipart/form-data'],
                //'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                /*'fieldConfig' => [
                    'template' => "{label}{input}{error}",
                ],*/
            ]);
        ?>

            <?php echo $form->field($actionform, 'order_status_id')->dropDownList($OrderStatuslist, ['prompt' => 'Select...']); ?>
            <?= $form->field($actionform, 'bb_invoice_no')->textInput() ?>
            <?= $form->field($actionform, 'comment')->textarea(['rows' => '6']) ?>
            <?= $form->field($actionform, 'notify')->checkbox(); ?>
            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary ml-1']) ?>
            </div>
            <?php ActiveForm::end(); ?>


<?php
$script = <<< JS
    $("#orderactionform-order_status_id").change(function(){
        if($(this).val()=="50" || $(this).val()=="60"){
            $(".field-orderactionform-bb_invoice_no").hide();
        }else{
            $(".field-orderactionform-bb_invoice_no").show();
        }
    });
JS;
$this->registerJs($script);
?>