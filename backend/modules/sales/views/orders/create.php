<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VIPOrder */

$this->title = 'Create Viporder';
$this->params['breadcrumbs'][] = ['label' => 'Viporders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="viporder-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
