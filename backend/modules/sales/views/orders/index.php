<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

use common\models\VIPOrderStatus;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Orders';
$this->params['breadcrumbs'][] = $this->title;

$OrderStatus = common\models\VIPOrderStatus::find()        
        ->andwhere(['!=', 'order_status_id', 10])
        ->orderBy(['order_status_id' => SORT_ASC,])
        ->all();
$OrderStatuslist = ArrayHelper::map($OrderStatus, 'order_status_id', 'name');
?>
<style>
    .actionbulk{display: none;}
</style>
<div class="content-body">
    <!-- Scroll - horizontal and vertical table -->
    <section id="horizontal-vertical">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><?= Html::encode($this->title) ?> - Accepted</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="row">
                                <div class="col-12 bulkact"> 
                                    <button class="btn btn-primary" id="btntest">Bulk Action</button>
                                </div>
                            </div>
                            <?=Html::beginForm(['/sales/orders/bulk-order'],'post');?>
                                <div class="actionbulk">
                                    <div class="row">
                                        <div class="col-lg-2">
                                        <label> Status</label>
                                        <?= Html::dropDownList('redemption_status', '', $OrderStatuslist, ['class' => 'form-control',]) ?>
                                        <label> Invoice Number </label>
                                        <?= Html::textInput('invoice_num', "", ['id' => 'downloadSourceCode', 'class' => 'form-control']); ?>
                                    </div>
                                    
                                    <div class="col-lg-6">
                                        <label> Comment</label>
                                        <?= Html::textarea('comment', "", ['id' => 'downloadSourceCode', 'rows' => 6, 'class' => 'form-control']); ?>
                                    </div>
                                    
                                    <div class="col-lg-4">
                                        <?= Html::checkBox('notify', false, ['label' => 'Notify to customer']); ?>
                                        <label class="control-label" for="redemptionbulk-comment"></label>
                                        <div class="form-group">
                                            <?=
                                            Html::submitButton('Submit', ['class' => 'btn btn-success', 'id' => 'btnsubmit',])
                                            ?>
                                            <?= Html::a('Cancel', ['/sales/orders'], ['class' => 'btn btn-default']) ?>

                                        </div>
                                    </div>
                                    </div>
                                    
                                    
                                </div>

                                <div class="table-responsive">
                                <?= GridView::widget([
        'tableOptions' => ['class' => 'table zero-configuration'],
                                'dataProvider' => $dataProvider,
                                'layout' => "{items}",
                                //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            //['class' => 'yii\grid\SerialColumn','headerOptions' => ['width' => '10'],],
            //['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],
            ['class' => '\yii\grid\CheckboxColumn',
                //'headerOptions' => ['width' => '10'],
                'checkboxOptions' => function ($model, $key, $index, $column) {
                    $totalpoint = $model->getTotalPoints() + $model->getShippingPointTotal();
                     if ($model->last_point_balance >= 0) {
                         return ['value' => $key];
                     }
                     return ['disabled' => true];
                 },
            ],
            [
                'attribute' => 'created_datetime1',
                'label' => 'Order Date',
                'format' => 'html',
                //'headerOptions' => ['width' => '120'],
                'value' => function ($model) {
                    return date('d/m/Y', strtotime($model->created_datetime));
                },
            ],
            [
                'attribute' => 'invoice_prefix',
                'label' => 'Order ID',
                'format' => 'html',
                //'headerOptions' => ['width' => '150'],
                'value' => function ($model) {
                    return $model->invoice_prefix;
                },
            ],
            /*[
                'attribute' => 'clients_ref_no',
                'label' => 'Code',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->customer_group_id;
                },
            ],*/            
            [
                'attribute' => 'full_name',
                'label' => 'Customer',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->shipping_firstname;
                },
            ],
            [
                'attribute' => 'company_name',
                'label' => 'Company Name',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->shipping_company;
                },
            ],            
            [
                'attribute' => 'No_of_Items',
                'label' => 'No of Items',
                'format' => 'html',
                //'headerOptions' => ['width' => '100'],
                'contentOptions' =>['class' => 'text-center'],
                'value' => function ($model) {
                    return $model->getTotalTransactions();
                },
            ],
            [
                'attribute' => 'total_point',
                'label' => 'Total Point',
                'format' => 'html',
                //'headerOptions' => ['width' => '100'],
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    $totalpoint = $model->getTotalPoints() + $model->getShippingPointTotal();
                    return Yii::$app->formatter->asInteger($totalpoint);
                },
            ],  

            
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}', // {update}{delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        return (Html::a('<i class="bx bx-show-alt"></i>', $url, ['title' => Yii::t('app', 'View'), 'class' => 'invoice-action-view mr-1']));
                    },
                    'update' => function ($url, $model) {
                        return (Html::a('<i class="bx bx-check"></i>', $url, ['title' => Yii::t('app', 'Edit'),]));
                    }, 
                ],
            ],             
        ],
    ]); ?>
                            </div>
                            <?= Html::endForm();?> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php
    $clientScript = '
        $("#DataTables_Table_0 table tbody tr").click(function()
        {
            recalculate();
        });
        $("#btntest").click(function()
        {
            $(".actionbulk").slideToggle( "slow" );
            $(".bulkact").hide();
            
            
        });
        $("#btnsubmit").click(function()
        {
            //var keys = $("#DataTables_Table_0").yiiGridView("getSelectedRows");
            var keys = $("#DataTables_Table_0").find(\'input[type=checkbox]:checked\').length;
            if(keys > "0"){
                return true;
            }else{
                alert("Please check at least one checkbox");
                return false;
            }
            
        });
        $(".select-on-check-all").change(function ()
        {
            recalculate();
        });

    ';
    $this->registerJs($clientScript, \yii\web\View::POS_END, 'booking-period');
    ?>