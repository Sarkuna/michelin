<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

use common\models\VIPOrderStatus;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VIPOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-body">
    <!-- Scroll - horizontal and vertical table -->
    <section id="horizontal-vertical">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><?= Html::encode($this->title) ?> - Refunded</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">

                                <div class="table-responsive">
                                <?= GridView::widget([
        'tableOptions' => ['class' => 'table zero-configuration'],
                                'dataProvider' => $dataProvider,
                                'layout' => "{items}",
                                //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //['class' => 'yii\grid\SerialColumn','headerOptions' => ['width' => '10'],],
            //['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],

            [
                'attribute' => 'created_datetime1',
                'label' => 'Order Date',
                'format' => 'html',
                //'headerOptions' => ['width' => '120'],
                'value' => function ($model) {
                    return date('d/m/Y', strtotime($model->created_datetime));
                },
            ],
            [
                'attribute' => 'invoice_prefix',
                'label' => 'Order ID',
                'format' => 'html',
                //'headerOptions' => ['width' => '150'],
                'value' => function ($model) {
                    return $model->invoice_prefix;
                },
            ],
            /*[
                'attribute' => 'clients_ref_no',
                'label' => 'Code',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->customer_group_id;
                },
            ],*/            
            [
                'attribute' => 'full_name',
                'label' => 'Customer',
                'format' => 'html',
                'value' => function ($model) {
                    return $model->shipping_firstname;
                },
            ],
            [
                'attribute' => 'company_name',
                'label' => 'Company Name',
                'format' => 'html',
                //'headerOptions' => ['width' => '180'],
                'value' => function ($model) {
                    return $model->shipping_company;
                },
            ],            
            [
                'attribute' => 'No_of_Items',
                'label' => 'No of Items',
                'format' => 'html',
                //'headerOptions' => ['width' => '100'],
                'contentOptions' =>['class' => 'text-center'],
                'value' => function ($model) {
                    return $model->getTotalTransactions();
                },
            ],
            [
                'attribute' => 'total_point',
                'label' => 'Total Point',
                'format' => 'html',
                //'headerOptions' => ['width' => '100'],
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    $totalpoint = $model->getTotalPoints() + $model->getShippingPointTotal();
                    return Yii::$app->formatter->asInteger($totalpoint);
                },
            ],  
            [
                'attribute' => 'updated_datetime1',
                'label' => 'Updated Date',
                'format' => 'html',
                //'headerOptions' => ['width' => '150'],
                'value' => function ($model) {
                    return date('d/m/Y', strtotime($model->updated_datetime));
                },
            ],
            /*[
                'attribute' => 'bb_invoice_no',
                'label' => 'Invoice Number',
                'format' => 'html',
                'headerOptions' => ['width' => '150'],
                'value' => function ($model) {
                    return $model->newInvoiceNumber;
                },
            ],*/            

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}', // {update}{delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        return (Html::a('<i class="bx bx-show-alt"></i>', $url, ['title' => Yii::t('app', 'View'), 'class' => 'invoice-action-view mr-1']));
                    },
                    'update' => function ($url, $model) {
                        return (Html::a('<i class="bx bx-check"></i>', $url, ['title' => Yii::t('app', 'Edit'),]));
                    }, 
                ],
            ],             
        ],
    ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>