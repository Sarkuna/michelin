<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VIPOrder */

$this->title = 'Edit Order: ' . $model->order_id;
$this->params['breadcrumbs'][] = ['label' => 'Manage Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->invoice_prefix, 'url' => ['view', 'id' => $model->order_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="viporder-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
