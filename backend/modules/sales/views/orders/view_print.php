<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\VIPOrder */

$this->title = $model->invoice_prefix;
$this->params['breadcrumbs'][] = ['label' => 'Manage Orders', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-body">
    <!-- app invoice View Page -->
    <section class="invoice-view-wrapper">
        <div class="row">
            <!-- invoice view page -->
            <div class="col-xl-12 col-md-12 col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body pb-0 mx-25">
                            <!-- header section -->
                            <div class="row">
                                <div class="col-xl-4 col-md-12">
                                    <span class="invoice-number mr-50">Order#</span>
                                    <span><?= $model->invoice_prefix ?></span>
                                </div>
                                <div class="col-xl-8 col-md-12">
                                    <div class="d-flex align-items-center justify-content-xl-end flex-wrap">
                                        <div class="mr-3">
                                            <span class="invoice-number mr-50">Order date:</span>
                                            <span><?= date('d/m/Y', strtotime($model->created_datetime)) ?></span>
                                        </div>
                                        
                                        
                                        <?php
                                    echo '<div class="badge badge-pill badge-light-' . $model->orderStatus->labelbg . '">' . $model->orderStatus->name . '</div>';
                                    //echo '<div class="clear"></div><b>Invoice #'.$model->newInvoiceNumber.'</b><br>';
                                    ?>
                                    </div>
                                </div>
                            </div>
                            <!-- logo and title -->

                            <hr>
                            <!-- invoice address and contact -->
                            <div class="row invoice-info">
                                <div class="col-6 mt-1">
                                    <h6 class="invoice-from">AD info</h6>
                                    <div class="mb-1">
                                        <span><?= $model->shipping_company ?></span>
                                    </div>
                                    <div class="mb-1">
                                        <span><?= $model->customer_group_id ?></span>
                                    </div>
                                    <div class="mb-1">
                                        <span><?= $model->shipping_firstname ?></span>
                                    </div>
                                    <div class="mb-1">
                                        <span><a href="mailto:<?= $model->email ?>"><?= $model->email ?></a></span>
                                    </div>
                                    <div class="mb-1">
                                        <span><?= $model->mobile_no ?></span>
                                    </div>
                                    <div class="mb-1">
                                        <span><?= $model->telephone_no ?></span>
                                    </div>
                                </div>
                                <div class="col-6 mt-1">
                                    <h6 class="invoice-to">Shipping Details</h6>
                                    <div class="mb-1">
                                        <span><?= $model->shipping_firstname ?> <?= $model->shipping_lastname ?></span>
                                    </div>
                                    <div class="mb-1">
                                        <span><?= $model->shipping_company ?></span>
                                    </div>
                                    
                                    <div class="mb-1">
                                        <span><?= $model->shipping_address_1 ?> <?= $model->shipping_address_2 ?><br><?= $model->shipping_postcode ?> ,<?= $model->shipping_city ?>, <?= $model->shipping_zone ?></span>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                            <hr>
                        </div>
                        <!-- product details table-->
                        <div class="invoice-product-details table-responsive mx-md-25">
                            <table class="table table-borderless mb-0">
                                <thead>
                                    <tr class="border-0">
                                        <th scope="col">Product</th>
                                        <th scope="col">Model</th>
                                        <th scope="col">Quantity</th>
                                        <th scope="col">Unit Points</th>
                                        <th scope="col" class="text-right">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                            //print_r($model->getOrderProducts())
                            $subtotal = 0; $total = 0; $shipping_point_total = 0;
                            foreach ($model->orderProducts as $subproduct) {
                                //echo $subproduct->name.'<br>';
                                $optlisit = '';
                                $OrderOption = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->count();
                                if ($OrderOption > 0) {
                                    $OrderOptionlisits = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->all();
                                    foreach ($OrderOptionlisits as $OrderOptionlisit) {
                                        $optlisit .= '<br>
                                  &nbsp;<small> - ' . $OrderOptionlisit->name . ': ' . $OrderOptionlisit->value . '</small>';
                                    }
                                }
                                $trackinfo = '';
                                if($subproduct->tracking_type == 'Y') {
                                    if(!empty($subproduct->tracking_link)) {
                                        $trackinginfo = '<a href="' . $subproduct->tracking_link . '" target="_blank">' . $subproduct->tracking_no . '</a>';
                                    }else {
                                        $trackinginfo = $subproduct->tracking_no;
                                    }
                                    $trackinfo = '<br><em>Tracking Info '.$trackinginfo.'</em>'; 
                                }
                                
                                echo '<tr>
                              <td class="text-left">
                                    <a href="' . Url::to(['/catalog/products/view', 'id' => $subproduct->product_id]) . '" target="_blank">' . $subproduct->name . '</a>
                                    ' . $optlisit . $trackinfo.'
                              </td>
                              <td class="text-left">' . $subproduct->model . '</td>
                              <td class="text-right">' . $subproduct->quantity . '</td>
                              <td class="text-right">' . $subproduct->point . 'pts</td>
                              <td class="text-right">' . Yii::$app->formatter->asInteger($subproduct->point_total) . 'pts</td>
                          </tr>';
                                $subtotal += $subproduct->point_total;
                                $shipingpoint = $subproduct->quantity * $subproduct->shipping_point;
                                //$shipping_point_total += $subproduct->shipping_point_total;
                                $shipping_point_total += $shipingpoint;
                                
                            }
                            $total = $subtotal + $shipping_point_total;
                            ?>
                                </tbody>
                            </table>
                        </div>

                        <!-- invoice subtotal -->
                        <div class="card-body pt-0 mx-25">
                            <hr>
                            <div class="row">
                                <div class="col-4 col-sm-6 mt-75">
                                    <p class="lead">History:</p>


                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                        <?php
                            $comments = common\models\VIPOrderHistory::find()->where(['order_id' => $model->order_id])->all();
                            if(count($comments) > 0){
                                foreach($comments as $comment){
                                    if(!empty($comment->comment)){
                                        //$redemptionstatus = common\models\VIPOrderStatus::find()->where(['order_status_id' => $comment->order_status_id])->one();
                                        $actionlabel = '<span class="badge badge-pill badge-light-'.$comment->orderstatus->labelbg.'">'.$comment->orderstatus->name.'</span>';
                                        echo $actionlabel.' <em class="pull-right">'.date('d-m-Y', strtotime($comment->date_added)).'</em><br>';
                                        if($comment->type == 1) {
                                            echo '<em>by: System</em><br>';
                                        }else {
                                            echo '<em>by: '.$comment->by->first_name.' '.$comment->by->last_name.'</em><br>';
                                        }
                                        echo $comment->comment.'<br><br>';
                                    }
                                }
                            }else{
                                echo 'No Comments';
                            }
                        ?>
                    </p>
                                </div>
                                <div class="col-8 col-sm-6 d-flex justify-content-end mt-75">
                                    <div class="invoice-subtotal">
                                        <div class="invoice-calc d-flex justify-content-between">
                                            <span class="invoice-title">Subtotal</span>
                                            <span class="invoice-value"><?= Yii::$app->formatter->asInteger($subtotal) ?>pts</span>
                                        </div>
                                        <div class="invoice-calc d-flex justify-content-between">
                                            <span class="invoice-title">Delivery Charges</span>
                                            <span class="invoice-value"><?= Yii::$app->formatter->asInteger($shipping_point_total) ?>pts</span>
                                        </div>
                                        <hr>
                                        <div class="invoice-calc d-flex justify-content-between">
                                            <span class="invoice-title">Total</span>
                                            <span class="invoice-value"><?= Yii::$app->formatter->asInteger($total) ?>pts</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- invoice action  -->
            
        </div>
    </section>

</div>


<script>
/***
  * Start Update Gardian Jquery
***/
window.onload = function () {
    window.print();
}
</script>