<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\VIPOrder */

$this->title = $model->invoice_prefix;
$this->params['breadcrumbs'][] = ['label' => 'Manage Orders', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $this->title;
//$balancepoint = Yii::$app->VIPglobal->customersAvailablePoint($model->customer_id);
$clientsetup = Yii::$app->VIPglobal->clientSetup();
$pointpersent = $clientsetup['point_value'];
$mark_up = $clientsetup['pervalue'];
?>
<style>
    .invoice-subtotal{width: 400px;}
    .invoice-view-wrapper .invoice-subtotal .invoice-calc .invoice-title {width: auto;}
</style>
<div class="content-body">
    <!-- app invoice View Page -->
    <section class="invoice-view-wrapper">
        <div class="row">
            <!-- invoice view page -->
            <div class="col-xl-12 col-md-12 col-12">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body pb-0 mx-25">
                            <!-- header section -->
                            <div class="row">
                                <div class="col-xl-4 col-md-12">
                                    <span class="invoice-number mr-50">Order#</span>
                                    <span><?= $model->invoice_prefix ?></span>
                                </div>
                                <div class="col-xl-8 col-md-12">
                                    <div class="d-flex align-items-center justify-content-xl-end flex-wrap">
                                        <div class="mr-3">
                                            <span class="invoice-number mr-50">Order date:</span>
                                            <span><?= date('d/m/Y', strtotime($model->created_datetime)) ?></span>
                                        </div>
                                        
                                        
                                        <?php
                                    echo '<div class="badge badge-pill badge-light-' . $model->orderStatus->labelbg . '">' . $model->orderStatus->name . '</div>';
                                    //echo '<div class="clear"></div><b>Invoice #'.$model->newInvoiceNumber.'</b><br>';
                                    ?>
                                    </div>
                                </div>
                            </div>
                            <!-- logo and title -->

                            <hr>
                            <!-- invoice address and contact -->
                            <div class="row invoice-info">
                                <div class="col-6 mt-1">
                                    <h6 class="invoice-from">AD info</h6>
                                    <div class="mb-1">
                                        <span><?= $model->shipping_company ?></span>
                                    </div>
                                    <div class="mb-1">
                                        <span><?= $model->customer_group_id ?></span>
                                    </div>
                                    <div class="mb-1">
                                        <span><?= $model->shipping_firstname ?></span>
                                    </div>
                                    <div class="mb-1">
                                        <span><a href="mailto:<?= $model->email ?>"><?= $model->email ?></a></span>
                                    </div>
                                    <div class="mb-1">
                                        <span><?= $model->mobile_no ?></span>
                                    </div>
                                    <div class="mb-1">
                                        <span><?= $model->telephone_no ?></span>
                                    </div>
                                </div>
                                <div class="col-6 mt-1">
                                    <h6 class="invoice-to">Shipping Details</h6>
                                    <div class="mb-1">
                                        <span><?= $model->shipping_firstname ?> <?= $model->shipping_lastname ?></span>
                                    </div>
                                    <div class="mb-1">
                                        <span><?= $model->shipping_company ?></span>
                                    </div>
                                    
                                    <div class="mb-1">
                                        <span><?= $model->shipping_address_1 ?> <?= $model->shipping_address_2 ?> <br><?= $model->shipping_postcode ?> ,<?= $model->shipping_city ?>, <?= $model->shipping_zone ?></span>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                            <hr>
                        </div>
                        <!-- product details table-->
                        <div class="invoice-product-details table-responsive mx-md-25">
                            <table class="table table-borderless mb-0">
                                <thead>
                                    <tr class="border-0">
                                        <th scope="col">Product</th>
                                        <th scope="col">Model</th>
                                        <th scope="col">Quantity</th>
                                        <th scope="col">Unit Points</th>
                                        <th scope="col" class="text-right">Total</th>
                                        <th scope="col">Unit RM</th>
                                        <th scope="col" class="text-right">Total RM</th>
                                        <th scope="col">Unit Product Cost</th>
                                        <th scope="col">Cost to Offset</th>
                                        <th scope="col">Actual Cost</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    echo '<pre>';
                            //print_r($model->getOrderProducts());
                            //die;
                            $subtotal = 0; $total = 0; $shipping_point_total = 0; $total_order_RM = 0; $total_offset = 0;
                            foreach ($model->orderProducts as $subproduct) {
                                $productsingle = common\models\VIPProduct::find() ->select('price, type') ->where(['product_id' => $subproduct->product_id]) ->one(); 
                                //echo $subproduct->name.'<br>';
                                $optlisit = '';
                                $OrderOption = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->count();
                                if ($OrderOption > 0) {
                                    $OrderOptionlisits = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->all();
                                    foreach ($OrderOptionlisits as $OrderOptionlisit) {
                                        $optlisit .= '<br>
                                  &nbsp;<small> - ' . $OrderOptionlisit->name . ': ' . $OrderOptionlisit->value . '</small>';
                                    }
                                }
                                $trackinfo = '';
                                if($subproduct->tracking_type == 'Y') {
                                    if(!empty($subproduct->tracking_link)) {
                                        $trackinginfo = '<a href="' . $subproduct->tracking_link . '" target="_blank">' . $subproduct->tracking_no . '</a>';
                                    }else {
                                        $trackinginfo = $subproduct->tracking_no;
                                    }
                                    $trackinfo = '<br><em>Tracking Info '.$trackinginfo.'</em>'; 
                                }
                                
                                $unitrm = $subproduct->point * $pointpersent;
                                $total_RM = Yii::$app->formatter->asDecimal($unitrm *  $subproduct->quantity);
                                if($productsingle->type == 'bb'){
                                    $per_unit_RM = '';
                                    $cost_offset = '';
                                    $act_cost = '';
                                }else {
                                    
                                    $per_unit_RM = Yii::$app->formatter->asDecimal($productsingle->price);
                                    $cost_offset = Yii::$app->formatter->asDecimal($productsingle->price *  $subproduct->quantity);
                                    $act_cost = $total_RM - $cost_offset;
                                }
                                
                                
                                
                                
                                echo '<tr>
                                    <td class="text-left">
                                          <a href="' . Url::to(['/catalog/products/view', 'id' => $subproduct->product_id]) . '" target="_blank">' . $subproduct->name . '</a>
                                          ' . $optlisit . $trackinfo.'
                                    </td>
                                    <td class="text-left">' . $subproduct->model . '</td>
                                    <td class="text-right">' . $subproduct->quantity . '</td>
                                    <td class="text-right">' . $subproduct->point . 'pts</td>
                                    <td class="text-right">' . Yii::$app->formatter->asInteger($subproduct->point_total) . 'pts</td>
                                    <td class="text-right">' . Yii::$app->formatter->asDecimal($unitrm). '</td>
                                    <td class="text-right">' . $total_RM . '</td>
                                    <td class="text-right">' . $per_unit_RM . '</td>
                                    <td class="text-right">' . $cost_offset . '</td>
                                    <td class="text-right">' . Yii::$app->formatter->asDecimal($act_cost) . '</td>';
                                echo '</tr>';
                                $subtotal += $subproduct->point_total;
                                $total_order_RM += $total_RM;
                                $total_offset += $cost_offset;
                                $shipingpoint = $subproduct->quantity * $subproduct->shipping_point;
                                //$shipping_point_total += $subproduct->shipping_point_total;
                                $shipping_point_total += $shipingpoint;
                                
                            }
                            $total = $subtotal + $shipping_point_total;
                            ?>
                                </tbody>
                            </table>
                        </div>

                        <!-- invoice subtotal -->
                        <div class="card-body pt-0 mx-25">
                            <hr>
                            <div class="row">
                                <div class="col-4 col-sm-6 mt-75">
                                    
                                </div>
                                <div class="col-9 col-sm-6 d-flex justify-content-end mt-75">
                                    <div class="invoice-subtotal">
                                        <div class="invoice-calc d-flex justify-content-between">
                                            <span class="invoice-title">Total Order RM</span>
                                            <span class="invoice-value"><?= Yii::$app->formatter->asDecimal($total_order_RM) ?></span>
                                        </div>
                                        <div class="invoice-calc d-flex justify-content-between">
                                            <span class="invoice-title">Total Cost to Offset</span>
                                            <span class="invoice-value"><?= Yii::$app->formatter->asDecimal($total_offset) ?></span>
                                        </div>
                                        <hr>
                                        <div class="invoice-calc d-flex justify-content-between">
                                            <span class="invoice-title">Actual Cost for this Order</span>
                                            <span class="invoice-value"><?= Yii::$app->formatter->asDecimal($total_order_RM - $total_offset) ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- invoice action  -->

        </div>
    </section>
    
    

</div>



<?php
    $script = <<<EOD

    $('#update').click(function(){
                var id = $(this).attr("data-id");
                $.ajax({
                  type:'GET',
                  url:'/sales/orders/orderapprove',
                  data: { id : id},
                  success: function(data)
                           {
                               $(".modal-content");
                               $('.modal-body').html(data);
                               $('#inlineForm').modal();

                           }
                });
});

EOD;
$this->registerJs($script);
    ?>
