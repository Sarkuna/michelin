<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VIPCategories */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Logo';
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];

$logo = $client->company_logo ? Html::img('/upload/client_logos/'.$client->company_logo, ['class'=>'kv-preview-data file-preview-image', 'alt'=>'Logo', 'title'=>'Logo']) : null;

?>
<style>
    .thumbnail{width: 100%;}
    .input-group-addon, .input-group-btn {width: auto;}
    .file-actions {display: none;}
</style>
<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><?= Html::encode($this->title) ?></h4>
                </div>
                <div class="card-content">
                    <div class="card-body pages-update">
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>


                        <?php
                            echo $form->field($model, 'file_image', [
                                        //'template' => "<fieldset class='form-group'><label for='basicInputFile'>{label} <sub>You may upload JPG, GIF, BITMAP or PNG images up to 6MB in size.</sub></label><div class='custom-file'>{input}<label class='custom-file-label' for='inputGroupFile01'>Choose file</label>{hint}</div>{error}</fieldset>",
                                        'options' => [
                                            'class' => 'file form-group',
                                        ],
                                ])->fileInput(['multiple'=>true, 'id' => 'inputGroupFile01']
                            );
                        ?>
                        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary mr-1 mb-1 waves-effect waves-light']) ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
$(document).ready(function() {
    $("#inputGroupFile01").fileinput({
        maxFileCount: 4,
        showDrag: false,
        showRemove: false,
        showClose: false,
        showUpload: false,
        showZoom: false,
        allowedFileExtensions: ["jpeg", "jpg", "gif", "png"],
        //initialPreview: ['<img src="https://picsum.photos/id/239/1920/1080" class="kv-preview-data file-preview-image">',],
        initialPreview: ['<?= $logo ?>',],
        layoutTemplates: {
            main1: "{preview}\n" +
            "<div class=\'input-group {class}\'>\n" +
            "   <div class=\'input-group-btn\ input-group-prepend'>\n" +
            "       {browse}\n" +
            "       {upload}\n" +
            "       {remove}\n" +
            "   </div>\n" +
            "   {caption}\n" +
            "</div>"
        }
    });
});
</script>