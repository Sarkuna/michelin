<?php
use backend\assets\LoginFrestAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
$session = Yii::$app->session;

/* @var $this \yii\web\View */
/* @var $content string */

LoginFrestAsset::register($this);
$client = \common\models\Client::findOne($session['currentclientID']);
$favicon = $client->favicon;
$programme_title = $client->programme_title;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/upload/favicon/<?= $favicon ?>" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
    <title><?= $programme_title ?></title>
    <?php $this->head() ?>
</head>

<body class="vertical-layout vertical-menu-modern semi-dark-layout 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column" data-layout="semi-dark-layout">
    <?php $this->beginBody() ?>
    
<!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row"></div>
            <div class="content-body">
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>
    </div>
<!-- END: Content--> 
    <?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
