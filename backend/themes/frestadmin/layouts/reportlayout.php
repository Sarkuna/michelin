<?php
//use frontend\assets\DashbordAsset;

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use backend\assets\ReportFrestAsset;
use app\components\BreadcrumbsFrestAdmin;
use common\widgets\Alert;

use app\components\HeaderFrestAdmin;
use app\components\LeftMenuFrestAdmin;
use app\components\CopyrightFrestAdmin;

/* @var $this \yii\web\View */
/* @var $content string */
$baseURL = Yii::$app->request->url;

ReportFrestAsset::register($this);
//AppAsset::register($this);
$controller = Yii::$app->controller;
$default_controller = Yii::$app->defaultRoute;
$isHome = (($controller->id === $default_controller) && ($controller->action->id === $controller->defaultAction)) ? true : false;
if($isHome == true){
    $dash = '';
}else {
    $dash = '-';
}
$session = Yii::$app->session;
$client = \common\models\Client::findOne($session['currentclientID']);
$favicon = $client->favicon;
$programme_title = $client->programme_title;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/upload/favicon/<?= $favicon ?>" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
    
    <title><?= $programme_title ?> <?= $dash ?> <?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">
<?php $this->beginBody() ?>
    <!-- BEGIN: Header-->
        <?= HeaderFrestAdmin::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
        <?= LeftMenuFrestAdmin::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <?= BreadcrumbsFrestAdmin::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
                </div>
            </div>
                <?= Alert::widget() ?>
                <?= $content ?>
                <!-- Dashboard Analytics Start -->
                    
                <!-- Dashboard Analytics end -->


        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <?= CopyrightFrestAdmin::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
    <!-- END: Footer-->
        
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
