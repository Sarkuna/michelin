<?php
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\PointOrder;
use common\models\Redemption;
    
use app\components\ProductOverviewPainterWidget;
use app\components\RedemptionRequestPainterWidget;
use app\components\PayOutPainterWidget;
use app\components\OrderSummaryWidget;
use app\components\RedemptionStateWidget;
use app\components\TopRedemptionWidget;
use app\components\RedemptionOverviewWidget;
/* This sets the $time variable to the current hour in the 24 hour clock format */
    $time = date("H");
    /* Set the $timezone variable to become the current timezone */
    $timezone = date("e");
    /* If the time is less than 1200 hours, show good morning */
    if ($time < "12") {
        $greetings = "Good Morning";
    } else
    /* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
    if ($time >= "12" && $time < "17") {
        $greetings = "Good Afternoon";
    } else
    /* Should the time be between or equal to 1700 and 1900 hours, show good evening */
    if ($time >= "17" && $time < "24") {
        $greetings = "Good Evening";
    }


$total_sales = common\models\VIPOrderProduct::find()->sum('point_total');
if($total_sales > 0) {
    $percentage = $total_sales / 100;
}else {
    $total_sales = 0;
    $percentage = 0;
}

?>
<?php
$n = 1000;
//echo Yii::$app->ici->customNumberFormat($n);
//echo Yii::$app->VIPglobal->ViewAvatar('Mohamed Shihan');

?>
<section id="dashboard-ecommerce">
    <div class="row">
        <!-- Greetings Content Starts -->
        <div class="col-xl-4 col-md-6 col-12 dashboard-greetings h-100">
            <div class="card">
                <div class="card-header">
                    <h3 class="greeting-text">Dear <?= Yii::$app->VIPglobal->profileName() ?>!</h3>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-end">
                            <div class="dashboard-content-left">
                                <p class="mb-0">Total Point Redeemed</p>
                                <h1 class="text-primary font-large-2 text-bold-500"><?= Yii::$app->formatter->asInteger($total_sales) ?> pts</h1>
                                <?= Html::a('View Sales', ['/sales/orders/index'], ['title' => Yii::t('app', 'View Sales'), 'class' => 'btn btn-primary glow']); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Multi Radial Chart Starts -->
        <div class="col-xl-4 col-md-6 col-12 dashboard-visit h-100">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h4 class="card-title">Visits</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <?php
                        //$labelsuser = ['Android', 'IOS', 'Desktop', 'Others'];
                        //$usertype = [44, 55, 41, 17];
                        echo \onmotion\apexcharts\ApexchartsWidget::widget([
                            'type' => 'radialBar', // default area
                            'height' => '200', // default 350
                            //'width' => '500', // default 100%
                            'chartOptions' => [
                                'plotOptions' => [
                                    'radialBar' => [
                                      'offsetY' => -10,
                                      'hollow' => [
                                        'size' => "60%"
                                      ],
                                      'track' => [
                                        'margin' => 6,
                                        'background' => '#fff',
                                      ],
                                      'dataLabels' => [
                                        'name' => [
                                          'fontSize' => '15px',
                                          'color' => ['#828D99'],
                                          'fontFamily' => "IBM Plex Sans",
                                          'offsetY' => 25,
                                        ],
                                        'value' => [
                                          'fontSize' => '30px',
                                          'fontFamily' => "Rubik",
                                          'offsetY' => -15,
                                        ],
                                        'total' => [
                                          'show' => true, 
                                          'label' => 'Total Visits',
                                          'color' => '#828D99'
                                        ]
                                      ]
                                    ]
                                  ],
                                'labels' => ['Target', 'Mart', 'Ebay'],
                                
                                'stroke' => [
                                    'lineCap' => 'round',
                                ],
                                'legend' => [
                                    'position' => 'bottom'
                                ]
                                
                            ],
                            'series' => [75, 80, 85],
                        ]);
                        ?>
                        <ul class="list-inline d-flex justify-content-around mb-0">
                            <li> <span class="bullet bullet-xs bullet-primary mr-50"></span>Target</li>
                            <li> <span class="bullet bullet-xs bullet-danger mr-50"></span>Mart</li>
                            <li> <span class="bullet bullet-xs bullet-warning mr-50"></span>Ebay</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4 col-md-6 col-12 dashboard-greetings h-100">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h4 class="card-title">Device Info</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <?php
                        $labelsuser = ['Android', 'IOS', 'Desktop', 'Others'];
                        $usertype = [44, 55, 41, 17];
                        echo \onmotion\apexcharts\ApexchartsWidget::widget([
                            'type' => 'donut', // default area
                            'height' => '200', // default 350
                            //'width' => '500', // default 100%
                            'chartOptions' => [
                                'plotOptions' => [
                                    'pie' => [
                                        'startAngle' => '-90',
                                        'endAngle' => '270'
                                    ]
                                ],
                                'labels' => $labelsuser,
                                'dataLabels' => [
                                    'enabled' => false
                                ],
                                'fill' => [
                                    'type' => 'gradient',
                                ],
                                'legend' => [
                                    'position' => 'bottom'
                                ],
                                'responsive' => [
                                    [
                                        'breakpoint' => '480',
                                        'options' => [
                                            'chart' => [
                                                'width' => '200'
                                            ],
                                            'legend' => [
                                                'position' => 'bottom'
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            'series' => $usertype,
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row" style="margin-bottom: 25px;">
        <div class="col-xl-6 col-md-6 col-12 dashboard-visit">
            <?= RedemptionStateWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?> 
        </div>
        
        
        <div class="col-xl-6 col-md-6 col-12 dashboard-greetings">
            <div class="card h-100">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h4 class="card-title">Orders</h4>
                    <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row" style="margin-bottom: 25px;">
        <div class="col-xl-12 col-12 dashboard-order-summary">
            <?= OrderSummaryWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        </div>

        <!-- Earning Swiper Starts -->
        <div class="col-xl-5 col-md-6 col-12 dashboard-earning-swiper" id="widget-earnings">
            <?= TopRedemptionWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        </div>
        <!-- Marketing Campaigns Starts -->
        <div class="col-xl-7 col-12 dashboard-marketing-campaign">
            <?= RedemptionOverviewWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?> 
        </div>
    </div>
</section>
