<?php
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\PointOrder;
use common\models\Redemption;
    
use app\components\ProductOverviewPainterWidget;
use app\components\RedemptionRequestPainterWidget;
use app\components\PayOutPainterWidget;
use app\components\UsersWidget;
use app\components\RedemptionStateWidget;
use app\components\TopRedemptionWidget;
use app\components\RedemptionOverviewWidget;
/* This sets the $time variable to the current hour in the 24 hour clock format */
    $time = date("H");
    /* Set the $timezone variable to become the current timezone */
    $timezone = date("e");
    /* If the time is less than 1200 hours, show good morning */
    if ($time < "12") {
        $greetings = "Good Morning";
    } else
    /* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
    if ($time >= "12" && $time < "17") {
        $greetings = "Good Afternoon";
    } else
    /* Should the time be between or equal to 1700 and 1900 hours, show good evening */
    if ($time >= "17" && $time < "24") {
        $greetings = "Good Evening";
    }


$total_sales = common\models\VIPOrderProduct::find()->sum('point');
$percentage = $total_sales / 100;

?>
<?php
$n = 1000;
//echo Yii::$app->ici->customNumberFormat($n);
?>
<section id="dashboard-ecommerce">
    <div class="row">
        <!-- Greetings Content Starts -->
        <div class="col-xl-4 col-md-6 col-12 dashboard-greetings">
            <div class="card">
                <div class="card-header">
                    <h3 class="greeting-text">Congratulations!</h3>
                    <p class="mb-0">Best seller of the month</p>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-end">
                            <div class="dashboard-content-left">
                                <h1 class="text-primary font-large-2 text-bold-500"><?= Yii::$app->formatter->asInteger($total_sales) ?> pts</h1>
                                <p>You have done <?= $percentage ?>% more sales today.</p>
                                <?= Html::a('View Sales', ['/sales/orders/index'], ['title' => Yii::t('app', 'View Sales'), 'class' => 'btn btn-primary glow']); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Multi Radial Chart Starts -->
        <div class="col-xl-4 col-md-6 col-12 dashboard-visit">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h4 class="card-title">Visits of 2019</h4>
                    <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <div id="multi-radial-chart"></div>
                        <ul class="list-inline d-flex justify-content-around mb-0">
                            <li> <span class="bullet bullet-xs bullet-primary mr-50"></span>Target</li>
                            <li> <span class="bullet bullet-xs bullet-danger mr-50"></span>Mart</li>
                            <li> <span class="bullet bullet-xs bullet-warning mr-50"></span>Ebay</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4 col-md-6 col-12 dashboard-greetings">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h4 class="card-title">User Type</h4>
                    <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <?php
                        $labelsuser = ['Android', 'IOS', 'Desktop', 'Others'];
                        $usertype = [44, 55, 41, 17];
                        echo \onmotion\apexcharts\ApexchartsWidget::widget([
                            'type' => 'donut', // default area
                            'height' => '230', // default 350
                            //'width' => '500', // default 100%
                            'chartOptions' => [
                                'plotOptions' => [
                                    'pie' => [
                                        'startAngle' => '-90',
                                        'endAngle' => '270'
                                    ]
                                ],
                                'labels' => $labelsuser,
                                'dataLabels' => [
                                    'enabled' => false
                                ],
                                'fill' => [
                                    'type' => 'gradient',
                                ],
                                'legend' => [
                                    'position' => 'bottom'
                                ],
                                'responsive' => [
                                    [
                                        'breakpoint' => '480',
                                        'options' => [
                                            'chart' => [
                                                'width' => '200'
                                            ],
                                            'legend' => [
                                                'position' => 'bottom'
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            'series' => $usertype,
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xl-6 col-md-6 col-12 dashboard-visit">
            <?= RedemptionStateWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?> 
        </div>
        
        
        <div class="col-xl-6 col-md-6 col-12 dashboard-greetings">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h4 class="card-title">Orders</h4>
                    <i class="bx bx-dots-vertical-rounded font-medium-3 cursor-pointer"></i>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <?php
                        $labelsorders = ['01 Jan 2001', '02 Jan 2001', '03 Jan 2001', '04 Jan 2001', '05 Jan 2001', '06 Jan 2001', '07 Jan 2001', '08 Jan 2001', '09 Jan 2001', '10 Jan 2001', '11 Jan 2001', '12 Jan 2001'];
                        $orders = [
                            [
                                'name' => 'Orders',
                                'data' => [44, 50, 41, 67, 22, 41, 20, 35, 75, 32, 25, 60],
                                'type' => 'column'
                            ],
                            [
                                'name' => 'Point Redeemed',
                                'data' => [23, 42, 35, 27, 43, 22, 17, 31, 22, 22, 12, 16],
                                'type' => 'line'
                            ]
                        ];
                        echo \onmotion\apexcharts\ApexchartsWidget::widget([
                            'type' => 'line', // default area
                            'height' => '400', // default 350
                            //'width' => '500', // default 100%
                            'chartOptions' => [
                                'chart' => [
                                    'toolbar' => [
                                        'show' => false,
                                    ],
                                ],
                                'stroke' => [
                                    'width' => [0,4],
                                ],
                                'dataLabels' => [
                                    'enabled' => true,
                                    'enabledOnSeries' => [1],
                                ],
                                'labels' => $labelsorders,
                                
                                'xaxis' => [
                                    'type' => 'datetime',
                                ],
                            ],
                            'series' => $orders,
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xl-12 col-12 dashboard-order-summary">
            <div class="card">
                <div class="row">
                    <!-- Order Summary Starts -->
                    <div class="col-md-8 col-12 order-summary border-right pr-md-0">
                        <div class="card mb-0">
                            <div class="card-header d-flex justify-content-between align-items-center">
                                <h4 class="card-title">Order Summary</h4>
                                <div class="d-flex">
                                    <button type="button" class="btn btn-sm btn-light-primary mr-1">Month</button>
                                    <button type="button" class="btn btn-sm btn-primary glow">Week</button>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body p-0">
                                    <?php
                                    $labelsgender = ['Male', 'Female'];
                                    $genders = [[
                                    'name' => 'Weeks',
                                    'data' => ['165', '175', '162', '173', '160', '195', '160', '170', '160', '190', '180'],
                                    'type' => 'area'],
                                        [
                                            'name' => 'Month',
                                            'data' => [168, 168, 155, 178, 155, 170, 190, 160, 150, 170, 140],
                                            'type' => 'line']
                                    ];
                                    echo \onmotion\apexcharts\ApexchartsWidget::widget([
                                        'type' => 'line', // default area
                                        'height' => '400', // default 350
                                        //'width' => '500', // default 100%
                                        'chartOptions' => [
                                            'chart' => [
                                                'toolbar' => [
                                                    'show' => false,
                                                ],
                                            ],
                                            'stacked' => false,
                                            'sparkline' => ['enabled' => true],
                                            //'labels' => $labelsgender,
                                            'colors' => ['#5A8DEE', '#5A8DEE'],
                                            'dataLabels' => [
                                                'enabled' => false
                                            ],
                                            'stroke' => [
                                                'curve' => 'smooth',
                                                'width' => 2.5,
                                                'dashArray' => [0, 8]
                                            ],
                                            'fill' => [
                                                'type' => 'gradient',
                                                'gradient' => [
                                                    'inverseColors' => false,
                                                    'shade' => 'light',
                                                    'type' => "vertical",
                                                    'gradientToColors' => ['#E2ECFF', '#5A8DEE'],
                                                    'opacityFrom' => 0.7,
                                                    'opacityTo' => 0.55,
                                                    'stops' => [0, 80, 100]
                                                ]
                                            ],
                                            'xaxis' => [
                                                'offsetY' => -50,
                                                'categories' => ['', 1, 2, 3, 4, 5, 6, 7, 8, 9, ''],
                                                'axisBorder' => [
                                                    'show' => false,
                                                ],
                                                'axisTicks' => [
                                                    'show' => false,
                                                ],
                                                'labels' => [
                                                    'show' => true,
                                                    'style' => [
                                                        'colors' => '#828D99'
                                                    ]
                                                ]
                                            ],
                                        ],
                                        'series' => $genders,
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Sales History Starts -->
                    <div class="col-md-4 col-12 pl-md-0">
                        <div class="card mb-0">
                            <div class="card-header pb-50">
                                <h4 class="card-title">Best Sellers</h4>
                            </div>
                            <div class="card-content">
                                <div class="card-body py-1">
                                    <div class="d-flex justify-content-between align-items-center mb-2">
                                        <div class="sales-item-name">
                                            <p class="mb-0">Airpods</p>
                                        </div>
                                        <div class="sales-item-amount">
                                            <h6 class="mb-0">750</h6>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-center mb-2">
                                        <div class="sales-item-name">
                                            <p class="mb-0">iPhone</p>
                                        </div>
                                        <div class="sales-item-amount">
                                            <h6 class="mb-0">659</h6>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-center mb-2">
                                        <div class="sales-item-name">
                                            <p class="mb-0">iPhone</p>
                                        </div>
                                        <div class="sales-item-amount">
                                            <h6 class="mb-0">659</h6>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-center mb-2">
                                        <div class="sales-item-name">
                                            <p class="mb-0">iPhone</p>
                                        </div>
                                        <div class="sales-item-amount">
                                            <h6 class="mb-0">659</h6>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="sales-item-name">
                                            <p class="mb-0">Macbook</p>
                                        </div>
                                        <div class="sales-item-amount">
                                            <h6 class="mb-0">512</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer border-top pb-0">
                                    <h5>Total Sales</h5>
                                    <span class="text-primary text-bold-500">82,950 pts</span>
                                    <div class="progress progress-bar-primary progress-sm my-50">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="78" style="width:78%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earning Swiper Starts -->
        <div class="col-xl-4 col-md-6 col-12 dashboard-earning-swiper" id="widget-earnings">
            <?= TopRedemptionWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        </div>
        <!-- Marketing Campaigns Starts -->
        <div class="col-xl-8 col-12 dashboard-marketing-campaign">
            <?= RedemptionOverviewWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?> 
        </div>
    </div>
</section>
