<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
$exception = Yii::$app->errorHandler->exception;
if ($exception !== null) {
        $statusCode = $exception->statusCode;
        $name = $exception->getName();
        $message = $exception->getMessage();
        
        if($statusCode == '404') {
            $img = $this->theme->baseUrl.'/app-assets/images/pages/404.png';
            echo '<section class="row flexbox-container">
                    <div class="col-xl-6 col-md-7 col-9">
                        <div class="card bg-transparent shadow-none">
                            <div class="card-content">
                                <div class="card-body text-center">
                                    <h1 class="error-title">'.$statusCode.' - '.$name.'</h1>
                                    <p class="pb-3">'.$message.'</p>
                                    <img class="img-fluid" src="'.$this->theme->baseUrl.'/app-assets/images/pages/404.png" alt="404 error">
                                    <a href="index" class="btn btn-primary round glow mt-3">BACK TO HOME</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>';
        }else if($statusCode == '504') {
            $img = $this->theme->baseUrl.'/app-assets/images/pages/500.png';
            echo '<section class="row flexbox-container">
                    <div class="col-xl-6 col-md-7 col-9">
                        <!-- w-100 for IE specific -->
                        <div class="card bg-transparent shadow-none">
                            <div class="card-content">
                                <div class="card-body text-center">
                                    <img src="'.$this->theme->baseUrl.'/app-assets/images/pages/500.png" class="img-fluid my-3" alt="branding logo">
                                    <h1 class="error-title mt-1">'.$statusCode.' - '.$name.'</h1>
                                    <p class="p-2">'.$message.'</p>
                                    <a href="index.html" class="btn btn-primary round glow">BACK TO HOME</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>';
        }else {
            $img = $this->theme->baseUrl.'/app-assets/images/pages/not-authorized.png';
            echo '<section class="row flexbox-container">
                    <div class="col-xl-7 col-md-8 col-12">
                        <div class="card bg-transparent shadow-none">
                            <div class="card-content">
                                <div class="card-body text-center">
                                    <img src="'.$this->theme->baseUrl.'/app-assets/images/pages/not-authorized.png" class="img-fluid" alt="not authorized" width="400">
                                    <h1 class="my-2 error-title">'.$statusCode.' - '.$name.'</h1>
                                    <p>'.$message.'</p>
                                    <a href="index" class="btn btn-primary round glow mt-2">BACK TO HOME</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>';
        }
}
?>
