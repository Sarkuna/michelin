<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Yii::t('app', 'Login');
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
?>


<!-- login page start -->
<section id="auth-login" class="row flexbox-container">
    <div class="col-xl-8 col-11">
        <div class="card bg-authentication mb-0">
            <div class="row m-0">
                <!-- left section-login -->
                <div class="col-md-6 col-12 px-0">
                    <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                        <div class="card-header pb-1">
                            <div class="card-title">
                                <?php
            if(!empty($session['currentLogo'])){
                echo '<img src="/upload/client_logos/'.$session['currentLogo'].'" class="img-responsive" width="100%">';                
            }else{
                echo '<img src="/images/logo.png" class="img-responsive avatar" style="margin-left: auto;margin-right: auto;">';
            }
            ?>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                
                                <?php $form = ActiveForm::begin(); ?>
                                    <?= $form->field($model, 'email', ['options' => [
                                            'tag' => 'div',
                                            'class' => 'form-group mb-50',
                                        ],
                                        'template' => '<label class="text-bold-600" for="exampleInputEmail1">'.$model->getAttributeLabel('email').'</label>{input}{error}{hint}'
                                    ])->textInput(['placeholder' => Yii::t('app', 'Email')])
                                    ?>
            
                                    <?= $form->field($model, 'password', ['options' => [
                                            'tag' => 'div',
                                            'class' => 'form-group',
                                        ],
                                        'template' => '<label class="text-bold-600" for="exampleInputPassword1">'.$model->getAttributeLabel('password').'</label>{input}{error}{hint}'
                                    ])->passwordInput(['placeholder' => Yii::t('app', 'Password')])
                                    ?>
            
                
                                    <div class="form-group d-flex flex-md-row flex-column justify-content-between align-items-center">
                                        <div class="text-left">
                                            <?= $form->field($model, 'rememberMe', ['options' => [
                                                    'tag' => 'div',
                                                    'class' => '',
                                                ],
                                                'template' => "<div class='checkbox checkbox-sm'>{input}<label class='checkboxsmall' for='exampleCheck1'><small>".$model->getAttributeLabel('rememberMe')."</small></label>{error}</div>"
                                            ])->checkbox(['class' => 'form-check-input', 'id' => 'exampleCheck1'],false)
                                            ?>
                                        </div>
                                        <div class="text-right" style="margin-top: -20px;"><a href="/site/request-password-reset" class="card-link"><small><?= Yii::t('app', 'Forgot Password?') ?></small></a></div>
                                    </div>
                                    <?= Html::submitButton(Yii::t('app', 'Login').'<i id="icon-arrow" class="bx bx-right-arrow-alt"></i>', ['class' => 'btn btn-primary glow w-100 position-relative', 'name' => 'login-button']) ?>                    

                                <?php ActiveForm::end(); ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- right section image -->
                <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                    <div class="card-content">
                        <img class="img-fluid" src="<?php echo $this->theme->baseUrl ?>/app-assets/images/pages/login.png" alt="branding logo">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- login page ends -->