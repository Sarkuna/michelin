<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', "Change Password"); 
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><?= Yii::t('app','Change Your Password') ?></h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        
                        <?php $form = ActiveForm::begin(['method' => 'post', 'id' => 'change-password-form', 'options' => [
                            'class' => 'form form-horizontal',
                            'novalidate' => ''
                         ]]); ?>
                        <div class="form-body">
                            <div class="row">
                            <?=
                            $form->field($model, 'password', [
                                'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                                'options' => [
                                    'class' => 'col-12 col-md-6',
                                    //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                                ],
                            ])->passwordInput();
                            ?>

                            <?=
                            $form->field($model, 'repassword', [
                                'template' => "<div class='form-group row'><div class='col-md-4'><span>{label}</span></div><div class='col-md-8'>{input}\n{hint}\n{error}</div></div>",
                                'options' => [
                                    'class' => 'col-12 col-md-6',
                                    //'tag' => ['class' => 'dddd'], // Don't wrap with "form-group" div
                                ],
                            ])->passwordInput();
                            ?>
                            <div class="col-md-10 offset-md-2">
                                <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary mr-1 mb-1 waves-effect waves-light']) ?>
                            </div>
                        </div>
                        </div>        
                        <?php ActiveForm::end(); ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
