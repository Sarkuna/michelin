<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$session = Yii::$app->session;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- forgot password start -->
<section class="row flexbox-container">
    <div class="col-xl-7 col-md-9 col-10  px-0">
        <div class="card bg-authentication mb-0">
            <div class="row m-0">
                <!-- left section-forgot password -->
                <div class="col-md-6 col-12 px-0">
                    <div class="card disable-rounded-right mb-0 p-2">
                        <div class="card-header pb-1">
                            <div class="card-title text-center">
                                <?php
            if(!empty($session['currentLogo'])){
                echo '<img src="/upload/client_logos/'.$session['currentLogo'].'" class="img-responsive" width="100%">';                
            }else{
                echo '<img src="/images/logo.png" class="img-responsive avatar" style="margin-left: auto;margin-right: auto;">';
            }
            ?>
                                <h4 class="text-center mb-2 mt-2"><?= Yii::t('app', 'Forgot Password?') ?></h4>
                            </div>
                        </div>

                        <div class="card-content">
                            <div class="card-body">
                                <div class="text-muted text-center mb-2"><small><?= Yii::t('app', 'Enter the email you used when you joined and we will send you temporary password') ?></small></div>
                                <?php $form = ActiveForm::begin(['class' => 'mb-2']); ?> 
                                    <?= $form->field($model, 'email', ['options' => [
                                        'tag' => 'div',
                                        'class' => 'form-group mb-2',
                                        ],
                                        'template' => '<label class="text-bold-600" for="exampleInputEmailPhone1">'.Yii::t('app', 'Email').'</label>{input}{error}{hint}'
                                        ]);
                                    ?>

                                    <button type="submit" class="btn btn-primary glow position-relative w-100"><?= Yii::t('app', 'Send Password') ?><i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                                <?php ActiveForm::end(); ?>
                                <div class="text-center mb-2"><a href="/site/login"><small class="text-muted"><?= Yii::t('app', 'I remembered my password') ?></small></a></div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- right section image -->
                <div class="col-md-6 d-md-block d-none text-center align-self-center">
                    <img class="img-fluid" src="<?php echo $this->theme->baseUrl ?>/app-assets/images/pages/forgot-password.png" alt="branding logo" width="300">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- forgot password ends -->