<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- reset password start -->
<section class="row flexbox-container">
    <div class="col-xl-7 col-10">
        <div class="card bg-authentication mb-0">
            <div class="row m-0">
                <!-- left section-login -->
                <div class="col-md-6 col-12 px-0">
                    <div class="card disable-rounded-right d-flex justify-content-center mb-0 p-2 h-100">
                        <div class="card-header pb-1">
                            <div class="card-title">
                                <h4 class="text-center mb-2"><?= Yii::t('app', 'Reset your Password') ?></h4>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <?php $form = ActiveForm::begin(['options' => ['class'=>'mb-2']]); ?>
                                    <?= $form->field($model, 'password', ['options' => [
                                        'tag' => 'div',
                                        'class' => 'form-group',
                                        ],
                                        'template' => '<label class="text-bold-600" for="exampleInputPassword1">'.Yii::t('app', 'New Password').'</label>{input}{error}{hint}'
                                        ])->passwordInput(['placeholder' => Yii::t('app', 'Enter a new password')]);
                                    ?>
                                    <?= $form->field($model, 'confirm_password', ['options' => [
                                        'tag' => 'div',
                                        'class' => 'form-group',
                                        ],
                                        'template' => '<label class="text-bold-600" for="exampleInputPassword1">'.Yii::t('app', 'Re-type New Password').'</label>{input}{error}{hint}'
                                        ])->passwordInput(['placeholder' => Yii::t('app', 'Confirm your new password')]);
                                    ?>
                                <?= Html::submitButton(Yii::t('app', 'Reset my password').'<i id="icon-arrow" class="bx bx-right-arrow-alt"></i>', ['class' => 'btn btn-primary glow position-relative w-100']) ?>
                                <?php ActiveForm::end(); ?> 
                            </div>
                        </div>
                    </div>
                </div>
                <!-- right section image -->
                <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                    <img class="img-fluid" src="<?php echo $this->theme->baseUrl ?>/app-assets/images/pages/reset-password.png" alt="branding logo">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- reset password ends -->