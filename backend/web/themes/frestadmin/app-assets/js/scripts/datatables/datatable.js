/*=========================================================================================
    File Name: datatables-basic.js
    Description: Basic Datatable
    ----------------------------------------------------------------------------------------
    Item Name: Frest HTML Admin Template
    Version: 1.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(document).ready(function() {

    /****************************************
    *       js of zero configuration        *
    ****************************************/
    $('.zero-configuration2').DataTable({
        dom: 'Bfrtip',
        buttons: [
            /*{
                extend: 'csvHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },*/
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ]
    }
    );
    $('.zero-configuration').DataTable();
    
    $('.custom-configuration').DataTable({
            dom:
                    '<"top d-flex flex-wrap"<"action-filters flex-grow-1"f><"actions action-btns d-flex align-items-center">><"clear">rt<"bottom"p>',
            language: {
                search: "",
                searchPlaceholder: "Search"
            },
            scroller: {
                loadingIndicator: true
            },
        });

    /********************************************
     *        js of Order by the grouping        *
     ********************************************/

    var groupingTable = $('.row-grouping').DataTable({
        "columnDefs": [{
            "visible": false,
            "targets": 2
        }],
        "order": [
            [2, 'asc']
        ],
        "displayLength": 10,
        "drawCallback": function(settings) {
            var api = this.api();
            var rows = api.rows({
                page: 'current'
            }).nodes();
            var last = null;

            api.column(2, {
                page: 'current'
            }).data().each(function(group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                        '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                    );

                    last = group;
                }
            });
        }
    });

    $('.row-grouping tbody').on('click', 'tr.group', function() {
        var currentOrder = groupingTable.order()[0];
        if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
            groupingTable.order([2, 'desc']).draw();
        }
        else {
            groupingTable.order([2, 'asc']).draw();
        }
    });

    /*************************************
    *       js of complex headers        *
    *************************************/

    $('.complex-headers').DataTable();


    /*****************************
    *       js of Add Row        *
    ******************************/

    var t = $('.add-rows').DataTable();
    var counter = 2;

    $('#addRow').on( 'click', function () {
        t.row.add( [
            counter +'.1',
            counter +'.2',
            counter +'.3',
            counter +'.4',
            counter +'.5'
        ] ).draw( false );

        counter++;
    });


    /**************************************************************
    * js of Tab for COLUMN SELECTORS WITH EXPORT AND PRINT OPTIONS *
    ***************************************************************/

    $('.dataex-html5-selectors').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                text: 'JSON',
                action: function ( e, dt, button, config ) {
                    var data = dt.buttons.exportData();

                    $.fn.dataTable.fileSave(
                        new Blob( [ JSON.stringify( data ) ] ),
                        'Export.json'
                    );
                }
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: ':visible'
                }
            }
        ]
    });
    
    $('.dataex-html5-selectors2').DataTable( {
        dom:'B<"col-12"<"left-col"l><"right-col"f>>rtip',
        buttons: {
            buttons: [
                /*{                
                    text: 'New',
                    tag: "button",
                    className: "btn btn-info",  
                    //className: 'btn-primary',
                    action: function ( e, dt, node, config ) {
                        onclick (window.location.href='/admin/internal-invoicing/create')
                    }
                },*/
                {
                    extend: 'excelHtml5',
                    className: "btn btn-primary",
                    exportOptions: {
                        columns: ':visible'
                    }
                }
            ],
            dom: {
                button: {
                    className: 'btn'
                },
                buttonLiner: {
                    tag: null
                }
            }
        }
        
    });
    
    if ($(".dataex-html5-selectors22").length) {
        $(".dataex-html5-selectors22").DataTable({
            columnDefs: [
                {
                    targets: 0,
                    className: "control"
                },
                {
                    orderable: true,
                    targets: 1,
                    checkboxes: {selectRow: true}
                },
                {
                    targets: [0, 1],
                    orderable: false
                },
            ],
            order: [2, 'asc'],
            dom:
                    '<"top d-flex flex-wrap"<"action-filters flex-grow-1"f><"actions action-btns d-flex align-items-center">><"clear">rt<"bottom"p>',
            language: {
                search: "",
                searchPlaceholder: "Search Invoice"
            },
            select: {
                style: "multi",
                selector: "td:first-child",
                items: "row"
            },
            responsive: {
                details: {
                    type: "column",
                    target: 0
                }
            }
        });
    }
    
    $('.dataex-html5-selectors3').DataTable( {
        //lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        //dom: 'Blfrtip',
        dom:'B<"col-12"<"left-col"l><"right-col"f>>rtip',
        buttons: {
            buttons: [
                /*{                
                    text: 'New Payment',
                    tag: "submit",
                    className: "btn btn-info",
                    attr: {id: 'btnsubmit' },
                    //className: 'btn-primary',
                    action: function ( e, dt, node, config ) {
                        this.submit();
                    }
                },*/
                {
                    extend: 'excelHtml5',
                    className: "btn btn-primary",
                    exportOptions: {
                        columns: ':visible'
                    }
                }
            ],
            dom: {
                button: {
                    className: 'btn'
                },
                buttonLiner: {
                    tag: null
                }
            }
        }
        
    });

    /**************************************************
    *       js of scroll horizontal & vertical        *
    **************************************************/

    $('.scroll-horizontal-vertical').DataTable( {
        "scrollY": 200,
        "scrollX": true
    });
    
    
     $('#dealerlist').DataTable( {
        serverSide: true,
        ordering: false,
        searching: true,
        ajax: function ( data, callback, settings ) {
            var out = [];
 
            for ( var i=data.start, ien=data.start+data.length ; i<ien ; i++ ) {
                out.push( [ i+'-1', i+'-2', i+'-3', i+'-4', i+'-5' ] );
            }
 
            setTimeout( function () {
                callback( {
                    draw: data.draw,
                    data: out,
                    recordsTotal: 5000000,
                    recordsFiltered: 5000000
                } );
            }, 50 );
        },
        scrollY: 200,
        scroller: {
            loadingIndicator: true
        },
    } );
});