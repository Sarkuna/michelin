<?php

namespace common\components;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\base\Component;
use yii\base\Exception;
use mPDF;
use kartik\mpdf\Pdf;

use common\models\PointCycle;

class VIPGlobal extends Component {
    
    public function getProfileInfo() {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $userID = \Yii::$app->user->id;

        $profile = \common\models\VIPCustomer::find()
            ->where(['userID' => $userID])
            ->one();
        $company = \common\models\CompanyInformation::find()
            ->where(['user_id' => $userID])
            ->one();
        
        //$session['distributors'] = $profile->kis_category_id;
        //$picPro = '<img alt="avatar" src="'.$this->Avatar($profile->full_name).'" class="round" height="40" width="40">';
        $users = [
            'full_name' => $profile->full_name,
            'code' => $profile->clients_ref_no,
            'company_name' => $company->company_name,
        ];

        return $users;
    }
    
    public function getSiteInfo() {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        //$userID = \Yii::$app->user->id;

        $client = \common\models\Client::find()->one();

        if (!empty($client->company_logo)) {
            $logo = Yii::getAlias('@back') . '/upload/client_logos/' . $client->company_logo;
        } else {
            $logo = Yii::getAlias('@back') . '/images/logo.png';
        }
        
        //$logo = Yii::getAlias('@back') . '/images/logo.png';
        $siteinfo = [
            'logo' => $logo,
            'membership' => $client->membership,
            'mobile_app' => $client->mobile_app,
            'site_offline' => $client->site_offline,
        ];

        return $siteinfo;
    }
    
    public function getTotalcostem($productID) {
        $point_value = $this->clientSetup()['point_value'];
        $model = \common\models\VIPProduct::find()->where(['product_id' => $productID])->one();
        $display_price0 = $model->price / 100 * $model->mark_up;
        $display_price = $model->price + $display_price0;

        $total_price = $display_price + $model->handling_fee_em + $model->insurance_em + $model->shipping_em;
        $display_point = $total_price / $point_value;
        return round($display_point);
    }
    
    public function getTotalcostwm($productID) {
        $point_value = $this->clientSetup()['point_value'];
        $model = \common\models\VIPProduct::find()->where(['product_id' => $productID])->one();
        $display_price0 = $model->price / 100 * $model->mark_up;
        $display_price = $model->price + $display_price0;

        $total_price = $display_price + $model->handling_fee_wm + $model->insurance_wm + $model->shipping_wm;
        $display_point = $total_price / $point_value;
        return round($display_point);
    }
    
    public function getTotalcostpoint($productID,$states) {
        if ($states == 1) {
            $point_value = $this->clientSetup()['point_value'];
            $model = \common\models\VIPProduct::find()->where(['product_id' => $productID])->one();
            $display_price0 = $model->price / 100 * $model->mark_up;
            $display_price = $model->price + $display_price0;

            $total_price = $display_price + $model->handling_fee_em + $model->insurance_em + $model->shipping_em;
            $display_point = $total_price / $point_value;
            return round($display_point);
        } else if ($states == 2) {
            $point_value = $this->clientSetup()['point_value'];
            $model = \common\models\VIPProduct::find()->where(['product_id' => $productID])->one();
            $display_price0 = $model->price / 100 * $model->mark_up;
            $display_price = $model->price + $display_price0;

            $total_price = $display_price + $model->handling_fee_wm + $model->insurance_wm + $model->shipping_wm;
            $display_point = $total_price / $point_value;
            return round($display_point);
        } else {
            return 0;
        }
    }
    
    public function getTotalcostprice($productID,$states) {
        if ($states == 1) {
            $point_value = $this->clientSetup()['point_value'];
            $model = \common\models\VIPProduct::find()->where(['product_id' => $productID])->one();
            $display_price0 = $model->price / 100 * $model->mark_up;
            $display_price = $model->price + $display_price0;

            $total_price = $display_price + $model->handling_fee_em + $model->insurance_em + $model->shipping_em;
            //$display_point = $total_price / $point_value;
            return round($total_price);
        } else if ($states == 2) {
            $point_value = $this->clientSetup()['point_value'];
            $model = \common\models\VIPProduct::find()->where(['product_id' => $productID])->one();
            $display_price0 = $model->price / 100 * $model->mark_up;
            $display_price = $model->price + $display_price0;

            $total_price = $display_price + $model->handling_fee_wm + $model->insurance_wm + $model->shipping_wm;
            //$display_point = $total_price / $point_value;
            return round($total_price);
        } else {
            return 0.00;
        }
    }
    
    public function passwordResetEmail($email,$username,$resetLink) {
        $session = Yii::$app->session;
        $client = \common\models\Client::findOne($session['currentclientID']);
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => 'RP05', 'clientID' => $session['currentclientID']])
                ->one();

        $emailSubjectTemp = $emailTemplate->subject;
        $emailSubject = $emailSubjectTemp;
        $emailSubject = str_replace('{name}', $username, $emailSubject);
        
        $emailBodyTemp = $emailTemplate->template;
        $emailBody = $emailBodyTemp;
        $emailBody = str_replace('{name}', $username, $emailBody);
        $emailBody = str_replace('{resetLink}', $resetLink, $emailBody);

        Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([Yii::$app->params['supportEmail'] => 'support'])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();
    }

    public function sendEmail($userId, $emailTemplateCode, $data = null, $subject = null)
    {
        $session = Yii::$app->session;
        $client = \common\models\Client::findOne($session['currentclientID']);
        //$siteUrl = Yii::$app->request->hostInfo;
        $siteUrl = $client->cart_domain;
        $admin_url = $client->admin_domain;
        $client_name = $client->company;
        $client_email = $client->clientAddress->email_address;
        $client_programme_title = $client->programme_title;
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode, 'clientID' => $session['currentclientID']])
                ->one();
        
        $emailSubject = $emailTemplate->subject;

        $profile = \common\models\VIPCustomer::find()->where(['userID' => $userId])->one();
        $name = $profile->full_name;
        $email = $profile->user->email;
        
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }

        $password = isset($data2['password']) ? $data2['password'] : null;
        $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;
        $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;
        $verification_email_key = isset($data2['verification_email_key']) ? $data2['verification_email_key'] : null;
        
        $emailBody = $emailTemplate->template;
        
        $emailSubject = str_replace('{name}', $name, $emailSubject);
        
        $emailBody = str_replace('{name}', $name, $emailBody);
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{password}', $password, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);
        $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);
        $emailBody = str_replace('{store_name}', $client_name, $emailBody);
        $emailBody = str_replace('{client_name}', $client_name, $emailBody);
        $emailBody = str_replace('{programme_title}', $client_programme_title, $emailBody);
        $emailBody = str_replace('{verification_email_key}', $verification_email_key, $emailBody);
        
        $emailBody = str_replace('/upload', $siteUrl . "/upload", $emailBody);

        if(!empty($client->email)){
            $bccemail = $client->email;
            Yii::$app->mailer->compose()
            ->setTo($email)
            ->setBcc($bccemail)
            ->setFrom([$client_email =>  $client_name])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();
        }else {
            Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$client_email =>  $client_name])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();
        }
        
    }
    
    public function sendEmailAdmin($userId, $emailTemplateCode, $data = null)
    {
        $session = Yii::$app->session;
        $client = \common\models\Client::findOne($session['currentclientID']);
        //$siteUrl = Yii::$app->request->hostInfo;
        $siteUrl = $client->cart_domain;
        $admin_url = $client->admin_domain;
        $client_name = $client->company;
        $client_email = $client->clientAddress->email_address;
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['clientID' => $session['currentclientID'],'code' => $emailTemplateCode])
                ->one();
        
        $emailSubject = $emailTemplate->subject;

        $profile = \common\models\User::find()->where(['id' => $userId])->one();
        $email = $profile->email;
        
        $UserProfile = \common\models\UserProfile::find()->where(['userID' => $userId])->one();
        $admin_name = $UserProfile->first_name.' '.$UserProfile->last_name; 
        
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }

        $password = isset($data2['password']) ? $data2['password'] : null;
        $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;

        $emailBody = $emailTemplate->template;

        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{password}', $password, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);
        $emailBody = str_replace('{name}', $admin_name, $emailBody);
        $emailBody = str_replace('{admin_name}', $admin_name, $emailBody);
        $emailBody = str_replace('{admin_url}', $admin_url, $emailBody);
        $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);
        
        if(!empty($client->email)){
            $bccemail = $client->email;
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setBcc($bccemail)
                ->setFrom([$client_email =>  $client_name])
                ->setSubject($emailSubject)
                ->setHtmlBody($emailBody)
                ->send();
        }else {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$client_email =>  $client_name])
                ->setSubject($emailSubject)
                ->setHtmlBody($emailBody)
                ->send();
        }
    }
    
    
    public function sendEmailOrder($order_id, $emailTemplateCode, $data = null)
    {
        $session = Yii::$app->session;
        $comment ='';
        $order = \common\models\VIPOrder::find()->where(['order_id' => $order_id])->one();

        $name = $order->shipping_firstname;
        $email = $order->email;
        
        $order_date = date('d/m/Y', strtotime($order->created_datetime));
        $order_id = $order->invoice_prefix;
        $ip = $order->ip;
        $status_name = $order->orderStatus->name;
        $comment = $order->comment;
        $telephone = $order->telephone_no;
        $mobile = $order->mobile_no;    
        $payment_address = $order->shipping_firstname.'<br>'.$order->shipping_address_1.'<br>'.$order->shipping_address_2.'<br>'.$order->shipping_city.'<br>'.$order->shipping_postcode.'<br>'.$order->shipping_zone.'<br>'.$order->shipping_country_id;
        $shipping_address = $order->shipping_firstname.'<br>'.$order->shipping_address_1.'<br>'.$order->shipping_address_2.'<br>'.$order->shipping_city.'<br>'.$order->shipping_postcode.'<br>'.$order->shipping_zone.'<br>'.$order->shipping_country_id;
        $tbl = '<table border="1" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse;border:none">
	<thead>
		<tr>
                    <td style="border:solid #dddddd 1.0pt;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p style="margin-bottom:15.0pt"><b><span style="font-size:9.0pt;color:#222222">Product</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p style="margin-bottom:15.0pt"><b><span style="font-size:9.0pt;color:#222222">Model</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt;color:#222222">Quantity</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt;color:#222222">Unit Points</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt;color:#222222">Total</span></b></p>
                    </td>
                </tr>
	</thead>
	<tbody>';
        
        $total = '';
        foreach ($order->orderProducts as $subproduct) {
            //echo $subproduct->name.'<br>';
            $optlisit = '';
            $OrderOption = \common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->count();
            if ($OrderOption > 0) {
                $OrderOptionlisits = \common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->all();
                foreach ($OrderOptionlisits as $OrderOptionlisit) {
                        $optlisit .= '<br>&nbsp;<small> - ' . $OrderOptionlisit->name . ': ' . $OrderOptionlisit->value . '</small>';
                    }
                }
                
                $tbl .= '<tr style="height:1.15pt">
                    <td style="border:solid #dddddd 1.0pt;border-top:none;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p style="margin-bottom:15.0pt"><span style="font-size:9.0pt">' . $subproduct->name . '</span> <span style="font-size:8.0pt">' . $optlisit . ' </span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p style="margin-bottom:15.0pt"><span style="font-size:9.0pt">' . $subproduct->model . '</span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . $subproduct->quantity . '</span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . Yii::$app->formatter->asInteger($subproduct->point) . 'pts</span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . Yii::$app->formatter->asInteger($subproduct->point_total) . 'pts</span></p>
                    </td>
                </tr>';
                $total += $subproduct->point_total;
        }
        
        $tbl .= '<tr style="height:1.15pt">
            <td colspan="4" style="border:solid #dddddd 1.0pt;border-top:none;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt">Total:</span></b><span style="font-size:9.0pt"></span></p>
            </td>
            <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . Yii::$app->formatter->asInteger($total) . 'pts</span></p>
            </td>
        </tr>
        </tbody></table>';
        
        $products = $tbl;
        
        $client = \common\models\Client::findOne($session['currentclientID']);
        //$siteUrl = Yii::$app->request->hostInfo;
        $siteUrl = $client->cart_domain;
        $siteUrl2 = $client->admin_domain;
        $client_name = $client->company;
        $client_email = $client->clientAddress->email_address;

        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode])
                ->one();
        
        $emailSubject = $emailTemplate->subject;
        $emailSubject = str_replace('{order_id}', $order_id, $emailSubject);
        $emailSubject = str_replace('{status_name}', $status_name, $emailSubject);
        
        
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }
        
        //$name = isset($data2['name']) ? $data2['name'] : null;
        //$email = isset($data2['email']) ? $data2['email'] : null;
        $password = isset($data2['password']) ? $data2['password'] : null;
        $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;
        $email_comment = isset($data2['email_comment']) ? $data2['email_comment'] : null;

        $emailBody = $emailTemplate->template;

        $emailBody = str_replace('{name}', $name, $emailBody);
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{password}', $password, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);

        $emailBody = str_replace('{store_name}', $client_name, $emailBody);
        $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);
        //$emailBody = str_replace('/files/'.$session['currentclientID'].'', $siteUrl2.'/files/'.$session['currentclientID'].'', $emailBody);
        
        //$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['accounts/my-account/view', 'id' => $order_id]);
        $resetLink = '<a href="' . Url::to([$siteUrl2.'/accounts/my-account/view', 'id' => $order_id]) . '" title="View Order Browser" class = "btn btn-info btn-sm">View Order</a>';
        $emailBody = str_replace('{order_link}', $resetLink, $emailBody);
        $emailBody = str_replace('{order_id}', $order_id, $emailBody);
        $emailBody = str_replace('{order_date}', $order_date, $emailBody);
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{telephone}', $telephone, $emailBody);
        $emailBody = str_replace('{mobile}', $mobile, $emailBody);
        $emailBody = str_replace('{ip}', $ip, $emailBody);
        $emailBody = str_replace('{status_name}', $status_name, $emailBody);
        $emailBody = str_replace('{comment}', $comment, $emailBody);
        $emailBody = str_replace('{payment_address}', $payment_address, $emailBody);
        $emailBody = str_replace('{shipping_address}', $shipping_address, $emailBody);
        $emailBody = str_replace('{products}', $products, $emailBody);
        $emailBody = str_replace('{email_comment}', $email_comment, $emailBody);
        
        if(!empty($client->email)){
            $bccemail = $client->email;
            Yii::$app->mailer->compose()
            ->setTo($email)
            ->setBcc($bccemail)
            ->setFrom([$client_email =>  $client_name])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();
        }else {
            Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$client_email =>  $client_name])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();
        }
        
    }
    
    public function sendEmailNewOrder($order_id, $emailTemplateCode, $data = null)
    {
        $session = Yii::$app->session;
        $comment ='';
        $order = \common\models\VIPOrder::find()->where(['order_id' => $order_id])->one();

        $name = $order->shipping_firstname;
        $email = $order->email;
        $code = $order->customer_group_id;
        $company_name = $order->shipping_company;

        $userId = $order->customer_id;
        $order_date = date('d/m/Y', strtotime($order->created_datetime));
        $order_id = $order->invoice_prefix;
        $ip = $order->ip;
        $status_name = $order->orderStatus->name;
        $comment = $order->comment;
        $telephone = $order->telephone_no;
        $mobile = $order->mobile_no;
        
        $address = $order->shipping_firstname.'<br>'.$order->shipping_lastname.'<br>'.$order->shipping_address_1.'<br>'.$order->shipping_address_2.'<br>'.$order->shipping_city.'<br>'.$order->shipping_postcode.'<br>'.$order->shipping_zone.'<br>'.$order->shipping_country_id;
        $payment_address = $address;
        $shipping_address = $address;
        $tbl = '<table border="1" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%;border-collapse:collapse;border:none">
	<thead>
		<tr>
                    <td style="border:solid #dddddd 1.0pt;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p style="margin-bottom:15.0pt"><b><span style="font-size:9.0pt;color:#222222">Product</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p style="margin-bottom:15.0pt"><b><span style="font-size:9.0pt;color:#222222">Model</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt;color:#222222">Quantity</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt;color:#222222">Unit Points</span></b></p>
                    </td>
                    <td style="border:solid #dddddd 1.0pt;border-left:none;background:#efefef;padding:5.25pt 5.25pt 5.25pt 5.25pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt;color:#222222">Total</span></b></p>
                    </td>
                </tr>
	</thead>
	<tbody>';
        
        $total = '';
        foreach ($order->orderProducts as $subproduct) {
            //echo $subproduct->name.'<br>';
            $optlisit = '';
            $OrderOption = \common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->count();
            if ($OrderOption > 0) {
                $OrderOptionlisits = \common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->all();
                foreach ($OrderOptionlisits as $OrderOptionlisit) {
                        $optlisit .= '<br>&nbsp;<small> - ' . $OrderOptionlisit->name . ': ' . $OrderOptionlisit->value . '</small>';
                    }
                }
                
                $tbl .= '<tr style="height:1.15pt">
                    <td style="border:solid #dddddd 1.0pt;border-top:none;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p style="margin-bottom:15.0pt"><span style="font-size:9.0pt">' . $subproduct->name . '</span> <span style="font-size:8.0pt">' . $optlisit . ' </span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p style="margin-bottom:15.0pt"><span style="font-size:9.0pt">' . $subproduct->model . '</span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . $subproduct->quantity . '</span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . Yii::$app->formatter->asInteger($subproduct->point + $subproduct->shipping_point) . 'pts</span></p>
                    </td>
                    <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                        <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . Yii::$app->formatter->asInteger($subproduct->point_total + + $subproduct->shipping_point_total) . 'pts</span></p>
                    </td>
                </tr>';
                $total += $subproduct->point_total + $subproduct->shipping_point_total;
        }
        
        $tbl .= '<tr style="height:1.15pt">
            <td colspan="4" style="border:solid #dddddd 1.0pt;border-top:none;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                <p align="right" style="margin-bottom:15.0pt;text-align:right"><b><span style="font-size:9.0pt">Total:</span></b><span style="font-size:9.0pt"></span></p>
            </td>
            <td style="border-top:none;border-left:none;border-bottom:solid #dddddd 1.0pt;border-right:solid #dddddd 1.0pt;padding:5.25pt 5.25pt 5.25pt 5.25pt;height:1.15pt">
                <p align="right" style="margin-bottom:15.0pt;text-align:right"><span style="font-size:9.0pt">' . Yii::$app->formatter->asInteger($total) . 'pts</span></p>
            </td>
        </tr>
        </tbody></table>';
        
        $products = $tbl;
        
        $client = \common\models\Client::find()->one();
        //$siteUrl = Yii::$app->request->hostInfo;
        $siteUrl = $client->cart_domain;
        $siteUrl2 = $client->admin_domain;
        $client_name = $client->company;
        $client_email = $client->clientAddress->email_address;
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode])
                ->one();
        
        $emailSubject = $emailTemplate->subject;
        $emailSubject = str_replace('{order_id}', $order_id, $emailSubject);

        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }

        $password = isset($data2['password']) ? $data2['password'] : null;
        $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;

        $emailBody = $emailTemplate->template;

        $emailBody = str_replace('{name}', $name, $emailBody);
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{password}', $password, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);

        $emailBody = str_replace('{store_name}', $client_name, $emailBody);
        $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);
        $emailBody = str_replace('/files/'.$session['currentclientID'].'', $siteUrl2.'/files/'.$session['currentclientID'].'', $emailBody);
        
        //$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['accounts/my-account/view', 'id' => $order_id]);
        $resetLink = '<a href="' . Url::to([$siteUrl2.'/accounts/my-account/view', 'id' => $order_id]) . '" title="View Order Browser" class = "btn btn-info btn-sm">View Order</a>';
        $emailBody = str_replace('{order_link}', $resetLink, $emailBody);
        $emailBody = str_replace('{order_id}', $order_id, $emailBody);
        $emailBody = str_replace('{order_date}', $order_date, $emailBody);
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{telephone}', $telephone, $emailBody);
        $emailBody = str_replace('{mobile}', $mobile, $emailBody);
        $emailBody = str_replace('{ip}', $ip, $emailBody);
        $emailBody = str_replace('{status_name}', $status_name, $emailBody);
        $emailBody = str_replace('{comment}', $comment, $emailBody);
        $emailBody = str_replace('{payment_address}', $payment_address, $emailBody);
        $emailBody = str_replace('{shipping_address}', $shipping_address, $emailBody);
        $emailBody = str_replace('{products}', $products, $emailBody);
        $emailBody = str_replace('{dealer_code}', $code, $emailBody);
        $emailBody = str_replace('{company_name}', $company_name, $emailBody);
        
        if(!empty($email)) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setCc([$client_email =>  $client_name.'-'.$order_id])    
                ->setBcc(\Yii::$app->params['copyemail'])   
                ->setFrom([Yii::$app->params['noreplyEmail'] =>  $client_name])
                ->setSubject($emailSubject)
                ->setHtmlBody($emailBody)
                ->send();
        }else {
            Yii::$app->mailer->compose()
            ->setTo([$client_email =>  $client_name.'-'.$order_id])
            ->setBcc(\Yii::$app->params['copyemail'])         
            ->setFrom([Yii::$app->params['noreplyEmail'] =>  $client_name])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();
        }
    }
    
    /*public function sendEmail($userId, $emailTo, $emailTemplateCode, $data = null, $subject = null)
    {
        $siteUrl = Yii::$app->request->hostInfo;
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode])
                ->one();

        $customer = \common\models\VIPCustomer::find()
                ->where(['userID' => $userId])
                ->one();
        
        $client_name = $customer->full_name;

        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }
        
        $name = isset($data2['name']) ? $data2['name'] : null;
        $email = isset($data2['email']) ? $data2['email'] : null;
        $password = isset($data2['password']) ? $data2['password'] : null;
        $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;
        $verification_email_key = isset($data2['verification_email_key']) ? $data2['verification_email_key'] : null;

        if ($subject) {
            $emailSubject = $subject;
        } else {
            $emailSubject = $emailTemplate->subject;
            $emailSubject = str_replace('{membership_ID}', $membership_ID, $emailSubject);
            $emailSubject = str_replace('{name}', $name, $emailSubject);
        }
        
        $emailBody = $emailTemplate->template;

        $emailBody = str_replace('{name}', $name, $emailBody);
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{password}', $password, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);
        $emailBody = str_replace('{verification_email_key}', $verification_email_key, $emailBody);
        $emailBody = str_replace('{client_name}', $client_name, $emailBody);
        $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);

        Yii::$app->mailer->compose()
            ->setTo($emailTo)
            ->setFrom([Yii::$app->params['supportEmail'] =>  ''])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();
    }*/
    
    /*function ismscURL($link) {
        $http = curl_init($link);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        return $http_result;
    }*/
    public static function stock($productID)
    {
        //$totalorder = \common\models\VIPOrderProduct::find()->where(['product_id' => $productID])->count();
        $instock = \common\models\VIPProduct::find()->where(['product_id' => $productID])->one();
        //$totalstock = $instock->quantity;
        $totalstock = $instock->quantity;
        return $totalstock;
    }
    
    public static function limited($productID)
    {
        //$productID = 481;
        //$totalorder = \common\models\VIPOrderProduct::find()->where(['product_id' => $productID])->count();
        $instock = \common\models\VIPProduct::find()->where(['product_id' => $productID, 'type' => 'michelin'])->count();
        //$instock = 1;
        if(!empty($instock)) {
            $totalqty = \common\models\VIPOrder::find()
                ->joinWith('vipOrderProduct')
                ->where(['customer_id' => Yii::$app->user->id, 'vip_order_product.product_id' => $productID])->sum('vip_order_product.quantity');
            if($totalqty >= Yii::$app->params['custom.limit']) {
                $totalstock = Yii::$app->params['custom.limit']; 
            }else {
                $totalstock = Yii::$app->params['custom.limit'] - $totalqty;
            }
            //$totalstock = $totalqty;
            
        }else {
            $totalstock = 0;
        }
        return $totalstock;
    }
            
    
    public static function myPoint($productID)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $pervalue = 0;
        $perpice = 0;
        
        $product = \common\models\VIPProduct::find()
           ->where(['product_id' => $productID])   
           ->one();
        $productvalue = $product->price;
        
        return round($productvalue);
    }
    
    public static function myPrice($productID)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $pervalue = 0;
        $perpice = 0;
        $client = \common\models\ClientOption::find()->where(['clientID' => $clientID])->one();

        /*$assignproduct = \common\models\VIPAssignProducts::find()
           ->select('p_type, p_value, msrp')
           ->where(['productID' => $productID, 'clientID' => $clientID])
           //->andWhere(['not', ['p_type' => null]])     
           ->one();*/
        $product = \common\models\VIPProduct::find()
           ->where(['product_id' => $productID])   
           ->one();
        $productvalue = $product->price;
        
        if(count($client) > 0) {
            $point_value = $client->point_value;
            $pervalue = $client->bb_percent;
        }else {
            $point_value = '0.01';
        }
        
        //$msrp = $assignproduct->msrp;
        $perpice =  $pervalue / 100 * $productvalue;
        /*if(!empty($assignproduct->p_type)) {
            if($assignproduct->p_type == 'P') {
                $pervalue = $assignproduct->p_value;
                $perpice =  $pervalue / 100 * $msrp;
            }else if($assignproduct->p_type == 'R') {
                $pervalue = $assignproduct->p_value;
                $perpice =  $pervalue;
            }
        }else {
            $perpice =  $pervalue / 100 * $msrp;
        }*/
        

        $totalprice = $productvalue + $perpice;

        //$productvalue = round($totalprice) / $point_value;
        
        return $totalprice;
    }
    
    public static function myPriceInvoice($productID)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $pervalue = 0;
        $perpice = 0;
        $client = \common\models\ClientOption::find()->where(['clientID' => $clientID])->one();
        
        $voucherproduct = \common\models\VIPProduct::find()
           ->where(['product_id' => $productID, 'voucher' => 'Y']) 
           ->count();
        if($voucherproduct > 0) {
            $assignproduct = \common\models\VIPAssignProducts::find()
               ->select('p_type, p_value, msrp')
               ->where(['productID' => $productID, 'clientID' => $clientID])
               //->andWhere(['not', ['p_type' => null]])     
               ->one();

            if(count($client) > 0) {
                $point_value = $client->point_value;
                $pervalue = $client->bb_percent;
            }else {
                $point_value = '0.01';
            }

            $msrp = $assignproduct->msrp;
            if(!empty($assignproduct->p_type)) {
                if($assignproduct->p_type == 'P') {
                    $pervalue = $assignproduct->p_value;
                    $perpice =  $pervalue / 100 * $msrp;
                }else if($assignproduct->p_type == 'R') {
                    $pervalue = $assignproduct->p_value;
                    $perpice =  $pervalue;
                }
            }else {
                $perpice =  $pervalue / 100 * $msrp;
            }



            $productvalue = $perpice / $point_value;
            $productrm = $productvalue * $point_value;
        }else {
            $productvalue = 0;
            $productrm = 0.00;
        }
        //return round($productvalue);
        

        $totalprice = ['adminfeepoint' => $productvalue, 'adminfeerm' => $productrm];

        //$productvalue = round($totalprice) / $point_value;
        
        return $totalprice;
    }
    
    public static function markUp($productID)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $pervalue = 0;
        $perpice = 0;
        $client = \common\models\ClientOption::find()->where(['clientID' => $clientID])->one();
        
        $assignproduct = \common\models\VIPAssignProducts::find()
           ->select('p_type, p_value, msrp')
           ->where(['productID' => $productID, 'clientID' => $clientID])   
           ->one();
        
        if(count($client) > 0) {
            $point_value = $client->point_value;
            $pervalue = $client->bb_percent;
        }else {
            $point_value = '0.01';
        }
        
        $msrp = $assignproduct->msrp;
        if(!empty($assignproduct->p_type)) {
            if($assignproduct->p_type == 'P') {
                $pervalue = $assignproduct->p_value;
                $perpice =  $pervalue / 100 * $msrp;
            }else if($assignproduct->p_type == 'R') {
                $pervalue = $assignproduct->p_value;
                $perpice =  $pervalue;
            }
        }else {
            $perpice =  $pervalue / 100 * $msrp;
        }

        $totalprice = $perpice;
        
        return $totalprice;
    }
    
    public static function markUpType($productID)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        $client = \common\models\ClientOption::find()->where(['clientID' => $clientID])->one();
        
        $assignproduct = \common\models\VIPAssignProducts::find()
           ->select('p_type, p_value')
           ->where(['productID' => $productID, 'clientID' => $clientID])
           //->andWhere(['not', ['p_type' => null]])     
           ->one();
        if(!empty($assignproduct->p_type)) {
            if($assignproduct->p_type == 'P') {
                $perpice =  $assignproduct->p_value.'%';
            }else if($assignproduct->p_type == 'R') {
                $perpice =  'RM'.$assignproduct->p_value;
            }
        }else {
            $perpice =  $client->bb_percent.'%';
        }
        return $perpice;
    }
    
    public static function clientSetup()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        $client = \common\models\ClientOption::find()->one();
        if(!empty($client)) {
            $point_value = $client->point_value;
            $pervalue = $client->bb_percent;
        }else {
            $point_value = '0.01';
            $pervalue = '0';
        }
        
        return ['point_value' => $point_value, 'pervalue' => $pervalue];
    }
    
    public static function clientPrefix()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        $prefix = \common\models\ClientOption::find()->one();
        if(count($prefix) > 0) {
            $myprefix = $prefix->invoice_prefix;
        }else {
            $myprefix = 'NA';
        }
        
        return $myprefix;
    }
    
    public function importProductUpload() {
        $errorMessage = "";
        $products = \common\models\ProductExcelUpload::find()
            ->where(['status' => 'P'])
            ->orderBy('id ASC')
            ->all();
        $manufacturer_id = 0;
        $category_id = 0;
        
        if(count($products) > 0) {
            foreach($products as $product) {
                $product_code = $product->product_code;
                
                if(!empty($product->manufacturer)){
                    $manufacturer = \common\models\VIPManufacturer::find()->where(['name' => $product->manufacturer])->one();
                    if(count($manufacturer) == 0) {
                        $manufacturer = new \common\models\VIPManufacturer();
                        $manufacturer->name = $product->manufacturer;
                        $manufacturer->slug = $this->createSlug($product->manufacturer);
                        $manufacturer->sort_order = 0;
                        $manufacturer->save(false);
                    }
                    $manufacturer_id = $manufacturer->manufacturer_id;
                }
                
                if(!empty($product->category)){
                    $category = \common\models\VIPCategories::find()->where(['name' => $product->category])->one();
                    if(count($category) == 0) {
                        $category = new \common\models\VIPCategories();
                        $category->name = $product->category;
                        $category->slug = $this->createSlug($product->category);
                        $category->save(false);
                    }
                    $category_id = $category->vip_categories_id;
                }
                


                $checkproduct = \common\models\VIPProduct::find()->where(['product_code' => $product_code])->count();
                if($checkproduct == 0) {
                    $model = new \common\models\VIPProduct();
                    $model->ref = substr(Yii::$app->getSecurity()->generateRandomString(),10);
                    $msg = 'Created';
                }else {
                    $model = \common\models\VIPProduct::find()->where(['product_code' => $product_code])->one();
                    //\common\models\VIPProductDeliveryZone::deleteAll(['product_id' => $model->product_id]);
                    $msg = 'Updated';                    
                }
                
                if(!empty($product->product_name)) {
                    $pname = $this->createSlug($product->product_name);
                }else {
                    $pname = '';
                }

                $model->manufacturer_id = $manufacturer_id ;
                $model->category_id = $category_id;
                $model->product_name = $product->product_name;
                $model->meta_tag_title = $pname;
                //$model->main_image = $product->product_code.'jpg';
                $model->product_code = $product->product_code;                
                $model->variant = $product->variant;
                $model->quantity = $product->quantity;
                $model->price = $product->price;
                $model->handling_fee_em = $product->handling_fee_em;
                $model->handling_fee_wm = $product->handling_fee_wm;
                $model->insurance_em = $product->insurance_em;
                $model->insurance_wm = $product->insurance_wm;
                $model->shipping_em = $product->shipping_em;
                $model->shipping_wm = $product->shipping_wm;
                $model->type = strtolower($product->type);
                $model->status = $product->status_pro;
                //$model->msrp = $product->msrp;
                //$model->price = $product->partner_price;
                //$model->stock_status_id = 7;
                //$model->suppliers_product_code = $product->sp_code;
                
                
                
                if($model->save(false)) { 
                    $productUpload = \common\models\ProductExcelUpload::find()
                            ->where(["product_code" => $product_code])
                            ->one();
                    $productUpload->status = "S";
                    $productUpload->save();
                    $errorMessage = $errorMessage . "Product has been (" . $product_code . ") ".$msg."<BR>";
                }else {
                    $errorMessage = $errorMessage . "Product not add(" . $product_code . ")<BR>";
                }    
            }
            
        }
        return $errorMessage;
    }
    

    public function myAvailablePoint(){
        $session = Yii::$app->session;        
        return $session['point'];
    }

    public function createSlug($str, $delimiter = '_'){
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;

    } 
    

    public function Terms() {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        $model = \common\models\Pages::find()
        ->where(['clientID' => $clientID])
        ->andWhere(['like', 'slug', 'terms-conditions'])       
        ->one();

        if(count($model) > 0) {
            $terms = $model->page_description;
        }else {
            $terms = '';
        }
        return $terms;

    }



    public function sendSMS($userId, $smsTemplateCode, $data = null)
    {
        $session = Yii::$app->session;
        $client = \common\models\Client::findOne($session['currentclientID']);
        //$siteUrl = Yii::$app->request->hostInfo;
        $siteUrl = $client->cart_domain;
        $admin_url = $client->admin_domain;
        $client_name = $client->company;
        $client_email = $client->clientAddress->email_address;
        $client_programme_title = $client->programme_title;
        
        if(!empty($client->sms_username)) {
            $username = $client->sms_username;
            $password = $client->sms_password;
        }else {
            $username = urlencode(Yii::$app->params['sms.username']);
            $password = urlencode(Yii::$app->params['sms.password']);
        }
        
        $smsTemplate = \common\models\SMSTemplate::find()
                ->where(['code' => $smsTemplateCode, 'clientID' => $session['currentclientID']])
                ->one();
        


        $profile = \common\models\VIPCustomer::find()->where(['userID' => $userId])->one();
        $name = $profile->full_name;
        $mobile_no = $profile->mobile_no;
        
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }

        $point = isset($data2['point']) ? $data2['point'] : null;
        $reason = isset($data2['reason']) ? $data2['reason'] : null;
        $invoice_no = isset($data2['invoice_no']) ? $data2['invoice_no'] : null;
        $effective_month = isset($data2['effective_month']) ? $data2['effective_month'] : null;
        $expired_point = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['expired_point']) ? $data2['expired_point'] : null;
        $balance_point = isset($data2['balance_point']) ? $data2['balance_point'] : null;
        $expired_date = isset($data2['expired_date']) ? $data2['expired_date'] : null;
        $today_date = isset($data2['today_date']) ? $data2['today_date'] : null;
        
        $smsBody = $smsTemplate->template;
        $smsBody = str_replace('{name}', $name, $smsBody);
        $smsBody = str_replace('{point}', $point, $smsBody);
        $smsBody = str_replace('{reason}', $reason, $smsBody);
        $smsBody = str_replace('{invoice}', $invoice_no, $smsBody);
        $smsBody = str_replace('{effective_month}', $effective_month, $smsBody);
        $smsBody = str_replace('{expired_point}', $expired_point, $smsBody);
        $smsBody = str_replace('{balance_point}', $balance_point, $smsBody);
        $smsBody = str_replace('{expired_date}', $expired_date, $smsBody);
        $smsBody = str_replace('{today_date}', $today_date, $smsBody);
        //$smsBody = str_replace('{client_name}', $client_name, $smsBody);        
        
        $message = $smsBody;

        $destination = $mobile_no;        
        $sender_id = urlencode("66300");    
        $message = html_entity_decode($message, ENT_QUOTES, 'utf-8');
        $message = urlencode($message);
        $type = '1';
        $agreedterm = 'YES';

        $fp = "https://www.isms.com.my/isms_send_all.php";
        $fp .= "?un=$username&pwd=$password&dstno=$destination&msg=$message&type=$type&agreedterm=$agreedterm&sendid=$sender_id";
        //$result = Yii::$app->VIPglobal->ismscURL($fp);
        
        $http = curl_init($fp);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        return $http_result;        
    }
    
    public function sendSMStest($mobile) {
        //function ismscURL($link) {
        $siteUrl = Yii::$app->request->hostInfo;
        $emailBody = "test";
        $message = $emailBody;
        $message = html_entity_decode($message, ENT_QUOTES, 'utf-8');
        $message = urlencode($message);

        $username = urlencode(Yii::$app->params['sms.username']);
        $password = urlencode(Yii::$app->params['sms.password']);
        $sender_id = urlencode("66300");
        $type = '1';

        $link = "https://www.isms.com.my/isms_send.php";
        $link .= "?un=$username&pwd=$password&dstno=$mobile&msg=$message&type=$type&sendid=$sender_id";

        //print_r($link);
        //die();
        $http = curl_init($link);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);
        print_r($http_result);
        die();
        //return $http_result;
    }

    public static function ismscURL($link) {
        $http = curl_init($link);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        return $http_result;
    }
    
    public function webapiuserinfo() {
        $session = Yii::$app->session;            
        $result = $this->webapi($session['name']);
        
        return [
            "name" => $result['RegistrationNumber'],
            "point" => $session['point'],
            "pointexpiring" => $session['pointexpiring'],
            "pointexpireddate" => $session['pointexpireddate'],
            "company_name" => $result['Name'],
            "ContactPerson" => $result['Contact']['ContactPerson'],
            "email" => $result['Contact']['EmailAddress'],
            "mobile_no" => $result['Contact']['SMSMobileNumber'],
            "telephone_no" => $result['Contact']['Telephone1'],
            
            "address1" => $result['Address']['Address1'],
            "address2" => $result['Address']['Address2'],
            "city" => $result['Address']['City'],
            "state" => $result['Address']['State']['Name'],
            "postcode" => $result['Address']['Postcode'],
            "country" => $result['Address']['Country']['Name'],
            "FullAddress" => $result['Address']['FullAddress'],
        ];
    }
    
    public function webapistate() {
        $session = Yii::$app->session;            
        $result = $this->webapiuserinfo()['state'];
        /*echo $result;
        die;*/
        if(!empty($result)) {
            $states = \common\models\VIPStates::find()->where(['like', 'state_name' , $result ])->one();
            if(!empty($states)) {
                $states = $states->zone_id;
            }else {
                $states = 0;
            }
        }else {
            $states = 0;
        }
        return $states;
    }
    
   
    
    public function webapi($ADRegNo) {
        $username = Yii::$app->params['api.username'];
        $password = Yii::$app->params['api.password'];
        
        $url = Yii::$app->params['url.login'].'/AuthorisedDealer/GetADByRegNo?ADRegNo='.$ADRegNo;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        return $result;
    }
    
    public function webapipost($data) {

        $username = Yii::$app->params['api.username'];
        $password = Yii::$app->params['api.password'];

        // the standard end point for posts in an initialised Curl
        $process = curl_init(Yii::$app->params['url.deduct.point']);

        // create an array of data to use, this is basic - see other examples for more complex inserts
        $data_string = $data;

        // create the options starting with basic authentication
        curl_setopt($process, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_POST, 1);
        // make sure we are POSTing
        curl_setopt($process, CURLOPT_CUSTOMREQUEST, "POST");
        // this is the data to insert to create the post
        curl_setopt($process, CURLOPT_POSTFIELDS, $data_string);
        // allow us to use the returned data from the request
        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
        // we are sending json
        curl_setopt($process, CURLOPT_HTTPHEADER, array(
            //'Authorization: Basic base64encode(username:password)',
            'Content-Type: application/json',
            //'Content-Length: ' . strlen($data_string),
            'Accept: application/json',
            'X-Requested-With: application/json',
            //'Content-Type: application/json',                                                                                
            
            //'Accept: application/json',
            )
            
        );

        // process the request
        $return = curl_exec($process);

        curl_close($process);

        return $return;
    }
    
    
    public function ViewAvatar($name) {
        $size = Yii::$app->request->get('size');
        //$name = Yii::$app->VIPglobal->webapiuserinfo()['ContactPerson'];
        if(!empty($name)){
            header('Content-Type: image/png');
            $imageSize = !empty($size) ? $size : 200;
            $setting = array('size' => $imageSize);
            $letterName = str_replace(' - ', ' ', $name);
            $letterName = str_replace(' -', ' ', $letterName);
            $avatar = \common\helper\Avatar::generateAvatarFromLetter($letterName, $setting);
            echo $avatar;
            exit();
        }else if(empty($photo)){
            $extension = 'png';
            $photo = Yii::getAlias('@webroot') . '/web/images/avatar_image.jpg';
        }
        
        header('Content-Type: image/'.$extension);
        $file = file_get_contents($photo);
        echo $file;
        exit();
    }
    
    
    public function profileName() {
        $userID = \Yii::$app->user->id;
        $profile = \common\models\UserProfile::find()
            ->where(['userID' => $userID])
            ->one();
        $firstname = $profile->first_name.' '.$profile->last_name;
        return $firstname;
    }
    
    public static function addtolog($status = null, $message = null, $uID = 0)
    {
        \common\models\ActionLog::add($status, $message, $uID);
    }
    
    public static function clientSetupPoint()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];

        $client = \common\models\ClientOption::find()->orderBy(['option_ID' => 'SORT_DESC'])->one();
        if(count($client) > 0) {
            $point_value = $client->point_value;
        }else {
            $point_value = '0.01';
        }
        
        return $point_value;
    }
}
