<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
         'authManager' => [
            'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\DbManager'
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'thousandSeparator' => ',',
            //'decimalSeparator' => '.',
            'dateFormat' => 'php:d-M-Y',
            //'datetimeFormat' => 'php:d-M-Y H:i:s',
            //'timeFormat' => 'php:H:i:s',
            //'timeZone' => 'Asia/Kuala_Lumpur',
        ],
        
        'image' => [
                'class' => 'yii\image\ImageDriver',
                'driver' => 'GD',  //GD or Imagick
        ],
        
        'VIPglobal' => [
            'class' => 'common\components\VIPGlobal',
        ],
        
    ],
    'on beforeRequest' => function () {
        //echo $_SERVER['SERVER_NAME'];
        /*if(empty(Yii::app()->session['community'])){
            $current_community = Community::model()->with(array(
                'communityHosts'=>array(
                    'joinType'=>'INNER JOIN',
                    'condition'=>'`communityHosts`.hostname= :hostname',
                    'params' => array(':hostname' => $_SERVER['SERVER_NAME'])
                ),
            ))->find(); 
            Yii::app()->session['community'] = serialize($current_community);
        }
        else{
            $current_community = unserialize(Yii::app()->session['community']);
        }*/
        //die();
    },
];
