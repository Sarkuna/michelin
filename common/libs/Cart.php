<?php
namespace common\libs;

use yii;
use yii\web\Session;
use common\models\VIPProduct;
//use common\models\VIPProductOptionValue;

class Cart
{
    function addCart($id,$qty=1,$selectoptionvalue){
        //$session = Yii::$app->session;
        $data = new VIPProduct();
        $dataProduct = $data->getInfoProductBy($id);
        $optlisits = '';
        $opttionlisititem = '';
        $pointsprefix = '';
        //$totalpoint = Yii::$app->VIPglobal->myPoint($id);
        //$totalprice = Yii::$app->VIPglobal->myPrice($id);
        
        $states = Yii::$app->VIPglobal->webapistate();
        if($states > 0) {
            $totalprice = Yii::$app->VIPglobal->getTotalcostprice($id,$states);
            $totalpoint = Yii::$app->VIPglobal->getTotalcostpoint($id,$states);
        }

        if(!isset(Yii::$app->session['cart'])){
            $cart[$id] = [
                'product_qty' => $qty,
                'selectoptionvalue' => $selectoptionvalue,
                'opttionlisititem' => $opttionlisititem,
                'product_name' => $dataProduct['product_name'],
                'product_code' => $dataProduct['product_code'],
                'points_value' => $totalpoint,
                'price' => $totalprice,
                'main_image' => $dataProduct['main_image'],                
            ];            
        }else {
            $cart = Yii::$app->session['cart'];
            if(array_key_exists($id, $cart)){
                $cart[$id] = [
                    'product_qty' => (int)$cart[$id]['product_qty'] + $qty,
                    'selectoptionvalue' => $selectoptionvalue,
                    'opttionlisititem' => $opttionlisititem,
                    'product_name' => $dataProduct['product_name'],
                    'product_code' => $dataProduct['product_code'],
                    'points_value' => $totalpoint,
                    'price' => $totalprice,
                    'main_image' => $dataProduct['main_image'],
                ]; 
            }else {
                $cart[$id] = [
                    'product_qty' => $qty,
                    'selectoptionvalue' => $selectoptionvalue,
                    'opttionlisititem' => $opttionlisititem,
                    'product_name' => $dataProduct['product_name'],
                    'product_code' => $dataProduct['product_code'],
                    'points_value' => $totalpoint,
                    'price' => $totalprice,
                    'main_image' => $dataProduct['main_image'],
                ]; 
            }
        }
        Yii::$app->session['cart'] = $cart;
    }
    
    function UpdateCartQty($id,$qty){
        if(isset(Yii::$app->session['cart'])){
            $cart = Yii::$app->session['cart'];
            if(array_key_exists($id, $cart)){
                $cart[$id] = [
                    'product_qty' => $qty,
                    'selectoptionvalue' => $cart[$id]['selectoptionvalue'],
                    'opttionlisititem' => $cart[$id]['opttionlisititem'],
                    'product_name' => $cart[$id]['product_name'],
                    'product_code' => $cart[$id]['product_code'],
                    'points_value' => $cart[$id]['points_value'],
                    'price' => $cart[$id]['price'],
                    'main_image' => $cart[$id]['main_image'],
                ]; 
            }
            Yii::$app->session['cart'] = $cart;
        }
    }
    
    function RemoveCart($id){
        if(isset(Yii::$app->session['cart'])){
            $cart = Yii::$app->session['cart'];
            if(array_key_exists($id, $cart)){
                unset($cart[$id]);
            }
        }
        Yii::$app->session['cart'] = $cart;
    }
}