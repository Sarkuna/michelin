<?php
namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;


class AdminUserForm extends Model
{
    public $csv;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['csv', 'required'],
            ['csv', 'file'],
        ];
    }

    
}
