<?php
namespace common\models;

use yii\base\Model;
use Yii;


class AgreeForm extends Model
{
    public $agree;
    public $comment;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agree'], 'required'],
            [['comment'], 'string'],
            ['agree', 'required', 'requiredValue' => 1, 'message' => 'Please Tick Above to Agree Terms & Condition'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'comment' => 'Comment',
            'agree' => 'Agree',
        ];
    }

    
}
