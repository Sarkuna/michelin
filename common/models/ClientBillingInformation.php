<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "client_billing_information".
 *
 * @property integer $billing_information_ID
 * @property integer $clientID
 * @property string $name
 * @property string $address1
 * @property string $address2
 * @property string $postcode
 * @property string $city
 * @property string $state
 * @property integer $country_id
 * @property string $email_address
 * @property string $telephone_no
 * @property string $fax_no
 * @property string $mobile_no
 */
class ClientBillingInformation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_billing_information';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID'], 'required'],
            [['clientID', 'country_id'], 'integer'],
            [['name', 'address1', 'address2', 'email_address'], 'string', 'max' => 200],
            [['postcode'], 'string', 'max' => 10],
            [['city', 'state'], 'string', 'max' => 100],
            [['telephone_no', 'fax_no', 'mobile_no'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'billing_information_ID' => 'Billing Information  ID',
            'clientID' => 'Client ID',
            'name' => 'Name',
            'address1' => 'Address1',
            'address2' => 'Address2',
            'postcode' => 'Postcode',
            'city' => 'City',
            'state' => 'State',
            'country_id' => 'Country ID',
            'email_address' => 'Email Address',
            'telephone_no' => 'Telephone No',
            'fax_no' => 'Fax No',
            'mobile_no' => 'Mobile No',
        ];
    }
    
    public function getCountry()
    {
        return $this->hasOne(VipCountry::className(), ['country_id' => 'country_id']);
    }
}
