<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "client_option".
 *
 * @property integer $option_ID
 * @property integer $clientID
 * @property string $invoice_prefix
 * @property string $receipts_prefix
 * @property string $point_value
 * @property string $bb_percent
 */
class ClientOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_option';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientID', 'invoice_prefix', 'point_value'], 'required'],
            [['clientID'], 'integer'],
            [['point_value'], 'number'],
            [['invoice_prefix'], 'string', 'max' => 20],
            [['receipts_prefix'], 'string', 'max' => 3],
            [['bb_percent'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_ID' => 'Option  ID',
            'clientID' => 'Client ID',
            'invoice_prefix' => 'Invoice Prefix',
            'receipts_prefix' => 'Receipts Prefix',
            'point_value' => 'Point Value',
            'bb_percent' => 'Bb Percent',
        ];
    }
}
