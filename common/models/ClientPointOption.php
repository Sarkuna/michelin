<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "client_point_option".
 *
 * @property integer $client_point_option_id
 * @property integer $clientID
 * @property string $point_value
 */
class ClientPointOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'client_point_option';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_value'], 'required'],
            [['client_point_option_id', 'clientID'], 'integer'],
            [['point_value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_point_option_id' => 'Client Point Option ID',
            'clientID' => 'Client ID',
            'point_value' => 'Point Value',
        ];
    }
}
