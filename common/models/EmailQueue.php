<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "email_queue".
 *
 * @property integer $id
 * @property integer $email_template_id
 * @property integer $email_template_global_id
 * @property integer $clientID
 * @property integer $user_id
 * @property string $name
 * @property string $email
 * @property string $mobile
 * @property string $data
 * @property string $status
 * @property string $date_to_send
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class EmailQueue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_queue';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email_template_id', 'email_template_global_id', 'clientID', 'user_id', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'required'],
            [['data'], 'string'],
            [['date_to_send', 'created_datetime', 'updated_datetime'], 'safe'],
            [['name', 'email'], 'string', 'max' => 150],
            [['mobile'], 'string', 'max' => 20],
            [['status', 'email_queue_type'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email_template_id' => 'Email Template ID',
            'email_template_global_id' => 'Email Template Global ID',
            'clientID' => 'Client ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'data' => 'Data',
            'status' => 'Status',
            'date_to_send' => 'Date To Send',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
