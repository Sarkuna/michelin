<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "email_template".
 *
 * @property integer $id
 * @property string $code
 * @property integer $clientID
 * @property string $name
 * @property string $subject
 * @property string $template
 * @property string $sms_text
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class EmailTemplate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'clientID', 'name', 'subject', 'template', 'created_datetime', 'updated_datetime', 'created_by', 'updated_by'], 'required'],
            [['clientID', 'created_by', 'updated_by'], 'integer'],
            [['template'], 'string'],
            [['created_datetime', 'updated_datetime'], 'safe'],
            [['code'], 'string', 'max' => 50],
            [['name'], 'string', 'max' => 150],
            [['subject'], 'string', 'max' => 300],
            [['sms_text'], 'string', 'max' => 500],
            //[['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'clientID' => 'Client ID',
            'name' => 'Name',
            'subject' => 'Subject',
            'template' => 'Template',
            'sms_text' => 'Sms Text',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
