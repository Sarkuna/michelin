<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "internal_invoicing_items".
 *
 * @property integer $internal_invoicing_items_id
 * @property integer $invoice_id
 * @property string $description
 * @property integer $quantity
 * @property string $per_unit
 * @property string $amount
 */
class InternalInvoicingItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'internal_invoicing_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['internal_invoicing_items_id', 'invoice_id', 'description', 'quantity', 'per_unit', 'amount'], 'required'],
            [['internal_invoicing_items_id', 'invoice_id', 'quantity'], 'integer'],
            [['per_unit', 'amount'], 'number'],
            [['description'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'internal_invoicing_items_id' => 'Internal Invoicing Items ID',
            'invoice_id' => 'Invoice ID',
            'description' => 'Description',
            'quantity' => 'Quantity',
            'per_unit' => 'Per Unit',
            'amount' => 'Amount',
        ];
    }
}
