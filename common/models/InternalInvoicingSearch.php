<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\InternalInvoicing;

/**
 * InternalInvoicingSearch represents the model behind the search form about `common\models\InternalInvoicing`.
 */
class InternalInvoicingSearch extends InternalInvoicing
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['internal_invoicing_id'], 'integer'],
            [['subject', 'bill_from_name', 'bill_from_add', 'bill_to_name', 'bill_to_add', 'invoice_date', 'due_date', 'terms_conditions', 'internal_invoicing_status', 'invoice_no'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InternalInvoicing::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'internal_invoicing_id' => $this->internal_invoicing_id,
            'invoice_date' => $this->invoice_date,
            'due_date' => $this->due_date,
        ]);

        $query->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'bill_from_name', $this->bill_from_name])
            ->andFilterWhere(['like', 'bill_from_add', $this->bill_from_add])
            ->andFilterWhere(['like', 'invoice_no', $this->invoice_no])
            ->andFilterWhere(['like', 'bill_to_name', $this->bill_to_name])
            ->andFilterWhere(['like', 'bill_to_add', $this->bill_to_add])
            ->andFilterWhere(['like', 'terms_conditions', $this->terms_conditions])
            ->andFilterWhere(['like', 'internal_invoicing_status', $this->internal_invoicing_status]);

        return $dataProvider;
    }
}
