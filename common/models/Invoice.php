<?php

namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
/**
 * This is the model class for table "invoice".
 *
 * @property integer $invoice_id
 * @property string $invoice_name
 * @property string $bill_from_name
 * @property string $bill_from_add
 * @property string $bill_to_name
 * @property string $bill_to_add
 * @property string $bill_create_date
 */
class Invoice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_name', 'bill_from_name', 'bill_from_add', 'bill_to_name', 'bill_to_add', 'comment'], 'required'],
            [['bill_from_add', 'bill_to_add'], 'string'],
            [['bill_create_date', 'terms_conditions'], 'safe'],
            [['invoice_name', 'bill_from_name', 'bill_to_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'invoice_id' => 'Invoice ID',
            'invoice_name' => 'Invoice Name',
            'bill_from_name' => 'Bill From Name',
            'bill_from_add' => 'Bill From Address',
            'bill_to_name' => 'Bill To Name',
            'bill_to_add' => 'Bill To Address',
            'bill_create_date' => 'Bill Create Date',
            'comment' => 'Comment (This comment will appear at order\'s Comment section)'
        ];
    }
    
    public function getActions() {

        $invoiceitems = \common\models\InvoiceItem::find()->where([
                'invoice_id' => $this->invoice_id,
            ])->all();
        
        $returnValue = "";

        $returnValue = $returnValue . ' <a href="' . Url::to(['/reports/report/download-invoice', 'id' => $this->invoice_id]) . '" title="Download Excel"><i class="fa fa-file-excel fa-lg text-success"></i></a> ';
        foreach($invoiceitems as $invoiceitem) { 
            //$invoiceitem->invoice_item_id
            if($invoiceitem->invoice_type == 'I') {
                $returnValue = $returnValue . ' <a href="' . Url::to(['/reports/report/download-pdf', 'id' => $this->invoice_id]) . '" title="Download Item"><i class="fa fa-file-pdf fa-lg text-warning"></i></a> ';
            }else if($invoiceitem->invoice_type == 'V') {
                $returnValue = $returnValue . ' <a href="' . Url::to(['reports/report/download-pdf', 'id' => $this->invoice_id]) . '" title="Download Voucher"><i class="fa fa-file-pdf fa-lg text-primary"></i></a> ';
            }
        }

        return $returnValue;
    }
}
