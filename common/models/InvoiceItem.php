<?php

namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "invoice_item".
 *
 * @property integer $invoice_item_id
 * @property integer $invoice_id
 * @property string $invoice_type
 * @property string $invoice_no
 * @property string $invoice_date
 * @property string $invoice_pono
 * @property string $invoice_due_date
 */
class InvoiceItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id', 'invoice_no', 'invoice_date', 'invoice_type', 'invoice_due_date'], 'required'],
            [['invoice_id'], 'integer'],
            [['invoice_date', 'invoice_due_date'], 'safe'],
            [['invoice_type'], 'string', 'max' => 1],
            [['invoice_no', 'invoice_pono'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'invoice_item_id' => 'Invoice Item ID',
            'invoice_id' => 'Invoice ID',
            'invoice_type' => 'Invoice Type',
            'invoice_no' => 'Invoice No',
            'invoice_date' => 'Invoice Date',
            'invoice_pono' => 'Invoice Pono',
            'invoice_due_date' => 'Invoice Due Date',
        ];
    }
    
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['invoice_id' => 'invoice_id']);
    }
    public function getActions() {
        
        $returnValue = "";

        $returnValue = $returnValue . ' <a href="' . Url::to(['/reports/report/download-invoice', 'id' => $this->invoice_id]) . '" title="Download Excel" target="_blank"><i class="fa fa-file-excel fa-lg text-success"></i></a> ';
        //foreach($invoiceitems as $invoiceitem) { 
            //$invoiceitem->invoice_item_id
            if($this->invoice_type == 'I') {
                $returnValue = $returnValue . ' <a href="' . Url::to(['/reports/report/download-pdf', 'invoiceitemid' => $this->invoice_item_id]) . '" title="Download Item" target="_blank"><i class="fa fa-file-pdf fa-lg text-warning"></i></a> ';
            }else if($this->invoice_type == 'V') {
                $returnValue = $returnValue . ' <a href="' . Url::to(['/reports/report/download-pdf', 'invoiceitemid' => $this->invoice_item_id]) . '" title="Download Voucher" target="_blank"><i class="fa fa-file-pdf fa-lg text-primary"></i></a> ';
            }
        //}

        return $returnValue;
    }
    
    public function getInvoiceAmount() {
        /*$invoiceitem = \common\models\InvoiceItem::find()
                        ->where('invoice_item_id = :invoice_item_id', [':invoice_item_id' => $invoiceitemid])
                        ->one();*/
        $mytotal = 0;
        $session = Yii::$app->session;
                
        $orderproducts = \common\models\VIPOrderProduct::find()
                ->joinWith([
                'order',      
            ])->where(['vip_order.clientID' => $session['currentclientID'], 'vip_order_product.invoice_item_id' => $this->invoice_item_id])
            ->orderBy(['vip_order.created_datetime' => SORT_DESC])->all();
        
        foreach($orderproducts as $key=>$model){
            /*if($this->invoice_type == 'V') {
                $total_pro = $model->point - $model->point_admin + $model->shipping_point;
                $totl_f = $model->quantity * $total_pro;
                $unit = $model->point - $model->point_admin;
            }else {*/
                $total_pro = $model->point + $model->shipping_point;
                $totl_f = $model->quantity * $total_pro;
                $unit = $model->point;
            //}
            $mytotal += $totl_f / 2;
        }
                
                
                
        return Yii::$app->formatter->asDecimal($mytotal);
    }
}
