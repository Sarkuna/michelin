<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "menu_items".
 *
 * @property integer $id
 * @property string $title
 * @property string $link
 * @property string $target
 * @property string $parent
 * @property integer $sort
 * @property string $created_at
 * @property string $updated_at
 * @property string $class
 * @property string $icon_font
 * @property integer $menu_id
 * @property integer $related_id
 * @property integer $depth
 */
class MenuItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'link', 'sort', 'menu_id'], 'required'],
            [['sort', 'menu_id', 'related_id', 'depth'], 'integer'],
            [['created_at', 'updated_at', 'type'], 'safe'],
            [['title', 'link', 'parent', 'icon_font'], 'string', 'max' => 255],
            [['target', 'class'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'link' => 'Link',
            'target' => 'Target',
            'parent' => 'Parent',
            'sort' => 'Sort',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'class' => 'Class',
            'icon_font' => 'Icon Font',
            'menu_id' => 'Menu ID',
            'related_id' => 'Related ID',
            'type' => 'Type',
            'depth' => 'Depth',
        ];
    }
}
