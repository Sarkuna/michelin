<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_excel_upload".
 *
 * @property integer $id
 * @property string $product_name
 * @property string $product_code
 * @property string $variant
 * @property string $price
 * @property string $handling_fee_em
 * @property string $handling_fee_wm
 * @property string $insurance_em
 * @property string $insurance_wm
 * @property string $shipping_em
 * @property string $shipping_wm
 * @property string $manufacturer
 * @property string $category
 * @property string $type
 * @property string $create_date
 * @property string $status
 */
class ProductExcelUpload extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_excel_upload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_name'], 'string'],
            [['product_code'], 'required'],
            [['price', 'handling_fee_em', 'handling_fee_wm', 'insurance_em', 'insurance_wm', 'shipping_em', 'shipping_wm'], 'number'],
            [['create_date'], 'safe'],
            [['product_code', 'variant', 'manufacturer', 'category'], 'string', 'max' => 100],
            [['type'], 'string', 'max' => 50],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_name' => 'Product Name',
            'product_code' => 'Product Code',
            'variant' => 'Variant',
            'price' => 'Price',
            'handling_fee_em' => 'Handling Fee Em',
            'handling_fee_wm' => 'Handling Fee Wm',
            'insurance_em' => 'Insurance Em',
            'insurance_wm' => 'Insurance Wm',
            'shipping_em' => 'Shipping Em',
            'shipping_wm' => 'Shipping Wm',
            'manufacturer' => 'Manufacturer',
            'category' => 'Category',
            'type' => 'Type',
            'create_date' => 'Create Date',
            'status' => 'Status',
        ];
    }
}
