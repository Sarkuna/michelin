<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "profile_pic".
 *
 * @property integer $id
 * @property integer $userID
 * @property string $file_name
 */
class ProfilePic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile_pic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userID', 'file_name'], 'required'],
            [['userID'], 'integer'],
            [['file_name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userID' => 'User ID',
            'file_name' => 'File Name',
        ];
    }
}
