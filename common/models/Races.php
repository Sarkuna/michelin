<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "races".
 *
 * @property integer $races_id
 * @property string $name
 * @property integer $sort_order
 */
class Races extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'races';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['sort_order'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'races_id' => 'Races ID',
            'name' => 'Name',
            'sort_order' => 'Sort Order',
        ];
    }
}
