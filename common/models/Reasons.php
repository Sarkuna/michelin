<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "reasons".
 *
 * @property integer $reasons_id
 * @property string $name
 */
class Reasons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reasons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reasons_id' => 'Reasons ID',
            'name' => 'Name',
        ];
    }
}
