<?php
namespace common\models;

use yii\base\Model;
use Yii;


class ReportForm extends Model
{
    public $date_range;
    public $type;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['date_range', 'required'],
            [['type'], 'safe'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'date_range' => 'Date Range',
            'type' => 'Type'
       ];
    }
    

    
}
