<?php
namespace common\models;

use yii\base\Model;
use Yii;


class ReportOrderForm extends Model
{
    public $date_range;
    public $dealer_code;
    public $status;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['date_range', 'required'],
            [['dealer_code','status'], 'safe'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'date_range' => 'Date Range',
            'dealer_code' => 'Dealer Code',
            'status' => 'Status'
       ];
    }
    

    
}
