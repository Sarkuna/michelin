<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StatementOfAccount;

/**
 * StatementOfAccountSearch represents the model behind the search form about `common\models\StatementOfAccount`.
 */
class StatementOfAccountSearch extends StatementOfAccount
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sa_id', 'internal_invoicing_id', 'invoice_item_id'], 'integer'],
            [['statement_date', 'particulars', 'payment_type', 'payment_status'], 'safe'],
            [['amount', 'payment'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StatementOfAccount::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['statement_date'=>SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'sa_id' => $this->sa_id,
            'statement_date' => $this->statement_date,
            'amount' => $this->amount,
            'payment' => $this->payment,
            'internal_invoicing_id' => $this->internal_invoicing_id,
            'invoice_item_id' => $this->invoice_item_id,
        ]);

        $query->andFilterWhere(['like', 'particulars', $this->particulars])
            ->andFilterWhere(['like', 'payment_type', $this->payment_type])
            ->andFilterWhere(['like', 'payment_status', $this->payment_status]);

        return $dataProvider;
    }
}
