<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "statement_of_redemption".
 *
 * @property integer $redemption_id
 * @property string $redemption_date
 * @property string $particulars
 * @property string $debit_in
 * @property string $credit_out
 * @property string $payment_type
 * @property string $status
 * @property string $payment_id
 * @property integer $order_id
 */
class StatementOfRedemption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statement_of_redemption';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['redemption_date'], 'required'],
            [['redemption_date', 'order_type'], 'safe'],
            [['particulars'], 'string'],
            [['debit_in', 'credit_out'], 'number'],
            [['order_id'], 'integer'],
            [['status'], 'string', 'max' => 10],
            [['payment_id'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'redemption_id' => 'Redemption ID',
            'redemption_date' => 'Redemption Date',
            'particulars' => 'Particulars',
            'debit_in' => 'Debit In',
            'credit_out' => 'Credit Out',
            'status' => 'Status',
            'payment_id' => 'Payment ID',
            'order_id' => 'Order ID',
        ];
    }
}
