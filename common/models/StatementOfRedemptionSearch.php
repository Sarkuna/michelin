<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\StatementOfRedemption;

/**
 * StatementOfRedemptionSearch represents the model behind the search form about `common\models\StatementOfRedemption`.
 */
class StatementOfRedemptionSearch extends StatementOfRedemption
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['redemption_id', 'order_id'], 'integer'],
            [['redemption_date', 'particulars', 'status', 'payment_id'], 'safe'],
            [['debit_in', 'credit_out'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StatementOfRedemption::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['redemption_date' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'redemption_id' => $this->redemption_id,
            'redemption_date' => $this->redemption_date,
            'debit_in' => $this->debit_in,
            'credit_out' => $this->credit_out,
            'order_id' => $this->order_id,
        ]);

        $query->andFilterWhere(['like', 'particulars', $this->particulars])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'payment_id', $this->payment_id]);

        return $dataProvider;
    }
}
