<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Userapp;

/**
 * UserappSearch represents the model behind the search form about `common\models\Userapp`.
 */
class UserappSearch extends Userapp
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'available_point'], 'integer'],
            [['username', 'auth_key', 'password_hash', 'access_token', 'token', 'created_datetime', 'date_last_login', 'user_agent'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Userapp::find();

        // add conditions that should always apply here
        $size = VIPProduct::find()->count();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['date_last_login'=>SORT_DESC]],
            'pagination' => [
                'pageSize' => $size,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_datetime' => $this->created_datetime,
            'available_point' => $this->available_point,
            'date_last_login' => $this->date_last_login,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'access_token', $this->access_token])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'user_agent', $this->user_agent]);

        return $dataProvider;
    }
}
