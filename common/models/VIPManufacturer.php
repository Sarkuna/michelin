<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vip_manufacturer".
 *
 * @property integer $manufacturer_id
 * @property string $name
 * @property string $image
 * @property integer $sort_order
 */
class VIPManufacturer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file_image;
    public static function tableName()
    {
        return 'vip_manufacturer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sort_order'], 'required'],
            [['sort_order'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['slug'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'manufacturer_id' => 'Manufacturer ID',
            'name' => 'Manufacturer Name',
            'file_image' => 'Image',
            'sort_order' => 'Sort Order',
        ];
    }
    
    public function getCategoriesEnabled()
    {
        $enabled = VIPProduct::find()->where(['status' => 'E', 'manufacturer_id' => $this->manufacturer_id])->count();
        return $enabled;
    }
    
    public function getCategoriesDisabled()
    {
        $disabled = VIPProduct::find()->where(['status' => 'D', 'manufacturer_id' => $this->manufacturer_id])->count();
        return $disabled;
    }
}
