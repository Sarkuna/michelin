<?php

namespace common\models;

use Yii;
use common\models\VIPOptionDescription;

/**
 * This is the model class for table "vip_option_value_description".
 *
 * @property integer $option_value_id
 * @property integer $language_id
 * @property integer $option_id
 * @property string $name
 *
 * @property VipOptionDescription $option
 */
class VIPOptionValueDescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $id;
    public static function tableName()
    {
        return 'vip_option_value_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['language_id', 'option_id'], 'integer'],
            [['name'], 'string', 'max' => 120],
            [['option_id'], 'exist', 'skipOnError' => true, 'targetClass' => VipOptionDescription::className(), 'targetAttribute' => ['option_id' => 'option_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_value_id' => 'Option Value ID',
            'language_id' => 'Language ID',
            'option_id' => 'Option ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(VipOptionDescription::className(), ['option_id' => 'option_id']);
    }

}
