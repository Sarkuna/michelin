<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "vip_order_option".
 *
 * @property integer $order_option_id
 * @property integer $order_id
 * @property integer $order_product_id
 * @property integer $product_option_id
 * @property integer $product_option_value_id
 * @property string $name
 * @property string $value
 * @property string $type
 */
class VIPOrderOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_order_option';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'order_product_id', 'product_option_value_id', 'name', 'value'], 'required'],
            [['order_id', 'order_product_id', 'product_option_id', 'product_option_value_id'], 'integer'],
            [['value'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_option_id' => 'Order Option ID',
            'order_id' => 'Order ID',
            'order_product_id' => 'Order Product ID',
            'product_option_id' => 'Product Option ID',
            'product_option_value_id' => 'Product Option Value ID',
            'name' => 'Name',
            'value' => 'Value',
            'type' => 'Type',
        ];
    }
}
