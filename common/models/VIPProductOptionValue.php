<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vip_product_option_value".
 *
 * @property integer $product_option_value_id
 * @property integer $product_id
 * @property integer $option_id
 * @property integer $option_value_id
 * @property integer $quantity
 * @property integer $subtract
 * @property string $price
 * @property string $price_prefix
 * @property integer $points
 * @property string $points_prefix
 * @property string $weight
 * @property string $weight_prefix
 */
class VIPProductOptionValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_product_option_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['option_value_id', 'price', 'price_prefix', 'points', 'points_prefix'], 'required'],
            [['product_id', 'option_id', 'option_value_id', 'quantity', 'subtract', 'points'], 'integer'],
            [['price', 'weight'], 'number'],
            [['price_prefix', 'points_prefix', 'weight_prefix'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_option_value_id' => 'Product Option Value ID',
            'product_id' => 'Product ID',
            'option_id' => 'Option ID',
            'option_value_id' => 'Option Value ID',
            'quantity' => 'Quantity',
            'subtract' => 'Subtract',
            'price' => 'Price',
            'price_prefix' => 'Price Prefix',
            'points' => 'Points',
            'points_prefix' => 'Points Prefix',
            'weight' => 'Weight',
            'weight_prefix' => 'Weight Prefix',
        ];
    }
    
    public function getOptiondescription()
    {
        return $this->hasOne(VIPOptionDescription::className(), ['option_id' => 'option_id']);
    }
    public function getOptiondescriptionvalue()
    {
        return $this->hasOne(VIPOptionValueDescription::className(), ['option_value_id' => 'option_value_id']);
    }
    
}
