<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vip_states".
 *
 * @property integer $states_id
 * @property integer $country_id
 * @property integer $zone_id
 * @property string $state_name
 */
class VIPStates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_states';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'zone_id', 'state_name'], 'required'],
            [['country_id', 'zone_id'], 'integer'],
            [['state_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'states_id' => 'States ID',
            'country_id' => 'Country ID',
            'zone_id' => 'Zone ID',
            'state_name' => 'State Name',
        ];
    }
    
    public function getZone() {
        return $this->hasOne(\common\models\VIPZone::className(), ['zone_id' => 'zone_id']);
    }
}
