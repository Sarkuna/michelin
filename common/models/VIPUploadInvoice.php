<?php

namespace common\models;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "vip_upload_invoice".
 *
 * @property integer $upload_id
 * @property integer $userID
 * @property integer $clientID
 * @property string $file_group
 * @property string $upload_file_name
 * @property string $upload_file_new_name
 * @property string $add_date
 */
class VIPUploadInvoice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vip_upload_invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userID', 'clientID', 'file_group', 'upload_file_name', 'add_date'], 'required'],
            [['userID', 'clientID','vip_receipt_id'], 'integer'],
            [['upload_file_name'], 'string'],
            [['add_date'], 'safe'],
            [['file_group'], 'string', 'max' => 250],
            [['upload_file_new_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'upload_id' => 'Upload ID',
            'userID' => 'User ID',
            'clientID' => 'Client ID',
            'file_group' => 'File Group',
            'upload_file_name' => 'Upload File Name',
            'upload_file_new_name' => 'Upload File New Name',
            'add_date' => 'Add Date',
        ];
    }
    
    public function getCustomer() {
        return $this->hasOne(\common\models\VIPCustomer::className(), ['userID' => 'userID']);
    }
    
    public function getTotalAdd() {
        $total = 0;
        $totaladd = \common\models\VIPUploadInvoice::find()
        ->where(['invoice_status' => 'P'])       
        ->andWhere(['file_group'=>$this->file_group])
        ->count();

        if(empty($totaladd)){
            $totaladd = 0; 
         }
        return $totaladd;
    }
    
    public function getActions() {
        $returnValue = "";
        $returnValue = $returnValue . ' <a target="_blank" href="' . Url::to(['/upload/upload_invoice/'.$this->upload_file_new_name]) . '" title="View" class="btn btn-info btn-sm"  ><i class="fa fa-paperclip"></i></a> ';
        
        return $returnValue;
    }
}
