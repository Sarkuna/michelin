<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "web_push_user".
 *
 * @property integer $id
 * @property string $player_id
 * @property integer $user_id
 * @property string $status
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $device_code
 */
class WebPushUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'web_push_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['player_id', 'user_id'], 'required'],
            [['id', 'user_id'], 'integer'],
            [['device_code', 'status'], 'safe'],
            [['player_id', 'device_code'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 1],
        ];
    }


	public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'player_id' => 'Player ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'device_code' => 'Device Code',
        ];
    }
}
