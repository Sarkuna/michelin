<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class VuexyFileUploadAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Montserrat:300,400,500,600',
        // BEGIN: Vendor CSS-->
        'themes/vuexy/app-assets/vendors/css/vendors.min.css',
        'themes/vuexy/app-assets/vendors/css/pickers/pickadate/pickadate.css',
        // END: Vendor CSS-->

        // BEGIN: Theme CSS-->
        'themes/vuexy/app-assets/css/bootstrap.css',
        'themes/vuexy/app-assets/css/bootstrap-extended.css',
        'themes/vuexy/app-assets/css/colors.css',
        'themes/vuexy/app-assets/css/components.css',
        'themes/vuexy/app-assets/css/themes/dark-layout.css',
        'themes/vuexy/app-assets/css/themes/semi-dark-layout.css',

        // BEGIN: Page CSS-->
        'themes/vuexy/app-assets/css/core/menu/menu-types/vertical-menu.css',
        'themes/vuexy/app-assets/css/core/colors/palette-gradient.css',
        // END: Page CSS-->

        // BEGIN: Custom CSS-->
        'themes/vuexy/assets/css/style.css',
        '//cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/css/fileinput.min.css'
        // END: Custom CSS-->
    ];
    public $js = [
        // BEGIN: Vendor JS-->
        //'//code.jquery.com/jquery-2.2.4.min.js',
        //'themes/vuexy/app-assets/vendors/js/vendors.min.js',
        ['themes/vuexy/app-assets/vendors/js/vendors.min.js', 'position' => \yii\web\View::POS_HEAD],
        // BEGIN Vendor JS-->

        // BEGIN: Page Vendor JS-->
        'themes/vuexy/app-assets/vendors/js/pickers/pickadate/picker.js',
        'themes/vuexy/app-assets/vendors/js/pickers/pickadate/picker.date.js',
        'themes/vuexy/app-assets/vendors/js/pickers/pickadate/picker.time.js',
        'themes/vuexy/app-assets/vendors/js/pickers/pickadate/legacy.js', 
        // END: Page Vendor JS-->

        // BEGIN: Theme JS-->
        'themes/vuexy/app-assets/js/core/app-menu.js',
        'themes/vuexy/app-assets/js/core/app.js',
        'themes/vuexy/app-assets/js/scripts/components.js',
        // END: Theme JS-->

        // BEGIN: Page JS-->
        //'themes/vuexy/app-assets/js/scripts/forms/form-tooltip-valid.js',
        '//cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.6/js/fileinput.min.js'
        // END: Page JS-->
        
    ];
    public $jsOptions = [
    	//'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}