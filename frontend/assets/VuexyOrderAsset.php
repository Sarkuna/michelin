<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class VuexyOrderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Montserrat:300,400,500,600',
        //<!-- BEGIN: Vendor CSS-->
        'themes/vuexy/app-assets/vendors/css/vendors.min.css',
        'themes/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css',
        'themes/vuexy/app-assets/vendors/css/file-uploaders/dropzone.min.css',
        'themes/vuexy/app-assets/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css',
        //<!-- END: Vendor CSS-->

        //<!-- BEGIN: Theme CSS-->
        'themes/vuexy/app-assets/css/bootstrap.css',
        'themes/vuexy/app-assets/css/bootstrap-extended.css',
        'themes/vuexy/app-assets/css/colors.css',
        'themes/vuexy/app-assets/css/components.css',
        'themes/vuexy/app-assets/css/themes/dark-layout.css',
        'themes/vuexy/app-assets/css/themes/semi-dark-layout.css',

        //<!-- BEGIN: Page CSS-->
        'themes/vuexy/app-assets/css/core/menu/menu-types/vertical-menu.css',
        'themes/vuexy/app-assets/css/core/colors/palette-gradient.css',
        'themes/vuexy/app-assets/css/plugins/file-uploaders/dropzone.css',
        'themes/vuexy/app-assets/css/pages/data-list-view.css',
        //<!-- END: Page CSS-->

        //<!-- BEGIN: Custom CSS-->
        'themes/vuexy/assets/css/style.css',
        //<!-- END: Custom CSS-->
        
        
        
        
    ];
    public $js = [
        // BEGIN: Vendor JS-->
        //'themes/vuexy/app-assets/vendors/js/vendors.min.js',
        //['themes/vuexy/app-assets/vendors/js/vendors.min.js', 'position' => \yii\web\View::POS_HEAD],
        // BEGIN Vendor JS-->

        //<!-- BEGIN: Vendor JS-->
    'themes/vuexy/app-assets/vendors/js/vendors.min.js',
    //<!-- BEGIN Vendor JS-->

    //<!-- BEGIN: Page Vendor JS-->
    'themes/vuexy/app-assets/vendors/js/extensions/dropzone.min.js',
    'themes/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js',
    'themes/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js',
    'themes/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js',
    'themes/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js',
    'themes/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js',
    'themes/vuexy/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js',
    //<!-- END: Page Vendor JS-->

    //<!-- BEGIN: Theme JS-->
    'themes/vuexy/app-assets/js/core/app-menu.js',
    'themes/vuexy/app-assets/js/core/app.js',
    'themes/vuexy/app-assets/js/scripts/components.js',
    //<!-- END: Theme JS-->

    //<!-- BEGIN: Page JS-->
    //'themes/vuexy/app-assets/js/scripts/ui/data-list-view.js',
    'themes/vuexy/assets/js/scriptsorder.js',   
    //<!-- END: Page JS-->
        // END: Page JS-->
        
    ];
    public $jsOptions = [
    	//'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
