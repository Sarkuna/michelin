<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
//use yii\helpers\Html;

class DashboardVuexy extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        //$session = Yii::$app->session;
        //$clientID = $session['currentclientID'];

        //return $this->render('products_bl_widget',array('bestsellerdataProvider'=>$bestsellerdataProvider,'lpdataProvider'=>$dataProvider2));      
        $categories = \common\models\VIPProduct::find()
        ->where(['status' => 'E'])        
        ->orderBy(new \yii\db\Expression('rand()'))
        ->groupBy(['category_id'])          
        ->limit(12)->all();
        
        $products = \common\models\VIPProduct::find()
        ->where(['status' => 'E'])        
        ->orderBy(new \yii\db\Expression('rand()'))        
        ->limit(30)->all();
        
        return $this->render('dashboardvuexy',['categories' => $categories, 'products' => $products]);
        
    }
}