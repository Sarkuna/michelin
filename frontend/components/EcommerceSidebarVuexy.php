<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;
//use yii\helpers\Html;

class EcommerceSidebarVuexy extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $flash_deals = 'N';
        
        
        $sql = "SELECT c.vip_categories_id, c.slug, c.name FROM vip_product AS vap INNER JOIN vip_categories AS c ON vap.category_id=c.vip_categories_id WHERE (vap.status = 'E') GROUP BY c.vip_categories_id ORDER BY `c`.`position` ASC";
        
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql);
        $categories = $command->queryAll(); 
        
        $sqlman = "SELECT `vip_manufacturer`.`manufacturer_id`, `vip_manufacturer`.`name`, `vip_manufacturer`.`slug` FROM `vip_product` 
LEFT JOIN `vip_manufacturer` ON `vip_product`.`manufacturer_id` = `vip_manufacturer`.`manufacturer_id`
WHERE (`status`='E') GROUP BY `vip_manufacturer`.`manufacturer_id` ORDER BY `vip_manufacturer`.`sort_order` ASC";
        
        $manufacturers = \common\models\VIPProduct::findBySql($sqlman)->asArray()->all();

        return $this->render('ecommercesidebarvuexy', ['categories' => $categories, 'manufacturers' => $manufacturers]);
        
    }
}