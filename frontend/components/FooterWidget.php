<?php
namespace app\components;

use Yii;
use yii\base\Widget;
//use yii\helpers\Html;

class FooterWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $footerpages = \common\models\Client::findOne($clientID);
        $footer1 = '';
        $footer2 = '';
        $footermenus = \common\models\Menus::find()
                ->where(['clientID' => $clientID])
                ->andWhere(['like', 'display_location', 'footer-menu'])
                ->one();
        return $this->render('footerwidget',[
            'footer1' => $footer1,
            'footer2' => $footer2,
            'footermenus' => $footermenus
        ]);
        
    }
}