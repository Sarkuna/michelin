<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\db\Query;
use yii\data\ActiveDataProvider;
//use yii\helpers\Html;

class ManufacturersWidget extends Widget
{
    public $path;

    public function init()
    {
        parent::init();
    }

    public function run()
    {   
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];


        $sql = "SELECT `vip_manufacturer`.`manufacturer_id`, `vip_manufacturer`.`image` FROM `vip_product` 
LEFT JOIN `vip_assign_products` ON `vip_product`.`product_id` = `vip_assign_products`.`productID`
LEFT JOIN `vip_manufacturer` ON `vip_product`.`manufacturer_id` = `vip_manufacturer`.`manufacturer_id`
WHERE (`status`='E') AND (`clientID`=$clientID) AND (`vip_manufacturer`.`image` IS NOT NULL) GROUP BY `vip_manufacturer`.`manufacturer_id`";
        
        $manufacturers = \common\models\VIPProduct::findBySql($sql)->asArray()->all();

        return $this->render('manufacturerswidget', ['manufacturers' => $manufacturers]);
        
    }
}