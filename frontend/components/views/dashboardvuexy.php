<?php
use yii\helpers\Html;
    /* This sets the $time variable to the current hour in the 24 hour clock format */
    $time = date("H");
    /* Set the $timezone variable to become the current timezone */
    $timezone = date("e");
    /* If the time is less than 1200 hours, show good morning */
    if ($time < "12") {
        $greetings = "GOOD MORNING";
    } else
    /* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
    if ($time >= "12" && $time < "17") {
        $greetings = "GOOD AFTERNOON";
    } else
    /* Should the time be between or equal to 1700 and 1900 hours, show good evening */
    if ($time >= "17" && $time < "24") {
        $greetings = "GOOD EVENING";
    }

?>
<style>
    .fonticon-wrap-desk {font-size: 2.9rem;}
    .pr-0-2{
        padding-right: 0.2rem !important;
    }
    .pl-0-2{
        padding-left: 0.2rem !important;
    }
    .cus-title {font-size: 1.02rem !important;}
    .btn-low{padding: 0.5rem 0.5rem !important;}
    .no-padding {
        padding-left: 5px;
        padding-right: 2px;
        margin-bottom: 5px;
    }
    a h5 {
        color: #7367F0 !important;
    }
</style>

<section id="dashboard-analytics">
    
    <div class="row">       
        <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="card bg-analytics">
                <div class="card-content">
                    <ul class="list-group list-group-flush">

                        <li class="list-group-item">
                            <?= Yii::t('app', 'Company Name') ?>
                            <span class="float-lg-right"><?= Yii::$app->VIPglobal->webapiuserinfo()['company_name'] ?></span>
                            
                        </li>
                        <li class="list-group-item">
                            <span class="float-right"><?= Yii::$app->VIPglobal->webapiuserinfo()['state'] ?></span>
                            <?= Yii::t('app', 'State') ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="col-lg-6 col-md-6 col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-end">
                    <h4 class="mb-0"><?= Yii::t('app', 'My Points') ?></h4>                    
                </div>
                <div class="card-content">
                    <div class="card-body px-0 pb-0" style="padding: 0px;">
                        <div id="goal-overview-chart" class="mt-75"></div>
                        <div class="row text-center mx-0">
                            <div class="col-6 border-top border-right d-flex align-items-between flex-column py-1">
                                <p class="mb-50"><?= Yii::t('app', 'Available Balance') ?></p>
                                <p class="font-large-1 text-bold-700">
                                    <?php
                                     echo number_format(Yii::$app->VIPglobal->webapiuserinfo()['point']);
                                    ?>
                                </p>
                            </div>
                            <div class="col-6 border-top d-flex align-items-between flex-column py-1" style="padding-left: 0px;">
                                <p class="mb-50"><?= Yii::t('app', 'Expiring on') ?> <?= date('d-m-y', strtotime(Yii::$app->VIPglobal->webapiuserinfo()['pointexpireddate'])) ?></p>
                                <p class="font-large-1 text-bold-700">
                                    <?php
                                        $exppoint = Yii::$app->VIPglobal->webapiuserinfo()['pointexpiring'];
                                        echo number_format($exppoint);

                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Account 2 Summery Box -->
            
        </div>

    </div>
    <div class="row">
        <div class="btn-group col-lg-12 col-md-12 mb-1 text-center" role="group" aria-label="Basic example">
            <a href="/rewards" class="btn btn-primary waves-effect waves-light col-md-4 col-lg-4"><div class="fonticon-wrap-desk mb-1"><i class="feather icon-gift"></i></div><?= Yii::t('app', 'Redeem Points') ?></a>
            <a href="/order-history" class="btn btn-danger waves-effect waves-light col-md-4 col-lg-4"><div class="fonticon-wrap-desk mb-1"><i class="feather icon-file-text"></i></div><?= Yii::t('app', 'Order History') ?></a>
            <a href="/terms-conditions" class="btn btn-info waves-effect waves-light col-md-4 col-lg-4"><div class="fonticon-wrap-desk mb-1"><i class="feather icon-help-circle"></i></div><?= Yii::t('app', 'Terms & Conditions') ?></a>
        </div>
    </div>
    
    <div class="row"><div class="col-lg-12"><h4 class="card-title mt-100 mb-50"><?= Yii::t('app', 'Categories') ?></h4></div></div>
    <div class="row" style="padding-left: 10px; padding-right: 12px;">
        <?php
        foreach($categories as $categorie) {
            if (file_exists(Yii::getAlias('@backend').'/web/upload/product_cover/thumbnail/'.$categorie->main_image)) {
                $imagename = $categorie->main_image;
            }else {
                $imagename = 'no_image.png';
            }
            $productcount = \common\models\VIPProduct::find()->where(['category_id' => $categorie->category_id])->count();
            echo '<div class="col-lg-2 col-sm-4 col-4 no-padding">
                <div class="card mb-1" style="height: 100%;">
                    <a href="/rewards?link='.$categorie->categories->slug.'" class="btn btn-default btn-block btn-low text-primary">
                        <img src="'.Yii::$app->params['imgdomain'].'/upload/product_cover/thumbnail/'.$imagename.'" alt="element 05" class="mb-0 img-fluid">           
                        <h5 class="card-title cus-title">'.$categorie->categories->name.'</h5>
                        <p class="card-text">'.$productcount.' items</p>
                    </a>

                </div>
            </div>';
        }
        ?>
    </div>
</section>

<h4 class="card-title mt-100 mb-0"><?= Yii::t('app', 'Products') ?></h4>
<section id="wishlist" class="grid-view wishlist-items">
    <?php
    $states = Yii::$app->VIPglobal->webapistate();

    foreach($products as $product) {
        if (file_exists(Yii::getAlias('@backend').'/web/upload/product_cover/thumbnail/'.$product->main_image)) {
            $proimagename = $product->main_image;
        }else {
            $proimagename = 'no_image.png';
        }
        if($states > 0) {
            $point = Yii::$app->VIPglobal->getTotalcostpoint($product->product_id,$states);
        }else {
            $point = 'N/A';
        }
        echo '<div class="card ecommerce-card">        
             <a href="/products/product/product-detail?id='.$product->product_id.'"><div class="card-content">
                <div class="item-img text-center">
                    <img src="'.Yii::$app->params['imgdomain'].'/upload/product_cover/thumbnail/'.$proimagename.'" class="img-fluid" alt="img-placeholder">
                </div>
                <div class="card-body">
                    <div class="item-name">'.$point.' - pts</div>
                    <div><p class="item-description">'.$product->product_name.'</p></div>
                </div>
            </div>
            </a>
        </div>';
    }
    ?>
    
</section>