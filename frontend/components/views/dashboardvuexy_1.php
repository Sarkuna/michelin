<?php
    /* This sets the $time variable to the current hour in the 24 hour clock format */
    $time = date("H");
    /* Set the $timezone variable to become the current timezone */
    $timezone = date("e");
    /* If the time is less than 1200 hours, show good morning */
    if ($time < "12") {
        $greetings = "GOOD MORNING";
    } else
    /* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
    if ($time >= "12" && $time < "17") {
        $greetings = "GOOD AFTERNOON";
    } else
    /* Should the time be between or equal to 1700 and 1900 hours, show good evening */
    if ($time >= "17" && $time < "24") {
        $greetings = "GOOD EVENING";
    }

?>

<section id="dashboard-analytics">
    
    <div class="row">       
        <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="card bg-analytics">
                <div class="card-content">
                    <div class="card-body text-center text-white">
                        <img src="<?php echo $this->theme->baseUrl ?>/app-assets/images/elements/decore-left.png" class="img-left" alt="
                             card-img-left">
                        <img src="<?php echo $this->theme->baseUrl ?>/app-assets/images/elements/decore-right.png" class="img-right" alt="
                             card-img-right">
                        <div class="avatar avatar-xl bg-primary shadow mt-0">
                            <div class="avatar-content">
                                <?php
                            //$picPro = '<img alt="User profile picture" src="/site/view-avatar" class="round" height="40" width="40">';
                            $picPro = '<img class="img-fluid" src="/site/view-avatar" alt="img placeholder">';
                            //echo '<img alt="avatar" src="/site/view-avatar" class="round" height="40" width="40">';
                            echo $picPro;
                        ?>
                            </div>
                        </div>
                        <div class="text-center">

                            <h1 class="mb-2 text-white">
                            <?= Yii::t('app', $greetings) ?><BR><?= Yii::$app->VIPglobal->webapiuserinfo()['ContactPerson'] ?></h1>
                            
                        </div>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <span class="float-right"><?= Yii::$app->VIPglobal->webapiuserinfo()['name'] ?></span>
                            <?= Yii::t('app', 'Code') ?>
                        </li>
                        <li class="list-group-item">
                            <?= Yii::t('app', 'Company Name') ?>
                            <span class="float-right"><?= Yii::$app->VIPglobal->webapiuserinfo()['company_name'] ?></span>
                            
                        </li>
                        <li class="list-group-item">
                            <span class="float-right"><?= Yii::$app->VIPglobal->webapiuserinfo()['state'] ?></span>
                            <?= Yii::t('app', 'State') ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="col-lg-6 col-md-6 col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-end">
                    <h4 class="mb-0"><?= Yii::t('app', 'My Points') ?></h4>                    
                </div>
                <div class="card-content">
                    <div class="card-body px-0 pb-0" style="padding: 0px;">
                        <div id="goal-overview-chart" class="mt-75"></div>
                        <div class="row text-center mx-0">
                            <div class="col-6 border-top border-right d-flex align-items-between flex-column py-1">
                                <p class="mb-50"><?= Yii::t('app', 'Available Balance') ?></p>
                                <p class="font-large-1 text-bold-700">
                                    <?php
                                     echo number_format(Yii::$app->VIPglobal->webapiuserinfo()['point']);
                                    ?>
                                </p>
                            </div>
                            <div class="col-6 border-top d-flex align-items-between flex-column py-1">
                                <p class="mb-50"><?= Yii::t('app', 'Expiring on') ?> <?= Yii::$app->VIPglobal->webapiuserinfo()['pointexpireddate'] ?></p>
                                <p class="font-large-1 text-bold-700">
                                    <?php
                                        $exppoint = Yii::$app->VIPglobal->webapiuserinfo()['pointexpiring'];
                                        echo number_format($exppoint);

                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Account 2 Summery Box -->
            
        </div>

    </div>
    <div class="row">
        
        <div class="col-xl-4 col-md-12 col-sm-12">
            <a href="/rewards">
            <div class="card text-center">
                <div class="card-content">                    
                    <div class="card-body">
                        <div class="p-50 m-0 mb-1">                            
                                <div class="avatar-content">
                                    <i class="fa fa-gift text-primary fa-5x"></i>
                                </div>                            
                        </div>
                        <h2 class="text-bold-700"><?= Yii::t('app', 'Redeem Your Points') ?></h2>
                        <p class="mb-0 line-ellipsis"><?= Yii::t('app', 'Choose from our wide variety of merchandise') ?></p>
                    </div>
                    
                </div>
            </div>
            </a>
        </div>
        </a>
        
        
        <div class="col-xl-4 col-md-12 col-sm-12">
            <a href="/order-history">
            <div class="card text-center">
                <div class="card-content" >
                    <div class="card-body">
                        <div class="p-50 m-0 mb-1">                            
                            <div class="avatar-content">
                                <i class="feather icon-file-text text-primary fa-5x"></i>
                            </div>
                        </div>
                        <h2 class="text-bold-700"><?= Yii::t('app', 'Order History') ?></h2>
                        <p class="mb-0 line-ellipsis"><?= Yii::t('app', 'View your complete order history') ?></p>
                    </div>
                </div>
            </div>
            </a>    
        </div>
        
        
        
        <div class="col-xl-4 col-md-12 col-sm-12">
            <a href="/terms-conditions">
            <div class="card text-center">
                <div class="card-content" >
                    <div class="card-body">
                        <div class="p-50 m-0 mb-1">
                            
                            <div class="avatar-content">
                                <i class="feather icon-help-circle text-primary fa-5x"></i>
                            </div>
                        </div>
                        <h2 class="text-bold-700"><?= Yii::t('app', 'Terms & Conditions') ?></h2>
                        <p class="mb-0 line-ellipsis"><?= Yii::t('app', 'View your Terms and Conditions') ?></p>
                    </div>
                </div>
            </div>
            </a>
        </div>
        </a>
    </div>
    
    
</section>