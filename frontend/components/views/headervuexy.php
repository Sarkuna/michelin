<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\components\ShoppingCartBagWidget;
//use common\models\AssignModules;
//$session = Yii::$app->session;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<style>
    .cart-dropdown .cart-dropdown-item-img {
    max-width: 100%;
    max-height: 100%;
    width: auto;
    transition: .35s;
}
.p-4 {
    padding: 1rem!important;
}
</style>
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav">
                        <li class="nav-item mobile-menu d-xl-none mr-auto">
                            <a class="nav-link nav-menu-main hidden-xs" href="<?= Yii::$app->homeUrl ?>">
                                <i class="ficon feather icon-home"></i>
                                
                            </a>
                            
                        </li>
                    </ul>
                    
                </div>
                
                <ul class="nav navbar-nav float-right">
                    <?php
                        $currentLang = \Yii::$app->language;
                        if ($currentLang == 'ms') {
                            $sellang = 'Bahasa';
                            $langicon = 'my';
                        } else if ($currentLang == 'zh-CN') {
                            $sellang = '简体中文';
                            $langicon = 'cn';
                        } else {
                            $sellang = 'English';
                            $langicon = 'us';
                        }
                    ?>
                    <li class="dropdown dropdown-language nav-item">
                        <a class="dropdown-toggle nav-link" id="dropdown-flag" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flag-icon flag-icon-<?= $langicon ?>"></i><span class="selected-language"><?= $sellang ?></span></a>
                        <div class="dropdown-menu" aria-labelledby="dropdown-flag">
                            <?= Html::a('<i class="flag-icon flag-icon-us mr-50"></i> English</a>', ['/site/language'], ['class' => 'dropdown-item', 'data-language' => 'en', 'data-method' => 'POST','data-params' => ['_lang' => "en",],]) ?>
                            <?= Html::a('<i class="flag-icon flag-icon-my mr-50"></i> Bahasa</a>', ['/site/language'], ['class' => 'dropdown-item', 'data-language' => 'ms', 'data-method' => 'POST','data-params' => ['_lang' => "ms",],]) ?>
                            <?= Html::a('<i class="flag-icon flag-icon-cn mr-50"></i> 简体中文</a>', ['/site/language'], ['class' => 'dropdown-item', 'data-language' => 'zh-CN', 'data-method' => 'POST','data-params' => ['_lang' => "zh-CN",],]) ?>
                        </div>
                    </li>
                    <?= ShoppingCartBagWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
                    

                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                            <div class="user-nav d-sm-flex">
                                <span class="user-name text-bold-600">
                                    <?php
                                    $picPro = '<img class="round" src="/site/view-avatar" alt="img placeholder" height="40" width="40">';
                            //echo '<img alt="avatar" src="/site/view-avatar" class="round" height="40" width="40">';
                            echo $picPro;
                                    ?>
                                    
                                </span>
                            </div>

                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <?= Yii::$app->VIPglobal->webapiuserinfo()['name'] ?>

                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
