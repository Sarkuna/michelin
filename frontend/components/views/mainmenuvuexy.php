<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
//use common\models\AssignModules;
//$session = Yii::$app->session;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//$clientID = $session['currentclientID'];
//$mod1 = AssignModules::find()->where(['clientID' => $clientID,'module_id' => '1',])->count();
//$mod2 = AssignModules::find()->where(['clientID' => $clientID,'module_id' => '2',])->count();
?>
<style>
   .blink_me {
  animation: blinker 1s linear infinite;
  color: red;
}

@keyframes blinker {  
  50% { opacity: 0; }
} 
</style>
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="/">
                    <img src="<?= Yii::$app->VIPglobal->getSiteInfo()['logo'] ?>" class="img-responsive" alt="" width="150">
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">

        <?php
            $myurl = Yii::$app->request->url;
            $items[] = '';
            $menuothers[] = '';
            $menuItems1[] = '';
            $menuItems2[] = '';
            $menuItems3[] = '';
            $productlist = '';
            $receipts_summary = '';
            if (!empty($pages->menuitems)) {
                $items[] = ['label' => '<span>Pages</span>', 'options' => ['class' => 'navigation-header']];
                foreach ($pages->menuitems as $page) {
                    $title = Html::encode($page->title);
                    if ($page->related_id > 0) {
                        $link = '/' . $page->link;
                    } else {
                        $link = $page->link;
                    }
                    $items[] = [
                        'label' => '<i class="feather icon-circle"></i><span class="menu-title" data-i18n="' . $title . '">' . $title . '</span>',
                        'template' => '<a href="'.$link.'" target="'.$page->target.'">{label}</a>',
                        'active' => $myurl == $link,
                    ];
                }
            }
            
            //echo '<pre>';print_r($items);die;

            //$items = array_merge($items,$menuItems);

            $home = ['label' => '<i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span>', 'url' => [Yii::$app->homeUrl]];
            /*if(Yii::$app->user->identity->type == 5){
                $menuothers = ['label' => '<i class="fa fa-file-text-o"></i><span class="menu-item" data-i18n="Shop">Receipts History</span>', 'url' => ['/receipts-history'], 'active' => in_array($myurl, ['/receipts-history','/receipts-history?page='.Yii::$app->request->getQueryParam('page'), '/receipts-history/submit-invoices']),];
            }
            
            if(Yii::$app->VIPglobal->flashDeal() > 0 ){
                $flashDeal = ['label' => '<i class="feather icon-gift"></i><span class="menu-item" data-i18n="Merchandise"><span class="blink_me">Flash Deal</span></span>', 'url' => ['/rewards'], 'active' => in_array($myurl,['/rewards']),];
            }else {
                $flashDeal = ['label' => '<i class="feather icon-gift"></i><span class="menu-item" data-i18n="Merchandise">Rewards</span>', 'url' => ['/rewards'], 'active' => in_array($myurl,['/rewards']),];
            }*/
            $menuItems1 = [
                $home,
                ['label' => '<span>Apps</span>', 'options' => ['class' => 'navigation-header']],
                //$flashDeal,
                ['label' => '<i class="feather icon-gift"></i><span class="menu-item" data-i18n="Merchandise">Rewards</span>', 'url' => ['/rewards'], 'active' => in_array($myurl,['/rewards']),],
                //['label' => '<i class="feather icon-award"></i><span class="menu-item" data-i18n="Shop">Reward Points</span>', 'url' => ['/my-reward-points'], 'active' => in_array($myurl, ['/my-reward-points','/my-reward-points?page='.Yii::$app->request->getQueryParam('page')]),],
                ['label' => '<i class="fa fa-wpforms"></i><span class="menu-item" data-i18n="Shop">Order History</span>', 'url' => ['/order-history'], 'active' => in_array($myurl, ['/order-history','/order-history?page='.Yii::$app->request->getQueryParam('page'),'/order-history/view/'.Yii::$app->request->getQueryParam('id')]),],
                //['label' => '<i class="fa fa-wpforms"></i><span class="menu-item" data-i18n="Shop">Flash Deal History</span>', 'url' => ['/flash-deal-history'], 'active' => in_array($myurl, ['/flash-deal-history','/view?page='.Yii::$app->request->getQueryParam('page'),'/flash-deal-history/view/'.Yii::$app->request->getQueryParam('id')]),],
                //$menuothers 
            ];

            $menuItems = array_merge($menuItems1,$items,$menuItems2,$menuItems3);
            

            
            echo Menu::widget([
                'options' => ['class' => 'navigation navigation-main', 'id' => 'main-menu-navigation', 'data-menu' => 'menu-navigation'],
                'items' => $menuItems,
                'submenuTemplate' => "<ul class='menu-content'>{items}</ul>",
                'encodeLabels' => false, //allows you to use html in labels
                //'activateParents' => true,
                ]);
        ?>
    </div>
</div>