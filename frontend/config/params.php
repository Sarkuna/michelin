<?php
return [
    'adminEmail' => 'admin@example.com',
    'msg.upload.receipt' => 'Please be reminded that the uploaded receipts/invoices must be less than 6 months from today\'s date.',
    'custom.limit' => 5,
    'languages'=>[
        'en'=>'English', 
        'ms'=>'Bahasa', 
        'zh-CN'=>'中国的',
    ],
];
