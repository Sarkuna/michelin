<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;
use YoHang88\LetterAvatar\LetterAvatar;

use common\models\LoginForm;
//use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    public function beforeAction($action)
    {
        if ($action->id == 'error') {
            $this->layout = 'main_error';
        }

        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error',  'login-fail', 'webapi', 'language'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'language', 'view-avatar', 'maintenance'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    

    /**
     * @inheritdoc
     */
    public function actions()
    {
        

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                //$this->context->layout = 'main_product_detail',
                //'layout' => '@app/themes/vuexy/layouts/main_product_detail',
                //$this->layout = '@app/themes/vuexy/layouts/main_product_detail';
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
   
    
    public function actionIndex()
    {
        $this->layout = 'main_dashboard';
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        if (!Yii::$app->user->isGuest) {
            return $this->render('index');
        } else {
            Yii::$app->user->logout();
        }
        //return $this->redirect(['home']);
    }
    
    /**
     * Ajax handler for language change dropdown list. Sets cookie ready for next request
     */
    public function actionLanguage()
    {
        //$param1 = Yii::$app->request->post('_lang', null);
        if ( Yii::$app->request->post('_lang') !== NULL && array_key_exists(Yii::$app->request->post('_lang'), Yii::$app->params['languages']))
        {
            Yii::$app->language = Yii::$app->request->post('_lang');
            $cookie = new yii\web\Cookie([
            'name' => '_lang',
            'value' => Yii::$app->request->post('_lang'),
            ]);
            Yii::$app->getResponse()->getCookies()->add($cookie);
            //Yii::$app->controller->refresh();
            //return $this->goHome();
            return $this->redirect(Yii::$app->request->referrer);
        }
        Yii::$app->end();
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = 'authmain';
        
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        return $this->render('log_out');
    }
    
    public function actionLoginFail()
    {
        $this->layout = 'authmain';
        return $this->render('login_fail');
    }
    

    public function actionWebapi()
    {
        $request = Yii::$app->request;
        $ADRegNo = htmlentities(trim($request->get('user')), ENT_QUOTES);
        $point = htmlentities(trim($request->get('point')), ENT_QUOTES);
        $pointexpiring = htmlentities(trim($request->get('pointexpiring')), ENT_QUOTES);
        $pointexpireddate = htmlentities(trim($request->get('pointexpireddate')), ENT_QUOTES);

        $result = Yii::$app->VIPglobal->webapi($ADRegNo);
        
        if( $result === null || $result == FALSE || $result == '' )
        {
            return $this->redirect(['/site/login-fail']);
        }else {
            $session = Yii::$app->session;
            $session->destroy();
            $session['name'] = $ADRegNo;
            
            $session['pointexpiring'] = $pointexpiring;
            $session['pointexpireddate'] = $pointexpireddate;
            
            $identity = \common\models\Userapp::findOne(['username' => $result['RegistrationNumber']]);
            if(empty($identity)) {
                $user = new \common\models\Userapp();
                $user->username = $result['RegistrationNumber'];         
                $user->access_token = Yii::$app->security->generateRandomString();
                $user->available_point = $point;
                $user->date_last_login = date('Y-m-d H:i:s');
                $user->user_agent = $_SERVER['HTTP_USER_AGENT'];
                if($user->save(false)) {
                    $identity = \common\models\Userapp::findOne(['username' => $result['RegistrationNumber']]);
                    $session['point'] = $identity->available_point;
                     if (Yii::$app->user->login($identity)) {
                         Yii::$app->VIPglobal->addtolog('success', Yii::$app->user->id);
                        return $this->goHome();
                    }
                }  else {
                    echo 'error';
                }
            }else {
                $identity = \common\models\Userapp::findOne(['username' => $result['RegistrationNumber']]);
                $identity->available_point = $point;
                $identity->date_last_login = date('Y-m-d H:i:s');
                $identity->user_agent = $_SERVER['HTTP_USER_AGENT'];
                $identity->save(false);
                
                $session['point'] = $identity->available_point;
                if (Yii::$app->user->login($identity)) {
                    Yii::$app->VIPglobal->addtolog('success', Yii::$app->user->id);
                    return $this->goHome();
                }
            }
            
        }
    }
    


    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    
    public function actionViewAvatar() {
        $size = Yii::$app->request->get('size');
        $name = Yii::$app->VIPglobal->webapiuserinfo()['ContactPerson'];
        if(!empty($name)){
            header('Content-Type: image/png');
            $imageSize = !empty($size) ? $size : 200;
            $setting = array('size' => $imageSize);
            $letterName = str_replace(' - ', ' ', $name);
            $letterName = str_replace(' -', ' ', $letterName);
            $avatar = \common\helper\Avatar::generateAvatarFromLetter($letterName, $setting);
            echo $avatar;
            exit();
        }else if(empty($photo)){
            $extension = 'png';
            $photo = Yii::getAlias('@webroot') . '/web/images/avatar_image.jpg';
        }
        
        header('Content-Type: image/'.$extension);
        $file = file_get_contents($photo);
        echo $file;
        exit();
    }

}
