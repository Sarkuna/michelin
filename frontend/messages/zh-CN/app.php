<?php
return [
    'Username' => '账号 / 用户名',
    'Mobile/Username' => '手机/ 账号 （用户名）',
    'Password' => '密码',
    'Keep me logged in' => '保持登录状态',
    'Forgot Password?' => '忘了密码',
    'Login' => '登录',
    'Don\'t have an account?' => '没有帐号',
    'Sign up' => '注册',
    'Contact Us' => '联系我们',
    'Username Cannot Be Blank' => '账号 不可留空',
    'Password Cannot Be Blank' => '密码不可留空',
    'Incorrect Username Or Password' => '账号 或密码不正确',
    'Please enter your details to sign up and be part of our great community' => '请输入您的详细信息以注册并加入我们的大家庭',
    'Personal Info' => '个人信息',
    'Name as per bank account' => '姓名（必须与银行账户上的姓名一致)',
    'NRIC No' => '身份证号码',
    'NRIC Photo' => '身份证照片',
    'Choose File' => '选择档案',
    'Browse' => '浏览',
    'H/P No.' => '手机',
    'Send' => '发送',
    'SMS Verification Code' => '短信验证码',
    'Email' => '电子邮箱',
    'Dealer Name' => '经销商名称',
    'Select' => '选择',
    'Company Info' => '公司信息',
    'Company Name' => '公司名称',
    'CIDB No' => 'CIDB编号',
    'Bank Info' => '银行信息',
    'Account Type' => '银行账号类别',
    'Bank Name' => '银行名字',
    'Account Name' => '银行账号名字',
    'Account Number' => '银行账号',
    'Address Info' => '地址信息',
    'Address 1' => '地址 1',
    'Address 2' => '地址 2',
    'City' => '城市',
    'State' => '州',
    'Postcode' => '邮政区码',
    'Country' => '国家',
    'I accept the {link}terms{link2} & conditions.' => '我接受 {link}条款{link2} 和条件', 
    'Sign Up' => '注册',
    'Name As Per Bank Account Cannot Be Blank' => '姓名（如银行帐户)不可留空',
    'Nric No Cannot Be Blank.' => '身份证号码不可留空',
    'Nric Photo Cannot Be Blank.' => '身份证照片不可留空',
    'H/P No. Cannot Be Blank.' => '手机号码不可留空',
    'Sms Verification Code Cannot Be Blank.' => '短信验正码不可留空',
    'Dealer Name Cannot Be Blank.' => '经销商名称不可留空',
    'Company Name Cannot Be Blank.' => '公司名称不可留空',
    'Account Type Cannot Be Blank.' => '银行账号类别不可留空',
    'Bank Name Cannot Be Blank.' => '银行名字不可留空',
    'Account Number Cannot Be Blank.' => '银行账号不可留空',
    'Address 1 Cannot Be Blank.' => '地址1不可留空',
    'State Cannot Be Blank.' => '州不可留空',
    'City Cannot Be Blank.' => '城市不可留空',
    'Postcode Cannot Be Blank.' => '邮政区码不可留空',
    'Home' => '首页',
    'Dashboard' => '仪表盘',
    'GOOD MORNING' => '早安',
    'Upload Your Receipt' => '请上载您的收据',
    'Pending Invoices' => '有待处理的发票',
    'Approved Invoices' => '已批准的发票',
    'Pay Out Pending' => '付款有待处理',
    'Hold Your Device Steady To Avoid A Blurry Image' => '请平稳的握住设备以避免图像模糊',
    'Click Here' => '点击这里',
    'Invoices Overview' => '发票总览',
    'Date' => '日期',
    'Invoice' => '发票',
    'Dealer Invoice' => '经销商发票',
    'Status' => '处理阶段',
    'Total' => '总额',
    'Claims Overview' => '索赔总览',
    'Edit Profile' => '修改个人资料',
    'Change Password' => '更改密码',
    'Logout' => '退出',
    'Full Name' => '全名',
    'Ic No' => '身份证号码',
    'Dob' => '出生日期',
    'Gender' => '性别',
    'Race' => '种族',
    'Submit' => '提交',
    'Back' => '返回',
    'Update Profile' => '更新个人信息',
    'Update' => '更新',
    'Full Name Cannot Be Blank.' => '全名不可留空',
    'Dob Cannot Be Blank.' => '出生日期不可留空',
    'Race Cannot Be Blank.' => '种族不可留空',
    'Change Your Password' => '更改您的密码',
    'Current Password' => '目前的密码',
    'New Password' => '新密码',
    'Confirm Password' => '确认密码',
    'This Is Required' => '必须填写',
    'Apps' => '应用程式',
    'Invoices' => '发票',
    'Upload Invoice' => '上传发票',
    'Decline' => '拒绝',
    'Manage My Account' => '我的账号管理',
    'My Profile' => '我的个人信息',
    'Company' => '公司',
    'All Rights Reserved' => '保留所有权利',
    'Mobile' => '手机号码',
    'Date Of Birth' => '出生日期',
    'Nric/Passport No.' => '身份证/护照号',
    'Edit' => '修改',
    'Information' => '信息',
    'Company' => '公司',
    'Dealer State' => '经销商的州属',
    'Update Company' => '更新公司',
    'Tel' => '电话号码',
    'Company Name Cannot Be Blank' => '公司名称不可留空',
    'Bank' => '银行',
    'Name of Account Holder' => '账户持有人姓名',
    'Upload Receipt' => '上载收据',
    'Receipt History' => '收据记录',
    'Receipt No' => '收据编号',
    'Receipt Date' => '收据日期',
    'Receipt[s]' => '收据',
    'Receipt[s] Cannot Be Blank.' => '收据不可留空',
    'Browser ..' => '浏览器',
    'Select Files...' => '选择档案',
    'Drag & Drop Files Here' => '将文件拖放到此处',
    'Invoice List' => '发票清单',
    'Search Invoices' => '搜索发票',
    'Transaction' => '交易',
    'Created Date' => '填写日期',
    'Submission Date' => 'Tarikh Penyerahan',
    'Previous' => '上页',
    'Next' => '下页',
    'No of Receipts Uploaded' => '已上载收据的数量',
    'Area' => '区域',
    'Dealer Invoice No' => '经销商发票编号',
    'Dealer Invoice Date' => '经销商发票日期',
    'Product Description' => '产品说明',
    'Price Total' => '总价',
    'Qty' => '数量',
    'Per Unit' => '每单位',
    'Total Items' => '顶目总数',
    'Grand Total' => '综计（总额）',
    'Paid' => '已付费',
    'Search Invoice' => '搜索发票',
    'Invoice' => '发票',
    'Declined' => '拒绝',
    'Phone number does not exist' => '该电话号码不存在',
    'Success' => '成功',
    'Your password has been successfully reset' =>  '您的密码已被重置',
    'Please make sure both fields are the same' => '请确保两个领域相同',
    'Pending' => '待定',
    'Approved' => '已被批准',
    'Processing' => '处理中',
    'Invoice History, Snap & Earn' => '发票记录，快照及赚取',
    'View and Update Company Info' => '查看及更新公司资料',
    'Cash Rebate Scheme' => '现金回扣格式',
    'Product Information' => '产品资讯',
    'Terms & Conditions' => '条款和条件',
    'Contact Us' => '联系我们',
    'Male' => '男',
    'Female' => '女',
    'Not Selected' => '未选择',
    'Snap & Earn' => '快照及赚取',
    'Scan Your Receipts' => '请扫描您的收据',
    'Copyright' => '版权',
    'Let\'s get started by keying in your mobile number' => '请开始输入您的手机号码',
    'Set your PIN' => '请设置密码',
    'Enter new 6-digit PIN' => '输入新的6位数密码',
    'Confirm your PIN' => '请确认密码',
    'Approved Amount' => '已批准金额',
    'Processing Invoices' => '处理发票',
    'Pages' => '页数',
    'Point System' => '积分系统',
    'Latest Promotion' => '最新促销',
    'Contact Us' => '联系我们',
    'All Declined Lists' => '所有被拒收据清单',
    'All Receipt Lists' => '所有收据清单',
    'All Paid Lists' => '所有已付费收据清单',
    'GOOD MORNING' => '早上好',
    'GOOD AFTERNOON' => '下午好',
    'GOOD EVENING' => '晚上好',
    'All rights Reserved' => '保留所有权利',
    'Name of Account Holder' => '账户持有人姓名',
    'Browse' => '浏览器',
    'Select files...' => '选择档案',
    'Browse ...' => '浏览器',
    'Hold your device steady to avoid a blurry image' => '请平稳的握住设备以避免图像模糊',
    'Already have an account' => '已有帐号',
    'Handphone No.' => '手机号码',
    'Personal' => '个人账户',
    'Company' => '公司',
    'Sign in' => '登录',
    'Choose file' => '选择档案',
    'Home' => '首页',
    'Your password has been changed successfully' => '您的密码已被成功更换',
    'Your password has not been updated' => '您的密码未被更新',
    'Receipt number has already been taken' => '收据编号已被采用.',
    'Your password haven\'t been reset' => '您的密码未被重置',
    'Your verification code is' => '您的验证码是',
    //'RM' => '马币',
    'Drafts' => '草稿',
    'Send Password' => '发送密码',
    'Enter the email or phone number you used when you joined and we will send you temporary password' => '输入您加入时使用的电子邮件或电话号码，我们将向您发送临时密码',
    'Email or Phone' => '邮件或者电话',
    'I remembered my password' => '我记得我的密码',
    'Re-type New Password' => '请重新输入新密码',
    'SMS OTP' => '短信OTP',
    'Enter a SMS OTP' => '输入短信OTP',
    'Enter a new password' => '输入新的密码',
    'Confirm your new password' => '确认新密码',
    'Reset my password' => '重置我的密码',
    'Reset your Password' => '请重置您的密码',
    'OTP has been successfully send' => '已成功发送暂时性密码',
    'SMS not sent' => '短信未被发送',
    'Phone number does not exist' => '电话号码不存在',
    'Password not match' => '密码不匹配',
    'OTP Code Invalid' => '一次性密码无效',
    'Reset Your Password' => '重置你的密码',
    'New password saved' => '新密码已被保存',
    'Code' => '代码',
'Redeem Your Points' => '兑换您的积分',
'Choose from our wide variety of merchandise' => '从我们各种商品中选择',
'My Points' => '我的积分',
'Available Balance' => '积分余额',
'Expiring on' => '截止日期',
'Order History' => '订单管理',
'View your complete order history' => '查看您的订单',
'FAQ' => '问题专区',
'Frequently Asked Questions' => '常问问题',
'All Orders' => '订单',
'ORDER DATE' => '订购日期',
'ORDER #' => '订单号码',
'TOTAL POINT' => '总积分',
'STATUS' => '订单状态',
'Complete' => '完成',
'Refunded' => '已退还',
'Accepted' => '已接受',
'Cancelled' => '已取消',
'ORDER NO.' => '订单号码',
'Delivery Address' => '邮寄地址',
'PRODUCT NAME' => '产品名称',
'MODEL' => '模型',
'QUANTITY' => '数量',
'UNIT POINTS' => '单位点',
'POINTS' => '积分',
'Terms And Conditions' => '附带条约',
'View your Terms and Conditions' => '查看您的附带条约',
    'Categories' => '类别',
    'Products' => '产品'
];