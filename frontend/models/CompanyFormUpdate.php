<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;
use YoHang88\LetterAvatar\LetterAvatar;
use borales\extensions\phoneInput\PhoneInputBehavior;
use borales\extensions\phoneInput\PhoneInputValidator;

use common\models\User;
use common\models\VIPCustomer;
use common\models\AuthAssignment;
use common\models\CompanyInformation;

$session = Yii::$app->session;

/**
 * Signup form
 */
class CompanyFormUpdate extends Model
{

    public $company_name;
    public $company_address;
    public $ssm_number;
    public $income_tax_no;
    

    //public $reCaptcha;

    public function behaviors() {
        return [
            BlameableBehavior::className(),
            'phoneInput' => PhoneInputBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_name', 'company_address', 'ssm_number','income_tax_no'], 'required'],
            [['company_address'], 'string', 'max' => 300],      
        ];
    }


    public function attributeLabels()
    {
        return [
            'company_name' => 'Company Name as registered with SSM',
            'company_address' => 'Company Address',
            'ssm_number' => 'SSM Number',
            'income_tax_no' => 'Income Tax No.'
        ];
    }
}