<?php

//namespace app\modules\support\controllers;
namespace app\modules\account\controllers;

use yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use Imagine\Image\Box;

use common\models\VIPProduct;
use common\models\VIPCustomerAddress;
use common\models\VIPCustomerRewardSearch;
use common\models\VIPOrder;
use common\models\VIPOrderSearch;
use common\models\VIPCustomer;
use common\models\VIPCustomerSearch;
use common\models\VIPCustomerBank;
use common\models\ClientOption;
use common\models\ReceiptHistory;
use common\models\VIPCustomerAccount2Search;

/**
 * Default controller for the `support` module
 */

class MyAccountController extends Controller
{
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $model = '';
        
        return $this->render('index', [
            'model' => $model,
        ]);
    }
    
    public function beforeAction($action) {
        $session = Yii::$app->session;

        if (parent::beforeAction($action)) {
            
            if (in_array($action->id, array('view'))) {
                if (!empty($_GET['id'])) {
                    $numRows = VIPOrder::find()
                            ->where([
                                'customer_id' => Yii::$app->user->id,
                                'order_id' => $_GET['id']
                            ])
                            ->count();
                    if ($numRows == 0)
                        throw new ForbiddenHttpException('You are trying to view or modify restricted record.', 403);
                }
            }
            return true;
        } else {
            return false;
        }

    }
    
    
    
    public function actionOrder(){
        $this->layout = '@app/themes/vuexy/layouts/main_order';
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $searchModel = new VIPOrderSearch();
        //$searchModel->clientID = $clientID;

        $searchModel->customer_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$dataProvider->query->andWhere(['flash_deals_id' => 0]);

        return $this->render('order', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        
    }
    
    public function actionView($id)
    {        
        $session = Yii::$app->session;
        $Model = VIPOrder::find()->where(['order_id' => $id,'customer_id' => Yii::$app->user->id])->one();
        if(count($Model) > 0){
            return $this->render('view', [
                'model' => $Model,
            ]);
        }else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionTermsConditions(){
        return $this->render('tc');
    }
    


    protected function clientOption()
    {
        $clientID = Yii::$app->user->identity->client_id;

        $rd = ClientOption::find()->where(['clientID' => $clientID])->one();
        return $rd->receipts_prefix;

    }
}
