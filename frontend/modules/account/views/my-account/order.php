<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\VIPOrderStatus;
$this->title = 'Order History';
//$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app',$this->title);
?>

<div class="content-body">
                <!-- Data list view starts -->
                <section id="data-list-view" class="data-list-view-header">
                    

                    <!-- DataTable starts -->
                    <div class="table-responsive">
                        <h4 class="card-title"><?= Yii::t('app', 'All Orders') ?></h4>
                        <table class="table data-list-view">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><?= Yii::t('app', 'ORDER DATE') ?></th>
                                    <th><?= Yii::t('app', 'ORDER #') ?></th>
                                    <th><?= Yii::t('app', 'TOTAL POINT') ?></th>
                                    <th><?= Yii::t('app', 'STATUS') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $orders = $dataProvider->getModels();
                                $count = count($orders);
                                if ($count > 0) {
                                    $totalpoint = 0;
                                    $n = 1;
                                    foreach ($orders as $order) {
                                        //echo '<li><a href="#" target="_self">' . Html::encode($announcement->message) . '</a></li>';
                                        $totalpoint = $order->getTotalPoints() + $order->getShippingPointTotal();
                                        echo '<tr>
                                            <td>'.$n.'</td>
                                            <td class="product-name">'.date('d/m/Y', strtotime($order->created_datetime)).'</td>
                                            <td class="product-category">'.Html::a($order->invoice_prefix, '/order-history/view/'.$order->order_id).'</td>
                                            
                                            <td class="product-price">'.$totalpoint.'</td>
                                            <td>
                                                <div class="chip chip-'.$order->orderStatus->labelbg.'">
                                                    <div class="chip-body">
                                                        <div class="chip-text">'.Yii::t('app', $order->orderStatus->name).'</div>
                                                    </div>
                                                </div>
                                            </td>
                                            
                                            
                                        </tr>';
                                        $n++;
                                        //<td>'.$order->getTotalTransactions().'</td><td class="product-action">
                                                //'.Html::a('<span class="action-edit"><i class="feather icon-eye"></i></span>', ['/order-history/view/'.$order->order_id], ['title' => Yii::t('app', 'View'), 'class' => 'sss']).'
                                            //</td>
                                    } 
                                }
                                ?>
                                
                                
                            </tbody>
                        </table>
                    </div>
                    <!-- DataTable ends -->

                    <!-- add new sidebar starts -->
                    
                    <!-- add new sidebar ends -->
                </section>
                <!-- Data list view end -->

            </div>


<?php
$currentLang = \Yii::$app->language;
if ($currentLang == 'ms') {
    $language = 'Malay.json';
} else if ($currentLang == 'zh-CN') {
    $language = 'Chinese.json';
} else {
    $language = 'English.json';
}
$script = <<< JS

$(document).ready(function() {
  "use strict"
  // init list view datatable
  var dataListView = $(".data-list-view").DataTable({
        destroy: true,
    language: {
       url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/$language"
    }
    
    });
});        
JS;
$this->registerJs($script);
?>  