<?php

use yii\helpers\Html;
//use yii\helpers\HtmlPurifier;
//use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */

$this->title = 'Terms & Conditions';
//$this->params['breadcrumbs'][] = ' <i class="fa fa-angle-right"></i>';
$this->params['breadcrumbs'][] = Yii::t('app',$this->title);
?>

<section id="grid-options" class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <!-- Page Start -->
                    <p><strong>1.&nbsp;&nbsp;&nbsp; Michelin is pleased to welcome you to this Internet Site (hereinafter &ldquo;Site&rdquo;). As used in this Agreement the terms &ldquo;you&rdquo; and &lsquo;your&rdquo; refers to a visitor of this Site and each of the terms &ldquo;Michelin&rdquo;, &ldquo;us&rdquo;, &ldquo;our&rdquo; and &ldquo;we&rdquo; shall refers collectively to Michelin Malaysia Sdn. Bhd., its affiliates, employees, directors and representatives.</strong></p>

<p>&nbsp;</p>

<p><strong>2.&nbsp;&nbsp;&nbsp; Please read the terms set out hereunder carefully before consulting this Site. By proceeding with the use of this Site or any its links or services you will be deemed to have consented to the terms set out hereunder.</strong></p>

<p>&nbsp;</p>

<p><strong>3.&nbsp;&nbsp;&nbsp; If you do not agree with the terms set out hereunder, please exit and do not view this Site.</strong></p>

<p>&nbsp;</p>

<p><strong>4.&nbsp;&nbsp;&nbsp; We reserve the right, in our sole discretion, to change, modify, add or remove provisions of the terms set out hereunder at any time. The terms set out hereunder may be modified occasionally, so kindly check this page regularly. By using this Site after any changes are posted to the terms set out hereunder or you are otherwise notified of such changes, you agree to accept those changes, whether or not you have reviewed them. If you do not agree to the changes, you should not use the Site and you should arrange to cancel any registered user account or subscription (if any) with us.</strong></p>

<p>&nbsp;</p>

<p><strong>5.&nbsp;&nbsp;&nbsp; TERMS OF USE</strong></p>

<p><strong>a.&nbsp;&nbsp;&nbsp; Unless otherwise specified, you are granted a non-exclusive, non-transferable, limited right to access, use and display the Site and the material provided hereon, for your personal, non-commercial use, provided that you comply fully with the provisions of these terms of service. You also understand that the information shared with you is for your personal use only and may not be suitable or applicable to others.</strong></p>

<p>&nbsp;</p>

<p><strong>b.&nbsp;&nbsp;&nbsp; In our sole discretion and without prior notice or liability, we may discontinue, modify or alter any aspect of the Site, including, but not limited to, (i) restricting the time the Site is available, (ii) restricting the amount of use permitted (iii) restricting or terminating any user&#39;s right to use the Site and/or any services, and (iv) restricting or modifying in any manner any services that are available on the Site. You agree that any termination or cancellation or modification of your access to, or use of, the Site may be effected without prior notice.</strong></p>

<p>&nbsp;</p>

<p><strong>c.&nbsp;&nbsp;&nbsp; If you do not abide by the terms set out hereunder, except as we may otherwise provide from time to time, you agree that we may immediately stop your visit to the Site and/or bar any further access to our Site (or part thereof). Further, you agree that we shall not be liable to you or any third-party for any termination or cancellation of your access to, or use of, our Site.</strong></p>

<p>&nbsp;</p>

<p><strong>d.&nbsp;&nbsp;&nbsp; You agree that the Site shall be used in a reasonable and lawful manner only and that communications will not be obscene, indecent or offensive in any way. You agree to refrain from conduct on the Site or in connection with the Site that would constitute a criminal offense or give rise to a civil liability, or constitute a violation of any law or regulation.</strong></p>

<p>&nbsp;</p>

<p><strong>e.&nbsp;&nbsp;&nbsp; Postings to the interactive areas of the Site are not private. All other information and material you supply is governed by our Privacy Policy, which is accessible at any time from the bottom of any page on the Site.</strong></p>

<p>&nbsp;</p>

<p><strong>You agree to indemnify us and our affiliates, directors, employees, agents and representatives, and to hold them harmless from any and all claims and liabilities (including legal fees) that may arise from your postings, or from your use of material obtained through the Site, or from your breach of this Agreement, or from your use of the terms set out hereunder.</strong></p>

<p>&nbsp;</p>

<p><strong>f.&nbsp;&nbsp;&nbsp; YOU ACKNOWLEDGE THAT YOU ARE USING THE SITE AT YOUR OWN RISK. THE SITE AND ANY ADVICE IS PROVIDED &quot;AS IS,&quot; AND MICHELIN OR EMPLOYEES OR DIRECTORS HEREBY EXPRESSLY DISCLAIM ANY AND ALL WARRANTIES, EXPRESS AND IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF ACCURACY, RELIABILITY, TITLE, MERCHANTABILITY, NON-INFRINGEMENT, FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER WARRANTY, CONDITION, GUARANTEE OR REPRESENTATION, WHETHER ORAL, IN WRITING OR IN ELECTRONIC FORM, INCLUDING BUT NOT LIMITED TO THE ACCURACY OR COMPLETENESS OF ANY MATERIAL ON THE SERVICE.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>WHILE EVERY CARE IS AND HAS BEEN TAKEN TO ENSURE THAT ACCESS TO THE SITE WILL BE UNINTERRUPTED OR THAT THERE WILL BE NO FAILURES, ERRORS OR OMISSIONS OR LOSS OF TRANSMITTED INFORMATION OR THAT NO VIRUSES WILL BE TRANSMITTED, MICHELIN OR ITS EMPLOYEES OR DIRECTORS DO NOT REPRESENT OR WARRANTY IN THIS REGARD..&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>NEITHER MICHELIN OR ITS EMPLOYEES OR DIRECTORS, NOR ANY OF OUR OR THEIR RESPECTIVE LICENSORS, LICENSEES, SERVICE PROVIDERS OR SUPPLIERS WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OR THE RESULTS OF THE USE OF THE PRODUCTS, OFFERINGS, CONTENT AND MATERIALS IN THE SITE IN TERMS OF THEIR CORRECTNESS, ACCURACY, RELIABILITY, OR OTHERWISE. THE INFORMATION PROVIDED AT THIS SITE IS OF A GENERAL NATURE. TIRE USAGE RECOMMENDATIONS MAY BE AFFECTED BY FACTORS SUCH AS GEOGRAPHIC LOCALE, VEHICLE AGE AND CONDITION, AND PERSONAL DRIVING HABITS. PLEASE CONSULT YOUR AUTHORIZED MICHELIN DEALER OR REPRESENTATIVE BEFORE MAKING ANY PURCHASING DECISIONS. MICHELIN IS NOT AND WILL NOT BE LIABLE FOR ANY MATTERS RELATING TO YOUR USAGE OR ACCESS TO THE SITES.&nbsp;&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>MICHELIN OR ITS EMPLOYEES OR DIRECTORS SHALL NOT BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY DIRECT, INDIRECT, CONSEQUENTIAL, INCIDENTAL, SPECIAL OR PUNITIVE OR OTHER DAMAGES ALLEGEDLY SUFFERED OR ARISING OUT OF YOUR ACCESS TO OR INABILITY TO ACCESS THE SITE, USAGE OF THE SITE, ANY STATEMENTS OR MATERIAL CONTAINED IN THE SITE, INCLUDING FOR VIRUSES ALLEGED TO HAVE BEEN OBTAINED FROM THE SITE, YOUR USE OF OR RELIANCE ON THE SITE OR ANY OF THE INFORMATION OR MATERIALS AVAILABLE ON THE SITE, REGARDLESS OF THE TYPE OF CLAIM OR THE NATURE OF THE CAUSE OF ACTION. MICHELIN IS ALSO NOT LIABLE IN RELATION TO ANY MATTERS IN CONNECTION WITH THE SITES INCLUDING BY REASON OF THE NEGLIGENCE OF MICHELIN OR ANY OF ITS AGENTS OR CONTRACTORS.&nbsp;&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>MICHELIN AND ITS EMPLOYEES OR DIRECTORS&rsquo; AGGREGATE LIABILITY IS LIMITED TO THE FULLEST EXTENT PERMITTED BY LAW. Notwithstanding any terms in these Conditions and in the event that Michelin&rsquo;s liability cannot be excluded by law, the maximum aggregate liability of Michelin to any person under or in connection with any matter relating to this Agreement (whether in contract, tort, law, equity or otherwise) shall be limited to (to the fullest extent permitted by law) USD$500. </strong></p>

<p>&nbsp;</p>

<p><strong>g.&nbsp;&nbsp;&nbsp; The Site contains copyrighted material, content, trademarks, and other proprietary information including, but not limited to, photos, graphics, text, software, music, sound and videos (&ldquo;Intellectual Property&rdquo;). Michelin owns rights in relation to all Intellectual Property (&ldquo;IP Rights&rdquo;) in the collective and entire work placed on the Site. You are prohibited from, and agree not to publish, copy, or in any way exploit, any of the content or Intellectual Property of the Site, in whole or in part. Except as otherwise expressly permitted under copyright law, no copying, publication, or commercial exploitation of material accessed or downloaded from the Site will be permitted without the express written consent of Michelin. You agree that you do not acquire any ownership rights by copying or downloading copyrighted material. You hereby grant to Michelin a royalty-free, non-exclusive license to use, reproduce, modify, adapt, publish, translate, distribute, and display any content posted on the Site by you, in whole or in part. The items presented on this Site may be modified without notice and are provided with no guarantee whatsoever, whether express or implied, and may not give rise to any right to compensation.</strong></p>

<p>&nbsp;</p>

<p><strong>h.&nbsp;&nbsp;&nbsp; The alteration and modification of content or its use for a purpose other than that authorized constitutes an infringement of Michelin&rsquo;s or third parties&rsquo; IP Rights. Within the limits set out below, Michelin grants you the right to download and circulate the content (i) when the downloading function exists, (ii) for non-commercial purposes, (iii) in good faith, and (iv) while retaining intact the proprietary information or the website publishing date shown on the content. This right may not under any circumstances be interpreted as a license, in particular a trademark or logo license.</strong></p>

<p>&nbsp;</p>

<p><strong>i.&nbsp;&nbsp;&nbsp; You agree that the jurisdiction for any legal action or proceeding in relation to the use of this shall be Malaysia. You expressly agree to submit to the jurisdiction of the courts of Kuala Lumpur.</strong></p>

<p>&nbsp;</p>

<p><strong>j.&nbsp;&nbsp;&nbsp; If any provision of the terms set out hereunder or portion thereof shall be declared invalid for any reason, the invalid provision or portion thereof shall be deemed omitted and the remaining terms shall be given full force and effect.</strong></p>

<p>&nbsp;</p>

<p><strong>PRIVACY POLICY</strong></p>

<p>&nbsp;</p>

<p><strong>1.&nbsp;&nbsp;&nbsp; All references to the term &ldquo;Site&rdquo; is to this Internet Site; the terms &ldquo;you&rdquo; and &lsquo;your&rdquo; refers to a visitor of this Site and each of the terms &ldquo;Michelin&rdquo;, &ldquo;us&rdquo;, &ldquo;our&rdquo; and &ldquo;we&rdquo; refers collectively to Michelin Malaysia Sdn. Bhd., its affiliates, employees, directors and representatives.</strong></p>

<p>&nbsp;</p>

<p><strong>2.&nbsp; &nbsp;&nbsp;Michelin is committed to maintaining the privacy of our customers or persons visiting our Site. This page sets out our policy on how we manage &ldquo;personal information&rdquo;. This policy is intended to help you understand how we manage the personal information that we collect, hold, use and disclose, how you can seek access to and correction of that information and, if necessary, how you can make a complaint on our handling of that information.</strong></p>

<p>&nbsp;</p>

<p><strong>3.&nbsp;&nbsp;&nbsp; Types of personal information that is being collected and held&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>You may be asked for personal information if you want to take advantage of specific services we offer such as, but not limited to, e-mail enquiries (contact us), consumer or tyre reviews, survey, social sharing features (such as the Facebook &quot;Like&quot; button or other tools) or specific promotions. It is entirely up to you whether or not you wish to provide any personal data or information to us. If you elect to provide such information or personal data (&ldquo;Personal Data&rdquo;) to us or if you provide such data to us in the course of accessing or using the Site, you are deemed to have expressly consented to our collection, processing, usage and retention of such Personal Data. We may automatically collect non-personal information about you such as the type of internet browsers you use or the website from which you linked to our website. We may also aggregate details which you have submitted to the site (for example, your age and the town where you live). You cannot be identified from this information and it is only used to assist us in providing an effective service on this web site. We may from time to time supply third parties with this non-personal or aggregated data for uses in connection with this website.</strong></p>

<p>&nbsp;</p>

<p><strong>This Site does not automatically capture or store personal information, other than logging the user&rsquo;s IP address and session info such as the duration of the visit and the type of browser used. This is recognized by the web server and is only used for system administration and to provide statistics which we use to evaluate use of the Site.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>Parts of our Site use cookies to store information on your computer to enhance your experience of our Site and to store preferences and session information. Your experience of sections of this Site may be degraded if you do not enable cookies to be store on your computer.</strong></p>

<p>&nbsp;</p>

<p><strong>4.&nbsp;&nbsp;&nbsp; Cookies</strong></p>

<p><strong>a.&nbsp;&nbsp;&nbsp; About Cookies:&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>A cookie is a small file saved on a user&rsquo;s computer to help store preferences and other information that is used on webpages that they visit. Cookies can save a user&rsquo;s settings on certain websites and can sometimes be used to track how visitors get to and interact with websites. It&#39;s important to clarify that cookies do not collect any personal data stored on your hard drive or computer. To find out more about cookies, visit www.allaboutcookies.org.</strong></p>

<p>&nbsp;</p>

<p><strong>b.&nbsp;&nbsp;&nbsp; How Michelin uses Cookies&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>Michelin works with carefully selected and monitored partners who help us use cookies to provide site analytics. Analytics help us to understand how visitors are interacting with a site in order to optimize the site to enhance visitor&#39;s experience. Also some of these partners may set cookies during your visit to support customization of adverts that you may see elsewhere on the Internet.</strong></p>

<p>&nbsp;</p>

<p><strong>c.&nbsp;&nbsp;&nbsp; Why does Michelin use cookies?&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>Michelin is constantly searching for new ways to enhance its website(s) and improve the service it delivers to you. Michelin uses different types of cookies, as mentioned below. Some of these cookies require the online user&#39;s prior permission (advertising cookies) before they can be installed on the user&#39;s device.</strong></p>

<p>&nbsp;</p>

<p><strong>d.&nbsp;&nbsp;&nbsp; How to delete Cookies&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>You have control over these cookies: you can block them, destroy them or manage the cookie settings.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>If you would like to disable / block our cookies from your hard drive, you can find out about removing them at http://www.allaboutcookies.org/manage-cookies/index.html. In addition you may refer to the information set out below. Please note that the information set out below is only for guidance and recommendation purposes only. Michelin shall not be liable for any consequences resulting from the guidance set out below.</strong></p>

<p>&nbsp;</p>

<p><strong>i.&nbsp;&nbsp;&nbsp; On a computer&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>There are two ways to uninstall cookies. The first way is to use your web browser to see the cookies already installed on your computer and delete them either individually or all at once.</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; Apple Safari: &quot;Privacy&quot; section in the &quot;Preferences&quot; menu, area dedicated to &quot;Cookies and other site data&quot;;</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; Google Chrome: &quot;Privacy&quot; section in the &quot;Settings&quot; menu, subsection &quot;Content settings&quot;, area dedicated to &quot;Cookies and site data&quot;;</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; Microsoft Internet Explorer: &quot;Tools&quot; menu, &quot;Internet Options&quot; section, then &quot;General&quot; then &quot;Browsing History&quot;, then &quot;Settings&quot;, &quot;Temporary Internet Files and History Settings&quot; window, and finally &quot;View files&quot;;</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; Mozilla Firefox: &quot;Tools&quot; menu, &quot;Options&quot; section, &quot;Privacy&quot; subsection, dedicated &quot;Remove individual cookies&quot; functionality.</strong></p>

<p>&nbsp;</p>

<p><strong>You can also delete cookies manually by following the steps below:</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; Go to your workstation and select the Windows folder in C:\</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; Open the &quot;Temporary Internet Files&quot; folder and select all files (CTRL+A)</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; Choose the &quot;delete&quot; option</strong></p>

<p>&nbsp;</p>

<p><strong>You can also configure your web browser to block cookies or display a warning before any cookies are installed. The steps to follow vary from one browser to another, but you will find instructions in the browser&#39;s &quot;Help&quot; menu. The browser you use to access Michelin websites can be configured independently on each device.</strong></p>

<p>&nbsp;</p>

<p><strong>ii.&nbsp;&nbsp;&nbsp; On a smartphone or tablet&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>Cookies can be removed or deleted as follows:</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; Android: Go to the &quot;Privacy &amp; security&quot; section in the &quot;Settings&quot; menu. From there, you can choose &quot;Clear cache&quot;, &quot;Clear passwords&quot;, &quot;Clear all cookie data&quot; and &quot;Clear form data&quot;. You can also configure these functionalities not to save cookies.</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; Google Chrome: in the &quot;Settings&quot; menu, go to &quot;Advanced options&quot;, then &quot;Clear browsing history&quot;, then the data you wish to delete.</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; Apple browser under iOS 8: &quot;Settings&quot; button, then in the &quot;Safari&quot; tab, select &quot;Block Cookies&quot; and choose one of options available: &quot;Always Allow&quot;, &quot;Allow from Websites I Visit&quot;, &quot;Allow from Current Websites Only&quot; or &quot;Always Block&quot;. Under iOS 7 or earlier, you can choose between the options &quot;Always&quot;, &quot;From third parties and advertisers&quot; and &quot;Never&rdquo;.</strong></p>

<p>&nbsp;</p>

<p><strong>Uninstalling or blocking the cookies used on Michelin websites can impede or even prevent your use of these websites.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>By using our Site, you agree to the use of cookies and other technologies as set out in this policy. You are free to block our cookies &amp; adjust your browser settings accordingly.</strong></p>

<p>&nbsp;</p>

<p><strong>e.&nbsp;&nbsp;&nbsp; Types of cookies Michelin uses&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>Michelin may use one or several of the cookies discussed here.</strong></p>

<p>&nbsp;</p>

<p><strong>Cookies can be transient (a &quot;session&quot; cookie), in which case they last only as long as your browsing session on the website, or persistent, with a longer lifespan depending on the lifespan assigned to them and your browser settings. There are many different types of cookies. They can be classified into four broad groups as follows:</strong></p>

<p>&nbsp;</p>

<p><strong>i.&nbsp;&nbsp;&nbsp; Strictly necessary cookies&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>The cookies strictly necessary for the provision of an online communication service over electronic means are essential for the Site to work properly. Disabling them will make it very difficult to use the Site, or even impossible to use the services it offers. These cookies do not retain any information about you after you leave the website. They are used, for example, to identify devices for routing the communication, to number the &quot;packets&quot; of data in order to route them in the right order, and to detect transmission errors or data losses.</strong></p>

<p>&nbsp;</p>

<p><strong>ii.&nbsp;&nbsp;&nbsp; Functional cookies&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>Functional cookies are strictly necessary for the provision of an online communication service expressly requested by the user. They provide a user-specific functionality. If these cookies are uninstalled, it will prevent the service being provided. Functional cookies can collect personal data. For some cookies, this information can be retained after the end of your browsing session on the website, and sent to partners, solely for the purposes providing necessary services. They include, for example, cookies that save user preferences such as language, &quot;user session&quot; cookies (that identify the user when he or she navigates on several pages, and which retain data only for the duration of the browser session), and &quot;shopping basket&quot; cookies.</strong></p>

<p>&nbsp;</p>

<p><strong>iii.&nbsp;&nbsp;&nbsp; Audience measurement cookies&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>Audience measurement cookies are used to recognize visitors when they return to the Site. These cookies store only an online user &quot;identifier&quot; (specific to each cookie) and are not, under any circumstances, used to collect nominative information about visitors. They record the pages visited, the length of the visit and any error messages, so that we can improve the browsing experience on Michelin websites.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>Audience measurement cookies can be installed and managed by partners, but we limit their use to the statistical analysis requested.</strong></p>

<p>&nbsp;</p>

<p><strong>iv.&nbsp;&nbsp;&nbsp; Targeted advertising cookies and social media tracking cookies generated by social media sharing buttons&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>Targeting and tracking cookies allow third parties to provide services, mainly advertising, and enhance their effectiveness. These cookies can retain the web pages and websites you visit, and collect personal data, primarily the IP address of the device used by the online user. The information collected can be shared with third parties.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>For example, these cookies allow Michelin&#39;s advertising departments to display advertisements that match your centers of interest, based on your browsing history on Michelin websites, or limit the repetition of advertisements. These cookies require your permission: a clearly visible banner will appear on the website page to request your permission to install these cookies.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>Michelin and third-party providers, including Google, use proprietary cookies and third-party cookies in conjunction to obtain information, and optimize and display advertisements based on the user&#39;s visits to the Site. Michelin and these providers also use these two types of cookies to determine the relationship between the visits recorded on the Site and the advertisements viewed on the screen, other uses of advertising services and the interactions with the advertisements viewed and the advertising services. Lastly, Michelin and these providers use these two types of cookies to remember your centers of interest and your demographic data with a view to offering you targeted advertising.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>Michelin uses what are known as social plug-ins (which we will refer to below as buttons) to link back to social networks such as Facebook, LinkedIn and YouTube.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>When you visit our Site, these buttons are disabled by default, i.e. they do not send any information to the social networks concerned unless you do something. Before the buttons can be used, you have to activate them by clicking them. The buttons remain active until you disable them again or delete your cookies. Once activated, the system establishes a direct link to the server of the social network selected. The content of the button is then transferred directly from the social networks to your web browser, which integrates them into the website you visited.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>Once you have activated a button, the social network concerned can collect data, whether or not you use the button. If you are logged into a social network, the network can attribute your visit to the website in question to your user account. When you visit other Michelin websites, your visit will not be linked up with the initial website until you activate the corresponding button on these websites too.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>If you belong to a social network and don&#39;t want the social network to establish a link between the data collected during your visit to our website and the information recorded when you registered, you must log out of the social network in question before activating the buttons.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>Please note that we have no influence on the volume of data collected by the social networks through these buttons. To know the volume of data collected and the reasons for collecting it, as well as how the information is processed and used by the social networks, and your rights and configuration options for protecting your privacy, please read the privacy terms of the social networks concerned.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>Our Site may use the social network buttons below:</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; facebook.com, is operated by Facebook Inc., 1601 S. California Ave, Palo Alto, CA 94304, USA (&quot;Facebook&quot;). You will find a list of Facebook plugins and thumbnails at the following address: https://developers.facebook.com/docs/plugins. For more information about Facebook&#39;s privacy policy, please visit: http://www.facebook.com/policy.php. You can also block Facebook social plugins with add-ons for your browser, for example with the &quot;Facebook Blocker&quot;.</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; Twitter feeds are managed and provided by Twitter Inc., 1355 Market Street, Suite 900, San Francisco, CA 94103 USA. You will find the code of conduct for developers at the following address: https://dev.twitter.com/overview/terms/agreement-and-policy. For more information about Twitter&#39;s privacy policy, please visit: https://twitter.com/privacy?lang=fr.</strong></p>

<p><strong>&bull;&nbsp;&nbsp; &nbsp;linkedin.com is operated by LinkedIn Ireland, Attn: Privacy Policy Issues, Wilton Plaza, Wilton Place, Dublin 2 Ireland. You will find a list of LinkedIn plugins and thumbnails at the following address: https://developer.linkedin.com/. For more information about LinkedIn&#39;s privacy policy, please visit: https://www.linkedin.com/legal/privacy-policy</strong></p>

<p>&nbsp;</p>

<p><strong>By using our Site, you agree to the use of cookies and other technologies as set out in this policy. You are free to block our cookies &amp; adjust your browser settings accordingly.</strong></p>

<p>&nbsp;</p>

<p><strong>5.&nbsp;&nbsp;&nbsp; Holding and protecting your personal information&nbsp;&nbsp;&nbsp; </strong></p>

<p><strong>Personal information submitted by you may be stored in physical or electronic form either on our own systems or the systems of our service providers. The personal information is protected using appropriate safeguards. We also require our service providers to follow strict security standards and bind them with obligations of confidentiality when holding and processing any personal information.</strong></p>

<p>&nbsp;</p>

<p><strong>6.&nbsp;&nbsp;&nbsp; Purposes for which we may collect, use and disclose your personal information&nbsp;&nbsp;&nbsp; </strong></p>

<p><strong>You may be asked for personal information if you want to take advantage of specific services we offer such as, but not limited to, e-mail enquiries (contact us), consumer or tyre reviews, survey, social sharing features (such as the Facebook &quot;Like&quot; button or other tools) or specific promotions. In each case we will only use the personal information you provide to deliver the services you have told us you wish to take part in. You will be given an opportunity to opt out of further contact if you wish. If you would prefer that we not collect any personally identifiable information from you, please do not provide us with any such information.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>We may share your personal data with third party suppliers or service providers. These suppliers and service providers are contractually obligated to keep your personal data confidential and secure, and they are required to use your personal data only for the needs of the services that were entrusted to them.</strong></p>

<p>&nbsp;</p>

<p><strong>7.&nbsp;&nbsp;&nbsp; Disclosure of personal information to third parties&nbsp;&nbsp;&nbsp; </strong></p>

<p><strong>We receive services from other Michelin Group companies and external service providers, some of which may be located outside your country of residence (see the additional section below titled &quot;Overseas Disclosures&quot;), and your information may be provided to them for this purpose.</strong></p>

<p>&nbsp;</p>

<p><strong>Third parties to whom we disclose your personal information may include:</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; related Michelin group companies outside your country of residence;</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; persons to whom disclosure of your personal information is necessary in order for us to provide or manage any services or transactions requested by you including:</strong></p>

<p><strong>o&nbsp;&nbsp;&nbsp; service providers, such as mail services, delivery agencies; service and support agencies; customer service agents</strong></p>

<p><strong>o&nbsp;&nbsp;&nbsp; service providers to whom we subcontract any tasks under our agreements (if any) with you</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; legal, audit, settlement, valuation and any other professional service providers under a duty of confidentiality to us</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; analytics and market research service providers who provide marketing, market research, or other related services relating to promotion of our business;</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; insurers, assessors and underwriters</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; fraud reporting agencies</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; any potential investors, purchasers or persons wishing to acquire an interest in any part of our business from time to time; and</strong></p>

<p><strong>&bull;&nbsp;&nbsp;&nbsp; any person to whom we are under an obligation to make disclosure under the requirements of any law, rules, regulations or guidelines including, regulators, governmental bodies or industry recognized bodies (all or some of which may be in or outside your country of residence)</strong></p>

<p>&nbsp;</p>

<p><strong>8.&nbsp;&nbsp;&nbsp; Overseas Disclosures&nbsp;&nbsp;&nbsp; </strong></p>

<p><strong>As a result of the management structure some recipients of personal information within Michelin may not be located in the country of your residence. While it is not reasonably practicable to list all of the countries to which your information may be transmitted from time to time but such countries are likely be those where Michelin has offices globally. A list of those countries is available through the Michelin website at www.michelin.com. Such organizations may in turn be required to disclose information we share with them under a foreign law.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>When we do share information as above, we remain responsible for that disclosure and will ensure that your personal information is handled according to the requirements of law.</strong></p>

<p>&nbsp;</p>

<p><strong>9.&nbsp;&nbsp;&nbsp; Requesting access to your personal information&nbsp;&nbsp;&nbsp; </strong></p>

<p><strong>You are entitled to access personal information we hold by contacting our privacy officer (contact details below). We will endeavour to respond within 30 days of such a request.</strong></p>

<p>&nbsp;</p>

<p><strong>10.&nbsp;&nbsp;&nbsp; Requesting correction of your personal information&nbsp;&nbsp;&nbsp; </strong></p>

<p><strong>You are entitled to request that we correct/rectify information that we hold about you. If you would like to do so please contact the privacy officer using the contact details below.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>Any corrective action by us will normally be done within 30 days of a request. We will notify you of any delay in writing.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>Correction of information is provided free of charge.</strong></p>

<p>&nbsp;</p>

<p><strong>11.&nbsp;&nbsp;&nbsp; Complaints and disputes&nbsp;&nbsp;&nbsp; </strong></p>

<p><strong>If you believe your rights under this privacy policy, or otherwise under law, have been breached, as a first recourse you must contact our Privacy Officer in writing (using the contact details provided below), setting out sufficient details of your grievance, the affected personal data and contact details.&nbsp;&nbsp;&nbsp; </strong></p>

<p>&nbsp;</p>

<p><strong>Upon receipt of your complaint, we will carry out an internal investigation and may contact you during this period for further information. Subject to your timely cooperation, we generally endeavour to provide a formal response setting out the results of our investigation and whether we agree that your rights have been breached, within 30 days of receiving your complaint.</strong></p>

<p>&nbsp;</p>

<p><strong>If we fail to deal with your complaint to your satisfaction you may refer the complaint to the appropriate privacy authorities in your country of residence.</strong></p>

<p>&nbsp;</p>

<p><strong>12.&nbsp;&nbsp;&nbsp; Contacting Us&nbsp;&nbsp;&nbsp; </strong></p>

<p><strong>If you wish to find out more information, or raise any specific or general concerns about Michelin and our Privacy Policy and practices please go to https://www.michelin.com.my/assistance</strong></p>

                    
                    <!-- Page End -->
                    
                    
                    
                    
                </div>
            </div>
        </div>
    </div>
</section>