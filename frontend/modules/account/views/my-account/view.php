<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\VIPOrder */

$this->title = $model->invoice_prefix;
//$this->params['breadcrumbs'][] = ['label' => 'Account', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Order History'), 'url' => ['/order-history']];
$this->params['breadcrumbs'][] = Yii::t('app',$this->title);
?>

<section class="card invoice-page">
    <div id="invoice-template" class="card-body">
        <!-- Invoice Company Details -->
        <div id="invoice-company-details" class="row">
            <div class="col-md-6 col-sm-12 text-left pt-1">
                <div class="media pt-1">
                </div>
            </div>
            <div class="col-md-6 col-sm-12 text-right">
                
                <?php echo '<div class="badge badge-'.$model->orderStatus->labelbg.' badge-lg mr-1 mb-1">' . Yii::t('app',$model->orderStatus->name) . '</div>'; ?>
                
            </div>
        </div>
        <!--/ Invoice Company Details -->

        <!-- Invoice Recipient Details -->
        <div id="invoice-customer-details" class="row pt-2">
            <div class="col-md-6 col-sm-12 text-left">
                <div class="invoice-details mt-2">
                    <h6><?= Yii::t('app', 'ORDER NO.') ?></h6>
                    <p><?= Yii::t('app', $model->invoice_prefix) ?></p>
                    <h6 class="mt-2"><?= Yii::t('app', 'ORDER DATE') ?></h6>
                    <p><?= date('d/m/Y', strtotime($model->created_datetime)) ?></p>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 text-right">
                <h5><?= Yii::t('app', 'Delivery Address') ?></h5>
                <div class="company-info my-2">
                    <p><?= $model->shipping_firstname ?> <?= $model->shipping_lastname ?></p>
                    <p><?= $model->shipping_address_1 ?> <?= $model->shipping_address_1 ?></p>
                    <p><?= $model->shipping_city ?> <?= $model->shipping_postcode ?></p>
                    <p><?= $model->shipping_zone ?> - <?= $model->shipping_country_id ?></p>
                </div>
            </div>
        </div>
        <!--/ Invoice Recipient Details -->

        <!-- Invoice Items Details -->
        <div id="invoice-items-details" class="pt-1 invoice-items-table">
            <div class="row">
                <div class="table-responsive col-sm-12">
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                                <th><?= Yii::t('app', 'PRODUCT NAME') ?></th>
                                <th><?= Yii::t('app', 'MODEL') ?></th>
                                <th class="text-right"><?= Yii::t('app', 'QUANTITY') ?></th>
                                <th class="text-right"><?= Yii::t('app', 'UNIT POINTS') ?></th>
                                <th class="text-right"><?= Yii::t('app', 'POINTS') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                    $total = ''; $productpoint = 0;
                                    $subtotal = 0; $total = 0; $shipping_point_total = 0;
                                    foreach ($model->orderProducts as $subproduct) {
                                        //echo $subproduct->name.'<br>';
                                        $optlisit = '';
                                        
                                        $OrderOption = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->count();
                                        if ($OrderOption > 0) {
                                            $OrderOptionlisits = common\models\VIPOrderOption::find()->where(['order_product_id' => $subproduct->order_product_id])->all();
                                            foreach ($OrderOptionlisits as $OrderOptionlisit) {
                                                    $optlisit .= '<br>&nbsp;<small> - ' . $OrderOptionlisit->name . ': ' . $OrderOptionlisit->value . '</small>';
                                                }
                                            }
                                            $productpoint = $subproduct->point + $subproduct->shipping_point;
                                            $totalproductpoint = $subproduct->quantity * $productpoint;
                                            $trackinfo = '';
                                            if($subproduct->tracking_type == 'Y') {
                                                if(!empty($subproduct->tracking_link)) {
                                                    $trackinginfo = '<a href="' . $subproduct->tracking_link . '" target="_blank">' . $subproduct->tracking_no . '</a>';
                                                }else {
                                                    $trackinginfo = $subproduct->tracking_no;
                                                }
                                                $trackinfo = '<br><em>Tracking Info '.$trackinginfo.'</em>'; 
                                            }
                                            echo '<tr>
                                              <td>' . $subproduct->name . '
                                                  ' . $optlisit . $trackinfo.'
                                              </td>
                                              <td>' . $subproduct->model . '</td>
                                              <td class="text-right">' . $subproduct->quantity . '</td>
                                              <td class="text-right">' .$productpoint. 'pts</td>
                                              <td class="text-right">' . Yii::$app->formatter->asInteger($totalproductpoint) . 'pts</td>
                                            </tr>';
                                            $total += $totalproductpoint;
                                    }
                                    ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <div id="invoice-total-details" class="invoice-total-table">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th><h3><?= Yii::t('app', 'Total') ?></h3></th>
                                    <td  class="text-right text-bold-800"><h3><?= Yii::$app->formatter->asInteger($total) ?><?= Yii::t('app', 'pts') ?></h3></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>