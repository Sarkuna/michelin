<?php

//namespace app\modules\support\controllers;
namespace app\modules\products\controllers;

use yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

use common\models\VIPProduct;
/**
 * Default controller for the `support` module
 */

class ProductController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($limit = 20)
    {
        Yii::$app->VIPglobal->addtolog('success', Yii::$app->user->id);
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        $min_value = 0;
        $max_value = 0;
        $min = 0;
        $max = 0;
        $startprice = ''; $endprice ='';
        $point_value = Yii::$app->VIPglobal->clientSetup()['point_value'];
        
        $request = Yii::$app->request;
	$q = htmlentities(trim($request->get('q')), ENT_QUOTES);
        $link = htmlentities(trim($request->get('link')), ENT_QUOTES);
        $paramm = htmlentities(trim($request->get('paramm')), ENT_QUOTES);
        $price = htmlentities(trim($request->get('price')), ENT_QUOTES);
        if(!empty($price)) {
            $price = explode(',', $price);
            $startprice = $price[0] * $point_value;
            $endprice = $price[1] * $point_value;
        }


        if(empty($q)) {
            //return $this->redirect('rewards');
            $q=null;
        }
        
        if(!empty($link)) {
            $subcat = \common\models\VIPCategories::find()
                    ->where(['slug' => $link])
                    ->one();
            $link = $subcat->vip_categories_id;
        }
        
        if(!empty($paramm)) {
            $manufacturer = \common\models\VIPManufacturer::find()
                    ->where(['slug' => $paramm])
                    ->one();
            $paramm = $manufacturer->manufacturer_id;
        }
        
        
        
        $limitvalue = array("30", "50", "80", "100");
        if (in_array($limit, $limitvalue)) {
            $limit = $limit;
        }else {
            $limit = 30;
        }
        

        
        $query = \common\models\VIPProduct::find()
                    ->where(['status' => 'E'])
                    ->orderBy('updated_datetime DESC');

        //$searchModel = new \common\models\VIPAssignProductsSearch();
        $products = new \yii\data\ActiveDataProvider([
                'query' => $query,
                'sort'=> ['defaultOrder' => ['updated_datetime'=>SORT_DESC]],
                'pagination' => ['pageSize' => $limit,]
            ]);
        $query->andFilterWhere(['like', 'product_name', $q]);
        $query->andFilterWhere(['like', 'category_id', $link]);
        $query->andFilterWhere(['=', 'manufacturer_id', $paramm]);
        $query->andFilterWhere(['between', 'price', $startprice, $endprice]);

        return $this->render('index', ['products' => $products]);
            
        //return $this->render('index', ['products' => $products, 'min' => $min, 'max' => $max, 'min1' => $min1, 'max1' => $max1]);

    }
    
    public function actionAll($limit = 15, $url = null, $manufacturer = null, $filter = null)
    {
        $session = Yii::$app->session;
        Yii::$app->VIPglobal->addtolog('success', Yii::$app->user->id);
        $clientID = $session['currentclientID'];
        $min_value = 0;
        $max_value = 0;
        $min = 0;
        $max = 0;
        
        $myurl = Yii::$app->getRequest()->getQueryParam('url');
        $myvalue = htmlentities(trim($myurl), ENT_QUOTES);
        //die;

        
        $limitvalue = array("15", "25", "50", "75", "100");
        if (in_array($limit, $limitvalue)) {
            $limit = $limit;
        }else {
            $limit = 1;
        }
        $proids = \common\models\VIPAssignSubCategories::find()
                ->where(['clientID' => $clientID, 'status' => 'A'])
                ->one();
        
        if(count($proids) > 0){

            $min1 = \common\models\VIPProduct::find()
                    ->joinWith('assignProduct')
                    ->where(['status' => 'E','clientID' => $clientID])
                    ->orderBy('points_value ASC')
                    ->count();
            
            $max1 = \common\models\VIPProduct::find()
                    ->joinWith('assignProduct')
                    ->where(['status' => 'E','clientID' => $clientID])
                    ->orderBy('points_value DESC')
                    ->count();
            
            if($min1 > 0) {
                $min = \common\models\VIPProduct::find()
                    ->joinWith('assignProduct')
                    ->where(['status' => 'E','clientID' => $clientID])
                    ->orderBy('points_value ASC')
                    ->one();
                $min_value = $min->points_value;
            }
            if($max1 > 0) {
            $max = \common\models\VIPProduct::find()
                    ->joinWith('assignProduct')
                    ->where(['status' => 'E','clientID' => $clientID])
                    ->orderBy('points_value DESC')
                    ->one();
                    
               $max_value = $max->points_value;
            }
            
            if(!empty($filter)){
               $filters = explode("-", $filter); 
               $filtermin = $filters[0];
               $filtermax = $filters[1];
            }else {               
               $filtermin = $min_value;
               $filtermax = $max_value;
            }
            
            
            $query = \common\models\VIPProduct::find()
                    ->joinWith('assignProduct')
                    ->where(['status' => 'E','clientID' => $clientID])
                    ->orderBy('main_image DESC');
            
            if(!empty($myvalue)){
                $query->andWhere(['or',
                ['like','product_name',$myvalue],
                ['like','product_code',$myvalue]]);
            }
            
            if(!empty($manufacturer)){
                $query->andWhere(['=', 'manufacturer_id', $manufacturer]);
            }
            
            if($filtermin > 0 && $filtermax > 0) {
                $query->andWhere(['between', 'points_value', $filtermin, $filtermax]);
            }

            $products = new \yii\data\ActiveDataProvider(['query' => $query,
                'pagination' => ['pageSize' => $limit,]
            ]); 
            
            return $this->render('index_all', ['products' => $products, 'min' => $min, 'max' => $max, 'min1' => $min1, 'max1' => $max1]);
        }else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionProductDetail($id)
    {
        //$id = Yii::$app->request->post('param1', null);
        Yii::$app->VIPglobal->addtolog('success', Yii::$app->user->id);
        $this->layout = '@app/themes/vuexy/layouts/main_product_detail';
        return $this->render('_product_detail', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionManufactures($manufacturer,$limit = 15,$filter = null)
    {
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $limitvalue = array("15", "25", "50", "75", "100");
        if (in_array($limit, $limitvalue)) {
            $limit = $limit;
        }else {
            $limit = 1;
        }

        if(!empty($manufacturer)){
            $gby = 'manufacturer_id';
        }else {
            $gby = '';
        }
        
        $min = \common\models\VIPProduct::find()
                ->joinWith('assignProduct')
                ->where(['status' => 'E','clientID' => $clientID, 'manufacturer_id' => $manufacturer])
                ->orderBy('points_value ASC')
                ->one();

        $max = \common\models\VIPProduct::find()
                ->joinWith('assignProduct')
                ->where(['status' => 'E','clientID' => $clientID, 'manufacturer_id' => $manufacturer])
                ->orderBy('points_value DESC')
                ->one();
        
        if(!empty($filter)){
            $filters = explode("-", $filter); 
            $filtermin = $filters[0];
            $filtermax = $filters[1];
         }else {
            $filtermin = $min->points_value;
            $filtermax = $max->points_value;
         }
        
        $query = \common\models\VIPProduct::find()
                ->joinWith('assignProduct')
                ->where(['status' => 'E','clientID' => $clientID, 'manufacturer_id' => $manufacturer])
                ->andFilterWhere(['between', 'points_value', $filtermin, $filtermax])
                //->andFilterWhere(['like','manufacturer_id', $manufacturer])
                //->groupBy($gby)
                ->orderBy('main_image DESC');


        $products = new \yii\data\ActiveDataProvider(['query' => $query,
            'pagination' => ['pageSize' => $limit,]
        ]); 

        return $this->render('index_manufacturer', ['products' => $products, 'min' => $min, 'max' => $max]);
    }
    
    protected function findModel($id)
    {
        if (($model = VIPProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
