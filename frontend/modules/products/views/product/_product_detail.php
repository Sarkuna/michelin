<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$session = Yii::$app->session;
$clientID = $session['currentclientID'];
$this->title = $model->product_code;
$this->params['breadcrumbs'][] = ['label' => 'Rewards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

//$totproducts = common\models\VIPOrderProduct::find()->where(['product_id' => $model->product_id])->count();

$hide = '';
$stockStatus = '<span class="text-success">In Stock</span>';

$cart = Yii::$app->session['cart'];
$currentqty = 1;
$addcartbtn = '<button class="addtobag btn btn-primary mr-0 mr-sm-1 mb-1 mb-sm-0 waves-effect waves-light" onclick="addCart('. $model->product_id.',1);"><i class="feather icon-shopping-cart mr-25"></i>ADD TO CART</button>';

if($model->type == 'michelin') {
    $stock = Yii::$app->VIPglobal->limited($model->product_id);
}else {
    $stock = $model->quantity;
}

if(!empty($cart)) {
    if(array_key_exists($model->product_id, $cart)){
        $currentqty = $cart[$model->product_id]['product_qty'];
        $stock = $stock - $currentqty;
        if($stock == 0){
            $hide = 'hidden';
            $stockStatus = '<span class="text-danger">Out of Stock</span>';
            $addcartbtn = '<button class="addtobag btn btn-danger mr-0 mr-sm-1 mb-1 mb-sm-0 waves-effect waves-light"><i class="feather icon-shopping-cart mr-25"></i>Out of Stock</button>';
        }
        //$hide = 'hidden';
    }
}

if($stock > 0) {
    $stockStatus = $stockStatus;    
}else {
    $stockStatus = '<span class="text-danger">Out of Stock</span>';
    $hide = 'hidden';
    $addcartbtn = '<button class="addtobag btn btn-danger mr-0 mr-sm-1 mb-1 mb-sm-0 waves-effect waves-light"><i class="feather icon-shopping-cart mr-25"></i>Out of Stock</button>';
}

$states = Yii::$app->VIPglobal->webapistate();
if($states > 0) {
    $point = Yii::$app->VIPglobal->getTotalcostpoint($model->product_id,$states);
}else {
    return Yii::$app->response->redirect(Url::to(['/site/index']));
}


?>
<style>
    .description img{width:100%}
    .box1{float:left;}
    .clear{clear:both;}
</style>

<section class="app-ecommerce-details">
    <div class="card">
        <div class="card-body">

            <div class="row mb-5 mt-2">
                <div class="col-12 col-md-5 d-flex align-items-center justify-content-center mb-2 mb-md-0">
                    <div class="owl-carousel prod-slider sync1">
                            <?php
                            //$uploadFiles   = \common\models\Uploads::find()->where(['ref'=>$model->ref])->all();
                            $bigimages = $model->images;
                            if(count($bigimages) > 0) {
                                foreach ($bigimages as $bigimage) {
                                    //echo '<div class="item"> <img src="'.Yii::getAlias('@back').'/upload/photolibrarys/'.$model->ref.'/thumbnail/'.$bigimage->real_filename.'" alt=""> </div>';
                                    echo '<div class="item"> 
                                                <img src="' . Yii::getAlias('@back') . '/upload/photolibrarys/' . $model->ref . '/thumbnail/' . $bigimage->real_filename . '" alt="">
                                                <a href="' . Yii::getAlias('@back') . '/upload/photolibrarys/' . $model->ref . '/' . $bigimage->real_filename . '" title="Product" class="caption-link"></a>
                                            </div>';
                                }
                            }else {
                                echo '<div class="item">';
                                if (file_exists(Yii::getAlias('@backend').'/web/upload/product_cover/thumbnail/'.$model->main_image)) {
                                    $imagename = $model->main_image;
                                    echo '<img src="' . Yii::getAlias('@back') . '/upload/product_cover/original/'.$model->main_image.'" alt="">';
                                }else {
                                    echo '<img src="' . Yii::getAlias('@back') . '/upload/product_cover/thumbnail/no_image.png" alt="">';
                                }
                                echo '</div>';
                            }
                            ?>

                        </div>

                </div>
                <div class="col-12 col-md-6">
                    <h5><?= $model->product_name ?></h5>
                    <p class="text-muted">by <?= $model->manufacturer_id === null ? 'N\A' : $model->manufacturer->name ?></p>
                    <div class="ecommerce-details-price d-flex flex-wrap">

                        <p class="text-primary font-medium-3 mr-1 mb-0"><?= $point ?> <em>- pts</em></p>
                    </div>
                    <hr>
                    <p>Product Code - <span class="text-success"><?= $model->product_code ?></span></p>
                    <p>Available - <?= $stockStatus ?></p>

                    
                    <?php
                        if(!empty($model->remarks))
                        {
                            echo '<div class="space20"></div>
                            <div class="sep"></div>
                            <h3>Remarks</h3>';
                            echo '<p>'.$model->remarks.'</p>';
                        }
                        ?>
                    <hr>
                    
                    

                    <div class="<?= $hide ?>">
                        <?php                           
                            
                            if($session['point'] > 0) {
                                echo '<div class="item-quantity mb-2">';
                                if($model->type == 'bb') {
                                            echo '<p class="quantity-title">Quantity</p>

                                            <div class="box1">
                                                <div class="input-group quantity-counter-wrapper">
                                                    <input id="ex_limit" name="ex_limit" type="text" class="ex_limit form-control" value="'.$currentqty.'"/>
                                                </div>
                                            </div>
                                            <div class="box2">'.$stock.' piece available</div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="d-flex flex-column flex-sm-row">
                                        <input type="hidden" value="0" id="aboption">
                                        <input type="hidden" value="'.$currentqty.'" id="myqty">';
                                }
                                echo $addcartbtn;
                                echo '</div>';
                            } else {
                                echo '<div class="alert alert-info" style="color: #ffffff;">
                                    <strong>Sorry!</strong> Your point balance is insufficient to make this order.
                                </div>';
                            }
                        ?>
                        
                        
                    </div>
                </div>
            </div>
        </div>

    </div>
    
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Product Details</h4>
        </div>
        <div class="card-content">
            <div class="card-body description">
                <div class="col-lg-12">
                    <?= $model->product_description ?>
                </div>                
            </div>
        </div>
    </div>
</section>


<?php

    $this->registerJs(
            
    '
    $( "#ex_limit" ).change(function() {
        
        var i = $( "#ex_limit" ).val();
        $( "#myqty" ).val(i);
    });
 $( ".ex_limit" ).TouchSpin({
    min: 1,
    max: '.$stock.',
    mousewheel: true,
    stepinterval: 50,
    maxboostedstep: 10000000,
});'
    );
  ?>
