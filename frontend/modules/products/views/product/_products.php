<?php

use yii\helpers\Html;
use yii\helpers\Url;
//$productdescription = strip_tags($model->product_description);
$session = Yii::$app->session;
$cart = Yii::$app->session['cart'];
$states = Yii::$app->VIPglobal->webapistate();
if($states > 0) {
    $point = Yii::$app->VIPglobal->getTotalcostpoint($model->product_id,$states);
}else {
     \Yii::$app->getSession()->setFlash('error',['title' => 'Sorry', 'text' => 'We\'re sorry, something went wrong. Please try again. If you continue to have issues, please contact support.']);
    return Yii::$app->response->redirect(Url::to(['/site/index']));
}

if($model->type == 'michelin') {
    $stock = Yii::$app->VIPglobal->limited($model->product_id);
}else {
    $stock = Yii::$app->VIPglobal->stock($model->product_id);
}

if(!empty($cart)) {
    if(array_key_exists($model->product_id, $cart)){
        $currentqty = $cart[$model->product_id]['product_qty'];
        $stock = $stock - $currentqty;
    }
}



/*if(!empty($model->main_image)) {
    $imagename = $model->main_image;
}else {
    $imagename = 'no_image.png';
}*/

if (file_exists(Yii::getAlias('@backend').'/web/upload/product_cover/thumbnail/'.$model->main_image)) {
    $imagename = $model->main_image;
}else {
    $imagename = 'no_image.png';
}

?>
<div class="card ecommerce-card">
    <div class="card-content">
        <div class="item-img text-center">
            <img class="img-fluid" src="<?= Yii::$app->params['imgdomain'] ?>/upload/product_cover/thumbnail/<?= $imagename ?>" alt="img-placeholder">
        </div>
        <div class="card-body">
            <div class="item-wrapper">
                <div class="item-cost">
                    <h6 class="item-price">
                        <?= $point ?> <em>- Pts</em>
                    </h6>
                </div>
            </div>
            <div class="item-name">
                <span>
                <?= Html::a($model->product_name, ['/products/product/product-detail', 'id' => $model->product_id,])?>
                </span>    
            </div>
            <div>

            </div>
        </div>
        <div class="item-options text-center">
            <div class="item-wrapper">
                <div class="item-rating">
                    <span><?= $model->product_code ?></span>
                </div>
                <div class="item-cost">
                    <h6 class="item-price">
                        <?= $point ?> <em>- Pts</em>
                    </h6>
                </div>
            </div>

            <?php
            if($stock > 0) {
                echo '<div class="cart cartlist" onclick="addCart('.$model->product_id.',1);">
                    <i class="feather icon-shopping-cart mr-25"></i> <span class="add-to-cart">Add to cart</span>
                </div>';
            }else {
                echo '<div class="cart cartlistnoitem">
                    <i class="feather icon-shopping-cart mr-25"></i> Out of Stock
                </div>';
            }
            ?>
            
        </div>
    </div>
</div>
                        

                        
                        