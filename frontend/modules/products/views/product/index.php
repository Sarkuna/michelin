<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\ShopbyCategoriesWidget;
use app\components\EcommerceSidebarVuexy;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rewards';
$this->params['breadcrumbs'][] = $this->title;
$cid = Yii::$app->request->get('id');
    $sid = Yii::$app->request->get('sid');
 //$dataProvider->getTotalCount();

?>
<style>
    .ecommerce-application .sidebar-shop .range-slider.noUi-horizontal .noUi-handle .noUi-tooltip {opacity: 100%;}
</style>

    <!-- BEGIN: Content-->
            <div class="content-detached content-right">
                <div class="content-body">
                    <!-- Ecommerce Content Section Starts -->
                    <section id="ecommerce-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="ecommerce-header-items">
                                    <div class="result-toggler">
                                        <button class="navbar-toggler shop-sidebar-toggler" type="button" data-toggle="collapse">
                                            <span class="navbar-toggler-icon d-block d-lg-none"><i class="feather icon-filter"></i>Filters</span>
                                        </button>
                                        <div class="search-results">
                                            <?= $products->totalCount ?> results found
                                        </div>
                                    </div>
                                    <div class="view-options">
                                        <div class="view-btn-option">
                                            <button class="btn btn-white view-btn grid-view-btn active">
                                                <i class="feather icon-grid"></i>
                                            </button>
                                            <button class="btn btn-white list-view-btn view-btn">
                                                <i class="feather icon-list"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <!-- Ecommerce Content Section Starts -->
                    <!-- background Overlay when sidebar is shown  starts-->
                    <div class="shop-content-overlay"></div>
                    <!-- background Overlay when sidebar is shown  ends-->

                    <!-- Ecommerce Search Bar Starts -->
                    <?=Html::beginForm(Url::to(['/rewards']), 'GET', ['id'=>'filter-form']);?>
                    <section id="ecommerce-searchbar">
                        <div class="row mt-1">
                            <div class="col-sm-12">
                                <fieldset class="form-group position-relative">
                                    <?= Html::input('text','q',Yii::$app->request->get('q'), $options=['class'=>'form-control search-product', 'id'=>'pname','placeholder' => 'Search product name here']) ?>
                                    <?= Html::hiddenInput('link',Yii::$app->request->get('link'), $options=['id'=>'plink']) ?>
                                    <div class="form-control-position">
                                        <i class="feather icon-search"></i>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </section>
                    <?=Html::endForm(); ?>
                    <!-- Ecommerce Search Bar Ends -->

                    <!-- Ecommerce Products Starts -->
                    <?php
  

                echo yii\widgets\ListView::widget([
                    'dataProvider' => $products,
                    /* 'options' => [
                      'tag' => 'div',
                      'class' => 'list-wrapper',
                      'id' => 'list-wrapper',
                      ], */
                    //'layout' => "{pager}\n{items}\n{summary}",
                    'layout' => '<section id="ecommerce-products" class="grid-view">{items}</section>
                                <section id="ecommerce-pagination"><div class="row"><div class="col-sm-12"><nav aria-label="Page navigation example">{pager}</nav>
                            </div>
                        </div>
                    </section>
                                ',
                    //'layout' => "<div class\"items\">{items}</div>{summary}",
                    'itemOptions' => ['tag' => false],
                    'options' => ['id' => false],
                    'itemView' => '_products',
                    'pager' => [
                        'hideOnSinglePage'=>true,
                        //'firstPageLabel' => '<i class="feather icon-chevron-left"></i>',
                        //'lastPageLabel' => '<li class="page-item next-item"><a class="page-link" href="#"><i class="feather icon-chevron-right"></i></a></li>',
                        'nextPageLabel' => '<i class="feather icon-chevron-right"></i>',
                        'prevPageLabel' => '<i class="feather icon-chevron-left"></i>',
                        'maxButtonCount' => 10,
                        'options' => [
                            //'tag' => 'div',
                            'class' => 'pagination justify-content-center mt-2',
                            //'id' => 'pager-container',
                        ],
                        'pageCssClass' => ['class' => 'page-item'],
                        'linkOptions' => ['class' => 'page-link'],
                        'activePageCssClass' => 'active',
                        'prevPageCssClass' => 'page-item prev-item',
                        'nextPageCssClass' => 'page-item next-item',
                        //'internalPageCssClass' => 'btn btn-info btn-sm',
                        //'disabledPageCssClass' => 'mydisable'
                    ],
                ]);
                ?>
                    
                    <!-- Ecommerce Products Ends -->

                    <!-- Ecommerce Pagination Starts -->

                    <!-- Ecommerce Pagination Ends -->

                </div>
            </div>
<?= EcommerceSidebarVuexy::widget(['path' => Yii::$app->request->getPathInfo()]) ?>

<?php
$point_value = Yii::$app->VIPglobal->clientSetup()['point_value'];
$minprices = \common\models\VIPProduct::find()->where(['status' => 'E'])->orderBy('price ASC')->one();
$maxprices = \common\models\VIPProduct::find()->where(['status' => 'E'])->orderBy('price DESC')->one();
$minprice = $minprices->price / $point_value;
$maxprice = $maxprices->price / $point_value;

if(!empty(Yii::$app->request->get('price'))){
    $price = htmlentities(trim(Yii::$app->request->get('price')), ENT_QUOTES);
    $price = explode(',', $price);
    $sminprice = $price[0];
    $smaxprice = $price[1];
}else {
    $sminprice = round($minprice);
    $smaxprice = round($maxprice);
}
$minprice = round($minprice);
$maxprice = round($maxprice);

$script = <<< JS
        $(document).ready(function() {
        $("#filter-form").submit(function() {
            if($("#pname").val()=="") {
                    $("#pname").prop('disabled', true);
            }
            if($("#plink").val()=="") {
                    $("#plink").prop('disabled', true);
            }
        });
    $('input[type=radio]').click(function() {
        window.location=$(this).attr('data-href');
    });
    // parse the url:
    var link = window.location.search.match(/link=(\w+)/)[1];
   
    if (typeof link !== 'undefined') {
        // update the correct radio button:
        $('input[value="' + link + '"]').prop("checked", true);
    }  
});
    $(function(){
      

$.extend($.expr[':'], {
  'containsi': function(elem, i, match, array) {
    return (elem.textContent || elem.innerText || '').toLowerCase()
        .indexOf((match[3] || "").toLowerCase()) >= 0;
  }
});

//price slider
        
  var slider = document.getElementById("price-slider2");
  if (slider) {
    noUiSlider.create(slider, {
      start: [$sminprice, $smaxprice],
      connect: true,
      step: 50,  
      tooltips: [wNumb({decimals: false}), true],
      format: wNumb({
        decimals: false,
        //thousand: ',',
      }),
      range: {
        "min": $minprice,
        "max": $maxprice
      }
    });
  }        
$('#iconLeft5').keyup(function() {
    var query = $(this).val();
    $(".ecommerce-card")
        .hide()
        .filter(':containsi("' + query + '")')
        .show();
});
   $('#myDiv').click(function(){
        location.href='/rewards?price='+slider.noUiSlider.get();  
});     
        

    });
JS;
$this->registerJs($script);
?>  