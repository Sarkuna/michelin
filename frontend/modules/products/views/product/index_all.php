<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\components\ShopbyCategoriesWidget;
use app\components\ManufacturesAll;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = ['label' => 'All Products', 'url' => ['all']];
$this->params['breadcrumbs'][] = $this->title;
$cid = Yii::$app->request->get('id');
    $sid = Yii::$app->request->get('sid');
?>
<div class="shop-content">
    <div class="container">
        <div class="row">
            <aside class="col-md-3 col-sm-4" id="column-left">
                <div class="side-widget">
                    <h3><span>Shop by</span></h3>
                    <div class="clearfix space20"></div>
                    <h5>Points Slider</h5>
                    <div id="slider-container1" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 100%;"></span></div>
                    <p>
                        <a id="filter" href="#" class="btn-black pull-left">Filter Now</a>
                        <span class="pull-right sc-range">
                            <label class="range-label" for="amount">Points:</label>
                            <input type="text" id="amount1" style="border: 0; color: #333333; font-weight: bold;">
                        </span>
                    </p>
                    <div class="clearfix space30"></div>
                    
                    <?= ManufacturesAll::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
                    <div class="clearfix space20"></div>
            </aside>
            <div id="content" class="col-md-9 col-sm-8">
                <div class="filter-wrap">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="btn-group">
                                <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="" data-original-title="List"><i class="fa fa-th-list"></i></button>
                                <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="" data-original-title="Grid"><i class="fa fa-th"></i></button>
                            </div>
                            
                        </div>

                        <div class="col-md-9 col-sm-9 col-xs-6">
                            <span class="pull-right">
                                Show:
                                <select id="dynamic_select">
                                    <option value="15">Default</option>
                                    <option value="25">25 items</option>
                                    <option value="50">50 items</option>
                                    <option value="75">75 items</option>
                                    <option value="100">100 items</option>
                                </select>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="space50"></div>

                <?php
                echo yii\widgets\ListView::widget([
                    'dataProvider' => $products,
                    /* 'options' => [
                      'tag' => 'div',
                      'class' => 'list-wrapper',
                      'id' => 'list-wrapper',
                      ], */
                    //'layout' => "{pager}\n{items}\n{summary}",
                    'layout' => '<div class="row pagelisit">{items}</div>
                                <div class="pagenav-wrap">
                                    <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                    {summary}
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="pull-right">
                                        {pager}
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                ',
                    //'layout' => "<div class\"items\">{items}</div>{summary}",
                    'itemView' => '_products',
                        /* 'pager' => [
                          'firstPageLabel' => 'first',
                          'lastPageLabel' => 'last',
                          //'nextPageLabel' => 'next',
                          //'prevPageLabel' => 'previous',
                          'maxButtonCount' => 3,
                          ], */
                ]);
                ?>

            </div>
        </div>
        <div class="space50"></div>
    </div>
</div>
    
    <?php
    $filter = Yii::$app->request->get('filter');
    if($min1 > 0 && $max1 > 0){
    if(!empty($filter)){
        $filters = explode("-", $filter); 
        $mymin1 = $filters[0];
        $mymax1 = $filters[1];
     }else {
        $mymin1 = $min->points_value;
        $mymax1 = $max->points_value;
     }
         
    if(count($min) > 0 && count($max) > 0){
        $mymin = $min->points_value;
        $mymax = $max->points_value;
    }else {
        $mymin = '0';
        $mymax = '0';
        //$mymin1 = '0';
        //$mymax1 = '0';
    }
    
    if(count($min) > 0 && count($max) > 0){
        $mymin = $min->points_value;
        $mymax = $max->points_value;
    }else {
        $mymin = '0';
        $mymax = '0';
    }
    
    }else {
       $mymin = '0';
        $mymax = '0'; 
        $mymin1 = 0;
        $mymax1 = 0;
    }
    $clientScript = '
        var val = getUrlParameter("limit");
        $("#dynamic_select").val(val); 
        $("#dynamic_select").on("change", function () {
          var url = "?id='.$cid.'&sid='.$sid.'&limit="+$(this).val(); // get selected value            
          if (url) { // require a URL
              window.location = url; // redirect
          }          
          return false;
      });
      $("#slider-container1").slider({
        range: true,
        min: '.$mymin.',
        max: '.$mymax.',
        values: ['.$mymin1.', '.$mymax1.'],
        create: function () {
            $("#amount1").val("'.$mymin1.' - '.$mymax1.'");
        },
        slide: function (event, ui) {
            $("#amount1").val(ui.values[0] + " - " + ui.values[1]);
            var mi = ui.values[0];
            var mx = ui.values[1];
        }
    })
    $("#filter").click(function(e){
        var ab = $("#amount1").val();
        var trimStr = ab.replace(/\s/g,"");
        var url = "?id='.$cid.'&sid='.$sid.'&filter="+trimStr; // get selected value            
          if (url) { // require a URL
              window.location = url; // redirect
          }          
          return false;
    });
    ';
    $this->registerJs($clientScript, \yii\web\View::POS_END, 'booking-period');
    ?>
    
    <script language="javascript">
        function getUrlParameter(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        }
    </script>