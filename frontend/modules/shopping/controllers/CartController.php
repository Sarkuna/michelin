<?php

//namespace app\modules\support\controllers;
namespace app\modules\shopping\controllers;

use yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;



use common\libs\Cart;
use yii\web\Session;

use common\models\VIPOrder;
use common\models\VIPOrderSearch;
use common\models\VIPOrderProduct;
use common\models\VIPCustomerReward;
use common\models\VIPOrderOption;
use common\models\VIPProductOptionValue;
use common\models\VIPCustomerAddress;


/**
 * Default controller for the `support` module
 */
class CartController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = '@app/themes/vuexy/layouts/main_checkout';
        $session = Yii::$app->session;
        $cart = Yii::$app->session['cart'];
        
        
        //$data = '[{"ADUserID":"JM0186879-A","IsSuccess":true,"Message":["Successfully Inserted"]}]';
        //$data = '[{"ADUserID":"testJM0186879-A","IsSuccess":false,"Message":["Invalid AD User ID."]}]';
        
        //print_r($data);
        //die;
        return $this->render('index',['lisitcart' => $cart]);
    }

    public function actionCheckout()
    {
        Yii::$app->VIPglobal->addtolog('view', Yii::$app->user->id);
        $clientsetup = Yii::$app->VIPglobal->clientSetup();
        $pointpersent = $clientsetup['point_value'];
        $session = Yii::$app->session;
        $selectoptionvalue = '';
        $cart = Yii::$app->session['cart'];
        if(empty($cart)) {
            return $this->redirect(['index']);
        }

        $model = new VIPOrder();
        $agreeform = new \common\models\AgreeForm();
        
        //$balancepoints = $this->TotalAvailablePoint();
        $balancepoints = Yii::$app->VIPglobal->myAvailablePoint();


            
        $model->shipping_firstname = Yii::$app->VIPglobal->webapiuserinfo()['ContactPerson'];
        $model->shipping_lastname = '';
        $model->shipping_company = Yii::$app->VIPglobal->webapiuserinfo()['company_name'];
        $model->shipping_address_1 = Yii::$app->VIPglobal->webapiuserinfo()['address1'];
        $model->shipping_address_2 = Yii::$app->VIPglobal->webapiuserinfo()['address2'];;
        $model->shipping_city = Yii::$app->VIPglobal->webapiuserinfo()['city'];
        $model->shipping_postcode = Yii::$app->VIPglobal->webapiuserinfo()['postcode'];
        $model->shipping_country_id = Yii::$app->VIPglobal->webapiuserinfo()['country'];
        $model->shipping_zone = Yii::$app->VIPglobal->webapiuserinfo()['state'];
        //$model->shipping_zone_id = $myaddress->states->zone->name;
        
        $total = 0;
        $model->order_status_id = 10;
        if ($agreeform->load(Yii::$app->request->post()) && $agreeform->validate()) {
            
            if(empty($cart)) {
                return $this->redirect(['index']);
            }
            $model->load(Yii::$app->request->post());
            
            $prefix = \common\models\ClientOption::find()->one();
            $globalprefix = VIPOrder::find()->select('invoice_no')->orderBy(['invoice_no' => SORT_DESC])->one();
            $data=VIPOrder::find()->select('invoice_no')->orderBy(['invoice_no' => SORT_DESC])->one();
            if(!empty($data)){
                $invoice_no = str_pad(++$data->invoice_no,5,'0',STR_PAD_LEFT);
            }else {
                $invoice_no = '00001';
            }

            $userAgent = \xj\ua\UserAgent::model();
            $platform = $userAgent->platform;
            $browser = $userAgent->browser;
            $version = $userAgent->version;
            

            
            $model->invoice_no = $invoice_no;
            //$model->invoice_prefix = $prefix->invoice_prefix.'-'.$invoice_no;
            $model->global_order_ID = Yii::$app->VIPglobal->clientPrefix().$invoice_no.date('dmy');
            $model->customer_id = Yii::$app->user->id;
            $model->email = Yii::$app->VIPglobal->webapiuserinfo()['email'];
            $model->mobile_no = Yii::$app->VIPglobal->webapiuserinfo()['mobile_no'];
            $model->telephone_no = Yii::$app->VIPglobal->webapiuserinfo()['telephone_no'];
            $model->order_status_id = 10;
            $model->comment = '';
            
            $model->ip = $_SERVER['REMOTE_ADDR'];
            $model->user_agent = $platform.'-'.$browser.'-'.$version;
            $model->customer_group_id = $session['name'];
 
            $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if ($flag) {
                            $shipping_Fee = 0;
                            foreach ($cart as $key => $carlist) {

                                $shipping_point = 0;
                                $shipping_price = 0;
                                $shipping_point_total = 0;
                                $shipping_price_total = 0;

                                
                                
                                $total_points = $carlist['points_value']* $carlist['product_qty'];

                                $addproduct = new VIPOrderProduct();
                                $addproduct->order_id = $model->order_id;
                                $addproduct->product_id = $key;
                                $addproduct->name = $carlist['product_name'];
                                $addproduct->model = $carlist['product_code'];
                                $addproduct->quantity = $carlist['product_qty'];
                                $addproduct->point = $carlist['points_value'];
                                $addproduct->point_admin = 0;
                                $addproduct->point_total = $total_points;
                                $addproduct->price = $carlist['price'];
                                $addproduct->total = $carlist['price']* $carlist['product_qty'];
                                $addproduct->shipping_price = $shipping_price;
                                $addproduct->shipping_price_total = $shipping_price_total;
                                $addproduct->shipping_point = $shipping_point;
                                $addproduct->shipping_point_total = $shipping_Fee;
                                $addproduct->product_status = 10;
                                
                                if (($flag = $addproduct->save(false)) === false) {
                                    $transaction->rollBack();
                                    break;
                                }
                                
                                $updateproduct = \common\models\VIPProduct::find()->where(['product_id' => $key])->one();
                                $updateproduct->quantity = $updateproduct->quantity - $carlist['product_qty'];
                                $updateproduct->save(false);
                                
                                //$product_total = $carlist['points_value']* $carlist['product_qty'];
                                
                                $total += $total_points + $shipping_point_total;
                            }

                        
                            $description = $model->invoice_prefix;
                            $flash_deals_id = 0;
                            $amountrm = 0.00;
                            $code = 'order.status_'.$model->order_status_id;
                            $rpage = '/order-history';
                            $rflash_deals_id = 0;

                            $history = new \common\models\VIPOrderHistory();
                            $history->order_id = $model->order_id;
                            $history->order_status_id = 10;
                            $history->comment = 'New Order - '.$description;
                            $history->date_added = date('Y-m-d');
                            $history->type = 1;
                            if (($flag = $history->save(false)) === false) {        
                                $transaction->rollBack();
                            }
                            
                            $point_value = Yii::$app->VIPglobal->clientSetup()['point_value'];
                            $statementofredemption = new \common\models\StatementOfRedemption();
                            $statementofredemption->redemption_date = date('Y-m-d');
                            $statementofredemption->particulars = $description;
                            $statementofredemption->credit_out = $total * $point_value;
                            $statementofredemption->status = 'accepted';
                            $statementofredemption->order_id = $model->order_id;
                            $statementofredemption->save(false);
                                
                        }
                    }

                    if ($flag) {
                        $transaction->commit();
                        
                        //return $this->redirect(['view', 'id' => $modelCatalogOption->id]);
                        unset(Yii::$app->session['cart']);

                        $lastorder = VIPOrder::find()->where(['order_id' => $model->order_id])->one();
                        $total = 0;
                        foreach ($lastorder->orderProducts as $subproduct) {
                            $productpoint = $subproduct->point + $subproduct->shipping_point;
                            $totalproductpoint = $subproduct->quantity * $productpoint;
                            $total += $totalproductpoint;
                        }
                        $data = \yii\helpers\Json::encode(array(
                            [
                                'ADUserID' => $lastorder->customer_group_id,
                                'TransactionDate' => date('d/m/Y', strtotime($lastorder->created_datetime)),
                                'TransactionTime' => date('hms', strtotime($lastorder->created_datetime)),
                                'Description' => 'Redemption - '.$lastorder->invoice_prefix,
                                'RewardQuantity' => 1,
                                'PointRedeemedPerReward' => $total,
                                'TotalRedeemedPoint' => $total,
                            ]
                        ));
                        $result = Yii::$app->VIPglobal->webapipost($data);
                        
                        if ($result != null) {
                            $data2 = \yii\helpers\Json::decode($result);
                        }
                        if(!empty($data2[0]['IsSuccess'])) {
                            $lastpoint = \common\models\Userapp::find()->where(['id' => Yii::$app->user->id])->one();
                            $lastpoint->available_point = $data2[0]['AvailablePoint'];
                            $lastpoint->save(false);
                            
                            $session['point'] = $lastpoint->available_point;
                            $session['pointexpiring'] = $lastpoint->available_point;
                            $mes = 'success';
                            \Yii::$app->getSession()->setFlash('success',['title' => 'Thank you', 'text' => 'Thank you for order.']);
                        }else {
                            \Yii::$app->getSession()->setFlash('error',['title' => 'Sorry', 'text' => 'sorry we cannot process your order']);
                            $mes = 'fail';
                            $lastorder->order_status_id = 60;
                        }
                        $lastorder->api_response = $mes;
                        $lastorder->api_response_datetime = date('Y-m-d H:i:s');
                        $lastorder->save(false);
                        Yii::$app->VIPglobal->addtolog('success', Yii::$app->user->id);
                        //$data = '';
                        
                        $returnedValue = Yii::$app->VIPglobal->sendEmailNewOrder($model->order_id, Yii::$app->params[$code], $data);
                        //return $this->redirect([$rpage.'/view', 'id' => $model->order_id]);
                        return $this->redirect($rpage.'/view/'.$model->order_id);
                    }else {
                        \Yii::$app->getSession()->setFlash('error',['title' => 'Sorry', 'text' => 'Thank you for order.']);
                        return $this->redirect(['index']);
                    }

                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            //$model->accept_language
            //$model->save()
        }else {
            $page = 'checkout';
            return $this->render($page,['lisitcart' => $cart,'model' => $model, 'agreeform' => $agreeform, 'balancepoints' => $balancepoints]);
        }
    }
    
    
    function actionAddCart($id,$qty,$selectoptionvalue=null){
        $cart = new Cart();
        $cart->addCart($id,$qty,$selectoptionvalue);
    }
    
    function actionUpdateCartQty($id,$qty){
        $cart = new Cart();
        $cart->UpdateCartQty($id,$qty);
        //$this->renderPartial('index',['lisitcart' => $cart]);
    }
    
    function actionRemoveCart($id){
        $cart = new Cart();
        $cart->RemoveCart($id);
        //return $this->renderPartial('index',['lisitcart' => $cart]);
    }
    
    function TotalAvailablePoint(){
        $session = Yii::$app->session;
        $clientID = $session['currentclientID'];
        
        $expired = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'E'])
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        $expired = str_replace('-', '', $expired);
        if(empty($expired)){
            $expired = 0; 
         }

        $totaladd = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'A'])  
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        
        if(empty($totaladd)){
            $totaladd = 0; 
         }
         
         $totalminus = \common\models\VIPCustomerReward::find()
        ->where(['clientID'=>$clientID, 'customer_id'=>Yii::$app->user->id, 'bb_type' => 'V'])  
        //->andWhere(['not like', 'description', 'Order ID'])
        ->sum('points');
        $totalminus = str_replace('-', '', $totalminus);
        if(empty($totalminus)){
            $totalminus = 0; 
         }
         
        $balance = $totaladd - $totalminus - $expired;
        
        return $balance;
    }
    
    /*function actionGetaddress($id){
        $session = Yii::$app->session;
        $oldaddressdisplay = \common\models\VIPCustomerAddress::find()
        ->where(['vip_customer_address_id' => $id,'userID' => Yii::$app->user->id, 'clientID' => $session['currentclientID']])
        ->orderBy([
            'vip_customer_address_id' => SORT_ASC,
        ])->one();
        
        if (count($oldaddressdisplay) > 0) {
            $response['shipping_firstname'] = $oldaddressdisplay->firstname;
            $response['shipping_lastname'] = $oldaddressdisplay->lastname;
            $response['shipping_company'] = $oldaddressdisplay->company;
            $response['shipping_address_1'] = $oldaddressdisplay->address_1;
            $response['shipping_address_2'] = $oldaddressdisplay->address_2;
            $response['shipping_city'] = $oldaddressdisplay->city;
            $response['shipping_postcode'] = $oldaddressdisplay->postcode;
            $response['shipping_country_id'] = $oldaddressdisplay->country_id;
            $response['shipping_country_id_text'] = $oldaddressdisplay->country->name;
            $response['shipping_zone'] = $oldaddressdisplay->state;
            $response['shipping_zone_text'] = $oldaddressdisplay->states->name;
            $response['shipping_zone_id'] = $oldaddressdisplay->zone_id;
            $response['shipping_zone_id_text'] = $oldaddressdisplay->delivery->name;
        }else{
           $response['shipping_firstname'] = "";
            $response['shipping_lastname'] = "";
            $response['shipping_company'] = "";
            $response['shipping_address_1'] = "";
            $response['shipping_address_2'] = "";
            $response['shipping_city'] = "";
            $response['shipping_postcode'] = "";
            $response['shipping_country_id'] = "";
            $response['shipping_country_id_text'] = "";
            $response['shipping_zone'] = "";
            $response['shipping_zone_text'] = "";
            $response['shipping_zone_id'] = "";
            $response['shipping_zone_id_text'] = ""; 
        }
        echo json_encode($response);
    }*/
    
    function actionRegion($id){
        $states = \common\models\VIPStates::find()->where(['country_id' => $id])->all();
        if ($states) {
            //echo $sle;
            $id = 1;
            //echo '<select>';
            foreach ($states as $state) {
                //echo '<li class="selectboxit-option">'.$state->state_name.'</li>';
                echo "<option value='" . $state->states_id . "'>" . $state->state_name . "</option>";
                /*echo '<li data-id="'.$id.'" data-val="'.$state->states_id.'" data-disabled="false" class="selectboxit-option" role="option">
                        <a class="selectboxit-option-anchor">
                                <span class="selectboxit-option-icon-container">
                                        <i class="selectboxit-option-icon  selectboxit-container"></i>
                                </span>'.$state->state_name.'
                        </a>
                </li>';*/
                $id++;
            }
            //echo '</select>';
        } else {
            echo "<option>-</option>";
        }
    }
    
    function actionAddressDefault($id){
        VIPCustomerAddress::updateAll(['default_ID' => 0], ['userID' => Yii::$app->user->id]);
        $address = \common\models\VIPCustomerAddress::find()->where(['userID' => Yii::$app->user->id, 'vip_customer_address_id' => $id])->one();
        $address->default_ID = 1;
        $address->save(false);
        $cart = Yii::$app->session['cart'];
        if(empty($cart)) {
            $response = 1;
        }else {
            $response = 0;
        }
        
        echo json_encode($response);

    }

}
