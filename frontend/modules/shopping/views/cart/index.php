<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Checkout';
$this->params['breadcrumbs'][] = ['label' => 'Rewards', 'url' => ['/products/product/index']];
$this->params['breadcrumbs'][] = $this->title;
//$cid = Yii::$app->request->get('id');
//$sid = Yii::$app->request->get('sid');
$session = Yii::$app->session;

$clientsetup = Yii::$app->VIPglobal->clientSetup();
$pointpersent = $clientsetup['point_value'];
?>
<style>
    .top{margin-top: 10px;}
    .img-box{width: 100px;float: left;vertical-align: top;margin: 0px;padding: 0px 10px 0px 0px;}
    .item-img img{max-width: 100%;}
    .box1{float:left;}
    .clear{clear:both;}
    .box2{color:#cccccc;}
</style>

<?php
    $carlists = $lisitcart;    
    $cartlistscount = count($carlists);
    

    if($cartlistscount > 0) {
        $total = 0;
        $subtotal = 0;
        $qtys = 0;
        $shipping_Fee = 0;
        //$stateid = $addressdefault->states->zone_id;

?>

<section id="place-order" class="list-view product-checkout">
    <div class="checkout-items">
            <?php
            foreach ($carlists as $key => $carlist) {
                $shipping_point = 0;
                $shipping_point_total = 0;

                $totalpts = $carlist['product_qty'] * $carlist['points_value'];
                
                if(!empty($carlist['main_image'])) {
                    if (file_exists(Yii::getAlias('@backend').'/web/upload/product_cover/thumbnail/'.$carlist['main_image'])) {
                        $thumbimage = $carlist['main_image'];
                    }else {
                        $thumbimage = 'no_image.png';
                    }
                }else {
                    $thumbimage = 'no_image.png';
                }
                
                $products = common\models\VIPProduct::find()->where(['product_id' => $key])->one();
                $totproducts = common\models\VIPOrderProduct::find()->where(['product_id' => $key])->count();
                
                if($products->type == 'michelin') {
                    $stock = Yii::$app->VIPglobal->limited($products->product_id);
                }else {
                    if($totproducts > 0) {
                        $stock = $products->quantity - $totproducts;
                    }else {
                        $stock = $products->quantity;
                    }                
                }
                
                echo '<div class="card ecommerce-card">
                    <div class="card-content">
                        <div class="item-img text-center">
                            <input type="hidden" id="proid" value="'.$key.'">
                            <img src="' . Yii::$app->params['imgdomain'] . '/upload/product_cover/thumbnail/' . $thumbimage . '" alt="img-placeholder">
                        </div>
                        <div class="card-body">
                            <div class="item-name">
                                <span><a href="/products/product/product-detail?id=' . $key . '">' . $carlist['product_name'] . '</a></span>
                                
                                <p class="stock-status-in">Product Code : ' . $carlist['product_code'] . '</p>
                            </div>
                            <div class="item-quantity123">
                                <p class="quantity-title">Quantity: ' . $carlist['product_qty'] . ' X ' . $carlist['points_value'] . ' pts</p>
                            </div>
                            <div class="item-quantity">
                                <p class="quantity-title">Quantity</p>
                                <div class="box1">
                                    <div class="input-group quantity-counter-wrapper">                                    
                                        <input id="quantity_'.$key.'" name="ex_limit" type="text" class="ex_limit'.$key.' form-control" value="'.$carlist['product_qty'].'"/>
                                    </div>
                                </div>
                                <div class="box2">'.$stock.' piece available</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item-options text-center">
                            <div class="item-wrapper">

                                <div class="item-cost">
                                    <h6 class="item-price">
                                        ' . $totalpts . ' pts
                                    </h6>

                                </div>
                            </div>
                            <input type="hidden" id="maxquantity_'.$key.'" value="'.$stock.'">
                            <div class="wishlist" onclick="itemQty('.$key.');">
                                <i class="feather icon-refresh-cw align-middle"></i> Update
                            </div>
                            <div class="cart remove-wishlist deletebagitem" onclick="deleteItem(' . $key . ');">
                                <i class="feather icon-trash align-middle"></i> Remove
                            </div>
                            
                        </div>
                    </div>
                </div>';

                
                $subtotal += $carlist['points_value'];
                $total += $carlist['points_value'] * $carlist['product_qty'];
                $qtys += $carlist['product_qty'];

                $this->registerJs(
                '
                    $( ".ex_limit'.$key.'" ).TouchSpin({
                       min: 1,
                       max: '.$stock.',
                       mousewheel: true,
                       stepinterval: 50,
                       maxboostedstep: 10000000,
                   });'
                );  
            }
            ?>
        
        
        
    </div>
    
    
    
    <div class="checkout-options">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Shipping</h4>
            </div>
            
            <div class="card-content">
                <div class="card-body">
                    <p><?= Yii::$app->VIPglobal->webapiuserinfo()['company_name'] ?></p>
                    <p class="mb-0"><?= Yii::$app->VIPglobal->webapiuserinfo()['FullAddress'] ?> </p>
                </div>
            </div>
            
            
            <div class="card-header">
                <h4 class="card-title">Order Summary</h4>
            </div>
            
            <div class="card-content">
                <div class="card-body">
                    <div class="detail">
                        <div class="detail-title">
                            Your Current Balance
                        </div>
                        <div class="detail-amt newbalance-amt">
                            <?php echo number_format(Yii::$app->VIPglobal->myAvailablePoint()); ?> pts
                        </div>
                    </div>
                    
                    <div class="detail">
                        <div class="detail-title">
                            Sub Total
                        </div>
                        <div class="detail-amt">
                            <?= Yii::$app->formatter->asInteger($total) ?> pts
                        </div>
                    </div>

                    <hr>
                    <div class="detail">
                        <div class="detail-title detail-total">Total</div>
                        <div class="detail-amt total-amt"><?= Yii::$app->formatter->asInteger($total + $shipping_Fee) ?> pts</div>
                    </div>
                    
                    <div class="detail">
                        <div class="detail-title detail-total">
                            New Balance
                        </div>
                        <div class="detail-amt discount-amt">
                            <?php
                            $newbalance = Yii::$app->VIPglobal->myAvailablePoint() - ($total + $shipping_Fee);
                            if($newbalance > 0) {
                                echo number_format($newbalance);
                            }else {
                                echo 0;
                            }                             
                            ?> pts
                        </div>
                    </div>
                    <?php 

                            $chktotal = $total + $shipping_Fee;                        
                            if($chktotal <= Yii::$app->VIPglobal->myAvailablePoint()) {
                                echo Html::a('PLACE ORDER', Url::to('/shopping/cart/checkout'), ['title' => Yii::t('app', 'Place Order'), 'class' => 'btn btn-primary btn-block place-order']);
                            }else {
                                echo '<div class="alert alert-info top" style="color: #ffffff;">
                                    <strong>Sorry!</strong> Your point balance is insufficient to make this order. Plese remove if u add more products.
                                  </div>';
                            }
                    ?>
                </div>
            </div>
        </div>
    </div>
     <?php }else { ?>    
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">You don't have any items in your cart.</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <?= Html::a('Browse Rewards', '/products/product/index', ['title' => Yii::t('app', 'Browse Shop'), 'class' => 'btn btn-primary place-order']); ?>
                </div>
            </div>
        </div>
    <?php } ?>
</section>


<?php

  ?>
