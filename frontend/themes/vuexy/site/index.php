<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use app\components\DashboardVuexy;


$this->title = 'Dashboard';
$imgurl = $this->theme->basePath;

//die();
?>


<?= DashboardVuexy::widget(['path' => Yii::$app->request->getPathInfo()]) ?>

<?php
$script = <<< JS
    //$(window).on('load',function(){
        //if ($.cookie('pop') == null) {
            $('#default').modal('show');
            //$.cookie('pop', '1');
        //}
    //});
JS;
$this->registerJs($script);
?>
