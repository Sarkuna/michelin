<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Logged out';
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;

?>
<style>
    .form-label-group.has-icon-left > label {left: 0px;}
    .help-block-error{
        color: #EA5455;
    }
</style>
<div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
    <p class="text-center"><img src="<?= Yii::$app->VIPglobal->getSiteInfo()['logo'] ?>" class="mt-100" width="200"></p>
    <img src="<?php echo $this->theme->baseUrl ?>/app-assets/images/pages/login.png" alt="branding logo">
</div>

<div class="col-lg-6 col-12 p-0">
    <div class="card rounded-0 mb-0 px-2">
        <p class="text-center"><img src="<?= Yii::$app->VIPglobal->getSiteInfo()['logo'] ?>" class="mt-100" width="200"></p>
        <div class="card-header pb-1">
            <div class="card-title">
                <h4 class="mb-0">Thank you</h4>
            </div>
        </div>

        <div class="card-content">
            <div class="card-body pt-1">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info mt-1 alert-validation-msg" role="alert">
                            <p><i class="feather icon-info mr-1 align-middle"></i>
                                You have been successfully logged out of Reward Solution. Please close this window to ensure you are completely logged out of the session.</p>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
   
    </div>
</div>

