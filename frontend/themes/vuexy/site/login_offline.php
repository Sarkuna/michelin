<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
?>

<div class="col-md-4 content-left">
    <div class="card card-login card-plain" style="margin-top: 20%; left: 5%;">

        <div class="header header-primary text-center">
            <div class="logo-container">
                <?php
                //echo '<pre>'.print_r($session).die();
                if (!empty($session['currentLogo'])) {
                    echo '<img src="' . Yii::getAlias('@back') . '/upload/client_logos/' . $session['currentLogo'] . '">';
                } else {
                    echo '<img src="' . Yii::getAlias('@back') . '/images/logo.png" class="img-responsive avatar" width="200">';
                }
                ?>
            </div>
        </div>

        <div class="content"> 
            <h3 style="color: #000000;">We&rsquo;ll be back soon!</h3>
            <p>Sorry for the inconvenience but we&rsquo;re performing some maintenance at the moment. We&rsquo;ll be back online shortly!</p>
        </div>


        


        <div class="clearfix"></div>




    </div>
</div>
